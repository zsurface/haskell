#!/usr/bin/env runhaskell

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import Text.RawString.QQ
-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

import AronModule 

pdf = "/Library/WebServer/Documents/xfido/pdf"
img = "/Library/WebServer/Documents/xfido/image"

helpme::IO()
helpme = pp ("copyfile file.pdf pdf :" ++ pdf) >> 
         pp ("copyfile file.png img " ++ img) 
main = do 
        pwd
        helpme
        argList <- getArgs
        pp $ length argList
        curr <- getPwd 
        if (length argList) == 2 
        then case head $ drop 1 argList of 
            "pdf" -> do 
                     system cmd >>= \x -> pp x 
                     system ("ls " ++ fn) >>= \x -> pp x 
                     system ([r| echo |] <> fn <> [r| | pbcopy |]) >>= \x -> pp x
                     pp $ "copy " ++ fn ++ " to clipboard"
                     return ()
                        where 
                            fn = head argList; 
                            cmd = "cp " ++ (curr </> fn) ++ " " ++ pdf 
            "img" -> do 
                     system cmd >>= \x -> pp x
                     system ("ls " ++ fn) >>= \x -> pp x 
                     system ([r| echo |] <> fn <> [r| | pbcopy |]) >>= \x -> pp x
                     pp $ "copy " ++ fn ++ " to clipboard"
                     return ()
                        where 
                            fn = head argList
                            cmd = "cp " ++ (curr </> fn) ++ " " ++ img 
            _     -> pp "Run pdf or img  file" 
        else print "Need Two arguments" 
        pp "done!"

import Data.Char

data Operator = Times | Plus | Minus | Div deriving(Show, Eq)
       
data Token = TokOp    Operator
           | TokIdent String
           | TokNum   Int
           | TokSpace
           | Digit
           | Alpha 
           | Ope
           | SpaceT deriving (Show, Eq)


isOpe::Char -> Bool
isOpe c = elem c "+-*/"

operator::Char -> Operator
operator c   | c == '+' = Plus
             | c == '-' = Minus
             | c == '*' = Times
             | c == '/' = Div
             | otherwise = error "Invalid character"


tokenizer::String -> [Token]
tokenizer [] = []
tokenizer (c:cs) | isDigit  c = Digit                : tokenizer cs
                 | isLetter c = Alpha                : tokenizer cs
                 | isSpace  c = SpaceT               : tokenizer cs
                 | isOpe    c = (TokOp $ operator c) : tokenizer cs
                 
charToInt::Char -> Int 
charToInt c = ord c - ord '0'
  
tokenizerChar::Char -> Token
tokenizerChar c | isDigit  c = TokNum $ charToInt c
                | isLetter c = TokIdent [c]
                | isSpace  c = TokSpace
                | isOpe    c = TokOp $ operator c
                | otherwise = error $ "Invalid Character" ++ [c]

{-|
     ab+c => ("ab", "+c")

     a  b+c
     ab  "+c"
     return ("ab" "+c")
-}
alnums::String ->(String, String)
alnums []     = ([], [])
alnums (c:cs) = als "" (c:cs)
  where
    als acc []     = (acc, [])
    als acc (x:xs) | isAlphaNum x = let (acc', xs') = als acc xs
                                    in (x:acc', xs')
                   | otherwise = (acc, (x:xs))

digits::String -> (String, String)
digits [] = ([], [])
digits (c:cs) = dig "" (c:cs)
  where
    dig acc [] = (acc, [])
    dig acc (x:xs) | isDigit x = let (acc', xs') = dig acc xs
                                 in  (x:acc', xs')
                   | otherwise = (acc, (x:xs))

{-|
   === KEY:

   See Prelude 'span'

   @
     span::(a -> Bool) -> [a] -> ([a], [a])
   @
-}
spanA::(a -> Bool) -> [a] -> ([a], [a])
spanA _ [] = ([], [])
spanA f (c:cs) = spanAcc [] (c:cs)
  where
    spanAcc acc []                 = (acc, [])
    spanAcc acc (x:xs) | f x       = let (acc', xs') = spanAcc acc xs
                                     in  (x:acc', xs')
                       | otherwise = (acc, (x:xs))


number c cs =
  let (dig, cs') = digits cs
  in TokNum (read (c:dig)) : tokenizer2 cs'

identifier c cs =
  let (aln, cs') = alnums cs
  in TokIdent (c:aln) : tokenizer2 cs'

number2 c cs =
  let (dig, cs') = spanA isDigit cs
  in TokNum (read (c:dig)) : tokenizer2 cs'
     
identifier2 c cs =
  let (ss, cs') = spanA isAlphaNum cs
  in TokIdent (c:ss) : tokenizer2 cs'

{-|
   === KEY: mutual recursion

   tokenizer2 ---- number
             |        |
             |        ⊥ - - → tokenizer2 
             |
             ⊥---- identifier
                     |
                     ⊥ - -  → tokenizer2
-}
tokenizer2::String -> [Token]
tokenizer2 [] = []
tokenizer2 (c:cs) | isDigit c = number2 c cs      -- 123
                  | isAlpha c = identifier2 c cs  -- x1
                  | isSpace c = tokenizer2 cs     -- ' '
                  | elem c "+-*/" = (TokOp $ operator c) : tokenizer2 cs  -- TokOp Plus
                  | otherwise = error $ "Error =>" ++ [c]
  
main = do
       print "hi"
       let s = "abc123+1* + "
       print $ tokenizer s
       print $ operator '+'
       let ls = map tokenizerChar s
       print "ok"
       print ls
       print $ alnums "abAD9 + c"
       print $ digits "123ab"
       print $ tokenizer2 "111aaa44"
       print $ tokenizer2 "12 + 14 / x1"
       -- print $ spanA isDigit "123abc99cc"
       



       

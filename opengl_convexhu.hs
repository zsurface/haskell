{-# LANGUAGE DataKinds #-}


module Main where

import Control.Concurrent (threadDelay)
import Control.Monad (unless, when)
import Graphics.Rendering.OpenGL as GL 
import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
import qualified Graphics.UI.GLFW as G
import System.Exit
import System.IO
import Control.Monad
import System.Random
import Data.Set(Set) 
import Data.IORef 
import qualified Data.Set as S 
import Data.List 
import Linear.V3
import Linear.V3(cross) 
import Linear.Vector
import Linear.Matrix
import Linear.Projection as P
import Linear.Metric(norm, signorm)

import AronModule
import AronGraphic

-- Mon Jan  6 21:27:09 2020 
-- compile: <F9> in Vim


--t1 (a, b, c) = a
--t2 (a, b, c) = b 
--t3 (a, b, c) = c 


addx::(Num n)=> n -> (n, n, n) -> (n, n, n)
addx m (a, b, c) = (m + a, b, c)

addy::(Num n)=> n -> (n, n, n) -> (n, n, n)
addy m (a, b, c) = (a, m + b, c)

mx::(Num n)=> n -> (n, n, n) -> (n, n, n)
mx m (a, b, c) = (m, b, c)

my::(Num n)=> n -> (n, n, n) -> (n, n, n)
my m (a, b, c) = (a, m, c)

-- | mod for Double, 6.28 `fmod` 2 => 0.28
fmod::Double->Integer->Double
fmod  a n = a - du
            where     
                num = realToFrac (div (round a) n)
                du = realToFrac ((round num)*n)

vertexList = [
            (0.3, 0.4, 0),
            (0.6, 0.9, 0),
            (0.5, 0.2, 0),
            (0.8, 0.3, 0),
            (0.4, 0.6, 0),
            (0.8, 0.1, 0),
            (0.6, 0.3, 0),
            (0.9, 0.5, 0),
            (0.9, 0.4, 0)
            ]

vertexList9 = [
            Vertex3 0.91 0.89 0,
            Vertex3 0.55 0.18 0,
            Vertex3 0.74 0.13 0,
            Vertex3 0.23 0.46 0,
            Vertex3 0.92 0.72 0,

            Vertex3 0.71 0.12 0,
            Vertex3 0.27 0.99 0,
            Vertex3 0.23 0.32 0,
            Vertex3 0.56 0.58 0,
            Vertex3 0.14 0.94 0,

            --Vertex3 0.719 0.33 0,
            Vertex3 0.721 0.33 0,  -- some problem
            Vertex3 0.41 0.51 0,
            Vertex3 0.52 0.64 0,
            Vertex3 0.33 0.62 0,
            Vertex3 0.35 0.25 0,

            Vertex3 0.11 0.39 0,
            Vertex3 0.52 0.71 0,
            Vertex3 0.89 0.33 0,
            Vertex3 0.21 0.31 0,
            Vertex3 0.69 0.13 0,

            Vertex3 0.62 0.51 0,
            Vertex3 0.19 0.23 0,
            Vertex3 0.46 0.16 0,
            Vertex3 0.24 0.36 0,
            Vertex3 0.14 0.23 0,

            Vertex3 0.42 0.51 0,
            Vertex3 0.23 0.33 0,
            Vertex3 0.37 0.69 0,
            Vertex3 0.24 0.85 0,
            Vertex3 0.50 0.82 0
            ]
--
vertexList2 = [
--    Vertex3 0.38 0.41 0.44,
--    Vertex3 0.72 6.0e-2 0.23,
--    Vertex3 0.73 0.21 0.38,
--    Vertex3 0.74 0.34 7.0e-2
        Vertex3 0.16 0.43 0,
        Vertex3 0.18 0.16 0,
        Vertex3 0.28 0.85 0,
        Vertex3 0.95 0.82 0
    ]
--vertexList2 = [
--            Vertex3 0.9 0.8 0,
--            Vertex3 0.23 0.4 0,
--            Vertex3 0.2 0.31 0,
--            Vertex3 0.1 0.95 0
----            Vertex3 0.89 0.33 0,
----            Vertex3 0.44 0.66 0
--            ]
--

mergeChunk::Int->[(GLfloat, GLfloat, GLfloat)]->[(GLfloat, GLfloat, GLfloat)]
mergeChunk n c = mergeList  (take n c)  (take n $ drop n c) 

nStep::Float
nStep = 40

sphere::[(GLfloat, GLfloat, GLfloat)]
sphere = [(cos(del*k)*cos(del*i), 
          _sin(del*k), 
          cos(rf del*k)*sin(rf del*i)) | k <- [1..n], i <-[1..n]]
            where 
                del = rf(2*pi/(n-1))
                n = nStep 

shear f = do
   m <-  (newMatrix RowMajor [1,f,0,0
                             ,0,1,0,0
                             ,0,0,1,0
                             ,0,0,0,1])
   multMatrix (m:: GLmatrix GLfloat)

poly::[Double]->[Double]->[Double]
poly [] _ = [] 
poly _ [] = [] 
poly xs sx = map(\s -> sum s) $ map(\x -> map(\(c, p) -> c*(x^p)) po) sx
            where
                po = zip xs [0..]


cosVec::Vertex3 GLfloat -> Vertex3 GLfloat -> Float
cosVec p0@(Vertex3 x0 y0 z0) p1@(Vertex3 x1 y1 z1) = d/n 
                    where
                        vx = Vertex3 (-1) 0 0  -- norm lv = 1
                        v01= Vertex3 (x1 - x0) (y1 - y0) (z1 - z0)
                        -- TODO: should use Vector3?
                        dot (Vertex3 a0 b0 c0) (Vertex3 a1 b1 c1) = a0*a1 + b0*b1 + c0*c1 
                        d = dot vx v01 
                        norm v = sqrt $ dot v v 
                        -- n = if norm v01 > 0 then norm v01 else 1
                        n = norm v01


det::Vertex3 GLfloat -> Vertex3 GLfloat -> Vertex3 GLfloat -> Float
det (Vertex3 x0 y0 z0) (Vertex3 x1 y1 z1) (Vertex3 x2 y2 z2) = det' v10 v12
                    where
                        -- vector 1 -> 0 
                        v10 = Vertex3 (x0 - x1) (y0 - y1) (z0 - z1)
                        -- vector 1 -> 2 
                        v12 = Vertex3 (x2 - x1) (y2 - y1) (z2 - z1)
                        -- dot v10 v12
                        --  1 0 
                        --  0 1 
                        -- | => det > 0, left turn from +X-axis to +Y-axis (CCW)
                        det' (Vertex3 x0 y0 _) (Vertex3 x1 y1 _) = x0*y1 - y1*y0 


renderTriangle::IO()
renderTriangle = do
    renderPrimitive Triangles $ do
        color  (Color3 1 0 0 :: Color3 GLdouble)
        vertex (Vertex3 (negate 0.6) (negate 0.4) 0 :: Vertex3 GLdouble)
        color  (Color3 0 1 0 :: Color3 GLdouble)
        vertex (Vertex3 0.6 (negate 0.4) 0 :: Vertex3 GLdouble)
        color  (Color3 0 0 1 :: Color3 GLdouble)
        vertex (Vertex3 0 0.6 0 :: Vertex3 GLdouble)

torus2::[(GLfloat, GLfloat, GLfloat)]
torus2= [((br + r*_cos(del*i))*_cos(del*k) + _cos(del*i), 
        _sin(del*i)*_cos(del*k), 
        (br + r*_cos(del*i))*_sin(del*k) ) | i <- [1..n], k <-[1..n]]
        where 
            del = 2*pi/(n-1)
            n = nStep 
            r = 0.1
            br = 0.2


curve1::[(GLfloat, GLfloat, GLfloat)]
curve1 = [let x = d*k  in 
            (x, f x, 0) | k <- [a..b]] 
            where
                a = -300
                b = 300
                d = 0.01
                f x = x*x*x

pointList::[(GLfloat, GLfloat, GLfloat)]
pointList = [let x = d*k  in 
            (x, f x, 0) | k <- [a..b]] 
            where
                a = -300
                b = 300
                d = 0.01
                f x = x*x*x

co    = Color3 0.5 0.5 0.5 :: Color3 GLdouble

toStr::(Show a)=>[a]->[String]
toStr [] = [] 
toStr xs = map(\x -> show x) xs 



bool :: Bool -> a -> a -> a
bool b falseRes trueRes = if b then trueRes else falseRes

unless' :: Monad m => m Bool -> m () -> m ()
unless' action falseAction = do
    b <- action
    unless b falseAction

maybe' :: Maybe a -> b -> (a -> b) -> b
maybe' m nothingRes f = case m of
    Nothing -> nothingRes
    Just x  -> f x
    
-- type ErrorCallback = Error -> String -> IO ()
errorCallback :: G.ErrorCallback
errorCallback err description = hPutStrLn stderr description

{-| 
    === Generate random Vertex3
-} 
randomVertex::Integer -> IO [Vertex3 GLfloat]
randomVertex n = do 
                    ranl <- randomDouble (fromIntegral n) 
                    let ranlist = map (\x -> 1.5*x) ranl 
                    let vexList = fmap (\x -> x - 0.5) $ fmap realToFrac ranlist 
                    let vexTuple = map(\x -> tripleToVertex3 x ) $ filter(\x -> length x == 3) $ partList 3 vexList 
                                where 
                                    tripleToVertex3::[GLfloat] -> Vertex3 GLfloat 
                                    tripleToVertex3 [a, b, c] = Vertex3 a b 0.0 
                    return vexTuple


keyBoardCallBack :: IORef (Set G.Key) -> G.KeyCallback
keyBoardCallBack ref window key scanCode keyState modKeys = do
    putStrLn $ "keyBoardCallBack=>keyState" ++ show keyState ++ " " ++ "keyBoardCallBack=>key=" ++ show key
    case keyState of
        G.KeyState'Pressed -> modifyIORef ref (S.insert key) >> readIORef ref >>= \x -> print $ "inside keyBoardCallBack=> readIORef ref=>" ++ show x 
        G.KeyState'Released -> modifyIORef ref (S.delete key)
        _ -> return ()
    when (key == G.Key'Escape && keyState == G.KeyState'Pressed)
        (G.setWindowShouldClose window True)

data Camera = Camera {
        cameraPos::V3 GLdouble,
        cameraFront::V3 GLdouble,
        cameraUp::V3 GLdouble
        } deriving Show


data MyCamera = MyCamera {
        x::IORef GLdouble,
        y::IORef GLdouble
        }

projection xl xu yl yu zl zu = do
    matrixMode $= Projection
    loadIdentity    
    GL.ortho xl xu yl yu zl zu
    matrixMode $= Modelview 0
    
frustumProj left right top bot near far = do 
    matrixMode $= Projection
    loadIdentity
    GL.frustum left right top bot near far
    matrixMode $= Modelview 0

mymain :: IO ()
mymain = do
  G.setErrorCallback (Just errorCallback)
  -- finally add double buffer
  successfulInit <- G.init
  G.windowHint (G.WindowHint'DoubleBuffer True)
  -- if init failed, we exit the program
  bool successfulInit exitFailure $ do
      mw <- G.createWindow 1000 1000 "Simple example, haskell style" Nothing Nothing
      maybe' mw (G.terminate >> exitFailure) $ \window -> do
          G.makeContextCurrent mw
          mainLoop window 
          G.destroyWindow window
          G.terminate
          exitSuccess
          
mainLoop :: G.Window -> IO ()
mainLoop w = unless' (G.windowShouldClose w) $ do
    lastFrame <- maybe 0 realToFrac <$> G.getTime

    (width, height) <- G.getFramebufferSize w
    let ratio = fromIntegral width / fromIntegral height
    viewport $= (Position 0 0, Size (fromIntegral width) (fromIntegral height))
    GL.clear [ColorBuffer, DepthBuffer]
    loadIdentity
    -- lighting info
    diffuse  (Light 0) $= lightDiffuse
    ambient  (Light 0) $= lightAmbient
    specular (Light 0) $= lightSpecular
    position (Light 0) $= lightPosition
    light    (Light 0) $= Enabled
    --lighting           $= Enabled
    depthFunc  $= Just Lequal
    blend          $= Enabled
    lineSmooth     $= Enabled
    -- end lighting info


    -- Form a projection matrix
--    let fovy = 30.0; aspect = 1.0; zNear = 2.0; zFar = (-2)
--        in GM.perspective fovy aspect zNear zFar 

--    ortho (negate ratio) ratio (negate 1.0) 1.0 1.0 (negate 1.0)

--    loadIdentity

    -- lookAt(Vertex3 pos of camera, Vertex3 point at, Vector up)
    -- Form a ModeView Matrix
--    GM.lookAt (Vertex3 0 0 1::Vertex3 GLdouble) (Vertex3 0 0 0:: Vertex3 GLdouble) (Vector3 0 1 0 :: Vector3 GLdouble)

    matrixMode $= Modelview 0
    matrixMode $= Projection
    
    ref <- newIORef S.empty
    G.setKeyCallback w (Just $ keyBoardCallBack ref)
    --render sphere
    renderCoordinates
    --coordTip
    coordTipX
    coordTipY
    coordTipZ
    
    vexTuple' <- randomVertex 130 

    let pp0 = Vertex3 0   0   0
    let pp1 = Vertex3 1   0   0
    let pp2 = Vertex3 0   1   0
    let pp3 = Vertex3 0.1 0.1 0
    -- let vexTuple' = [pp0, pp1, pp2, pp3] 
    let stVex = fmap(\x -> show x) vexTuple'
    writeToFile "/tmp/hu.x" stVex 
    -- let vexTuple' = vertexList9 

    -- Find the top point on y-Axis
    let listVex = qqsort cmp vexTuple' where cmp (Vertex3 x1 y1 z1) (Vertex3 x2 y2 z2) = y1 > y2 

    -- draw a point on top of Y-axis
    drawCircleColor'  green 0.02 (head listVex) 
    let sortVex = mergeSortC cmp $ map(\p -> (cosVex3 rp p0 p, p0, p)) cx  -- p0 to all the rest of pts
                    where
                        p0 = head listVex
                        rp = addx1 p0 where addx1 (Vertex3 x y z) = Vertex3 (x+1) y z
                        cx = tail listVex
                        cmp c1 c2  = t1 c1 > t1 c2 
    let top = t2 $ head' sortVex             
    let xi = top
    let x1 = t3 $ head' sortVex             
    let segment = [top, x1]
--    drawPrimitive' Lines blue segment 
--    drawPrimitive' Points green vexTuple' 
    mapM_(\c -> drawCircle' c 0.01) $ vexTuple' 
--    mapM_(\x -> do 
--                drawPrimitive' Lines green x
--                threadDelay 10000
--                ) $ let len = length listVex in convexHull len top top x1 listVex 
    let hullList = convexHull2 le listVex where le = len listVex
    let posx = addx1 top where addx1 (Vertex3 x y z) = Vertex3 (x + 1) y z
                

    mapM_(\vx -> do 
                drawSegment green vx 
                -- threadDelay 500
                ) $ convexHullAllSeg vexTuple' -- let len = length $ tail listVex in convexHull len listVex 


        

    -- form three triangle from given pt and triangle if pt is inside the triangle
    let fun::[Vertex3 GLfloat] -> [[Vertex3 GLfloat]] -> [[Vertex3 GLfloat]] 
        fun [] _ = []
        fun _ [] = []
        fun (p:cx) (t:cy) = (g p t)++(fun cx cy)
            where
                g p' t' = if is then ff p' t' else []
                    where
                        is = fst $ ptInsideTri p' $ h t' 
                        h [x, y, z] = (x, y, z)
                        ff p [x, y, z] = [[p, x, y], [p, y, z], [p, x, z]]
    G.swapBuffers w
    G.pollEvents
    mainLoop w 

main = mymain


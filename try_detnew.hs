-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

--import Data.Array
--import Text.Regex.TDFA -- the module will conflict with Text.Regex

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}


-- zo - open
-- za - close

import AronModule 

type Vector = [Int]
type Matrix = [Vector]

cut :: [a] -> Int -> [a]
cut [] n = []
cut xs n
  | n < 1 || n > (length xs) = xs
  | otherwise = (take (n-1) xs) ++ drop n xs

numRows :: Matrix -> Int
numRows = length


numColumns :: Matrix -> Int
numColumns = length . head

remove :: Matrix -> Int -> Int -> Matrix
remove m i j  
  | m == [] || i < 1 || i > numRows m || j < 1 || j > numColumns m
    = error "(i,j) out of range"
  | otherwise = tran( cut (tran( cut m i ) ) j )

determinant :: Matrix -> Int
determinant [] = error "determinant: 0-by-0 matrix"
determinant [[n]] = n
determinant m = sum [ (-1)^(j+1) * (head m)!!(j-1) * determinant (remove m 1 j) | 
  j <- [1..(numColumns m) ] ]

showMatrix::Matrix -> IO()
showMatrix m = putStrLn $ rowWithEndLines m 
    where
        rowWithEndLines m = concat ( map(\r -> (show r) ++ "\n") m)

cofactor :: Matrix -> Int -> Int -> Int
cofactor m i j = (-1)^(i+j) * determinant (remove m i j)

cofactorMatrix :: Matrix -> Matrix
cofactorMatrix m = [ [ (cofactor m i j) | j <- [1..n] ] | i <- [1..n] ]
  where
  n = length m

inverse':: Matrix -> Matrix
inverse' m = tran[ [ quot x ( determinant m) | x <- row ] | row <- (cofactorMatrix m) ]

main = do 
        rm <- randomMatrix  10 10 
        pa rm
        pp $ inverse' rm 

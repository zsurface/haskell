import Data.Char 
import System.IO
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import System.Directory
import System.Environment
import System.FilePath.Posix
import Text.Regex.Posix
import Text.Regex
import Control.Monad 
import Data.List.Split

import AronModule


main = do 
        ls <- readFileToList "/tmp/x.x"
        pp ls
        fl
        pp $ splitFile ls "^[[:space:]]*$"
        fl
        pp $ splitFile ls "^[[:space:]]*(---){1,}"
        pp "done"

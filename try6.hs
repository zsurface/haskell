import Data.Char 
import System.IO
import System.Environment
import Text.Regex.Posix
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import AronModule


style::String->String->String->[String]->[String]
style pat l r list = map(`op` rep) list 
                        where op = subRegex $ mkRegex pat 
                              rep = l ++ match ++ r
                              match = "\\0"

main = do 
        print "Hello World"
        list <- readFileToList "./enu.ion"
        --print list
        --let r6 = mkRegex "(values:\\[[a-zA-Z0-9_;]+\\])"
        --putStrLn $ subRegex r6 "this file values:[file1;file_3;]"   "{\\0} {\\1}{\\2}"
        let ll = style "values:\\[[a-zA-Z0-9_;]+\\]"  "left" "right" ["values:[file1]", "values:[file2]"]
        mapM_ print ll

        let line = "values:[file1;]what values:[file2;file4;]"
        let splitcode  = splitRegex(mkRegex "(values:\\[[a-zA-Z0-9_;]+\\])") line
        mapM_ print splitcode

import System.Process
import System.IO
import System.Exit

main =  do
        --(_,_,Just hout,_) <- createProcess (proc "/usr/bin/grep" ["-Hnis", "--color", "--include=\"*.hs\"", "String"]) { cwd = Just ".", std_out=CreatePipe } 
        -- (_,Just hout,_,_) <- createProcess (proc "/usr/bin/grep" ["-Hnis", "--color", "--include=\"*.hs\"", "String"]) { cwd = Just ".", std_out=CreatePipe } 
        -- (_,Just hout,_,_) <- createProcess (proc "/bin/ls" []) { cwd = Just ".", std_out=CreatePipe } 
        -- (_,Just hout,_,_) <- createProcess (proc "/usr/bin/grep" ["-Hrnis", "--color", "String"]) { cwd = Just ".", std_out=CreatePipe } 
        -- out <- hGetContents hout
        -- sout <- readCreateProcess(shell "/usr/bin/grep -Hnris --color --include=\"*.hs\" String .", { cwd = Just "." } ) ""
        -- sout <- readCreateProcess(shell "ls" { cwd = "/bin/" } ) ""
        -- out <- readCreateProcess (shell "pwd" { cwd = Just "/etc/" }) ""
        -- (Just hIn, Just hOut, Nothing, ph) <- createProcess (proc "cat" ["-", "/etc/passwd" ]) { std_in = CreatePipe, std_out = CreatePipe}
        -- (Just hIn, Just hOut, Nothing, ph) <- createProcess (proc "cat" ["-", "/etc/passwd" ]) { std_in = CreatePipe, std_out = CreatePipe}
        (Just hIn, Just hOut, Nothing, ph) <- createProcess (proc "/usr/bin/grep"  ["-Hnris", "--color", "String"]) { cwd = Just ".", std_out = CreatePipe}
        out <- hGetContents hOut
        mapM_ putStrLn $ lines out
        ec <- waitForProcess ph
        if (ec == ExitSuccess)
            then return ()
            else error $ "grep process failed:" ++ show ec


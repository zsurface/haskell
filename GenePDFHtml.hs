-- {{{ begin_fold
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE QuasiQuotes       #-} -- RawString QQ 

module GenePDFHtmlLib where

-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"


-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Default
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import qualified Data.ByteString as BS
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import           Data.Int (Int64)
import           Data.Text (Text)
import qualified Data.Text as T
import           Database.SQLite.Simple
import           Database.SQLite.Simple.FromRow
import           Database.SQLite.Simple.FromField
import           Database.SQLite.Simple.ToField
import           Database.SQLite.Simple.Internal
import           Database.SQLite.Simple.Ok
import qualified Text.Regex.TDFA as TD

-- {-# LANGUAGE QuasiQuotes       #-} -- RawString QQ, Quasi 
import Text.RawString.QQ          -- QuasiQuotes needs for Text.RawString.QQ 

import AronModule 
import AronHtml

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}


-- zo - open
-- za - close


data LHead = LHead {xtitle::String, desc::String} deriving(Show, Eq)

instance Default LHead where
   def = LHead{xtitle="", desc=""}

data PDFInfo = PDFInfo 
    { pdfId :: Int64
    , title    :: Text
    , pdesc     :: Text
    , path     :: Text
    } deriving (Eq,Read,Show)

instance FromRow PDFInfo where
  fromRow = PDFInfo <$> field <*> field <*> field <*> field

instance ToRow PDFInfo where
  toRow (PDFInfo _pId title pdesc path) = toRow (title, pdesc, path)

-- test.tex
-- read the first two lines from Latex file
-- % title : nice title
-- % desc : nice description
-------------------------------------------------------------------------------
pdfFile = "/Library/WebServer/Documents/zsurface/pdf"        


titleDesc::[String] ->  LHead 
titleDesc [] = def LHead
titleDesc cx = he 
        where
         ls = map (splitStrChar "[:]") cx
         t = if (containStr "title" ((head.head) ls)) then (last.head) ls else ""
         d = if (containStr "desc" ((head.last) ls)) then (last.last) ls else ""
         he = LHead{xtitle = trim t, desc = trim d}

hempty = LHead {xtitle="", desc=""}
pdftable = "pdftable" 


td_ f d = [r|<td id="notme"><a href="|] <> f <> [r|">|] <> d <> [r|</a></td> |]

toT = strToStrictText
toS = strictTextToStr 

baseNameT::T.Text -> T.Text
baseNameT s = toT $ baseName $ toS s
    where
        toS = strictTextToStr 

styleT cs    =  " style='" <> (T.concat cs) <> "' "

ahrefT::T.Text -> T.Text -> T.Text
ahrefT link desc = [r| <a href='|] <> link <> [r|'>|] <> desc <> [r|</a>|]

tdTT::T.Text -> T.Text -> T.Text
tdTT att a = [r|<td att>|] <> a <> [r|</td>|]

trCT s        =  [r|<tr>|] <> s <> [r|</tr>|]


tableTT s     =  [r|<table |] <> s <> [r|>|] 
tableACT a s  =  (tableTT a) <> s <> [r|</table>|]

tableA::[[[T.Text]]] -> T.Text 
tableA cs = rc 
    where
        rc = tableACT (styleT st) 
                     (T.concat $ map(\r -> trCT $ T.concat $ map(\x -> let h = baseNameT $ x ! 2 
                                                                           p = x ! 2
                                                                       in tdTT att (ahrefT p h)
                                                                 ) r) cs) 
        st = ["font-size:18px;", "background:#DDDDDD;"]::[T.Text]
        att = [r| id='notme' |] :: T.Text
        (!) = (!!)

st = ["font-size:18px;", "background:#DDDDDD;"]::[T.Text]
tableAA::[[[T.Text]]] -> [T.Text] -> T.Text 
tableAA cs st = rc 
    where
        rc = tableACT (styleT st) 
                     (T.concat $ map(\r -> trCT $ T.concat $ map(\x -> let h = baseNameT $ x ! 2 
                                                                           p = x ! 2
                                                                       in tdTT att (ahrefT p h)
                                                                 ) r) cs) 
        att = [r| id='notme' |] :: T.Text
        (!) = (!!)


pdfMain::IO BS.ByteString
pdfMain = do
        home <- getEnv "HOME"
        let htmlHeader = home </> "myfile/bitbucket/testfile/pdfhtmlheader.html"    
        let htmlTailer = home </> "myfile/bitbucket/testfile/pdfhtmltailer.html"      
        let laxFile = home </> "myfile/bitbucket/testfile/test.tex"                   
        let texFile = home </> "myfile/bitbucket/math"                              
        let testdb = home </> "myfile/bitbucket/testfile/test.db"                     
        let pdfdb = home </> "myfile/bitbucket/database/sqlite3_pdfhtml.db"
        
        allTexFile <- lsRegexFull texFile "\\.tex$"
        allPDFfile <- lsRegexFull pdfFile "\\.pdf$"
        ls <- readFileLatin1ToList laxFile      
        allheads <- mapM(\x -> do 
                                ls <- readFileLatin1ToList x
                                let t = take 2 ls
                                return (t, x)
                                ) allTexFile 
        let texfile = filter (\x -> (titleDesc $ fst x) /= hempty) allheads 
        conn <- open pdfdb 
        let createTableQuery = [r| CREATE TABLE IF NOT EXISTS |] <> pdftable <> [r| (id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, pdesc TEXT, path TEXT) |]
        execute_ conn createTableQuery 
        execute_ conn ([r| DELETE FROM |] <> pdftable) 
        mapM_(\x -> let title = toT $ (head.fst) x
                        pdesc  = toT $ (last.fst) x
                        path  = toT $ snd x 
                    in execute conn ([r| INSERT INTO |] <> pdftable <> [r| (title, pdesc, path) VALUES (?,?,?) |]) (PDFInfo 0 title pdesc path)
                       ) texfile
        mapM_(\x -> let toT = strToStrictText 
                        title = "" 
                        pdesc  = "" 
                        path  = toT x 
                    in execute conn ([r| INSERT INTO |] <> pdftable <> [r| (title, pdesc, path) VALUES (?,?,?) |]) (PDFInfo 0 title pdesc path)
                       ) allPDFfile 
        pdfQuery <- query_ conn ([r| SELECT id, title, pdesc, path from |] <> pdftable) :: IO [PDFInfo]
        let plist = partList 4 pdfQuery -- [[PDFInfo]]
        let pdfLL = map (\r -> map(\c -> let t = if (T.length $ trimT $ title c) == 0 then toT $ "no title" else trimT $ title c
                                             d = if (T.length $ trimT $ pdesc c) == 0 then baseNameT $ path c else pdesc c
                                             p = path c
                                         in [t, d, p] 
                                   ) r) plist

        let tablea = tableAA pdfLL st
        return $ strictTextToStrictByteString tablea

-- main = do
--     bs <- pdfMain
--     print bs

module Main where

import Control.Monad (unless, when)
import Graphics.Rendering.OpenGL as GL 
import qualified Graphics.UI.GLFW as G
import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
import System.Exit
import System.IO
import Control.Monad
import Data.Set(Set) 
import Data.IORef 
import qualified Data.Set as S 

-- import Graphics.UI.GLUT
import Graphics.UI.GLUT.Callbacks.Global
import AronModule  hiding (rw)
import AronGraphic hiding (dist)
import AronOpenGL

-- | -------------------------------------------------------------------------------- 
-- | compile: runh keyboardpress_simple.hs  
-- | ghc -i/Users/cat/myfile/bitbucket/haskelllib -o file file.hs
-- | 
-- | KEY: keyboard example, keypress example, modifyIORef example,  
-- | -------------------------------------------------------------------------------- 

mymain :: IO ()
mymain = do
  -- finally add double buffer
  successfulInit <- G.init
  G.windowHint (G.WindowHint'DoubleBuffer True)
  -- if init failed, we exit the program
  bool successfulInit exitFailure $ do
      mw <- G.createWindow 600 600 "Simple example, haskell style" Nothing Nothing
      maybe' mw (G.terminate >> exitFailure) $ \window -> do
          G.makeContextCurrent mw
          ref <- newIORef initCam 
          refStep <- newIORef initStep 
          mainLoop window ref refStep
          G.destroyWindow window
          G.terminate
          exitSuccess

vertexList = [
            Vertex3 0.9 0.8 0,
            Vertex3 0.5 0.1 0,

            Vertex3 0.7 0.1 0,
            Vertex3 0.2 0.4 0,
            Vertex3 0.9 0.7 0,

            Vertex3 0.71 0.1 0,
            Vertex3 0.2 0.9 0,
            Vertex3 0.23 0.3 0
            ]
          

mainLoop :: G.Window -> IORef Cam -> IORef Step -> IO ()
mainLoop w ref refStep = unless' (G.windowShouldClose w) $ do
    (width, height) <- G.getFramebufferSize w
    viewport $= (Position 0 0, Size (fromIntegral width) (fromIntegral height))
    GL.clear [ColorBuffer, DepthBuffer]
    G.setKeyCallback w (Just $ keyBoardCallBack refStep)

    matrixMode $= Modelview 0
    loadIdentity
    keyboardRot ref refStep
    renderCoordinates

    -- drawTriangle

    -- render code here

    -- ptInsidePolygon::Vertex3 GLfloat -> [Vertex3 GLfloat] -> Bool
    let vv = let le = len vertexList in convexHull le vertexList
    mapM_ (\x -> drawSegmentWithEndPt red x) vv 
    
    let p0 = Vertex3 0.23 0.3 0 
    drawCircle' p0 0.02 
    let b = let le = len vertexList
                in ptInsidePolygon p0 vertexList 
    pp $ "b="<<< b

    G.swapBuffers w
    G.pollEvents
    mainLoop w ref refStep
main = mymain


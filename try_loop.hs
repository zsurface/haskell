import Data.Char 
import System.IO
import System.Environment
import Text.Regex.Posix
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import AronModule

for::[a]->(a -> IO ()) -> IO ()
for [] f = return ()
for (x:xs) f = f x >> for xs f


main = do 
        print "Hello World"
        for [[1,2], [3, 4]] (\l -> do 
            mapM_ print l
            print "dog"
            print "cat"
            let s = 3
            print s
            )
        print "Hello World"


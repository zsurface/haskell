{-# LANGUAGE OverloadedStrings #-}
import Network.Wai
import Network.HTTP.Types
import Network.Wai.Handler.Warp (run)

app :: Application
app _ respond = do
    respond $ responseLBS
        status200
        [("Content-Type", "text/plain")]
        "Hello, World!"

main :: IO ()
main = do
    putStrLn $ "http://localhost:8080/"
    run 8080 app

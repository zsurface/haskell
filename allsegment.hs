module Main where

import Control.Monad (unless, when)
import Graphics.Rendering.OpenGL as GL 
import qualified Graphics.UI.GLFW as G
import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
import System.Exit
import System.IO
import Control.Monad
import Data.Set(Set) 
import Data.Maybe(fromJust) 
import Data.IORef 
import qualified Data.Set as S 
import Control.Concurrent (threadDelay)

-- import Graphics.UI.GLUT
import Graphics.UI.GLUT.Callbacks.Global
import AronModule  hiding (rw)
import AronGraphic hiding (dist)
import AronOpenGL

-- | -------------------------------------------------------------------------------- 
-- | compile: runh keyboardpress_simple.hs  
-- | ghc -i/Users/cat/myfile/bitbucket/haskelllib -o file file.hs
-- | 
-- | KEY: keyboard example, keypress example, modifyIORef example,  
-- | -------------------------------------------------------------------------------- 


mymain :: IO ()
mymain = do
  -- finally add double buffer
  successfulInit <- G.init
  G.windowHint (G.WindowHint'DoubleBuffer True)
  -- if init failed, we exit the program
  bool successfulInit exitFailure $ do
      mw <- G.createWindow 600 600 "Simple example, haskell style" Nothing Nothing
      maybe' mw (G.terminate >> exitFailure) $ \window -> do
          G.makeContextCurrent mw
          ref <- newIORef initCam 
          refStep <- newIORef initStep 
          mainLoop window ref refStep
          G.destroyWindow window
          G.terminate
          exitSuccess
          

tripleToVertex3::[GLfloat] -> Vertex3 GLfloat 
tripleToVertex3 [a, b, c] = Vertex3 a b 0.0 

--drawAllSeg::[(Vertex3 GLfloat, Vertex3 GLfloat)] -> 
--            (Vertex3 GLfloat) -> 
--            [(Vertex3 GLfloat, Vertex3 GLfloat)]
--drawAllSeg cx p = 

mainLoop :: G.Window -> IORef Cam -> IORef Step -> IO ()
mainLoop w ref refStep = unless' (G.windowShouldClose w) $ do
    (width, height) <- G.getFramebufferSize w
    viewport $= (Position 0 0, Size (fromIntegral width) (fromIntegral height))
    GL.clear [ColorBuffer, DepthBuffer]
    G.setKeyCallback w (Just $ keyBoardCallBack refStep)

    matrixMode $= Modelview 0
    loadIdentity
    keyboardRot ref refStep
    renderCoordinates
--    drawTriangle

    ranl <- randomDouble 30 
    let ranlist = map (\x -> 1.5*x) ranl 
    let vexList = fmap (\x -> x - 0.5) $ fmap realToFrac ranlist 
    let vexT = map(\x -> tripleToVertex3 x ) $ filter(\x -> length x == 3) $ partList 3 vexList 
    let segList = join $ (map . map)(fromJust) $ map(\r -> filter(\x -> x /= Nothing) r) [[if p0 /= p1 then Just (p0, p1) else Nothing| p0 <- vexT ] | p1 <- vexT] 
    let segPairList = join $ (map . map)(fromJust) $ map(\r -> filter(\x -> x /= Nothing) r) [[if p0 /= p1 then Just (p0, p1) else Nothing| p0 <- segList ] | p1 <- segList] 

    -- pp segList
--    pp segPairList
--    mapM_(\sp -> do  
--                    case intersectSegNoEndPt (fst sp) (snd sp) of 
--                             Just tu -> return () 
--                             Nothing -> drawSegment blue $ fst sp
--                    threadDelay 50
--          ) segPairList 
    
    -- cx is a list of segments
--    let fv cx = unique $ join $ map(\x -> [fst x, snd x]) cx 
--    let mkSeg cx p = map(\x -> (p, x)) $ fv cx
--    let checkInter oldSeg newSeg = zipWith(\x y -> (intersectSegNoEndPt x y) == Nothing) oldSeg newSeg
    let f cx p = checkInter oldSeg newSeg 
            where
                -- Cartesian product
                -- checkInter oldSeg newSeg = join $ map(\x -> map(\y -> (intersectSegNoEndPt x y) /= Nothing) oldSeg) newSeg
                checkInter oldSeg newSeg = join $ map(\x -> map(\y -> ((intersectSegNoEndPt x y) /= Nothing, x)) oldSeg) newSeg
                mkSeg cx p = map(\x -> (x, p)) $ fv cx -- [(p0, p1)], p => [(p0, p), (p1, p)]
                oldSeg = cx         -- [(p0, p1)]
                newSeg = mkSeg cx p -- [(p0, p), (p1, p)]
                fv cx = unique $ join $ map(\x -> [fst x, snd x]) cx -- [(p0, p1), (p0, p2), (p1, p2)] => [p0, p1, p2]

    let fv cx = unique $ join $ map(\x -> [fst x, snd x]) cx -- [(p0, p1), (p0, p2), (p1, p2)] => [p0, p1, p2]
    let mkSeg cx p = map(\x -> (x, p)) $ fv cx -- [(p0, p1)], p => [(p0, p), (p1, p)]
    let cx = [(p0, p1)] 
            where
                p0 = Vertex3 0 0   0
                p1 = Vertex3 0 0.5 0
                p2 = Vertex3 0.8 0.8 0 

            
    let p0 = Vertex3 0 0   0
    let p1 = Vertex3 0.5 0.1 0
    let p2 = Vertex3 0.2 0.5 0 
    let p3 = Vertex3 0.8 0.7 0 
    let p4 = Vertex3 0.3 0.1 0 
    let cx1 = [(p0, p1), (p1, p2), (p2, p3), (p1, p4)] 

    let frm::[(Vertex3 GLfloat, Vertex3 GLfloat)] -> [(Vertex3 GLfloat, Vertex3 GLfloat)] -> [(Vertex3 GLfloat, Vertex3 GLfloat)]
        frm cx [] = cx
        frm cx (b:bx) = frm (g cx b) bx
            where
                g cx b = filter(\x -> x /= b) cx
                
    let sseg = frm cx1 [(p1, p2)]
    pp $ "sseg=" <<< sseg

    let p2 = Vertex3 1 1 0 
    pp $ "mkSeg cx p2" <<< mkSeg cx p2
    let newSeg = mkSeg cx1 p2
    pp $ "newSeg=" <<< newSeg 
    let allPairSeg = f cx1 p2 
    pp $ "allPairSeg=" <<< allPairSeg 
    let badSeg = map(\x -> snd x) $ filter(\x -> fst x == True) allPairSeg
    pp $ "badSeg=" <<< badSeg 
    -- let goodPairSeg = frm newSeg badSeg 
    let goodPairSeg = nonCrossSegment cx1 p2 
    pp $ "goodPairSeg=" <<< goodPairSeg 

    mapM_(\sp -> do 
                    drawSegment red sp
                    threadDelay 50
          ) cx1 
    mapM_(\sp -> do 
                    drawSegment blue sp
                    threadDelay 50
          ) goodPairSeg 


    -- render code here

    G.swapBuffers w
    G.pollEvents
    mainLoop w ref refStep
main = mymain


-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

{-# LANGUAGE QuasiQuotes       #-}

{-# LANGUAGE MultiWayIf #-}

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 
import Text.RawString.QQ         -- Need QuasiQuotes too 

import qualified Text.Regex.TDFA as TD

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

import AronModule 

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close

filterMM::(a -> Maybe a) -> [a] -> [a]
filterMM f [] = []
filterMM f (a:cs) = maybeconst (f a) (filterMM f cs) 
    where
        maybeconst::Maybe a -> [a] -> [a] 
        maybeconst Nothing l = l 
        maybeconst (Just a) l = a:l
        
foo::a -> Maybe a 
foo a = Just a 

insertMid::(a-> a -> a) -> [a] -> [a]
insertMid f [] = []
insertMid f (x:[]) = [x]
insertMid f x = zipWith f h t
        where
           h = init x
           t = tail x

interMiddle:: a -> [a] -> [a] 
interMiddle a xs = init $ interleave xs $ repeatN (len xs) a 

{-| 
    === Emacs Org mod forms a list 

    > ["dog", "cat"] => ["|", "dog", "|", "cat", "|"]
-} 
orgList::[String] -> [String]
orgList cs = ["|"] ++ (interleave cs $ repeat "|")

orgTable::FilePath -> [[String]] -> IO()
orgTable p cs = mapM_ (\r -> writeToFileAppend p [concat r]) css 
    where
        css = map(\r -> orgList r) cs

--  1 2 3 [4] 
--  x x x x
-- 1 x 2 x 3 x 4 

main = do 
        let f x = if x `mod` 2 == 0 then (Just x) else Nothing
        let g x = if x > 2 then (Just x) else Nothing
        pp $ filterMM f [1, 2, 3, 4]

        let ls = ['a'..'z']
        let us = ['A'..'Z']
        let m1 = map(\x -> [x]) us 
        let m2 = map(\x -> [r|$\mathscr{|]  <> [x] <> [r|}$|]) us 
        let m3 = map(\x -> [r|$\mathfrak{|] <> [x] <> [r|}$|]) us 
        let m4 = map(\x -> [x]) ls 
        let m5 = map(\x -> [r|$\mathscr{|]  <> [x] <> [r|}$|]) ls 
        let m6 = map(\x -> [r|$\mathfrak{|] <> [x] <> [r|}$|]) ls 
        let m7 = [m1, m2, m3, m4, m5, m6]
        let tr = tran m7
        orgTable "/tmp/m1.x" tr
        pp "done!"

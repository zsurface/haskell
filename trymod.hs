import Control.Monad
import Data.Char
import Data.List
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import AronModule 

fmod::Double->Integer->Double
fmod  a n = a - du
            where     
                num = realToFrac (div (round a) n)
                du = realToFrac ((round num)*n)
main = do 
        print "Hello World"
        let r = fmod 10.3 3
        print r

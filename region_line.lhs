This is my first literate programming, it sounds cool, 

Problem: Given n straight line, find the maximum regions that n straight line can divide it 
We can use recursion to solve the problem, 
1. find the number of regions that n-1 number of lines can divide
2. Induction can be used to find the n number of line can divide a region

>import AronModule 
>region::Int->Int
>region n | n == 0 = 1
>         | otherwise = region (n - 1) + n 
>
>main = do 
>        print "Hello World"
>        print $ region 0 == 1
>        print $ region 1 == 2 
>        print $ region 2 == 4 
>        pp "done!"

import Data.Char 
import System.IO
import System.Environment
import Text.Regex.Posix
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import AronModule

--f::(Num a)=>a->a->a->(a, a)
--root a b c =  
--    let   
--        delta = b*b - 4*a*c 
--        r = if signum delta == 0 then sqrt delta else 0 
--        x1 =  div (-b + r) (2*a)
--        x2 =  div (-b - r) (2*a)
--    in
--        (x1, x2)


class MyClass a where
    myadd::a->a->a
    (>+)::a->a->a

data MyData = MyData{x::Int, y::Int} deriving(Show) 

data MyPoint = MyPoint{x1::Int, y1::Int} deriving(Show)

instance MyClass MyData where
        myadd (MyData x1 y1) (MyData x2 y2) = MyData (x1 + x2) (y1 + y2) 
        (>+) (MyData x1 y1) (MyData x2 y2)  = MyData (x1 + y2) (y1 - x2)


myqsort::(Ord a)=>[a]->[a]
myqsort [] = []
myqsort (x:xs) = myqsort ([ e | e <- xs, e < x]) ++ [x] ++ myqsort [ e | e <- xs , e >= x]

root a b c =
        let
            delta = b*b - 4*a*c 
            r = if signum delta == 0 then sqrt delta else 0
            x1 = -b + (delta /2*a) 
            x2 = -b - (delta /2*a)
        in
            delta*delta + r + x2

fun a b = 
        let 
            x1 = a + 1 
            x2 = div x1 b
            x3 = div (x1 + 1) b
            x4 = div (x1 + 1) (b + 1) 
            x5 = div (-x1 + 1) 2*b
            r  = sqrt 3
            x6 =  div (b - 2) (2*a)
        in
            x2 + x5 

cmp::String->String->Bool
cmp s1 s2 = length s1 < length s2

f::MyPoint->MyPoint->Bool
f (MyPoint x1 y1) (MyPoint x2 y2) = x1 > x2

main = do 
        print "Hello World"
        let x = root 1 2
        print "dog"
        let x1 = fun 1 2
        print x1
        let d1 = MyData 1 2
        let d2 = MyData 2 3 
        let d3 = myadd d1 d2
        let d4 = (>+) d1 d2
        print d3
        print d4
        let l = myqsort [1, 3, 0, 4, 2]
        mapM_ print l 
        --let ll = qqsort cmp [1, 3, 0, 4, 2]
        let ll = qqsort cmp ["a", "ab", "c", ""]
        let l2 = qqsort f [(MyPoint 1 2), (MyPoint 3 4)]
        fl
        mapM_ print ll
        fl
        mapM_ print l2 
        fl

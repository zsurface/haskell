
-- KEY: haskell script

-- [0,0]  OK
-- [0,0]  OK
--
-- [0,0]  OK
-- [1,2]  OK
--
-- [0,3]  OK
-- [1,2]  OK
--
-- [1,2]  OK
-- [0,0]  OK
--
-- [1,0] x
-- [1,0] x
--
-- [0,1]  OK
-- [0,2]  OK
--
-- [1,2]
-- [1,3]

:{
let
compareArray :: [Int] -> [Int] -> Int
compareArray xx@(x:y:_) yy@(x':y':_) | x == 1 && y == 0 = error "Invalid format 1"
                            | x' == 1 && y' == 0 = error "Invalid format 2" 
                            | x == x' && x == 0 && y == y' && y == 0 = 0
                            | x == 0 && x' == 1 = 1
                            | x == 1 && x' == 0 = -1
                            | x == 0 && x' == 0 = f xx yy
                            | x == 1 && x' == 1 = negate $ f xx yy
                            | otherwise = error "ERROR"
  where
    dropWith cx cy = dropWhile (== 0) $ zipWith (\a b -> a == b ? 0 $ (a > b ? 1 $ -1)) cx cy
    f cx cy = let ls = dropWith (tail cx) (tail cy) in len ls == 0 ? 0 $ head ls
  
:}



  
let s0 = [0,0]
let s1 = [0,0]
compareArray s0 s1 == 0
  
let s0 = [1,1]
let s1 = [1,1]
compareArray s0 s1 == 0
  
let s0 = [0,1]
let s1 = [1,1]
compareArray s0 s1 == 1
  
let s0 = [1,1]
let s1 = [0,1]
compareArray s0 s1 == -1
  
let s0 = [1,1]
let s1 = [0,0]
compareArray s0 s1 == -1
  
let s0 = [1,1,3]
let s1 = [0,1,3]
compareArray s0 s1 == -1
  
let s0 = [0,1,3]
let s1 = [1,1,3]
compareArray s0 s1 == 1
  

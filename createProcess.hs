import System.Exit
import System.Process
import System.IO

main :: IO ()
main = do
    let p = (shell "grep --color --include=\"*.hs\" -Hnris String .")
            { std_in  = Inherit
            , std_out = CreatePipe
            , std_err = Inherit
            }
    (Nothing, Just hout, Nothing, ph) <- createProcess p
    ec <- waitForProcess ph
    out <- hGetContents hout 
    mapM_ putStrLn $ lines out

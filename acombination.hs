{-# LANGUAGE FlexibleContexts        #-}
module Main where
import Control.Monad (unless, when)
import Graphics.Rendering.OpenGL as GL 
import qualified Graphics.UI.GLFW as G
import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
import System.Exit
import System.IO
import Control.Monad
import Data.Set(Set) 
import Data.IORef 
import qualified Data.Set as S 

-- import Graphics.UI.GLUT
import Graphics.UI.GLUT.Callbacks.Global
import AronModule  hiding (rw)
import AronGraphic hiding (dist)
import AronOpenGL
import AronConstant

-- | -------------------------------------------------------------------------------- 
-- | compile: runh keyboardpress_simple.hs  
-- | ghc -i/Users/cat/myfile/bitbucket/haskelllib -o file file.hs
-- | 
-- | KEY: keyboard example, keypress example, modifyIORef example,  
-- | -------------------------------------------------------------------------------- 

--p0 = Vertex3 0 0   0 
--p1 = Vertex3 0.5 0 0 
--p2 = Vertex3 0 0.5 0 

mymain :: IO ()
mymain = do
  -- finally add double buffer
  successfulInit <- G.init
  G.windowHint (G.WindowHint'DoubleBuffer True)
  -- if init failed, we exit the program
  bool successfulInit exitFailure $ do
      mw <- G.createWindow 600 600 "Simple example, haskell style" Nothing Nothing
      maybe' mw (G.terminate >> exitFailure) $ \window -> do
          G.makeContextCurrent mw
          ref <- newIORef initCam 
          refStep <- newIORef initStep 
          mainLoop window ref refStep
          G.destroyWindow window
          G.terminate
          exitSuccess

initWin:: G.Window -> IORef Cam -> IORef Step -> IO ()
initWin w ref refStep = do
        (width, height) <- G.getFramebufferSize w
        viewport $= (Position 0 0, Size (fromIntegral width) (fromIntegral height))
        GL.clear [ColorBuffer, DepthBuffer]
        G.setKeyCallback w (Just $ keyBoardCallBack refStep)

        matrixMode $= Modelview 0
        loadIdentity
        keyboardRot ref refStep
        renderCoordinates

{-| 
    === Given a interval [a, b] and a list of Float, find all the Float in the interval [a, b]
    use BS to find the a and b
-} 
rangeOneDim::(Float, Float)->[Float] -> [Float]
rangeOneDim (a, b) cx = 
            where
                sort = quickSortAny cx

mainLoop :: G.Window -> IORef Cam -> IORef Step -> IO ()
mainLoop w ref refStep = unless' (G.windowShouldClose w) $ do
    initWin w ref refStep

    -- render code here

    drawSegment red (p0, p1)
    drawSegment red (p0, p2)
    drawSegment red (p1, p2)
    mapM_(\c -> drawCircle' c 0.01) $ affineTri p0 p1 p2
--    mapM_(\h -> mapM_(\t -> if h + t <= 1 then let p = affine h t p0 p1 p2 in drawCircle' p 0.01 else return ()) tt) hh







    G.swapBuffers w
    G.pollEvents
    mainLoop w ref refStep
main = mymain


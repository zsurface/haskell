import Data.Char 
import System.IO
import System.Environment
import Text.Regex.Posix
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import AronModule

-- term        = coefficient [variable]
-- variable    = 'x'
-- coefficient = number
-- number      = digit, {digit}
-- digit       = ['0'..'9']

checkDigit s 
        | s == '0' = True
        | s == '1' = True
        | otherwise = False
number::String->String
number s = filter(\x -> isDigit x) s

breakNum::String->(String, String)
breakNum s = break(\x -> isDigit x == False) s

main = do 
        print "Hello World"
        print $ isDigit '0'
        print $ number "344abc" 
        let br = breakNum "123dog"
        print br 

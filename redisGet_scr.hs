-- KEY: haskell script, haskell redis

:set -XLambdaCase
import Control.Lens

s = "101.11"
print s

redisSet "k33" "10.12"

:{
n <- redisGet "k33" <&> \case  
                         Just x -> read x :: Double 
                         Nothing -> 0
:}

fw "redisGet \"k33\""
print n

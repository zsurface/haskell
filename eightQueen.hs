-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
{-# LANGUAGE MultiWayIf #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import qualified Text.Regex.TDFA as TD

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}


-- zo - open
-- za - close

import AronModule hiding (ls) 


{-| 
    === Eight queens problem.
-} 
ls = [
      [0, 0, 0, 0],
      [0, 0, 0, 0],
      [0, 0, 0, 0],
      [0, 0, 0, 0]
      ]
ls1 = [
      [0, 0, 0, 0],
      [0, 1, 0, 0],
      [0, 0, 0, 0],
      [0, 0, 0, 0]
      ]
ls11 =[
      [1, 0, 0, 0],
      [0, 1, 0, 0],
      [0, 0, 0, 0],
      [0, 0, 0, 0]
      ]
ls2 = [
      [0, 0, 0, 0],
      [0, 0, 0, 0],
      [0, 0, 0, 1],
      [0, 0, 0, 0]
      ]
ls22 =[
      [0, 0, 0, 0],
      [0, 0, 0, 0],
      [0, 0, 0, 1],
      [0, 0, 1, 0]
      ]
ls3 = [
      [0, 0, 0, 0],
      [0, 1, 0, 0],
      [0, 0, 0, 1],
      [0, 0, 0, 0]
      ]
ls33 =[
      [0, 0, 0, 0],
      [0, 1, 0, 0],
      [0, 0, 0, 1],
      [1, 0, 0, 0]
      ]
(!) = (!!)

col::[[Integer]] -> Integer -> [Integer]
col ls c = (tran ls) ! fromIntegral c

--fun::[[Integer]] -> Integer -> Integer  
--fun ls m n = 

to = fromIntegral

{-| 
    === right diagonal matrix elements

    > diagonalMat ls 1 2 => [0, 0, 0]

    @
    ls = [
          [0, 0, 3, 0],
          [0, 1, 0, 4],
          [0, 0, 2, 0],
          [0, 1, 0, 3]
          ]
    @
-} 
diaMatRight::[[Integer]] -> Integer -> Integer -> [Integer]
diaMatRight [] _ _ = []
diaMatRight ls m n = dia ls m n 0 
    where
        dia::[[Integer]] -> Integer -> Integer -> Integer -> [Integer] 
        dia[] _ _ _ = [] 
        dia ls m n c = let m' = m - (min m n) 
                           n' = n - (min m n) 
                       in if m' + c < len ls && n' + c < len (ls ! 0) 
                          then ls ! ( to $ m'+c) ! (to $ n'+c) : dia ls m' n' (c + 1) else []
                where
                    (!) = (!!)


{-| 
    === left diagonal matrix elements

    > diagonalMat ls 0 2 => [3, 1, 0]

    @
    ls = [
          [0, 0, 3, 0],
          [0, 1, 0, 4],
          [0, 0, 2, 0],
          [0, 1, 0, 3]
          ]
    @
-} 
diaMatLeft::[[Integer]] -> Integer -> Integer -> [Integer]
diaMatLeft [] _ _ = []
diaMatLeft ls m n = dia ls m n 0 
    where
        dia::[[Integer]] -> Integer -> Integer -> Integer -> [Integer] 
        dia[] _ _ _ = [] 
        dia ls m n c = let m' = m - (min m n) 
                           n' = n - (min m n) 
                       in if m' + c < len ls && n' - c  >= 0  
                          then ls ! ( to $ m'+c) ! (to $ n' - c) : dia ls m' n' (c + 1) else []
                where
                    (!) = (!!)

{-| 
    === Check whether the move is valid or not

    >check:: 2d array -> column position -> row position -> bool
-} 
check::[[Integer]] -> Integer -> Integer -> Bool
check [] _ _ = False
check cs m n = if | (cs ! to m ! to n) == 1 -> False 
                  | (sum $ cs ! to m) == 1 -> False            -- sum mth row 
                  | (sum $ (tran cs) ! to n) == 1   -> False   -- sum nth col 
                  | (sumRight cs m n) == 1 -> False   -- right diagonal
                  | (sumLeft cs m n) == 1  -> False   -- left  diagonal
                  | otherwise                      -> True    -- valid move
                  where
                    to = fromIntegral

{-| 
    Boolean valiate(list, int c, int r);

        eightQueen(int[][] arr, int c, int r){
            if(c == len){
                print solution
            }else{
                for(int r=0; r<len; r++){
                    if(validate(arr, c, r)){
                        eightQueen(arr, c+1, 0); 
                    }
                }
            }
        }
    }
-} 

{-| 
    1 2 3 
    4 5 6
    7 8 10

ls = [
      [1, 0, 0, 0],
      [0, 0, 1, 0],
      [0, 0, 0, 0],
      [0, 0, 0, 0]
      ]
-} 
main = do 
        print "Hello World"
        argList <- getArgs
        pp $ length argList
        fw "index"
        pp $ ls ! 1 ! 1
        fw "sum"
        pp $ sum $ ls ! 1
        fw "col"
        pp $ col ls 1
        pp "done!"
        fw "diaMatRightl"
        fw "mat"
        pa mat
        fw "kk"
        pp $ (diaMatRight mat 0 1) == [2, 6] 
        fw "tran"
        pp $ (diaMatRight mat 0 2) == [3]
        fw "left"
        pp $ (diaMatLeft mat 0 2) == [3, 5, 7] 
        pp $ diaMatLeft mat 1 2 
        pp $ "check="<<< (check ls1 0 0 == False)
        pp $ "check="<<< (check ls2 3 2 == False)
        pp $ "check="<<< (check ls2 0 2 == True) 
        pp $ "check="<<< (check ls2 3 0 == True) 
        pp $ "check="<<< (check ls2 2 3 == False) 
        pp $ "rep2d="<<< (rep2d [[0]] 0 0 1 == [[1]])
        pp $ "rep2d="<<< (rep2d [[0, 0]] 0 1 1 == [[0, 1]])
        pp $ "rep2d="<<< (rep2d ls1 0 0 1 == ls11)
        pp $ "rep2d="<<< (rep2d ls2 3 2 1 == ls22) 
        pp $ "rep2d="<<< (rep2d ls3 3 0 1 == ls33) 
        let m = 0
        let n = 0
        pp $ (sumLeft mat 0 0) == 1 
        pp $ (sumLeft mat 2 2) ==  10 
        pp $ (sumLeft mat 1 1) == 15
        pp $ (sumLeft mat 0 2) == 15 
        pp $ (sumRight mat 0 0) == 16 
        pp $ (sumRight mat 2 2) == 16 
        pp $ (sumRight mat 1 1) == 16 
        pp $ (sumRight mat 0 2) == 3 
        pp $ (sumRight mat 1 2) == 8  
        pp $ eightQueen [[0]] [] 0 0 
        pa $ eightQueen ls [] 0 0 
        fl 
        let m1 = geneMat 10 Zero
        pa $ eightQueen m1 [] 0 0 
        fl
        pa mat
        fl
        pp $ leftDiagonal mat 1 1 
        fl
        pp $ rightDiagonal mat 1 1 
        fl
        pp $ rightDiagonal mat 0 1 
        fl
        pp $ leftDiagonal mat 0 1 

import Control.Monad
import Data.Char
import Data.List
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import AronModule 

libNum ="21383027629265" 
email = "mymail@gmail.com" 

runM::String->IO()
runM s = run ("echo " ++ s ++ " | pbcopy") >> return () 

helpMe::IO()
helpMe = do
        let list = [libNum, email]
        let zz = map(show) $ zipWith(\x y -> (x, y)) [1..] list
        mapM putStrLn zz
        fl

main = do 
        helpMe
        fl
        argList <- getArgs
        let len = length argList
        pp argList
        if len > 0 then do
            case head argList of 
                "1" -> runM libNum 
                "2" -> runM email 
                _   -> return ()
            else return () 
        pp "done!"

{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ApplicativeDo              #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE OverloadedStrings #-}
import Options.Applicative
import Control.Monad
import qualified Data.Char as C
import System.FilePath.Posix
import System.Directory
import Network.HTTP.Conduit
import qualified Data.ByteString.Lazy as B
import qualified Data.Text.IO as TIO
import qualified Data.Text as T

data Opts = Opts {
    optDownloadSitemaps :: Bool,
    optParseSitemapUrls :: Bool
  }

main = do
  opts <- parseCLI
  when (optDownloadSitemaps opts) downloadSitemaps
  when (optParseSitemapUrls opts) getAllBookUrls

parseCLI :: IO Opts
parseCLI = execParser $ info parseOptions (header "nowwhatdoiread")

parseOptions :: Parser Opts
parseOptions = do
    shouldDownloadsitemaps <- switch (long "downloadsitemaps")
    shouldParsesitemaps <- switch (long "parsesitemapurls")
    return $ Opts {
      optDownloadSitemaps=shouldDownloadsitemaps,
      optParseSitemapUrls=shouldParsesitemaps
    }

sitemapsDirectory = "data/sitemaps"
sitemapRange = [1..3]

sitemaps :: [String]
sitemaps = map sitemapUrl sitemapRange
  where
  sitemapUrl i = "https://www.goodreads.com/sitemap." ++ show i ++ ".xml.gz"

sitemapFilepath i = joinPath [sitemapsDirectory, show i ++ "_sitemap.txt"]

downloadSitemaps :: IO ()
downloadSitemaps = mapM_ downloadSitemap $ zip [1..] sitemaps
  where
    downloadSitemap (i, url) = do
      putStrLn $ "Downloading sitemap " ++ show i
      let path = sitemapFilepath i
      createDirectoryIfMissing True $ takeDirectory path
      simpleHttp url >>= B.writeFile path 

getAllBookUrls :: IO ()
getAllBookUrls = do
    let fs = map sitemapFilepath sitemapRange
    mapM_ getUrlsFromFile fs
  where
    getUrlsFromFile f = do
      b <- TIO.readFile f
      TIO.appendFile "data/bookurls.txt" $ T.unlines (findBookUrls b)

findBookUrls :: T.Text -> [T.Text]
findBookUrls = map getUrlFromLine . filter (T.isInfixOf "/book/") . T.lines
  where
    getUrlFromLine = T.replace "</loc>" "" . T.replace "<loc>" "" . T.strip 

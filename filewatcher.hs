-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}  --
{-# LANGUAGE MultiWayIf #-}  --
-- 
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Turtle (shell, empty)                      
import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent(threadDelay)

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".

import AronModule 

path = "/Users/cat/myfile/bitbucket/haskelllib"

data OSType = MacOS | FreeBSD deriving (Show, Eq)

table = [(MacOS, "darwin"), (FreeBSD, "freebsd")]

instance Enum OSType where
    fromEnum = fromJust . flip lookup table 
    toEnum   = fromJust . flip lookup (map swap table)
        
main = do 
        home <- getEnv "HOME"
        configMap <- readConfig "./config.txt"
        osv <- getEnv "OSTYPE"
        let os = if | containStr (fromEnum MacOS) osv  -> fromEnum MacOS 
                    | containStr (fromEnum FreeBSD) osv -> fromEnum FreeBSD 
                    | otherwise                -> error "unknown"
        let graphic = home </> "myfile/bitbucket/haskelllib/AronGraphic.hs"
        let aronmo = home </> "myfile/bitbucket/haskelllib/AronModule.hs"
        let waiLib = home </> "myfile/bitbucket/haskelllib/WaiLib.hs"
        let wai = home </> "myfile/bitbucket/snippets/snippet.hs"
        let pdf = home </> "myfile/bitbucket/math/number.tex"
        let aronGraphicTest = home </> "myfile/bitbucket/haskell/AronGraphicTest.hs"
        let aronopengl = home </> "myfile/bitbucket/haskelllib/AronOpenGL.hs"
        let javalib = home </> "myfile/bitbucket/javalib/Aron.java"
        let passworddir = home </> "myfile/password"
        let haskelllib = home </> "myfile/bitbucket/haskelllib"

        let javadoc= "cd $b/javalib && javadoc -cp $(ls -d $jlib/jar/*.jar | tr '\n' ':') -noqualifier all -d /Library/WebServer/Documents/zsurface/htmljavadoc  *.java"

        forever $ do
            nvar1 <- getFileStatus graphic     >>= return . modificationTime
            nvar2 <- getFileStatus aronmo      >>= return . modificationTime
            nvar3 <- getFileStatus wai         >>= return . modificationTime
            nvar4 <- getFileStatus pdf         >>= return . modificationTime
            nvar5 <- getFileStatus aronopengl  >>= return . modificationTime
            nvar6 <- getFileStatus javalib     >>= return . modificationTime
            nvar7 <- getFileStatus passworddir >>= return . modificationTime
            nvar8 <- getFileStatus waiLib      >>= return . modificationTime
            nvar9 <- getFileStatus haskelllib  >>= return . modificationTime
            prevDirMo <- dirModified haskelllib 

            nowTime <- timeNowSecond
            sleepSec 1

            now1 <- getFileStatus graphic     >>= return . modificationTime
            now2 <- getFileStatus aronmo      >>= return . modificationTime
            now3 <- getFileStatus wai         >>= return . modificationTime
            now4 <- getFileStatus pdf         >>= return . modificationTime
            now5 <- getFileStatus aronopengl  >>= return . modificationTime
            now6 <- getFileStatus javalib     >>= return . modificationTime
            now7 <- getFileStatus passworddir >>= return . modificationTime
            now8 <- getFileStatus waiLib      >>= return . modificationTime
            now9 <- getFileStatus haskelllib  >>= return . modificationTime
            currDirMo <- dirModified haskelllib 
            let dirMod = or $ zipWith(\x y -> x < y) prevDirMo currDirMo

            if | now1 > nvar1 -> return ()
               | dirMod || (now2 > nvar2) || (now9 > nvar9) -> do 
                    system "gene_haskell_haddock" 
                    system $ "runh2 " ++ aronGraphicTest
                    system $ dropExtension aronGraphicTest
                    cd path
                    system "gg p 'add'"
                    return ()
               | now3 > nvar3 -> system "killall wai" >> system "wai &" >> return ()
               | now4 > nvar4 -> do 
                    let outdir = " -outdir=" ++ (takeDirectory pdf) ++ " "
                    let cmd = "latexmk -pdf -synctex=1 -file-line-error " ++ outdir ++ pdf
                    out <- system $ "latexmk -pdf -synctex=1 -file-line-error " ++ outdir ++ pdf
                    pp out
                    -- following line is from ~/.latexmk
                    -- out <- system $ "open -a /Applications/Skim.app/Contents/MacOS/Skim"
                    let pf = (dropExtension pdf) ++ ".pdf" -- .tex => .pdf
                    out <- system $ "open -a /Applications/Skim.app/Contents/MacOS/Skim " ++ pf
                    return ()

               | now5 > nvar5 -> system "gene_haskell_haddock" >> return ()
               | now6 > nvar6 -> run javadoc  >> return ()
               | os == (fromEnum MacOS) && now7 > nvar7 -> do 
                    if now7 > nvar7
                    then do
                      txtList <- lsRegex passworddir "\\.txt$"
                      if len txtList > 0
                      then do
                        let errorFile = home </> "/myfile/bitbucket/testfile/error.txt"
                        date <- getLocalDate
                        pp $ "Error: Remove txt file from " ++ passworddir ++ " "
                        -- use run MacOS script to send notification
                        -- out <- run $ "notify.macsh " ++ "\"Urgent: Remove txt file from password dir:" ++ (concatStr txtList " ") ++ "\""
                        system $ "notify.macsh " ++ "\"Urgent: Remove txt file from password dir:" ++ (concatStr txtList " ") ++ "\""
                        let txtError = date ++ " -> " ++ (concatStr txtList " ")
                        putStrLn (unlines txtList)
                        writeToFile errorFile [txtError]
                        pp txtList
                      else pp $ "Ok in " ++ passworddir
                    else return ()
               | now8 > nvar8 -> system "gene_haskell_haddock" >> return ()
               | otherwise -> return ()
        pp "done!"


-- Thu Aug 16 18:50:49 2018 
-- compile: ghc -o flw flw.hs
-- 
-- remove GLFW.windowHint, otherwise it will crash  
-- 
-- some issue at the following code, need to comment it out
--    GLFW.windowHint (GLFW.WindowHint'ContextVersionMajor 3)
--    GLFW.windowHint (GLFW.WindowHint'ContextVersionMinor 3)
--    GLFW.windowHint (GLFW.WindowHint'OpenGLProfile GLFW.OpenGLProfile'Core)
--    GLFW.windowHint (GLFW.WindowHint'Resizable False)
-- It seems to me that code use OpenGLRaw-3.2.7.0 in my current Haskell package


-- base
import Control.Monad (when)
import Control.Exception (bracket)
-- GLFW-b, qualified for clarity
import qualified Graphics.UI.GLFW as GLFW
-- gl, all types and funcs here will already start with "gl"
import Graphics.GL.Core33
import Graphics.GL.Types

winWidth = 800

winHeight = 600

winTitle = "Hello Window"

-- | Ensures that we only run GLFW code while it's initialized, and also that we
-- always terminate it when we're done. Also, this function should only be used
-- from the main thread.
bracketGLFW :: IO () -> IO ()
bracketGLFW act = bracket GLFW.init (const GLFW.terminate) $ \initWorked ->
    when initWorked act

-- type KeyCallback = Window -> Key -> Int -> KeyState -> ModifierKeys -> IO ()
callback :: GLFW.KeyCallback
callback window key scanCode keyState modKeys = do
    print key
    when (key == GLFW.Key'Escape && keyState == GLFW.KeyState'Pressed)
        (GLFW.setWindowShouldClose window True)

main :: IO ()
main = bracketGLFW $ do
--    GLFW.windowHint (GLFW.WindowHint'ContextVersionMajor 3)
--    GLFW.windowHint (GLFW.WindowHint'ContextVersionMinor 3)
--    GLFW.windowHint (GLFW.WindowHint'OpenGLProfile GLFW.OpenGLProfile'Core)
--    GLFW.windowHint (GLFW.WindowHint'Resizable False)
    kkk <- GLFW.init
    maybeWindow <- GLFW.createWindow winWidth winHeight winTitle Nothing Nothing
    case maybeWindow of
        Nothing -> putStrLn "Failed to create a GLFW window!"
        Just window -> do
            -- enable keys
            GLFW.setKeyCallback window (Just callback)
            -- calibrate the viewport
            GLFW.makeContextCurrent (Just window)
            (x,y) <- GLFW.getFramebufferSize window
            glViewport 0 0 (fromIntegral x) (fromIntegral y)
            -- enter our main loop
            let loop = do
                    shouldContinue <- not <$> GLFW.windowShouldClose window
                    when shouldContinue $ do
                        -- event poll
                        GLFW.pollEvents
                        -- drawing
                        glClearColor 0.2 0.3 0.3 1.0
                        glClear GL_COLOR_BUFFER_BIT
                        -- swap buffers and go again
                        GLFW.swapBuffers window
                        loop
            loop

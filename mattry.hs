import Control.Monad
import Data.Char
import Data.List
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import AronModule 

s = [
    ["2/4", "100000"],
    ["0", "1"]
    ]
    
main = do 
        print "Hello World"
        argList <- getArgs
        pp $ length argList
        pp "done!"
        let ss = multiRatMat s s
        pa ss
        let p = trans ss
        fl
        pa p
        fl
        pa mat
        fl
        let tt = sortRow mat
        pa tt
        fl
        pa mat
        fl 
        let ma = inverse mat 
        pa $ fst ma
        fl
        pa $ snd ma
        l <- randomList 10
        pp l 
        fl
        m1 <- randomMatrix 7 7 
        pa m1
        fl
        let m2 = inverse m1 
        pa $ fst m2
        fl
        pa $ snd m2

{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE UnicodeSyntax #-}

module AronPoly where
import Control.Monad
import Control.Concurrent
import Data.Char 
import Data.List
import Data.List.Split
import Data.Time
import Data.Ratio
import Data.Maybe(fromJust, isJust)
import Data.Time.Clock.POSIX
import Data.Foldable (foldrM)
import qualified Data.HashMap.Strict as M
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import System.Random
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import qualified Text.Regex as TR 
import Text.Printf
import Data.Array
import Debug.Trace (trace)
import qualified Text.Regex.TDFA as TD

import qualified Data.Set as DS
import qualified Data.Text as Text (unpack, strip, pack)
import qualified Data.Text.Lazy hiding (map) 

import qualified Data.Vector as V 

--import Data.Array
--import Text.Regex.TDFA -- the module will conflict with Text.Regex

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}


-- zo - open
-- za - close

import AronModule 

(+:)::(Num a)=>[(a, a)] -> [(a, a)] -> [(a, a)]
(+:) [] _ = []
(+:) _ [] = []
(+:) (x:cs) (y:cy) = ((fst x) + (fst y), snd x) : (+:) cs cy 


main = do 
        print "Hello World"
        argList <- getArgs
        pp $ length argList
        pp "done!"

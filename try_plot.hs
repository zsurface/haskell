module Main where

import Control.Monad (unless, when)
import Graphics.Rendering.OpenGL as GL 
import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
import qualified Graphics.UI.GLFW as G
import System.Exit
import System.IO
import Control.Monad
import System.Random
import Data.Set(Set) 
import Data.IORef 
import qualified Data.Set as S 
import Linear.V3
import Linear.V3(cross) 
import Linear.Vector
import Linear.Matrix
import Linear.Projection as P
import Linear.Metric(norm, signorm)

import AronModule

-- tiny utility functions, in the same spirit as 'maybe' or 'either'
-- makes the code a wee bit easier to read
-- compile: ghc -o glfw glfw.hs

-- | Shorten realToFrac 
rf:: (Real a, Fractional b) => a -> b
rf = realToFrac

-- | mod for Double, 6.28 `fmod` 2 => 0.28
fmod::Double->Integer->Double
fmod  a n = a - du
            where     
                num = realToFrac (div (round a) n)
                du = realToFrac ((round num)*n)

mergeChunk::Int->[(GLfloat, GLfloat, GLfloat)]->[(GLfloat, GLfloat, GLfloat)]
mergeChunk n c = mergeList  (take n c)  (take n $ drop n c) 

nStep::Float
nStep = 40

-- sphere parametic equation center at (0, 0, 0)
sphere::[(GLfloat, GLfloat, GLfloat)]
sphere = [(cos(del*k)*cos(del*i), 
          _sin(del*k), 
          cos(rf del*k)*sin(rf del*i)) | k <- [1..n], i <-[1..n]]
            where 
                del = rf(2*pi/(n-1))
                n = nStep 

shear f = do
   m <-  (newMatrix RowMajor [1,f,0,0
                             ,0,1,0,0
                             ,0,0,1,0
                             ,0,0,0,1])
   multMatrix (m:: GLmatrix GLfloat)

poly::[Double]->[Double]->[Double]
poly [] _ = [] 
poly _ [] = [] 
poly xs sx = map(\s -> sum s) $ map(\x -> map(\(c, p) -> c*(x^p)) po) sx
            where
                po = zip xs [0..]

-- torus paremater equation center at (0, 0, 0)
torus::[(GLfloat, GLfloat, GLfloat)]
torus= [((br + r*cos(del*i))*cos(del*k), 
        sin(rf del*i), 
        (br + r*cos(rf del*i))*sin(rf del*k) ) | i <- [1..n], k <-[1..n]]
        where 
            del = rf(2*pi/(n-1))
            n = nStep 
            r = 0.2
            br = 0.3

-- preservingMatrix::IO a-> IO a
coordTip::IO()
coordTip = do 
    renderPrimitive Lines $ mapM_(\(x, y, z) -> do
        color (Color3 0 1 0 :: Color3 GLdouble)
        vertex (Vertex3 x y z ::Vertex3 GLfloat)
        ) conic 

coordTipX::IO()
coordTipX = do 
    preservingMatrix $ do
        translate (Vector3 1 0 0 :: Vector3 GLdouble)
        --rotate (90)$ (Vector3 0 0 1 :: Vector3 GLdouble)
        coordTip

coordTipY::IO()
coordTipY = do 
    preservingMatrix $ do
        translate (Vector3 0 1 0 :: Vector3 GLdouble)
        rotate (90)$ (Vector3 0 0 1 :: Vector3 GLdouble)
        coordTip

coordTipZ::IO()
coordTipZ = do 
    preservingMatrix $ do
        translate (Vector3 0 0 1 :: Vector3 GLdouble)
        rotate (-90)$ (Vector3 0 1 0 :: Vector3 GLdouble)
        coordTip

renderCoordinates::IO()
renderCoordinates = do
    renderPrimitive Lines $ do
        -- x-Axis
        color  (Color3 1 0 0 :: Color3 GLdouble)
        vertex (Vertex3 (-1) 0 0 ::Vertex3 GLdouble)
        color  (Color3 1 0 0 :: Color3 GLdouble)
        vertex (Vertex3 1 0 0 :: Vertex3 GLdouble)
        -- y-Axis
        color  (Color3 0 1 0 :: Color3 GLdouble)
        vertex (Vertex3 0 (-1) 0 :: Vertex3 GLdouble)
        color  (Color3 0 1 0 :: Color3 GLdouble)
        vertex (Vertex3 0 1 0 :: Vertex3 GLdouble)
        -- z-Axis
        color  (Color3 0 0 1 :: Color3 GLdouble)
        vertex (Vertex3 0 0 (-1) :: Vertex3 GLdouble)
        color  (Color3 0 0 1 :: Color3 GLdouble)
        vertex (Vertex3 0 0 1 :: Vertex3 GLdouble)

renderTriangle::IO()
renderTriangle = do
    renderPrimitive Triangles $ do
        color  (Color3 1 0 0 :: Color3 GLdouble)
        vertex (Vertex3 (negate 0.6) (negate 0.4) 0 :: Vertex3 GLdouble)
        color  (Color3 0 1 0 :: Color3 GLdouble)
        vertex (Vertex3 0.6 (negate 0.4) 0 :: Vertex3 GLdouble)
        color  (Color3 0 0 1 :: Color3 GLdouble)
        vertex (Vertex3 0 0.6 0 :: Vertex3 GLdouble)

triangle::[(GLfloat, GLfloat, GLfloat)]
triangle = [
               (0.9, 0.4, 0),
               (0.6, 0.4, 0),
               (0.4, 0.1, 1)
            ]

renderSurface::[(GLfloat, GLfloat, GLfloat)]->IO()
renderSurface xs = do 
        iterateList (bigChunk 80 xs) (\chunk -> do
                        let len = length chunk
                        let n = div len 2 
                        let c = mergeChunk n chunk
                        let cc = zipWith(\x y -> (x, y)) c [1..]
                        renderPrimitive TriangleStrip $ mapM_(\((x, y, z), n) -> do 
                            case mod n 3 of 
                                0 -> do 
                                        color(Color3 0.8 1 0 :: Color3 GLdouble) 
                                1 -> do 
                                        color(Color3 0 0.5 1 :: Color3 GLdouble) 
                                _ -> do 
                                        color(Color3 1 0 0.7 :: Color3 GLdouble) 
                            normal $ (Normal3 x y z::Normal3 GLfloat)
                            vertex $ Vertex4 x y z 0.8) cc  
                            )

torus2::[(GLfloat, GLfloat, GLfloat)]
torus2= [((br + r*_cos(del*i))*_cos(del*k) + _cos(del*i), 
        _sin(del*i)*_cos(del*k), 
        (br + r*_cos(del*i))*_sin(del*k) ) | i <- [1..n], k <-[1..n]]
        where 
            del = 2*pi/(n-1)
            n = nStep 
            r = 0.1
            br = 0.2

circle::[(GLfloat, GLfloat, GLfloat)]
circle =[ let alpha = (pi2*n)/num in (1, r*_sin(alpha), r*_cos(alpha)) | n <- [1..num]]
        where
            num = 40 
            r = 0.5 
            pi2 = 2*pi::Float 

conic::[(GLfloat, GLfloat, GLfloat)]
conic= [ let r' = r - rd*i in (d'*i, r'*_sin(del*k), r'*_cos(del*k)) | i <- [0..m], k <-[1..n]]
        where 
            n = 40::Float 
            pi2 = 2*pi::Float 
            del = pi2/n
            h = 0.2
            m = 20
            r = 0.1 
            d' = h/m
            rd = r/m

twopt10::[(GLfloat, GLfloat, GLfloat)]
twopt10 = take 20 sphere

twopt20::[(GLfloat, GLfloat, GLfloat)]
twopt20 = take 20 $ drop 20 sphere

bigChunk::Int->[(GLfloat, GLfloat, GLfloat)]->[[(GLfloat, GLfloat, GLfloat)]]
bigChunk n xs = splitPt n xs 

fourPt::[(GLfloat, GLfloat, GLfloat)]
fourPt = mergeList twopt10 twopt20 

testPt1::[(GLfloat, GLfloat, GLfloat)]
testPt1 = [(-1, 0, 0), (1, 0, 0), (2, 0, 0)]

testPt2::[(GLfloat, GLfloat, GLfloat)]
testPt2 = [(-1, 1, 0), (1, 1, 0), (2, 1, 0)]

testPt3 = mergeList testPt1 testPt2

splitPt::Int->[(GLfloat, GLfloat, GLfloat)]->[[(GLfloat, GLfloat, GLfloat)]]
splitPt _ [] = []
splitPt n xs = take n xs : (splitPt n $ drop n xs)

toStr::(Show a)=>[a]->[String]
toStr [] = [] 
toStr xs = map(\x -> show x) xs 


block = splitPt 10 sphere
pt100 = (join $ take 1 $ drop 0 block)::[(GLfloat, GLfloat, GLfloat)]
pt200 = (join $ take 1 $ drop 1 block)::[(GLfloat, GLfloat, GLfloat)]

pp1 = splitPt 2 pt100
pp2 = splitPt 2 pt200
zipPt = zipWith(\x y -> x ++ y) pp1 pp2
flatPt = join zipPt 

bool :: Bool -> a -> a -> a
bool b falseRes trueRes = if b then trueRes else falseRes

unless' :: Monad m => m Bool -> m () -> m ()
unless' action falseAction = do
    b <- action
    unless b falseAction

maybe' :: Maybe a -> b -> (a -> b) -> b
maybe' m nothingRes f = case m of
    Nothing -> nothingRes
    Just x  -> f x
    
-- type ErrorCallback = Error -> String -> IO ()
errorCallback :: G.ErrorCallback
errorCallback err description = hPutStrLn stderr description

-- type KeyCallback = Window -> Key -> Int -> KeyState -> ModifierKeys -> IO ()
keyBoardCallBack :: IORef (Set G.Key) -> G.KeyCallback
keyBoardCallBack ref window key scanCode keyState modKeys = do
    putStrLn $ show keyState ++ " " ++ show key
    case keyState of
        G.KeyState'Pressed -> modifyIORef ref (S.insert key)
        G.KeyState'Released -> modifyIORef ref (S.delete  key)
        _ -> return ()
    when (key == G.Key'Escape && keyState == G.KeyState'Pressed)
        (G.setWindowShouldClose window True)

data Camera = Camera {
        cameraPos::V3 GLfloat,
        cameraFront::V3 GLfloat,
        cameraUp::V3 GLfloat
        } deriving Show

isPressed::G.KeyState->Bool
isPressed G.KeyState'Pressed   = True
isPressed G.KeyState'Repeating = True
isPressed _                    = False

pressUp::G.Window->IO Bool
pressUp win = isPressed `fmap` G.getKey  win G.Key'Up

updateCamera::Set G.Key -> GLfloat -> Camera -> Camera
updateCamera keySet speed cam = S.foldr(\key cam@(Camera cameraPos cameraFront cameraUp) -> case key of
                                                G.Key'Escape -> cam{cameraPos = (V3 3 4 5),
                                                cameraFront              = (V3 3 4 5),
                                                cameraUp                 = (V3 3 4 5)}
                                                _ -> cam{cameraPos       = (V3 3 4 5),
                                                cameraFront              = (V3 3 4 5),
                                                cameraUp                 = (V3 3 4 5)}
                                        ) cam keySet

keyCallback :: G.KeyCallback
keyCallback window key scancode action mods 
                    | (key == G.Key'Escape && action == G.KeyState'Pressed) = G.setWindowShouldClose window True
                    | otherwise = do 
                                    shear 0.3 

data MouseInfo = MouseInfo {
            lastXY::Maybe (Double, Double),
            oldPitchYaw::(Double, Double),
            frontVec::V3 GLfloat
                } deriving Show

cursorPosCallBack::IORef MouseInfo -> G.CursorPosCallback
cursorPosCallBack ref window xpos ypos = do
    pp ("xpos=" ++ (show xpos) ++ " ypos=" ++ (show ypos)) 
    modifyIORef ref $ \oldInfo -> let
        (lastX, lastY) = case lastXY oldInfo of
            Nothing -> (xpos, ypos)
            (Just (lastX, lastY)) -> (lastX, lastY)
        sensitivity = 0.5
        xoffset = (xpos -lastX)* sensitivity
        yoffset = (lastY - ypos)* sensitivity
        lastX' = xpos
        lastY' = ypos
        (oldPitch, oldYaw) = oldPitchYaw oldInfo
        newYaw = (oldYaw + xoffset) `fmod` 360
        newPitch = min(max (oldPitch + yoffset) (-89)) 89
        toRadians = realToFrac . (*(pi/180))::Double -> GLfloat
        pitchR = toRadians newPitch
        yawR = toRadians newYaw
        front = signorm $ V3 (cos yawR * cos pitchR) (sin pitchR) (sin yawR * cos pitchR) 
        in MouseInfo (Just (lastX', lastY')) (newPitch, newYaw) front


toViewMatrix::Camera -> M44 GLfloat
toViewMatrix (Camera pos front up) = P.lookAt pos (pos ^+^ front) up


lightDiffuse ::Color4 GLfloat
lightDiffuse = Color4 0.6 1.0 0.5 0.6

lightAmbient ::Color4 GLfloat
lightAmbient = Color4 0.0 0.0 1.0 1.0

lightPosition ::Vertex4 GLfloat
lightPosition = Vertex4 1.0 1.0 1.2 0.0

lightSpecular ::Color4 GLfloat
lightSpecular = Color4 1.0 0.7 1.0 0.8

--GLfloat light_ambient[] = { 0.0, 0.0, 0.0, 1.0 };
--GLfloat light_diffuse[] = { 1.0, 1.0, 1.0, 1.0 };
--GLfloat light_specular[] = { 1.0, 1.0, 1.0, 1.0 };
--GLfloat light_position[] = { 1.0, 1.0, 1.0, 0.0 };
--
--glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
--glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
--glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
--glLightfv(GL_LIGHT0, GL_POSITION, light_position);

v3ToVertex3::V3 GLfloat-> Vertex3 GLfloat 
v3ToVertex3 (V3 x y z) = (Vertex3 x y z::Vertex3 GLfloat)

oldCamera = Camera (V3 1 1 1) (V3 1 0 0) (V3 0 1 0)

main :: IO ()
main = do
  G.setErrorCallback (Just errorCallback)
  -- finally add double buffer
  successfulInit <- G.init
  G.windowHint (G.WindowHint'DoubleBuffer True)
  -- if init failed, we exit the program
  bool successfulInit exitFailure $ do
      mw <- G.createWindow 600 600 "Simple example, haskell style" Nothing Nothing
      maybe' mw (G.terminate >> exitFailure) $ \window -> do
          G.makeContextCurrent mw
          mainLoop window
          G.destroyWindow window
          G.terminate
          exitSuccess
          
mainLoop :: G.Window -> IO ()
mainLoop w = unless' (G.windowShouldClose w) $ do
    (width, height) <- G.getFramebufferSize w
    let ratio = fromIntegral width / fromIntegral height
    viewport $= (Position 0 0, Size (fromIntegral width) (fromIntegral height))

    clear [ColorBuffer, DepthBuffer]

    let cameraSpeed = 0.5 
    ref <- newIORef S.empty
    G.setKeyCallback w (Just $ keyBoardCallBack ref)
    keyDown <- readIORef ref
    print $ show keyDown

    let camera = updateCamera keyDown cameraSpeed oldCamera
    print camera
    let pos   = v3ToVertex3 $ cameraPos camera
    let front = v3ToVertex3 $ cameraFront camera
    let up    = v3ToVertex3 $ cameraUp camera
    print $ " pos=" ++ show pos ++ " font=" ++ show front ++ " up=" ++ show up

    mainLoop w

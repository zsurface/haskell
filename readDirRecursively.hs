import System.FilePath.Posix
import System.Directory
import System.IO
import System.Posix.Files
import Data.Foldable
import Data.Time
import Data.Time.Clock
import Data.Char
import AronModule 


--    ([] ++ [f])
--    /         \
--  (d)         (f, d1)
--                  / 
--                (f2)
-- <,>::VxV -> R
-- Int + Int 
-- (+):: Int -> Int 
-- M(x) -> m x 
-- bind(m a) -> a
-- M(y) -> m y 
-- M bind( M(x) )
-- bind::M(y)->f(y)->M(x)
-- bind:: M(x) -> (m a -> a) -> M(y)
-- bind:: m a -> (a -> m b) -> m b 


-- | walking inside the dir
-- | if there is file, then append to list
-- | otherwise, walking inside the dir 
dirWalk :: FilePath -> (FilePath -> IO [String]) -> IO [String] 
dirWalk top filefunc = do
  isDirectory <- doesDirectoryExist top
  if isDirectory
    then 
      do
        files <- listDirectory top
        foldrM(\f d -> (dirWalk (top </> f) filefunc >>= \x -> return (x ++ d))) [] files 
    else
      filefunc top
 
main :: IO ()
main = do
         hSetEncoding stdout utf8
         hSetEncoding stdin  utf8
--         let worker1 fname = 
--              do if (takeExtension fname == ".hs")
--                   then return [fname]
--                   else return [] 
--         list1 <- dirWalk "/Users/cat/myfile" worker1
--         mapM_(print) list1

         let worker2 fname = 
              do if (toUpperStr (takeExtension fname) == (toUpperStr ".png"))
                   then return [fname]
                   else return []
         let myx = "dog"
         list2 <- dirWalk "/Users/cat/myfile" worker2 

         mapM_(print) list2

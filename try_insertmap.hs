-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

--import Data.Array
--import Text.Regex.TDFA -- the module will conflict with Text.Regex

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}
-- zo - open
-- za - close
import AronModule 

import qualified Data.Map as M

s = [(["dog", "cat"], 1), (["cow", "cat"], 2)]

type MMap = M.Map String [Int]

addMore::[([String], Int)] -> MMap -> MMap
addMore [] m = m
addMore (s:cs) m = addMore cs $ add s m 

add::([String], Int) -> MMap -> MMap
add ([], _)   m = m
add (s:cs, n) m = case ls of 
                       Just x -> add (cs, n) $ M.insert s (n:x) m 
                       _      -> add (cs, n) $ M.insert s [n] m
    where
        ls = M.lookup s m -- Maybe a

main = do 
        let empty = M.empty::M.Map String [Int]
        pp empty
        pp $ addMore [(["dog", "cat", "eel"], 1), (["dog"], 4), (["cat"], 4), (["dog"], 9), (["rat"], 4)] empty
        fl
        pp $ add (["dog", "cat"], 1) empty 

fBretz :: XYZ -> Double
fBretz (x,y,z) = ((x2+y2/4-1)*(x2/4+y2-1))^2 + z*z
  where
  x2 = x*x
  y2 = y*y

voxel :: Voxel
{-# NOINLINE voxel #-}
voxel = unsafePerformIO $ makeVoxel fBretz ((-2.5,2.5),(-2.5,2.5),(-1,1))

trianglesBretz :: Double -> IO [Triangle]
trianglesBretz level = computeContour3d voxel level

display :: Context -> DisplayCallback
display context = do
  clear [ColorBuffer, DepthBuffer]
  rot <- get (contextRotation context)
  triangles <- get (contextTriangles context)
  loadIdentity
  rotate rot $ Vector3 1 0 0
  renderPrimitive Triangles $ do
    materialDiffuse FrontAndBack $= red
    mapM_ drawTriangle triangles
  swapBuffers
  where
    drawTriangle (v1,v2,v3) = do
      triangleNormal (v1,v2,v3) -- the normal of the triangle
      vertex v1
      vertex v2
      vertex v3

keyboard :: IORef Float      -- rotation angle
         -> IORef Double     -- isolevel
         -> IORef [Triangle] -- triangular mesh
         -> KeyboardCallback
keyboard rot level trianglesRef c _ = do
  case c of
    'e' -> rot $~! subtract 2
    'r' -> rot $~! (+ 2)
    'h' -> do
             l $~! (+ 0.1)
             l' <- get l
             triangles <- trianglesBretz l'
             writeIORef trianglesRef triangles
    'n' -> do
             l $~! (- 0.1)
             l' <- get l
             triangles <- trianglesBretz l'
             writeIORef trianglesRef triangles
    'q' -> leaveMainLoop
    _   -> return ()
  postRedisplay Nothing

main :: IO ()
main = do
  _ <- getArgsAndInitialize
  _ <- createWindow "Bretzel"
  windowSize $= Size 500 500
  initialDisplayMode $= [RGBAMode, DoubleBuffered, WithDepthBuffer]
  clearColor $= white
  materialAmbient FrontAndBack $= black
  lighting $= Enabled
  lightModelTwoSide $= Enabled
  light (Light 0) $= Enabled
  position (Light 0) $= Vertex4 0 0 (-100) 1
  ambient (Light 0) $= black
  diffuse (Light 0) $= white
  specular (Light 0) $= white
  depthFunc $= Just Less
  shadeModel $= Smooth
  rot <- newIORef 0.0
  level <- newIORef 0.1
  triangles <- trianglesBretz 0.1
  trianglesRef <- newIORef triangles
  displayCallback $= display Context {contextRotation = rot,
                                      contextTriangles = trianglesRef}
  reshapeCallback $= Just yourReshapeCallback
  keyboardCallback $= Just (keyboard rot level trianglesRef)
  idleCallback $= Nothing
  putStrLn "*** Bretzel ***\n\
        \    To quit, press q.\n\
        \    Scene rotation:\n\
        \        e, r, t, y, u, i\n\
        \    Increase/Decrease level: h, n\n\
        \"
  mainLoop

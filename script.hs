{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE UnicodeSyntax #-}
import System.IO
import System.Environment
import Text.Regex.Posix
import Data.List
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import AronModule
import AronTest

--m = [ 
--        [168 , 336 , 1008 , 168 , 0    , 0]  , 
--        [0   , -24 , 0    , 4   , -256 , 36] , 
--        [0   , 0   , 1008 , 36  , -288 , 36]  
--    ]

m = [ 
        [1 , 2 , 3 , 1 , 0, 0], 
        [4 , 5 , 6 , 0 , 1, 0], 
        [7 , 8 , 10, 0 , 0, 1]  
    ]

mat9 = [ 
        [1 , 2 , 3 , 1 , 0, 0], 
        [4 , 5 , 6 , 0 , 1, 0], 
        [7 , 8 , 10, 0 , 0, 1],  

        [7 , 8 , 10, 0 , 0, 1],  
        [7 , 8 , 10, 0 , 0, 1],  
        [7 , 8 , 10, 0 , 0, 1]  
    ]

mat1 = [
      [1, 2, 3],
      [4, 5, 6],
      [7, 8, 10]
      ]
mat11 = [
      [2, 3, 4],
      [5, 6, 7],
      [8, 9, 11]
      ]

mat2 = [
      [1, 4, 1],
      [6, 67,3],
      [1,  -2,  1]
      ]

mat3 = [
        [1, 2, 3 , 3],
        [4, 5, 6 , 9],
        [7, 8, 10, 21],
        [17, 48, 9, 4]
       ]
mat4 = [
        [1.0, 2.0, 3.0, 3.0],
        [4.0, 5.0, 6.0, 9.0],
        [7.0, 8.0, 10.0, 21.0],
        [17.0, 48.0, 9.0, 4.0]
        ]

mat5 = [
        [-1.3361188486536677, 1.3054781801299906, -0.3649025069637883, -0.019498607242339833],
        [0.3853296193129062, -0.48003714020427113, 0.14484679665738162, 0.03064066852367688],
        [0.42618384401114207, 0.2618384401114206, -0.16991643454038996, -0.016713091922005572],
        [0.09563602599814298, -0.37697307335190344, 0.19498607242339833, 0.002785515320334262]
        ]


matr1 = [
        ["4/2", "1/1"],
        ["4/2", "1/1"]
       ]

matr2 = [
        ["4/2", "1/1"],
        ["9/3", "1/1"]
       ]

matn1 = [
        [2, 1],
        [2, 1]
       ]

matn2 = [
        [2, 1],
        [3, 1]
        ]

-- "2/3" + "4/3" = "6/3"
------------------------------------------------------------------ 
--selection::[a]->[(a, [a])]
--selection [] = []
--selection (x:cx) = (x,cx):[(y, x:ys)|(y, ys) <- selection cx]
--perm::[a]->[[a]]
--perm xs = [y:zs | (y, ys) <- selection xs, zs <-perm ys] 

data Pair a b = Pair a b deriving Show

infix 6 |+|
Pair a b |+| Pair a' b' = Pair (a + a') (b + b')

Vec u <+> Vec v = Vec $ zipWith(\x y -> addR x y) u v 
Vec u <-> Vec v = Vec $ subRL u v 
Rat x </> Rat y = Rat $ divR x y 
Rat x ⟋	 Rat y = Rat $ divR x y 
Rat x <.> Vec u = Vec $ multRL x u 
Rat x ⨉ Vec u = Vec $ multRL x u 
Vec u <?> Vec v =  Rat $ normR u v

Vec u <&> Vec v = normR u v 
Vec u <#> Vec v = normR u v 
Vec u <@> Vec v = normR u v 
Vec u <\> Vec v = normR u v 
Vec u ₩ Vec v = normR u v 
Vec u 😀  Vec v = "Emoji is awesome" 

--diag::(Num a)=>[[a]]->[[a]]
--diag ([]:_) = []
--diag cx = (map(\x -> head x) cx):diag(tail $ map tail cx)



matmult::(Num a)=>[[a]]->[[a]]->[[a]]
matmult m1 m2 = [[ dot r c | r <- m1] | c <- trans m2]
        where
            dot::(Num a)=>[a]->[a]->a
            dot r1 r2 = sum $ zipWith(\x y->x*y) r1 r2


--[1,2,3]
--[4,5,6]
--[7,8,10]
-- =>
--[1,2,3]
--  [3,6]
--    [3]

-- Gussian Elimination
gc::[[Integer]]->[[Integer]]
gc [] = []
gc m =  (head ma):(gc $ tail (map tail ma))
        where 
            ma = zipWith(\mx my -> zipWith(\x y -> (let mm = head my in if mm == 0 then 1 else mm)*x - (let mm = head mx in if mm == 0 then 1 else mm)*y) mx my) m1 m2 
            m1 = map(\x -> head m) m 
            m2 = (repeat' (length.head $ m) 0):(tail m)

------------------------------------------------------------------ 
-- Sat Mar  3 22:49:04 2018  
-- Better Gussian Elimination
-- 1. Support the element is zero
-- 2. Sort first element for each row
gc2::[[Integer]]->[[Integer]]
gc2 [] = []
gc2 m =  (head ma):(gc2 $ sortRow $ tail (map tail $ ma))
        where 
            ma = zipWith(\mx my -> if eq mx my then mx else zipWith(\x y ->(let mm = head my in if mm == 0 then y else (mm*x - (let mm = head mx in mm*y))) ) mx my) m1 m2 
            m1 = map(\x -> head m) m 
            m2 = m 
            eq::[Integer]->[Integer]->Bool
            eq _ [] = True
            eq (x:cx) (y:cy) = (x == y) && eq cx cy

        
main :: IO ()
main = do
    pa m
    fl
    let s = zipWith(\x y -> 4*x - y) (m !! 0) (m !! 1)
    let m1 = (head m):s:(last m):[]
    print s 
    fl 
    pa m1
    fl
    let m2 = zipWith(\x y -> 7*x - y) (m1 !! 0) (m !! 2)
    let m3 = (init m1) ++ [m2]
    pa  m3

    let m4 = zipWith(\x y -> 2*x - y) (m3 !! 1) (m3 !! 2)
    let m5 = (init m3)++[m4] 
    fl
    pa m5
    fl
    let m6 = zipWith(\x y -> 6*y - x) (m5 !! 1) (m5 !! 2)
    print m6
    fl
    let m7 = [head m5] ++ [m6] ++ drop 2 m5
    pa m7

    let m8 = zipWith(\x y -> x - 3*y) (m7 !! 0) (m7 !! 2)
    let m9 = m8:(drop 1 m7)
    fl
    print m8
    fl
    pa m9
    fl
    let m10 = zipWith(\x y -> 3*x + 2*y) (m9 !! 0) (m9 !! 1)
    print m10
    fl
    let m11 = m10:(drop 1 m9)
    pa m11
    let mm0 = map(\x -> x/3) (m11 !! 0)
    let mm1 = map(\x -> (-x)/3) (m11 !! 1)
    let mmm = mm0:mm1:(drop 2 m11)
    fl
    pa mmm
    fl
    let inv = map(\x -> drop 3 x) mmm
    pa inv 
    let mm = multiMat mat1 mat2 
    let mmm = multiMat mat4 mat5 
    fl
    pa mm
    fl
    pa mmm
    let u = [4, 5, 6]
    let v = [1, 2, 3]
    let w = [3, 4, 5]
    let s1 = scaleList (32/14) v
    let s2 = scaleList (52/50) w
    let s3 = listAdd s1 s2
    let s4 = listSub u s3
    fl
    print s4
    let nn = addR "1/1" "2/2"
    print nn
    let s1 = "1/1"
    let s2 = "2/2"
    let ss1 = splitRegex(mkRegex "/") s1
    let ss2 = splitRegex(mkRegex "/") s2
    let n1 = map(\x -> read x ::Integer) ss1
    let n2 = map(\x -> read x ::Integer) ss2
    let nn1 = map(\x -> x*(last n2)) n1 
    let nn2 = map(\x -> x*(last n1)) n2 
    fl
    print nn1
    fl
    print nn2
    let nn = ((head nn1) + (head nn2)):(tail nn1)
    fl 
    print nn
    let g = gcd (head nn) (last nn)
    let list = map(\x -> div x g) nn
    let mm = show (head list) ++ "/" ++ show (last list)
    let list = ["77/44", "43/2324", "0/4"]
    let f = foldl(addR) "0/1" list
    print f
    fl
    let s11 = splitR "3"
    print s11
    let s12 = splitR "3/1"
    print s12
    let dd = addR "0/1" "3/4"
    print dd
    let dd = addR "0/1" "0/4"
    print dd
    let ss9 = multR "2/3" "0/4"
    print ss9
    let ss9 = multR "2/3" "4/4"
    print ss9
    let ss9 = multR "4/3" "8/6"
    print ss9
    fl
    let ss1 = ["1/2", "1/1"]
    let ss2 = ["2", "2/2"]
    let sss = addRL ss1 ss2 
    print sss
    let ss3 = normR ss1 ss2
    fl
    print ss1
    print ss2
    fl
    print ss3
    let ss1 = ["2/4", "5/4"]
    let ss2 = ["3/1", "4"]
    let sss = addRL ss1 ss2 
    print sss
    let ss3 = normR ss1 ss2
    fl
    print ss1
    print ss2
    fl
    print ss3
    fl
    let ss = multRL "3/2" ss1
    print ss1
    print ss
    fl
    let ss = invR "3"
    print ss
    let ss = invR "3/4"
    print ss
    let ss = invR "3/4"
    print ss
    fl
    let ss = divR "3/4" "3/4"
    print ss
    fl
    let ss = divR "0/4" "3/4"
    print ss
    fl
    let mat = multiRatMat matr1 matr2
    pa mat
    fl
    let mat = multiMat matn1 matn2
    pa mat
    let s1 = ["1", "2", "3"]
    let s2 = ["7", "8", "10"]
    let s3 = divR (normR s1 s2)  (normR s1 s1)
    let ss = multRL s3  s1
    print ss
    fl
    let s = negR "3"
    print s
    let s = negR "3/4"
    print s
    let s = negR "0/4"
    print s
    fl
    let s = subR "1/4" "2/4"
    print s
    fl
    let s1 = ["1/2", "3/4"]
    let s = negList s1 
    print s
    fl
    let u1 = ["1", "2", "3"]
    let s2 = ["4", "5", "6"]
    let s3 = ["7", "8", "10"]
    let u2 = subRL s2   (multRL (divR (normR u1 s2)  (normR u1 u1))  u1)
    print u1 
    print s2 
    print s3 
    print u2 
    fl
    let p1 =  proj u1 s3 
    let p2 =  proj u2 s3 
    let u3 = subRL s3 (addRL p1 p2)
    print p1
    print p2
    print u3
    fl
    let p1 = Pair 1 2
    let p2 = Pair 2 3
    let p3 = p1 |+| p2
    print p3
    let v1 = Vec u1
    let v2 = Vec u2
    let v3 = v1 <+> v2
    fl
    print v3
    let v1 = Vec ["1", "2", "3"] 
    let v2 = Vec ["4", "5", "6"] 
    let v3 = Vec ["7", "8", "9"] 
    
    let u1 = v1
    let u2 = v2 ⦵ ((( v1 ⨂  v2) ⟋ (v1 ⨂ v1)) ⨉ v1)
    let p1 = ((( u1 ⨂  v3) ⟋ (u1 ⨂ u1)) ⨉ u1) 
    let p2 = ((( u2 ⨂  v3) ⟋ (u2 ⨂ u2)) ⨉ u2) 
    let u3 = v3 ⦵ (p1 ⨁	p2)
    let ss = v1 😀 v2
    print ss 
    fl
    let e1 = u1 ⨂  u2
    let e2 = u1 ⨂  u3
    let e3 = u2 ⨂  u3
    print e1
    print e2
    print e3
    let m = map(\y ->map(\x -> x) y) mat1
    print m
    fl
    let m = diag1 mat1
    print m
    fl
    pa mat1
    fl
    let m = uptri mat1
    pa m
    fl
    let m = zipWith(\x y -> zipWith(\x' y' -> (head y)*x' - (head x)*y') x y) (init mat1) (tail mat1)
    let m1 = (head mat1):m
    pa m1
    fl
    let m = gc mat1
    pa mat1
    fl
    pa m
    fl
    let ss = sortRow [[1, 2, 3], [0, 2, 5], [8, 4, 9]]
    pa ss
    print ss
    fl
    let m = gc mat9 
    pa m
    fl
    pa mat1
    fl
    let mat00 = [[1, 2, 3], [0, 2, 5], [8, 4, 9]]
    fl
    pa mat00
    fl 
    let smat = sortRow mat00
    pa smat
    fl
    let mat11 = [[1, 2, 3], [7, 2, 5], [8, 4, 9]]
    pa mat11
    fl
    let smat11 = sortRow mat11
    pa smat11
    fl
    let m = gc2 smat11
    pa m
    fl
    let m = gc2 mat1 
    pa mat1 
    fl
    pa m
    fw "mat7"
    let mat7 = [
          [1, 2, 3],
          [4, 5, 6],
          [7, 8, 10]
          ]
    let m = upperTri $ sortRow mat7 
    pa mat7 
    fl
    pa m
    let mat8 = [
          [1, 2, 3],
          [7, 0, 10],
          [7, 0, 10]
          ]
    let m = gc2 mat8 
    pa mat8 
    fl
    pa m
    fl
    pa m
    let mat9 = [
--            [1, 5],
--            [1, 2]
--          [1, 2, 3],
--          [4, 5, 6],
--          [7, 8, 10]
--          [1, 2, 3],
--          [0, 5, 6],
--          [7, 8, 10]
          [1, 2, 3, 4],
          [5, 99, 7, 19],
          [9, 10, 11, 12],
          [13, 14, 15, 17]
          ]
    let m = upperTri mat9 
    pa mat9 
    fl
    pa m
    fl
    let z0 = [0, 0]
    let z1 = [0, 0, 0]
    let z2 = [0, 0, 3]
    fw "sortRow" 
    let mat10 = [
          [0, 0, 3, 4],
          [2, 0, 10, 7],
          [0, 5, 0, 0],
          [0, 1, 10, 3]
          ]
    let m1 = sortRow mat10
    pa mat10
    fl 
    pa m1
    fw "mergeSortM"
    let m1 = mergeSortM mat10
    pa m1
    fw "-------------------"
    let mat = [
          [1, 2, 3],
          [4, 5, 6],
          [7, 8, 10]
          ]
    pa mat
    let mm = upperTri mat
    let m1 = zipWith(\x y -> (repeat' x 0)++y) [0..] mm 
    pa m1
    fl
    let mat = [
          [1, 2, 3],
          [0, 5, 6],
          [7, 0, 10]
          ]
    pa mat
    fw "-------------------"
    fw "upperTri no sortRow"
    let mm = upperTri mat
    pa mm
    fl
    let m1 = zipWith(\x y -> (repeat' x 0)++y) [0..] mm 
    pa m1
    fw "sortRow"
    pa $ sortRow mat
    fw "upperTri sortRow" 
    pa $ upperTri $ sortRow mat
    fl
    let mat = [
          [0, 0, 3],
          [7, 5, 6],
          [7, 5, 6]
          ]
    pa mat
    fw "no sortRow"
    print $ eqBool (isInversable [[1, 3], [0, 4]]) True
    print $ eqBool (isInversable [[0, 3], [0, 4]]) False 
    print $ eqBool (isInversable [[1, 0], [0, 1]]) True 
    print $ eqBool (isInversable [[1, 0], [0, 0]])  False
    print $ eqBool (isInversable [[1, 1], [0, 0]]) False 
    print $ eqBool (isInversable [[1, 1], [1, 0]]) True
    print $ eqBool (isInversable mat) True
    
    let mat = [ 
          [1, 2, 3, 1, 0, 1],
          [4, 5, 6, 0, 1, 0],
          [7, 8, 10,0, 0, 1]
          ]
    let m = upperTri mat
    let md = map(\x -> map(\y -> (realToFrac y) / (realToFrac $ head x))x) m 
    fw "md"
    pa md
    fl
    let m1 = zipWith(\x y -> (repeat' x 0)++y) [0..] md 
    pa m1
    let imm = map(\x -> drop (div (length x) 2) x ) m1
    fl
    pa imm 
    let mat = [
          [1, 2, 3],
          [4, 5, 6],
          [7, 8, 10]
          ]
    let id = multiMat mat imm
    fl
    pa id

import Data.Char 
import AronModule 

import System.Environment
import AronModule
import System.Directory 

import System.FilePath.Posix
import System.IO

f::Complex->Complex
f z = z*z

fs::Complex->Complex->Int->Complex
fs _ _ 0 = 0
fs c z n = c + (fs c (z*z) (n-1))

heads::[[a]]->[a]
heads ll = concatMap (take 1) ll

-- (+)::Integer->Integer->Integer

mandelbrot1::[(Integer, Integer)]
mandelbrot1 = do
        x <- [-10..10]
        if x > 0 
        then []
        else return (x, x+1)


fun::Integer->Integer
fun x = x + 1

mymap::(a->b)->[a]->[b]
mymap _ [] = []
mymap f (x:xs) = f x : mymap f xs

main = do 
        print "Hello World"
        argList <- getArgs 
        mapM_ print argList
        let c = Complex 1 2
        let z = Complex 1 2
        print z
        print $ f z
        fl
        print $ fs c z 0 
        fl
        print $ fs c z 4 
        fl
        print $ fs c z 6 
        let ll = [if (x `mod` 2 == 1) then 3*x+1 else (div x 2) | x <- [2..100]]
        mapM_ print ll
        let ml = mandelbrot1 
        pa1 ml


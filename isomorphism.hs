import Control.Monad
import Data.Char
import Data.List
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import AronModule 


f::Int->[()]
f n = take n (repeat ()) 

f'::[()] -> Int
f' = length

main = do 
        print "Isomorphism"
        let n1 = f' [(), ()] + f' [()]
        let n2 = f' ([(), ()] ++ [()]) 
        print $ n1 == n2 

        let dd = (f 3) ++ (f 2)
        let ff = f $ 3 + 2
        print $ dd == ff

        let bb = (f' . f) 1 
        let cc = (f . f')  [()] 
        print $ dd == ff

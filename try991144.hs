import Data.Array
import Text.Regex.TDFA
-- regex example, search and replace, search replace
replace::String -> (Int, Int) -> String -> String
replace s (m, off) r = first ++ r ++ rest 
    where
        first = take m s
        rest = drop (m + off) s
main = do 
        let s = "mydog dog dog cat dog"
        let r = makeRegex "\\<dog\\>" ::Regex
        let ar = matchAllText r s 
        pp $ ar 
        pp $ map(\x -> snd $ x ! 0 ) ar 
        pp $ replace s (2, 3) "KKK"
        pp $ map(\x -> let tu = x ! 0 in replace s (snd tu) ("<" ++ (fst tu) ++ ">")) ar 

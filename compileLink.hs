import Control.Monad
import Data.Char
import Data.List
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix

import AronModule 

cl::[String]->IO()
cl xs = if length xs == 2 then compileHaskellToBin (head xs) (last xs) else putStr "Invalid # of arguments:\n cmd f.hs $sym/symLik\n" 

main = do 
        argList <- getArgs
        cl argList
        fl 

-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}  --
-- 
-- import Turtle                       --
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import Graphics.Rendering.OpenGL as GL 
import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
import qualified Graphics.UI.GLFW as G
import Data.Set(Set) 
import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".

import AronModule 


eeps::Float -> Maybe Float -> Maybe Float -> Bool
eeps _ Nothing _ = False 
eeps _ _ Nothing = False 
eeps e (Just x) (Just y) = if (abs (x - y)) <= e then True else False

epMaybe::GLfloat -> 
       Maybe (Vertex3 GLfloat, (GLfloat, GLfloat)) -> 
       Maybe (Vertex3 GLfloat, (GLfloat, GLfloat)) -> Bool
epMaybe e Nothing _ = False
epMaybe e _ Nothing = False
epMaybe e (Just (Vertex3 a b c, (x, y))) (Just (Vertex3 a' b' c', (x', y'))) = t1 && t2 && t3 && t4 && t5
        where
            t1 = abs (a - a') < e
            t2 = abs (b - b') < e
            t3 = abs (c - c') < e
            t4 = abs (x - x') < e
            t5 = abs (y - y') < e

main = do 
        print "Hello World"
        argList <- getArgs
        pp $ length argList
        pp $ "eeps=" <<< eeps 0.01 (Just 1.00) (Just 1.001)
        pp "done!"

import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import AronModule 


-------------------------------------------------------------------------------- 
-- Tue Nov 20 21:24:09 2018 
-- implement a stack with list
-- 
-- Get maximum value in constant time 
-- push() and pop(), getMax()
-- newtype can be substituded with 'data' here
-------------------------------------------------------------------------------- 
newtype Stack a   = Stk a deriving (Show)
empty_            = Stk []  
push_ x (Stk xs)  = Stk (x:xs)
pop_ (Stk (x:xs)) = Stk xs
top_ (Stk (x:xs)) = x
isEmpty_ (Stk xs) = null xs
get (Stk xs)      = xs

getMax::Stack [Int] -> Int 
getMax (Stk []) = 0 
getMax (Stk (x:cx))  = x 

xpush::Int->Stack [Int]->Stack [Int]->(Stack [Int], Stack [Int])
xpush n (Stk xs) (Stk ys) = if isEmpty_ (Stk ys) || (top_ (Stk ys)) < n 
                            then (push_ n (Stk xs), push_ n (Stk ys)) 
                            else (push_ n (Stk xs), Stk ys)

xpop::Stack[Int]->Stack[Int]->(Stack[Int], Stack[Int])
xpop (Stk (x:cx)) (Stk (y:cy)) = if x == y 
                                 then ((Stk cx), (Stk cy)) 
                                 else ((Stk cx), (Stk (y:cy)))


  
main = do 
        print "Hello World"
        argList <- getArgs
        pp $ length argList
        let (st, ma) = xpush 1 empty_ empty_ 
        fl
        pp st
        pp ma
        let (st1, ma1) = xpush 4  st ma 
        let (st2, ma2) = xpush 2  st1 ma1 
        let (st3, ma3) = xpush 5  st2 ma2
        let (st4, ma4) = xpush 0  st3 ma3
        fl
        pp st4
        pp ma4
        let (s5, m5) = xpop st4 ma4
        let (s6, m6) = xpop s5 m5
        let (s7, m7) = xpop s6 m6
        let (s8, m8) = xpop s7 m7
        let (s9, m9) = xpop s8 m8
        pp $ getMax m7
        pp $ (s6, m6) 
        pp $ (s7, m7) 
        pp $ (s8, m8) 
        pp $ (s9, m9) 
        pp "done!"


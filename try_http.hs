#!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {{{ begin_fold
-- script
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

--import Data.Array
--import Text.Regex.TDFA -- the module will conflict with Text.Regex

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

import AronModule;
import Network.HTTP

url = "http://127.0.0.1:8080/snippet?id="
main = do
        line <- getArgs 
        let param = init $ foldr (\x y -> x ++ "+" ++ y) [] line
        pp param
        if length line > 0 then do
            let req = url ++ param 
            pp $ "[" <<< req <<< "]"
            rsp <- Network.HTTP.simpleHTTP (getRequest req)
                         -- fetch document and return it (as a 'String'.)
            s <- fmap (take 10000) (getResponseBody rsp)
            writeFile "/tmp/xx.html" s
            text <- run "w3m -dump /tmp/xx.html"
            mapM putStrLn text 
        else return [()]

import Graphics.UI.Gtk
import Control.Monad(forever, forM_)
import Control.Monad.Trans(liftIO)
import Control.Concurrent
import Control.Concurrent.STM
import qualified Control.Exception as Ex

interact1 :: String -> (String -> String) -> IO ()
interact1 title f = do
  initGUI

  -- essential elements
  inpbuf <- textBufferNew Nothing
  inp <- textViewNewWithBuffer inpbuf
  outbuf <- textBufferNew Nothing
  out <- textViewNewWithBuffer outbuf
  inpbut <- buttonNewWithLabel "enter"
  brkbut <- buttonNewWithLabel "break"
  brkbut `set` [widgetSensitive := False]
  clrbut <- buttonNewWithLabel "clear"
  clobut <- buttonNewWithLabel "clear"

  abortbox <- newEmptyTMVarIO
  jobbox <- newEmptyTMVarIO

  inpbut `on` buttonActivated $ do
    inpbut `set` [widgetSensitive := False]
    brkbut `set` [widgetSensitive := True]
    x <- get inpbuf textBufferText
    textBufferSetText outbuf ""
    atomically (putTMVar jobbox (f x))
  brkbut `on` buttonActivated $ do
    atomically (putTMVar abortbox ())
  clrbut `on` buttonActivated $ do
    textBufferSetText inpbuf ""
    widgetGrabFocus inp
  clobut `on` buttonActivated $ do
    textBufferSetText outbuf ""
    widgetGrabFocus inp

  let enable_input = do inpbut `set` [widgetSensitive := True]
                        brkbut `set` [widgetSensitive := False]
                        widgetGrabFocus inp
  supervisor_thread <- forkIO $ forever $ do
    fx <- atomically (takeTMVar jobbox)
    dropbox <- newEmptyTMVarIO
    work_thread <- forkIO (compute fx (atomically . putTMVar dropbox))
    c <- atomically ((takeTMVar abortbox >> return Nothing) `orElse`
                     (Just `fmap` takeTMVar dropbox))
    case c of
      Nothing -> do
        killThread work_thread
        postGUIAsync enable_input
      Just y -> postGUIAsync $ do
        textBufferSetText outbuf (either show id y)
        enable_input

  -- placement elements
  inpframe <- text_and_but inp [clrbut, inpbut, brkbut] "input"

  outframe <- text_and_but out [clobut] "output"

  pane <- vPanedNew
  panedPack1 pane inpframe True True
  panedPack2 pane outframe True True

  top <- windowNew
  set top [windowTitle:=title,
           windowDefaultWidth:=640, windowDefaultHeight:=480]
  top `on` deleteEvent $ liftIO (mainQuit >> return False)
  containerAdd top pane
  widgetGrabFocus inp
  widgetShowAll top
  mainGUI

text_and_but text buts title = do
  scroll <- scrolledWindowNew Nothing Nothing
  containerAdd scroll text

  row <- hBoxNew False 2
  boxSetHomogeneous row False
  boxPackStart row scroll PackGrow 0

  butsbox <- vButtonBoxNew
  set butsbox [boxSpacing := 2, buttonBoxLayoutStyle := ButtonboxStart]
  forM_ buts (containerAdd butsbox)
  boxPackStart row butsbox PackNatural 0

  frame <- frameNew
  set frame [frameLabel := title, frameShadowType := ShadowIn]
  containerAdd frame row

  return frame

compute fx reply = try_all (Ex.evaluate (seq_all fx)) >>= reply

try_all :: IO a -> IO (Either Ex.SomeException a)
try_all = Ex.try

seq_all xs = go xs `seq` xs where
  go [] = ()
  go (x:xs) = x `seq` go xs


-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

--import Data.Array
--import Text.Regex.TDFA -- the module will conflict with Text.Regex

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

import AronModule;
import Network.HTTP
import Network.URI.Encode

-- KEY: Get data from Wai Http Server, query snippet from httpd
-- 
-- Server: /Users/cat/myfile/bitbucket/haskell_webapp/wai.hs
--
-- Wed May  8 15:52:21 2019 
-- Snippet:         "s regex" => Snippet
-- Aron.java:       "j list" => Java Aron.*
-- Print.java:      "p pr"   => Java Print.*
-- AronModule.hs    "h list" => Haskell
--
-- Sun May 12 11:58:33 2019  
-- Remove code from Html to Text
-- return Text file from Wai server directly
-------------------------------------------------------------------------------- 
-- url = "http://127.0.0.1:8080/snippet?id=s+emacs"
url = "http://127.0.0.1:8080/snippet?id="
main = do
        line <- getArgs 
        if length line > 0 then do
            let param = init $ foldr (\x y -> x ++ "+" ++ y) [] line
            -- putStrLn param
            let req = url ++ param 
            pp $ "[" <<< req <<< "]"
            -- TODO: get text without any Html
            rsp <- Network.HTTP.simpleHTTP (getRequest req)
            -- fetch document and return it (as a 'String'.)
            s <- fmap (take 10000) (getResponseBody rsp)
            putStr s
            -- writeFile "/tmp/xx.html" s
            -- html to text file
            -- text <- run "w3m -dump /tmp/xx.html"
            -- pp text
            -- mapM putStrLn text 
            -- putStr "END" 
        else do
            putStrLn "The client DOES NOT connect to Redis."
            putStrLn "Output will be HTML data."
            putStrLn $ concatStr (repeatN 80 "-") []
            putStrLn "httpSnippet s regex => Snippet => snippet.hs"
            putStrLn "httpSnippet j list  => Java    => Aron.java"
            putStrLn "httpSnippet p pr    => Java    => Print.java"
            putStrLn "httpSnippet h list  => Haskell => AronModule.hs"

        -- run "haskellbin httpSnippet.hs httpSnippet" 

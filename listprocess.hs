import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

import AronModule 

main = do 
        print "Hello World"
        argList <- getArgs
        pp $ length argList
        list <- run "ps aux"
        pa list
        let lp = L.map(\x -> splitRegex(mkRegex "[[:space:]]+") x) list 
        let lp' = L.map(\x -> (take 1 (drop 1 x)) ++ (drop 10 x)) lp
        fl 
        pa lp
        fl
        mapM_(putStrLn) $ join lp'
        pp "done!"

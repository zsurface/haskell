-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import qualified Text.Regex.TDFA as TD

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}


-- zo - open
-- za - close

import Data.Default
import AronModule 

--class MyClass<T>{
--    T a;
--    public MyClass(int n){}
--    public MyClass fun(MyClass c){
--    }
--}
--
--MyClass c = new MyClass(3);
--fun(c);


-- type constructor , value constructor
data MyMaybe a = MyNothing | MyJust a

fun::MyMaybe Int -> Int 
fun MyNothing = 0 
fun (MyJust 3) = 1 

data Re = Re {xn::String, ag :: Integer -> Integer}

myag::Integer -> Integer
myag n = n + 1

data MyRecord = MyRecord{name::String, age::Integer} deriving(Show)

instance Default MyRecord where
    def = MyRecord {name = "name", age = 0}

ford::MyRecord
ford = def {age = 3}
-- pp ford

main = do 
        print "Hello World"
        argList <- getArgs
        pp $ length argList
        let rec = MyRecord{name="kkk"}
        pp rec
        pp ford
        pp "done!"

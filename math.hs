import qualified AronModule  as A

mm1 = [["a", "b"], ["c", "d"]]

vv1 = ["1", "2"]

sym1 =[
        ["0",   "-pi1", "-pi2",  "-pi3"],
        ["pi1", "0",    "pi3",   "-pi2"],
        ["pi2", "-pi3", "0",     "pi1"],
        ["pi3", "pi2",  "pi1",   "0"]
      ]

q0 = ["q0", "q1", "q2", "q3"]

qi = [
        [0,-1, 0, 0], 
        [1, 0, 0, 0], 
        [0, 0, 0,-1], 
        [0, 0, 1, 0] 
      ]

qj = [
        [0, 0,-1, 0], 
        [0, 0, 0, 1], 
        [1, 0, 0, 0], 
        [0,-1, 0, 0] 
      ]

qk = [
        [0, 0, 0, -1], 
        [0, 0,-1, 0], 
        [0, 1, 0, 0], 
        [1, 0, 0, 0] 
      ]

v1 = [1..2]
v2 = [2..3]

m1 = [[1, 2], 
      [3, 4]
     ]

m2 = [[5, 7], 
      [8, 9]
     ]

m3 = [[1, 2, 3],
      [0, (-2), 1],
      [1, 1, 0]
      ]

m4 = [[1, 2, 3, 4],
      [0, (-2), 1, 5],
      [1, 1, 0, 10],
      [1, 1, 0, 10]
      ]


m8 = [
      [1, 2, 3],
      [4, 5, 6],
      [7, 8, 9]
     ]
ms1 = [
        ["a11", "a12"],
        ["b11", "b12"]
      ]


ms2 = [
        ["c11", "c12"],
        ["d11", "d12"]
      ]

qv = ["q0", "q1", "q2", "q3"]
pv = ["p0", "p1", "p2", "p3"]




det::[[Integer]]->Integer
det  [] = 0
det  cx = if length cx > 1 && length(head cx) > 1 then sum (map(\x -> (fst x) * (-1)^(1 + (snd x))*det (A.removeRowCol 0 (snd x - 1) cx)) tuple) else head(head cx) 
            where
                row    = head cx
                tuple  = zipWith(\x y ->(x, y)) row [1..]

isSameList::[Integer]->[Integer]->Bool
isSameList  s1 s2 = if length s1 /= length s2 then False else f s1 s2 
                where
                    f [] []         = True
                    f (x:xs) (y:ys) = if x /= y then False else f xs ys

isSameMat::[[Integer]]->[[Integer]]->Bool
isSameMat  [] []         = True
isSameMat  (x:xs) (y:ys) = if isSameList x y == False then False else isSameMat xs ys


data Pair = Pair{int1::Integer, int2::Integer} deriving (Show)
instance Num Pair where 
    (Pair x1 y1) + (Pair x2 y2) = Pair (x1 + x2) (y1 + y2)   
    (Pair x1 y1) - (Pair x2 y2) = Pair (x1 - x2) (y1 - y2)   
    (Pair x1 y1) * (Pair x2 y2) = Pair (x1 * x2) (y1 * y2)   
    negate (Pair x1 y1) = Pair (-x1) (-y1)    

showPair::Pair->String
showPair Pair{int1=int1, int2=int2} = "[" ++ show(int1) ++ "," ++ show(int2) ++ "]"

instance Eq Pair where 
    (Pair x1 y1) == (Pair x2 y2) = x1 == x2 && y1 == y2
    (Pair x1 y1) /= (Pair x2 y2) = not (x1 == x2) || not (y1 == y2)

data Mat = Mat{m::[[Int]]} 





third::(a, b, c) -> c
third (_, _, c) = c


--addPoly::[(Float, String, Int)]->[(Float, String, Int)]->[(Float, String, Int)]
--addPoly [] _ = []
--addPoly _ [] = []
--addPoly p1 p2 =  

add::(Float, String, Int)->(Float, String, Int)->[(Float, String, Int)]
add (x1, s1, p1) (x2, s2, p2) = if p1 == p2 && s1 == s2 then [(x1 + x2, s1, p1)]  else [(x1, s1, p1), (x2, s2, p2)] 

simplify::[(Float, String, Int)]->[(Float, String, Int)]
simplify [] = []
--simplify xs = map(\x ->  
simplify xs = []  

sq::[Int]->[Int]
sq [] = []
sq [x] = [x]
sq (x:xs) = sq ([ s | s <- xs, s < x ]) ++ [x] ++ sq ([ s | s <- xs, s > x]) 

sqt::[(Int, Int)]->[(Int, Int)]
sqt [] = []
sqt [x] = [x]
sqt (x:xs) = sqt ([ s | s <- xs, snd s < snd x ]) ++ [x] ++ sqt ([ s | s <- xs, snd s > snd x]) 

sq2::[Int]->[Int]
sq2 [] = []
sq2 (x:xs) = sq2 (filter(\s -> s < x) xs ) ++ [x] ++ sq2 (filter(\s -> s > x) xs) 


f::[Int]->[Int]->[Int]
f [] l = [] 
f l [] = [] 
f (x:xs) l = map(+x) l ++ (f xs l)


f3::[Int]->[Int]->[[Int]]
f3 [] l = [] 
f3 l [] = [] 
f3 (x:xs) l = map (\a -> a*x:[]) l ++ (f3 xs l)

mulVect::[Int]->[Int]->[[Int]]
mulVect [] l = [] 
mulVect l [] = [] 
mulVect (x:xs) l = (map (\a -> x*a) l):(mulVect xs l)

transpose::(Num a)=>[[a]]->[[a]]
transpose ([]:_) = [] 
transpose l = (map head l) : transpose (map tail l) 

transposeS::[[String]]->[[String]]
transposeS ([]:_) = [] 
transposeS l = (map head l) : transposeS (map tail l) 

mul::(Num a)=>[[a]]->[[a]]->[[[a]]]
mul ([]:_) _ = []
mul _ ([]:_) = []
mul _  []    = []
mul [] _     = []
mul (x:xs) (y:ys) =  (mulVect x y):(mul xs ys)
                where
                    mulVect::(Num a)=>[a]->[a]->[[a]]
                    mulVect [] l = [] 
                    mulVect l [] = [] 
                    mulVect (x:xs) l = (map (\a -> x*a) l):(mulVect xs l)
                    
mmult::(Num a)=>[[a]]->[[a]]->[[a]]
mmult a b = [[sum $ zipWith (*) ar bc | bc <- (transpose b)] | ar <- a]

mulMat::(Num a)=>[[a]]->[[a]]->[[[a]]]
mulMat ([]:_) _ = []
mulMat _ ([]:_) = []
mulMat [] _ = []
mulMat _ [] = []
mulMat xs ys =  addMat (m !! 0)  (m !! 1) :[]
                where
                    m = mul xs (transpose ys)

multiMatVec::(Floating a)=>[[a]]->[a]->[a]
multiMatVec mm v = map(\m -> sum $ zipWith(*) m v) mm

multiMatVecS::[[String]]->[String]->[String]
multiMatVecS mm v = map(\r -> init $ foldl(++) [] $ map(\x -> "[" ++ x ++ "]+") $ zipWith(\x y -> x ++ " " ++ y) r v) mm

multiMat2::(Num a)=>[[a]]->[[a]]->[[a]]
multiMat2 mat1 mat2= transpose $ map(\m2 -> map(\m1 -> sum $ zipWith(*) m1 m2) mat1) $ transpose mat2

rotateMat2::(Floating a)=>a->[[a]]
rotateMat2 x = [
            [sin(x), -cos(x)], 
            [cos(x), sin(x)]
            ]

-- y -> z -> x 
rotX3::(Floating a)=>a->[[a]]
rotX3 x = [
            [1,       0,       0],
            [0,  cos(x), -sin(x)], 
            [0,  sin(x),  cos(x)]
          ]

-- x -> y -> z =>  x*z = -y 
rotY3::(Floating a)=>a->[[a]]
rotY3 x = [
            [cos(x),     0,  sin(x)], 
            [     0,     1,       0],
            [-sin(x),    0,  cos(x)]
       ]

-- y*z => x 
rotZ3::(Floating a)=>a->[[a]]
rotZ3 x = [
            [sin(x), -cos(x), 0], 
            [cos(x),  sin(x), 0],
            [     0,      0,  1]
           ]

identity::Integer->[[Integer]]
identity 0 = [] 
identity n = [[ if x == y then 1 else 0 | x <-[1..n]] | y <- [1..n]]

identityNegate::Integer->[[Integer]]
identityNegate 0 = [] 
identityNegate n = [[ if x == y then -1 else 0 | x <-[1..n]] | y <- [1..n]]

addMat::(Num a)=>[[a]]->[[a]]->[[a]]
addMat ([]:_) _ = []
addMat _ ([]:_) = []
addMat [] _ = []
addMat _ [] = []
addMat (x:xs) (y:ys) = (zipWith(\x1 y1 ->x1+y1) x y) : (addMat xs ys)

-- quaterion multiplication
quaterion::[String]->[String]->[[String]]
quaterion [] _ = []
quaterion _ [] = []
quaterion qv pv  = map(\qx -> map(\px -> qx ++ px) pv ) qv 

multiMatS::[[String]]->[[String]]->[[String]]
multiMatS a b = [[foldl(++) [] $ zipWith (++) ar bc | bc <- (transposeS b)]  | ar <- a]

main = do
    print "Hello World"
    mapM_ print m3
    print "-------------"
    mapM_ print m4
    print "-------------"
    mapM_ print $ mmult m3 m3
    print "-------------"
    mapM_ print $ mmult m4 m4
    print "-------------"
    mapM_ print $ mmult (mmult m3 m3) m3
    print "-------------"
    mapM_ print $ multiMatS ms1 ms2 
    print v1 
    print v2 
    let matq = quaterion qv pv
    mapM_ print $ matq 
    let mmm = multiMat2 m3 m3
    mapM_ print $ mmm 
    A.fl 
    mapM_ print $ multiMatVecS sym1 q0
    A.fw "i*i"
    A.pa $ mmult qi qi 
    A.fw "j*j"
    A.pa $ mmult qj qj 
    A.fw "k*k"
    A.pa $ mmult qk qk 
    A.fw "i*j"
    mapM_ print $ mmult qi qj
    A.fw "j*i"
    mapM_ print $ mmult qj qi
    A.fw "j*k"
    mapM_ print $ mmult qj qk
    A.fw "k*j"
    mapM_ print $ mmult qk qj
    A.fw "k*i"
    mapM_ print $ mmult qk qi
    A.fw "i*k"
    mapM_ print $ mmult qi qk
    let n = det m8
    print n
    A.fw "det m1"

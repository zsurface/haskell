-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}  --
-- 
-- import Turtle                       --
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

-- zo - open
-- za - close

import AronModule 


{-| 
    === Permutation of any type 

    > perm "12"
    >[["12", "21"]]

    @
        abc
            a   bc
                b c
                    [[]]
                c b
                    [[]]

            b   ac
                a c
                    [[]]
                c a
                    [[]]

            c   ab
                a b
                    [[]]
                b a
                    [[]]
    @ 

-} 
--perm2::(Eq a)=>[a] -> [[a]]
--perm2 [] = [[]]
--perm2 cx = [ pre:r | pre <- cx, 
--                   r <- perm2 $ filter(\x -> x /= pre) cx]


perm3 :: [a] -> [[a]]
perm3 []        = [[]]
perm3 il@(x:xs) = concatMap ((rotations len).(x:)) (perm3 xs)
                  where len = length il
                        rotations :: Int -> [a] -> [[a]]
                        rotations len xs = take len (iterate (\(y:ys) -> ys ++ [y]) xs)

main = do 
        print "Hello World"
        pp $ "per=" <<< perm3 "dog"
        pp "done!"

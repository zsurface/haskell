-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

{-# LANGUAGE MultiWayIf        #-}
{-# LANGUAGE QuasiQuotes       #-} -- support raw string [r|<p>dog</p> |]
import Text.RawString.QQ       -- Need QuasiQuotes too 


-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import qualified Text.Regex.TDFA as TD

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

-- shell command template:
-- 
--        argList <- getArgs
--        if len argList == 2 then do
--            let n = stringToInt $ head argList
--            let s = last argList
--            putStr $ drop (fromIntegral n) s
--        else print "drop 2 'abcd'"


import AronModule 

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close

f = \x -> 1/x

-- 0, 1, 2
-- d*1, d*2
-- 0 1 2 3
-- 0 1 2
-- 1 2 3
-- (0+1)/2, (1+2)/2, (2+3)/2
-- 0   1 2 3
-- 0 + d*1|  d*2 | d*3 |

natureE::Integer -> Float 
natureE k = integral
    where
        n = 100
        del = (k-1)*(divI 1 n)
        stepList = map(\x -> 1 + del*x) [0..n]
        yList = map(\x -> f x) stepList
        midValues = zipWith(\x y -> divI (x + y) 2 ) (init yList) (tail yList)
        integral = fromIntegral $ sum $ map(\x -> x*del) midValues 

main = do 
        print "Hello World"
        argList <- getArgs
        let n = 2000 
        pp $ length argList
        pp "done!"
        --  k = 3 -- 2*3 + 1
        let k = 5 -- 4*4 + 1
        let del = (k-1)*(divI 1 n)
        fw "del"
        pp del
        let stepList = map(\x -> 1 + del*x) [0..n]
        fw "stepList"
        pp $ len stepList
        pp stepList
        fl
        let yList = map(\x -> f x) stepList
        fw "yList"
        pp yList
        let midValues = zipWith(\x y -> divI (x + y) 2 ) (init yList) (tail yList)
        fw "midValues"
        pp midValues 
        let integral = sum $ map(\x -> x*del) midValues 
        fl
        fw "integral"
        pp integral 








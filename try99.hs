
{-# LANGUAGE CPP #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE TypeFamilies              #-}

import Diagrams.Prelude
import Diagrams.Backend.SVG.CmdLine

myCircle :: Diagram B
myCircle = circle 1

main = mainWith myCircle

#if 0
main = do
        print "dog" 
        writeFileBS
        readFileLatin1ToList
        geneMat
        file
        drop
        take
        strTo

        fl   strto
         
        matrix
        mat
#endif        

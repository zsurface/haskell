import Control.Monad
import Data.Char
import Data.List
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Concurrent 

import AronModule 

-- KEY: delay, sleep, thread
maybePrint :: IORef Bool -> IORef Bool -> IO ()
maybePrint myRef yourRef = do
   writeIORef myRef True
   yourVal <- readIORef yourRef
   unless yourVal $ putStrLn "critical section"

main :: IO ()
main = do
r1 <- newIORef False
r2 <- newIORef False
forkIO $ maybePrint r1 r2
forkIO $ maybePrint r2 r1
threadDelay 1000000

import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.Set
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

import AronModule 



σ::Int->Int
σ n = n + 1

ø::Int->Int->Int
ø m n = m + n

swapList::Int->Int->[a]->[a]
swapList m n l =
  where
    left = take (m + 1) l
    left' = head left
    mid = drop (m + 1) $ take n l
    right = drop n l
    right' = head $ drop n l
  


fun::(Num a)=>[a]->a
fun _ = 3


main = do 
        print "Hello World"
        argList <- getArgs
        pp $ length argList
        let n = σ 3
        pp n
        pp "done!"

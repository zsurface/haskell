import Data.Char 
import AronModule 

import System.Environment
import System.Directory 

import System.FilePath.Posix
import System.IO
import AronModule

data Tree a = Empty | Node a (Tree a) (Tree a) deriving(Show, Eq)

insert::(Ord a)=>Tree a -> a -> Tree a
insert Empty a = Node a Empty Empty 
insert (Node a left right) b = if b < a then (Node a (insert left b) right) else (Node a left (insert right b))

insertFromList::(Ord a)=>Tree a ->[a]->Tree a
insertFromList Empty [] = Empty
insertFromList (Node a l r) [] = Node a l r
insertFromList Empty (x:xs) = insertFromList (insert Empty x) xs 
insertFromList (Node a l r) (x:xs) = insertFromList (insert (Node a l r) x) xs

inorder::(Ord a)=>Tree a ->[a]
inorder Empty = []
inorder (Node a l r) = (inorder l) ++ [a] ++ (inorder r)

-- | -------------------------------------------------------------------------------- 
-- | check whether a Tree is Binary tree
-- | Thu Nov  8 09:16:34 2018 
-- | -------------------------------------------------------------------------------- 
-- | defintion of BST
-- | 1. Null is BST
-- | 2. Left subtree is BST
-- | 3. Right subtree is BST
-- | 4. minimum of left subtree is less than parent node
-- | 5. maximum of right subtree is greater than parent node
-- | -------------------------------------------------------------------------------- 
isBST::(Ord a)=>Tree a -> Bool
isBST Empty = True
isBST (Node a l r) =   isBST l 
                    && isBST r 
                    && (if l /= Empty then max l < a else True) 
                    && (if r /= Empty then min r > a else True)   
            where
                min::(Ord a)=>Tree a -> a 
                min Empty                = error "min error"
                min (Node a Empty Empty) = a
                min (Node a l _)         = min l

                max::(Ord a)=>Tree a -> a 
                max Empty                = error "max error"
                max (Node a Empty Empty) = a
                max (Node a _ r)         = max r
                

mmin::(Ord a)=>Tree a -> a 
mmin Empty                = error "min error"
mmin (Node a Empty Empty) = a
mmin (Node a l _)         = mmin l

max'::(Ord a)=>Tree a -> a 
max' Empty                = error "max error"
max' (Node a Empty Empty) = a
max' (Node a _ r)         = max' r
 
main = do 
        print "Hello World"
        let tree1 = Node 5 (Node 4 Empty Empty) (Node 9 Empty Empty)
        let t1 = insert tree1 1
        print t1
        let t2 = insert tree1 10
        print t2
        let t3 = Node 5 Empty Empty
        let tlist = insertFromList t3 [1, 2] 
        fl
        print tlist
        let ilist = inorder tree1
        print ilist
        pp $ isBST tree1 == True
        let tree2 = Node 5 (Node 20 Empty Empty) (Node 9 Empty Empty)
        pp $ isBST tree2 == False
        let t1 = Empty::(Tree Int)
        pp $ isBST t1 
--        let t2 = Node 10 (Node 8 Empty Empty) (Node 20 Empty (Node 4 Empty Empty)) 
--        pp $ isBST t2 == False

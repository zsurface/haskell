module AronTest where
import Text.Regex
import Text.Read
import Data.List
import System.Process
import System.Environment
import qualified Data.Text as Text
import qualified Data.Vector as Vec 
import qualified AronModule as A 

-------------------------------------------------------------------------------- 
-- load shell environments inside GHC
-- use the SAME NAME as in shell environments
-------------------------------------------------------------------------------- 

--data MyVector = Vec1 [Integer] | VecS[String] | VecInt[Int] | Vec2 [[Integer]] 
data MyVector = Vec1 [Integer] | VecS[String] | Vec2 [[Integer]] | VecD [Double] deriving Eq 


--eqAssert::[Integer]->[Integer]->Bool
--eqAssert []  []  = True
--eqAssert  _  []  = False 
--eqAssert (x:cx) (y:cy) = if length cx /= length cy then False else
--                             (if x == y then eqAssert cx cy else False)

eqAssert::MyVector->MyVector->Bool
eqAssert (Vec1 cx) (Vec1 cy) = cx == cy
eqAssert (VecS cx) (VecS cy) = cx == cy
eqAssert (VecD cx) (VecD cy) = cx == cy
eqAssert (Vec2 (x:cx)) (Vec2 (y:cy)) = x == y && eqAssert (Vec2 cx) (Vec2 cy) 
eqAssert (Vec2 []) (Vec2 []) = True
eqAssert   _      _      = False

eqAssertList::(Eq a)=>[a]->[a]->Bool
eqAssertList [] [] = True
eqAssertList (x:cx) (y:cy) = x == y && eqAssertList cx cy

eqAssertList2::(Eq a)=>[[a]]->[[a]]->Bool
eqAssertList2 [] [] = True
eqAssertList2 (x:cx) (y:cy) = x == y && eqAssertList cx cy


eqBool::Bool->Bool->Bool
eqBool a b = a == b

--eqAssert::[[Integer]]->[[Integer]]
--eqAssert [] [] = True
--eqAssert  _ [] = False
--eqAssert  _ _  = True

--main = do 
--        print "Hello World"
--        let v1 = Vec1 [1, 3, 4]
--        let v2 = Vec1 [1, 3, 4]
--        let vs1 = VecS ["a", "b"]
--        let vs2 = VecS ["a", "b"]
--        let vv1 = Vec2 [[1], [2]]
--        let vv2 = Vec2 [[1], [2]]
--        let vv3 = Vec2 [[1], [3]]
--        let vd1 = VecD [0.0]
--        let vd2 = VecD [0.0]
--        let vdd1 = VecD [0.0, 0.1, 0.3]
--        let vdd2 = VecD [0.0, 0.1, 0.3]
--        let vdd3 = VecD [0.0, 0.1, 0.31]
--        print $ eqAssert v1 v2
--        print $ eqAssert vs1 vs2 
--        print $ eqAssert vv1 vv2
--        print $ eqBool (eqAssert vv1 vv3) False
--        print $ eqAssert vd1 vd2
--        print $ eqAssert vd1 vd2
--        print $ eqBool (eqAssert vdd1 vdd3) False

-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- import Turtle
-- echo "turtle"
{-# LANGUAGE DuplicateRecordFields #-} 
-- {-# LANGUAGE OverloadedStrings, DeriveGeneric, DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric, DeriveAnyClass #-}
-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import Data.Char
import qualified Data.Array as DR 
import Data.Maybe(fromJust, fromMaybe)
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
--import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Data.Typeable (typeOf)
import Control.Monad (unless, when, liftM, liftM2, liftM3)
import Control.Monad.IO.Class
import Control.Concurrent 
import Database.Redis
import GHC.Generics

import qualified Text.Regex.TDFA as TD

import qualified Data.List as L
import qualified Data.ByteString.Lazy as BL
import qualified Data.ByteString.Char8 as BS
import qualified Data.ByteString as SB -- strict ByteString 
import qualified Data.ByteString.Lazy.UTF8 as BLU
import qualified Data.ByteString.UTF8 as BSU
import qualified Data.Text as DT
import qualified Data.Text.Lazy as DL 
import qualified Data.Map.Strict as M
import qualified Data.Aeson as DA
import qualified Data.Aeson.Text as AAT  -- encodeToLazyText

import AronModule 

fname = "/Users/cat/myfile/bitbucket/testfile/test.txt"
jname = "/Users/cat/myfile/bitbucket/javalib/Aron.java"
hname = "/Users/cat/myfile/bitbucket/haskelllib/AronModule.hs"

{-| 
    File format:
    jname = "/Users/cat/myfile/bitbucket/javalib/Aron.java"

    Parse Java method name, extract method name and form a list:
        [([String], Integer, [String])]

    The list can be used in Redis Server

    ["line1", "line2"] -> [([k0, k1], 1, ["line1"])]
-} 
redisMap::[String] -> [([String], Integer, [String])]
redisMap [] = []
redisMap cx = rMap
    where
        list = filter(\e -> matchTest (mkRegex "public static") e) cx 
        ls = zipWith(\n x -> (n, x)) [10000..] list
        m = map(\x -> (let may = matchRegex (mkRegex regexJavaMethod ) (snd x)
                       in case may of
                                Just e -> head e 
                                _      -> [] 
                       , fst x, trim $ snd x)
                         
               ) ls   
        lss = map(\x -> (takeWhile(\e -> isLetter e || isDigit e) (t1 x), t2 x, t3 x)) m 
        rMap = map(\x ->let la = splitStrRegex "[[:space:]]" $ t3 x 
                            lb = init $ foldr(\a b -> a ++ " " ++ b) [] $ drop 2 la 
                        in (prefix $ t1 x, t2 x, [lb])) lss
        regexJavaMethod = "([a-zA-Z0-9_]+[[:space:]]*\\([^)]*\\))"

mapAronModule::[String] -> [([String], Integer, [String])]
mapAronModule [] = []
mapAronModule cx = rMap 
    where
       list = filter(\e -> matchTest (makeRegex "(^[a-zA-Z0-9_]+)[[:space:]]*::"::TD.Regex) e) cx 
       lss = map(\e -> (matchAllText (makeRegex "(^[a-zA-Z0-9_]+)[[:space:]]*::"::TD.Regex) $ e, e)) list 
       ln = map(\x -> (DR.elems $ (fst x) !! 0, snd x)) lss
       lns = map(\x -> (fst $ head $ tail $ fst x, snd x)) ln
       rMap = zipWith(\n x -> (prefix $ fst x, n, [snd x])) [3000..] lns

main = do 
       pp "dog"
       haskellBlock <- readFileToList hname 
       let list = filter(\e -> matchTest (makeRegex "(^[a-zA-Z0-9_]+)[[:space:]]*::"::TD.Regex) e) haskellBlock 
       let lss = map(\e -> (matchAllText (makeRegex "(^[a-zA-Z0-9_]+)[[:space:]]*::"::TD.Regex) $ e, e)) list 
       let ln = map(\x -> (DR.elems $ (fst x) !! 0, snd x)) lss
       let lns = map(\x -> (fst $ head $ tail $ fst x, snd x)) ln
       let lnum = zipWith(\n x -> (prefix $ fst x, n, [snd x])) [3000..] lns
       fl
       pp lns
       fl
       pp ln
       fl
       pp lnum 
       pp "done"

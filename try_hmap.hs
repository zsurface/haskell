-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 
import qualified Data.HashMap.Strict as M 

import qualified Text.Regex.TDFA as TD

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}


-- zo - open
-- za - close

import AronModule 

type HMap = M.HashMap String [[String]] 

mapClear::[String] -> HMap -> HMap
mapClear [] m = m 
mapClear (x:cx) m = mapClear cx (M.delete x m)
    
insertAll::[(String, [[String]])] -> HMap -> HMap
insertAll [] m = m 
insertAll (x:cx) m = insertAll cx (M.insert (fst x) (snd x) m)

main = do 
        ref <- newIORef M.empty
        ref1 <- newIORef M.empty
        modifyIORef ref (M.insert "k" [["dog"]])
        modifyIORef ref (M.insert "d" [["cat"]])
        modifyIORef ref (M.insert "d1" [["dog"]])
        m1 <- readIORef ref
        let k = M.lookup "d1" m1
        fw "k"
        pp k
        let lm = [("kk", [["kk1"]]), ("k1", [["v1"]])]
        modifyIORef ref1 (insertAll lm)
        m1 <- readIORef ref
        mf1 <- readIORef ref1
        pp m1
        let list = M.keys m1 
        let m11 = mapClear list m1
        fw "m11"
        pp list
        pp m11
        fw "mf1"
        pp mf1 
        pp "done!"

-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"
{-# LANGUAGE DuplicateRecordFields #-} 
{-# LANGUAGE DeriveGeneric, DeriveAnyClass #-}
-- import Data.Set   -- collide with Data.List 
import Control.Monad
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import Data.Char
import Data.Maybe(fromJust)
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
--import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when, liftM, liftM2, liftM3)
import Control.Monad.IO.Class
import Control.Concurrent 
import Database.Redis

--import Data.Array
--import Text.Regex.TDFA -- the module will conflict with Text.Regex

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}
-- zo - open
-- za - close
import AronModule 
import Data.Text.Lazy (Text)
import qualified Data.Aeson as DA
import Data.Aeson.Text (encodeToLazyText)
--import Data.Aeson (ToJSON, decode, encode)
import GHC.Generics

import qualified Data.ByteString               as B
import qualified Data.ByteString.Internal      as BI
import qualified Data.ByteString.Lazy          as BL
import qualified Data.ByteString.Lazy.Internal as BLI
import qualified Data.Map.Strict as M

import AronModule 



--add::([String], Integer) -> MMap String Integer -> MMap String Integer
--add ([], _)   m = m
--add (s:cs, n) m = case ls of 
--                       Just x -> add (cs, n) $ M.insert s (n:x) m 
--                       _      -> add (cs, n) $ M.insert s [n] m
--    where
--        ls = M.lookup s m -- Maybe a

--geneMap::[String]->M.Map String [[String]]
--geneMap [] = M.empty 
--geneMap cx = addMore keyMap M.empty 
--    where
--        blo = filter(\x -> len x > 0) $ splitBlock cx "^[[:space:]]*(---){1,}[[:space:]]*"
--        pblock = zip blo [0..] 
--        rblock = map(\x ->(snd x, fst x)) $ let bs = "^[[:space:]]*(---){1,}[[:space:]]*" 
--                                                ws = "[,. ]" 
--                                            in parseFileBlock bs ws cx 
--        -- [([k0, k1], ["block1"]), ([k2, k3], ["block2"])]
--        keyMap = zipWith(\x y -> (fst x, fst y)) rblock pblock

--addMore::(Ord e)=>[([e], a)] -> MMap e a-> MMap e a 
--addMore [] m = m
--addMore (s:cs) m = addMore cs $ add s m 

--add::(Ord e)=>([e], a) -> MMap e a -> MMap e a 
--add ([], _)   m = m
--add (s:cs, n) m = case ls of 
--                       Just x -> add (cs, n) $ M.insert s (n:x) m 
--                       _      -> add (cs, n) $ M.insert s [n] m
--    where
--        ls = M.lookup s m -- Maybe a

--add::([String], Integer) -> MMap String Integer -> MMap String Integer
--add ([], _)   m = m
--add (s:cs, n) m = case ls of 
--                       Just x -> add (cs, n) $ M.insert s (n:x) m 
--                       _      -> add (cs, n) $ M.insert s [n] m
--    where
--        ls = M.lookup s m -- Maybe a

-- type MMap = M.Map String [Integer]
type MMap a b = M.Map a [b]

geneMap::[String]->M.Map String [[String]]
geneMap [] = M.empty 
geneMap cx = addMore keyMap M.empty 
    where
        blo = L.filter(\x -> len x > 0) $ splitBlock cx "^[[:space:]]*(---){1,}[[:space:]]*"
        pblock = L.zip blo [0..] 
        rblock = L.map(\x ->(snd x, fst x)) $ let bs = "^[[:space:]]*(---){1,}[[:space:]]*" 
                                                  ws = "[,. ]" 
                                              in parseFileBlock bs ws cx 
        -- [([k0, k1], ["block1"]), ([k2, k3], ["block2"])]
        keyMap = L.zipWith(\x y -> (fst x, fst y)) rblock pblock

        addMore::(Ord e)=>[([e], a)] -> MMap e a-> MMap e a 
        addMore [] m = m
        addMore (s:cs) m = addMore cs $ add s m 

        add::(Ord e)=>([e], a) -> MMap e a -> MMap e a 
        add ([], _)   m = m
        add (s:cs, n) m = case ls of 
                               Just x -> add (cs, n) $ M.insert s (n:x) m 
                               _      -> add (cs, n) $ M.insert s [n] m
            where
                ls = M.lookup s m -- Maybe a

data Person = Person {
      name :: Text
    , age  :: Int
    , list  :: [String] 
    , index:: [Int] 
    } deriving (Generic, Show)
    
data Block = Block{block::[[String]]} deriving (Generic, Show)

instance DA.ToJSON Person where
    -- No need to provide a toJSON implementation.

    -- For efficiency, we write a simple toEncoding implementation, as
    -- the default version uses toJSON.
    toEncoding = DA.genericToEncoding DA.defaultOptions

instance DA.ToJSON Block where
    -- No need to provide a toJSON implementation.

    -- For efficiency, we write a simple toEncoding implementation, as
    -- the default version uses toJSON.
    toEncoding = DA.genericToEncoding DA.defaultOptions

instance DA.FromJSON Person
instance DA.FromJSON Block 

data MyClass = MyClass{name::String}

main = do 
        let c = MyClass{name = "dog"}
        fblock <- readFileToList "/tmp/file.x"
        let gmap = geneMap fblock
        pp $ M.toList gmap

        -- toStrictBS: lazy -> strict
        let mr = L.map(\x -> (fst x::B.ByteString, toStrictBS $ DA.encode $ Block {block = snd x})) $ M.toList gmap
        pw "mr" mr
        let js1 = DA.encode (Person {name = "Joe", age = 12, list = ["dog", "cat"], index=[1, 2, 3]})
        let b1 = DA.encode (Block{block=[["block1", "block2"]]})
        let mp = DA.decode "{\"name\":\"Joe\",\"age\":12, \"list\":[\"dog\", \"cow\"]}"::Maybe Person 
        case mp of
            Nothing -> return ()
            (Just p) -> print p
        conn <- connect defaultConnectInfo
        runRedis conn $ do
              set ("hello"::BI.ByteString) $ toStrictBS js1 
              set ("block"::BI.ByteString) $ toStrictBS b1 
--              mset [("dog", toStrictBS b1)] 
--              mset mr 
--              set ("world"::B.ByteString) "Value world"
--              hello <- get "hello"::B.ByteString
--              -- convert json to record 
--              let n = case hello of
--                           Right x -> DA.decode $ BL.fromChunks $ [fromJust x]::Maybe Person
--                           _    -> DA.decode "{}" :: Maybe Person 
--              world <- get "world"::B.ByteString
--              block <- get "block"::B.ByteString
--              let b = case block of
--                           Right x -> DA.decode $ BL.fromChunks $ [fromJust x]::Maybe Block
--                           _ -> DA.decode "{}" :: Maybe Block
--              liftIO $ print b 
--              liftIO $ fl
--              liftIO $ print n
--              liftIO $ fl
--              liftIO $ print (hello,world)
        pp "done"

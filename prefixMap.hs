{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes #-}
import Control.Monad
import Data.Char
import qualified Data.HashMap.Strict as M 
import qualified Data.Set as DS
import qualified Data.List as DL
import Data.List.Split
import Data.List(groupBy)
--import qualified Data.Map as M 
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 
import AronModule 

--path = "/Users/cat/myfile/bitbucket/snippets/snippet_test.m"
path = "/Users/cat/myfile/bitbucket/snippets/snippet.m"

myPath::M.HashMap k v -> String
myPath _ = "dog"

doGuessing m = do
    putStrLn "Enter your guess:"
    key <- getLine
    clear
    case M.lookup key m of
        Just s -> (mapM_ . mapM_) print s >> doGuessing m
        _      -> print "nothing" 


getInput::IO (M.HashMap String [[String]])
getInput = do
        list <- readFileToList path;
        let ll = filter(\x -> length x > 0) $ splitWhen(\x -> (length $ trim x) == 0) list
        -- fpp "ll" ll 
        let sortList = qqsort(\x y -> trim(head x) > trim(head y)) ll 
        -- fpp "sortList" sortList
        pplist <- readSnippet path
        let keylist = DL.map(\x -> 
                                (foldr(++) [] $ DL.map(\y -> prefix y ) (fst x),
                                 snd x
                                )
                            ) pplist 
        let mymap = map(\cx -> [(x, y) | x <- fst cx, y <- [snd cx]]) keylist
        let lmap = foldr(++) [] mymap
        -- fpp "lmap" lmap
        let sortlist = qqsort(\x y -> (fst x) > (fst y)) lmap
        -- fpp "sortlist" sortlist
        let mmap = M.fromList lmap
        -- fpp "mmap" mmap
        let group = groupBy(\x y -> (fst x) == (fst y)) sortlist
        -- fpp "group" group
        let uzip = map(\x -> unzip x) group
        -- fpp "uzip" uzip
        let kmap = map(\x -> ((head (fst x)), snd x)) uzip
        -- fpp "kmap" kmap 
        let hmap = M.fromList kmap
        -- fpp "hmap" hmap 
        clear
        -- fpp "len" $ length ll
        -- fpp "size" $ M.size hmap 
        return hmap



getInput::String->IO (Maybe [[String]])
getInput key = do
        list <- readFileToList path;
        let ll = filter(\x -> length x > 0) $ splitWhen(\x -> (length $ trim x) == 0) list
        -- fpp "ll" ll 
        let sortList = qqsort(\x y -> trim(head x) > trim(head y)) ll 
        -- fpp "sortList" sortList
        pplist <- readSnippet path
        let keylist = DL.map(\x -> 
                                (foldr(++) [] $ DL.map(\y -> prefix y ) (fst x),
                                 snd x
                                )
                            ) pplist 
        let mymap = map(\cx -> [(x, y) | x <- fst cx, y <- [snd cx]]) keylist
        let lmap = foldr(++) [] mymap
        fpp "lmap" lmap
        let sortlist = qqsort(\x y -> (fst x) > (fst y)) lmap
        fpp "sortlist" sortlist
        let mmap = M.fromList lmap
        fpp "mmap" mmap
        let group = groupBy(\x y -> (fst x) == (fst y)) sortlist
        fpp "group" group
        let uzip = map(\x -> unzip x) group
        fpp "uzip" uzip
        let kmap = map(\x -> ((head (fst x)), snd x)) uzip
        fpp "kmap" kmap 
        let hmap = M.fromList kmap
        fpp "hmap" hmap 
        clear
        fpp "len" $ length ll
        fpp "size" $ M.size hmap 
        case M.lookup key hmap of
            --Just s -> (mapM_ . mapM_) print s
            Just s -> return (Just s) 
            _      -> return Nothing 
        
main = do 
        print "Hello World"
        maybe <- getInput "haskell map" 
        case maybe of
            Just s -> (mapM_ . mapM_) print s
            _      -> print "nothing"
--        clear
--        list <- readFileToList path;
--        let ll = filter(\x -> length x > 0) $ splitWhen(\x -> (length $ trim x) == 0) list
--        -- fpp "ll" ll 
--        let sortList = qqsort(\x y -> trim(head x) > trim(head y)) ll 
--        -- fpp "sortList" sortList
--        pplist <- readSnippet path
--        let keylist = DL.map(\x -> 
--                                (foldr(++) [] $ DL.map(\y -> prefix y ) (fst x),
--                                 snd x
--                                )
--                            ) pplist 
--        let mymap = map(\cx -> [(x, y) | x <- fst cx, y <- [snd cx]]) keylist
--        let lmap = foldr(++) [] mymap
--        fpp "lmap" lmap
--        let sortlist = qqsort(\x y -> (fst x) > (fst y)) lmap
--        fpp "sortlist" sortlist
--        let mmap = M.fromList lmap
--        fpp "mmap" mmap
--        let group = groupBy(\x y -> (fst x) == (fst y)) sortlist
--        fpp "group" group
--        let uzip = map(\x -> unzip x) group
--        fpp "uzip" uzip
--        let kmap = map(\x -> ((head (fst x)), snd x)) uzip
--        fpp "kmap" kmap 
--        let hmap = M.fromList kmap
--        fpp "hmap" hmap 
--        clear
--        fpp "len" $ length ll
--        fpp "size" $ M.size hmap 
--        putStrLn "Please select key to search from the map"
--        doGuessing hmap 
        pp "done"

-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

--import Data.Array
--import Text.Regex.TDFA -- the module will conflict with Text.Regex

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}


-- zo - open
-- za - close

import AronModule 

import qualified Data.ByteString.Lazy      as BL
import qualified Data.ByteString.Lazy.UTF8 as BLU
import qualified Data.ByteString           as BS
import qualified Data.Text.Lazy            as TL
import qualified Data.Text                 as TS 
import qualified Data.ByteString.UTF8      as BSU
import qualified Data.Text.Encoding        as TSE
import qualified Data.Text.Lazy.Encoding   as TLE

-- haskell ByteString, Text, String, Lazy, Strict
-- String <-> ByteString
--BLU.toString   :: BL.ByteString -> String
--BLU.fromString :: String -> BL.ByteString
--BSU.toString   :: BS.ByteString -> String
--BSU.fromString :: String -> BS.ByteString

-- String <-> Text
--TL.unpack :: TL.Text -> String
--TL.pack   :: String -> TL.Text
--TS.unpack :: TS.Text -> String
--TS.pack   :: String -> TS.Text

-- ByteString <-> Text

--TLE.encodeUtf8 :: TL.Text -> BL.ByteString
--TLE.decodeUtf8 :: BL.ByteString -> TL.Text
--TSE.encodeUtf8 :: TS.Text -> BS.ByteString
--TSE.decodeUtf8 :: BS.ByteString -> TS.Text

-- Lazy <-> Strict

--BL.fromStrict :: BS.ByteString -> BL.ByteString
--BL.toStrict   :: BL.ByteString -> BS.ByteString
--TL.fromStrict :: TS.Text -> TL.Text
--TL.toStrict   :: TL.Text -> TS.Text

main = do 
        print "Hello World"
        -- String to Lazy ByteString 
        let stol = BLU.fromString "String to Lazy ByteString"
        pw "stol" stol
        -- Lazy ByteString to String
        let ltos = BLU.toString stol 
        pw "ltos" ltos
        -- String to Strict ByteString 
        let stos = BSU.fromString "String to Strict ByteString"
        pw "stos" stos
        let btos = BSU.toString stos
        pw "Strict ByteString to String" btos
        -- String to Lazy Text
        let st2ltext = TL.pack "String to Lazy Text"
        pw "String to Lazy Text" st2ltext
        pp "done!"

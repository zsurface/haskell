-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

--import Data.Array
--import Text.Regex.TDFA -- the module will conflict with Text.Regex

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}


-- zo - open
-- za - close

import AronModule 

alphabet::Char -> Bool
alphabet c = isLetter c || isDigit c

--par::String -> [(Char, String)]
--par [] = []
--par (c:cs) =  

left = '"' 
right = '"'

eat::String -> String
eat [] = []
eat (c:cs) | c /= '"' = eat cs  
           | otherwise = "\""

newtype Parser a = Parser {parse::String -> [(a, String)]} 

char::Char -> Parser Char
char c = satisfy (c ==)

item::Parser Char
item = Parser $ \s ->
    case s of
        [] -> []
        (c:cs) -> [(c, cs)]

instance Monad Parser where
    return = unit
    (>>=) = bind

instance Applicative Parser where
    pure = return 
    (Parser cs1) <*> (Parser cs2) = Parser (\s -> [(f a, s2) | (f, s1) <- cs1 s, (a, s2) <- cs2 s1])

instance Functor Parser where
    fmap f (Parser cs) = Parser (\s -> [(f a, b) | (a, b) <- cs s])

bind::Parser a -> (a -> Parser b) -> Parser b
bind p f = Parser $ \s -> concatMap(\(a, s') -> parse (f a) s') $ parse p s


unit :: a -> Parser a
unit a = Parser (\s -> [(a, s)])

satisfy::(Char -> Bool) -> Parser Char
satisfy p = item `bind` \c ->
    if p c
    then unit c 
    else (Parser (\cs -> []))

string::String -> Parser String
string [] = return []  
string (c:cs) = do { char c; string cs; return (c:cs)}


--data MyMaybe a = MyNothing | MyJust a
--
--f::MyMaybe (MyMaybe a) -> a
--f (MyJust (MyJust x)) = x 
--f (MyJust MyNothing) = MyNothing 



main = do 
        print "Hello World"
        let a = MyJust Nothing
        pp "done!"

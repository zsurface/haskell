import Test.QuickCheck
import AronModule

prop_RevRev :: Eq a => [a] -> Bool
prop_RevRev xs = reverse (reverse xs) == xs

prop_RevApp :: [Int] -> [Int] -> Bool
prop_RevApp xs ys = reverse (xs ++ ys) == reverse ys ++ reverse xs

prop_revapp :: [Int] -> [Int] -> Bool
prop_revapp xs ys = reverse (xs++ys) == reverse xs ++ reverse ys

prop_f::Int->Int
prop_f x = x + 1


-- elem x ['a'..'e']
-- x `elem` ['a'..'e']
-- \x -> x `elem` ['a'..'e']
-- \x -> elem x ['a'..'e']
-- a + 1 = + a 1 
-- a + 1 = \a -> a + 1 = f a = a + 1 = f = \a -> a + 1 = (+1)

take5::[Char]->[Char]
--take5 s = take 5 $ filter (\x -> x `elem` ['a'..'e']) s   
--take5 s = take 5 $ filter (\x -> elem x ['a'..'e']) s   
take5 s = take 5 $ filter (`elem` ['a'..'e']) s   

main = quickCheck prop_RevApp
--main = do 
--       fl
--       let s = take5 ['e', 'e', 'a', 'z']
--       pp s

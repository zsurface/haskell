\usepackage{fancyvrb}
\DefineVerbatimEnvironment{code}{Verbatim}{fontsize=\small}
\DefineVerbatimEnvironment{example}{Verbatim}{fontsize=\small}
\newcommand{\ignore}[1]{}

* First Haskell Literal Programming

This text is a literate comment.
This is an example program that prints the 11th Fibonacci number:

>main = print (fibs !! 10)
>  where fibs = 0 : 1 : [ x + y | (x,y) <- zip fibs (tail fibs) ]

This is also a comment.  When we use this style of literate programming,
we do not need to separate the text and the code blank lines.

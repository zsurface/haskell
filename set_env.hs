#!/usr/bin/env stack
-- stack --resolver lts-10.2 script
{-# LANGUAGE OverloadedStrings #-}
import System.Process.Typed

main :: IO ()
main = do
    putStrLn "1:"
    runProcess_ "pwd"
    putStrLn "\n2:"
    runProcess_ $ setWorkingDir "/tmp" "pwd"

    putStrLn "\n3:"
    runProcess_ "env"
    putStrLn "\n4:"
    runProcess_ $ setEnv [("HELLO", "WORLD")] "env"

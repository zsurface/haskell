-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}  --
-- 
-- import Turtle                       --
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".

import AronModule 

-- Java Node
-- class Node<T>{
--     T data
--     Node left;
--     Node right;
-- }

data XTree a = Empty | Node a (XTree a) (XTree a)

-- Expr::* -> *
-- Expr::* -> * => a -> Expr a 
data Expr a = Con a | Add (Expr a) (Expr a)

eval::(Expr Integer) -> Integer
eval (Con n) = n
eval (Add l r) = (eval l) + (eval r)

eval'::(Expr String) -> Integer
eval' (Con s) = stringToInt s 
eval' (Add l r) = (eval' l) + (eval' r)

main = do 
        print "Hello World"
        argList <- getArgs
        pp $ length argList
        pp "done!"
        pp $ eval (Con 3) == 3
        pp $ eval (Add (Con 3) (Con 4)) == 7
        pp $ eval' (Con "3") == 3
        pp $ eval' (Add (Con "3") (Con "4")) == 7

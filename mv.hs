import Control.Monad
import Data.Char
import Data.List
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import AronModule 

-- type constructor
-- data constructor
-- Nothing is data constructor with zero parameter
-- Just is data constructor with one parameter
data MyMaybe a = CNothing | CJust a 

instance Functor MyMaybe where 
    fmap f CNothing = CNothing
    fmap f (CJust a) = CJust (f a)

fac::Int->Int
fac 0 = 1 
fac 1 = 1
fac n = n*fac(n-1) 

main = do 
        print "Hello World"
        argList <- getArgs
        pp $ length argList
        pp "done!"
        --mv "try44.txt" "try44444.txt"
        pp "cool"
        fmap id (CJust 3) 
        pp "dog"


>-- {{{ begin_fold
>-- script
>-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
>-- {-# LANGUAGE OverloadedStrings #-}
>-- {-# LANGUAGE DuplicateRecordFields #-} 
>-- import Turtle
>-- echo "turtle"

>{-# LANGUAGE GADTs #-}


>module Main where

>-- import Data.Set   -- collide with Data.List 
>import Control.Monad
>import Data.Char
>import qualified Data.List as L
>import Data.List.Split
>import Data.Time
>import Data.Time.Clock.POSIX
>import System.Directory
>import System.Environment
>import System.Exit
>import System.FilePath.Posix
>import System.IO
>import System.Posix.Files
>import System.Posix.Unistd
>import System.Process
>import Text.Read
>import Text.Regex
>import Text.Regex.Base
>import Text.Regex.Base.RegexLike
>import Text.Regex.Posix
>import Data.IORef 
>import Control.Monad (unless, when)
>import Control.Concurrent 

>--import Data.Array
>--import Text.Regex.TDFA -- the module will conflict with Text.Regex

>-- import Graphics.Rendering.OpenGL as GL 
>-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
>-- import qualified Graphics.UI.GLFW as G
>-- import Data.Set(Set) 
>-- import qualified Data.Set as S 

>--if (length argList) == 2 
>--then case head argList of 
>--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
>--            where 
>--                cmd = "pwd" 
>--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
>--            where 
>--                cmd = "pwd" 
>--    _     -> print "more arg" 
>--else print "Need more arguments" 

>--    takeFileName gives "file.ext"
>--    takeDirectory gives "/directory"
>--    takeExtension gives ".ext"
>--    dropExtension gives "/directory/file"
>--    takeBaseName gives "file"
>--    "/directory" </> "file.ext".
>--    "/directory/file" <.> "ext".
>--    "/directory/file.txt" -<.> "ext".
>-- |  end_fold ,}}}



>-- zo - open
>-- za - close

>import AronModule 


Java code:
interface Person<T>{
    public Person<T> getAge1();
}

class Age implement Person<T>{
    public Int getAge1(){
    }
}

class Name implement Person<T>{
    public String getName(){
    }
}


bad type:
Age a is not subtype  of a Person
Name a is not subtype of a Person

>data Person a = Age a | Name a 

>getAge1::Person Int -> Int 
>getAge1 (Age a) =  a

>getName1::Person String -> String
>getName1 (Name s) = s

>addAge::Person Int -> Person Int -> Int 
>addAge (Age a) (Age b) = a + b 
 
>data MyPerson a where
>    MyAge :: Int -> MyPerson a
>    MyName :: String -> MyPerson a
>    MyAddAge :: MyPerson Int -> MyPerson Int -> MyPerson Int 
>    MyConcat :: MyPerson String -> MyPerson String -> MyPerson String

>mygetAge::MyPerson Int -> Int
>mygetAge (MyAge a) = a 


>data Employee a where
>    EmpSalary :: Int -> MyPerson a -> Employee a


>main = do 
>        let age = Age 3
>        let name = Name "Jenny and modifiers"
>        pp $ getAge1 age
>        pp $ getName1 name 
>        pp $ addAge age age
>        print "Hello World"
>        pp "done!"

-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}  --
-- 
-- import Turtle                       --
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".

import AronModule 

{-
    facebook interview question

    a b c
    a p(bc)
        b p(c)
        c p(b)
    b p(ac)
       a p(c)
       c p(a) 
    c p(ab)
       a p(b)
       b p(a)
-}
--permRangeNum::[Integer] -> [[Integer]]
--permRangeNum [] = [[]]
--permRangeNum cs = map(\x -> (cs !! x):permRangeNum (removeIndex x cs)) [0..len cs]

-- a b c
-- a p(bc)
--     b p(c)
--       c p()
-- b p(ac)
-- c p(ab)
--
perm5::[Integer]-> [[Integer]]
perm5 [] = [[]]
perm5 cx = [ (fst pre):p | pre <- tup, p <- perm5 (drop ((snd pre) + 1) cx)]
    where
        tup = zip cx [0..]

main = do 
        print "Hello World"
        argList <- getArgs
        pp $ length argList
        pp "done!"
        let k = 2
        pp $ perm5 [1, 2, 3]








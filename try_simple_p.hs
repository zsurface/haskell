-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"
{-# LANGUAGE MultiParamTypeClasses #-}
-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

--import Data.Array
--import Text.Regex.TDFA -- the module will conflict with Text.Regex

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}


-- zo - open
-- za - close

import AronModule 

-- In Java, Function<T1, Maybe T2> f = a -> Maybe b
type Dict a b = a -> Maybe b 

--class MyDict a b where
--    getValue2:: a -> Maybe b

insertDict::(Eq k)=> k -> v -> Dict k v -> Dict k v
insertDict k v dict = \k' -> if k == k' then Just v else dict k' 

emptyDict::Dict k v
emptyDict = \_ -> Nothing 

type Parser a = String -> (a, String)

several :: Parser a -> String -> [a]
several p "" = []
several p str = let (a, str') = p str
                    as = several p str'
                in a:as

num :: Parser Int
num str = 
    let (digs, str') = span isDigit str  -- span::(a -> Bool) -> [a] -> ([a], [a])
        (_, str'')   = span isSpace str'
    in (read digs, str'')

isSym::Char -> Bool
isSym c = '<' == c || '>' == c

punctuation::Parser Char
punctuation [] = ([], [])
punctuation str = let (pu, str') = span isSym str
                      (_, str'') = span isSpace str'
                  in  (head pu, str'')

main = do 
        print $ several num "13 3 4"
        print $ several punctuation " >"
        pp $ num "12 3 4"
        let d = emptyDict
        let dii = insertDict 1 "b" $ insertDict 3 "k" emptyDict 
        print $ dii 1 
        print $ dii 3 
        print $ sentence "dog cat 133"
        pp "done"

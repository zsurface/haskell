-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}  --
-- 
-- import Turtle                       --
-- echo "turtle"
-- {-# LANGUAGE FlexibleInstances #-}

  
-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import Graphics.Rendering.OpenGL as GL 
import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
import qualified Graphics.UI.GLFW as G
import Data.Set(Set) 
import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".

import AronModule 

{-|
    === Gram Schmidt Process 
    Wed Jan  9 09:32:48 2019  
    gx <<http://localhost/pdf/gram_schmidt.pdf Gram Schmidt>>
-}

fun::Double -> GLfloat 
fun n = realToFrac n

fun'::GLfloat -> Double
fun' n = realToFrac n

matt = [
      [1, 2, 3],
      [4, 5, 6],
      [7, 8, 10]
      ]

v1 =[[1], 
     [4], 
     [7]]
v2 =[[2], 
     [5], 
     [8]]
v3 =[[3], 
     [6], 
     [10]]

(++:) v1 v2 = tran $ v1' ++ v2' 
        where
            v1' = tran v1 
            v2' = tran v2 

--f::[[a]] -> [[a]]
--f []     = []
--f cx = g (take 1 cx) (drop 1 cx) 
--    where
--        g l vx = [(v - (foldr(+) idv [(projn [v] [a]) | a <- l ])):l | v <- vx]
--            where
--                idv = [[0, 0, 0]]

--instance Num a => Num [a] where 
--    (+) = zipWith (+); 
--    (-) = zipWith (-); 
--    (*) = zipWith (*); 
--    abs = map abs; 
--    signum = map signum; 
--    fromInteger = repeat . fromInteger
-- v3 - foldl(\x acc -> projn x acc) z [a1, a2, a3]
a1 = v1  
a2 = v2 - (projn v2 a1)
a3 = v3 - ((projn v3 a1) + (projn v3 a2)) 
--an v al = v -: map(\a -> projn v a) al


e1 = nl a1 
e2 = nl a2 
e3 = nl a3

sqsum x = sum $ join $ zipWith2 (*) x x

nl = normList
nd = listDots

-------------------------------------------------------------------------------- 
an = [
      v1 
     ]
vv= [ 
      v2,
      v3
    ]
f an (vv:cx) = f ([(v - (foldr(\x acc -> x + acc) z $ join $ [map(\a -> projn v a) an ])) | v <- [vv]] ++ an)  cx
        where
            z = [[0],
                 [0],
                 [0]]
            v1= [[2],
                 [3],
                 [4]]


-- [e1, e2, e3]
fe::[[[Double]]] -> [[[Double]]]
fe an = an


--pp $ "<e1 v1> <e1 v2> <e1 v3>"
--pp $ "        <e2 v2> <e2 v3>"
--pp $ "                <e3 v3>"

-- [e1, e2, e3]
-- [v1, v2, v3]
ex = reverse $ map nl $ rejection an vv 
vx = [v1, v2, v3]

--isInver::(Fractional a, Ord a)=> [[a]] -> Bool 
--isInver m = if len (filter(< 0.0001) cx) > 0 then False else True
--    where
--        -- compute all the dot products of [a1, a2, ..] => [a1*a1, a2*a2, ...]
--        -- if any dot product of (a_k, a_k) is zero or approx zero then the matrix is singular
--        cx = map(\col -> listDots col col) alist
--        -- [a1, a2, ..]
--        alist = rejection vFir vTai
--        -- first column
--        vFir = [getVec 0 m]
--        -- rest of columns
--        vTai = f m     
--        -- function to extract all columns from matrix: 
--        -- m = [[a]] => [a1, a2..] = [[[a]], [[a]], ..]
--
--        f ma = map(\n -> getVec n ma) [1..(len ma - 1)]

-- qm ex vx = (zipWith . zipWith) (\e v -> (e, v)) ex' vx'


qrDecompose::[[[Double]]]->[[[Double]]]->([[Double]], [[Double]])
qrDecompose ex vx = (qm, rm)
    where
        tm = rMatrixUpperTri ex vx
        rm = zipWith(\n  x -> (replicate n 0) ++ x) [0..] tm 
        qm = tran $ map (\x -> foldr(++) [] x) ex

--v1 =[[1], 
--     [4], 
--     [7]]
--v2 =[[2], 
--     [5], 
--     [8]]
--v3 =[[3], 
--     [6], 
--     [10]]
-- vx = [v1, v2, v3]
-- vx =[[1], [4], [7]], [[2], [5], [8]], [[3], [6], [10]]]
-- 

-- [a1, a2, a3]

rf = realToFrac

toFm::IO[[Integer]]->IO[[Double]]
toFm m = m >>= \x -> return $ (map . map) rf x 


main = do
--        pp $ "a1=" <<< a1
--        pp $ "a2=" <<< a2
--        pp $ "a3=" <<< a3
        pp $ "e1=" <<< e1 
        pp $ "e2=" <<< e2 
        pp $ "e3=" <<< e3 
--        pp $ "|e1|=" <<< sqsum e1
--        pp $ "|e2|=" <<< sqsum e2
--        pp $ "|e3|=" <<< sqsum e3
        pp $ "<e1 e2> = "<<< nd e1 e2
        pp $ "<e1 e3> = "<<< nd e1 e3
        pp $ "<e2 e3> = "<<< nd e2 e3
        fl

        let ev11 = listDots e1 v1
        pp $ "<e1 v1> = "<<< ev11
        let ev12 = listDots e1 v2
        pp $ "<e1 v2> = "<<< ev12
        let ev13 = listDots e1 v3
        pp $ "<e1 v3> = "<<< ev13
        let ev22 = listDots e2 v2
        pp $ "<e2 v2> = "<<< ev22
        let ev23 = listDots e2 v3
        pp $ "<e2 v3> = "<<< ev23
        let ev33 = listDots e3 v3
        pp $ "<e3 v3> = "<<< ev33
        pp $ "<e1 v1> <e1 v2> <e1 v3>"
        pp $ "        <e2 v2> <e2 v3>"
        pp $ "                <e3 v3>"
        fl
        pp $ "" <<< ev11 <<< " " <<< ev12 <<< " " <<< ev13 
        pp $ "" <<< " "  <<< " " <<< ev22 <<< " " <<< ev23 
        pp $ "" <<< " "  <<< " " <<< " "  <<< " " <<< ev33 
        fl
        pp $ " ex="<<< (ppad ex) 
        pp $ " vx="<<< (ppad vx) 
        let v1' = [[ev11], 
                   [0],
                   [0]]
        let v2' = [[ev12], 
                   [ev22], 
                   [0]]
        let v3' = [[ev13], 
                   [ev23], 
                   [ev33]]
        let qqm = tran ex
        pp "QQm= "
        (pa . ppad) $ qqm 
        let rr = v1' ++: v2' ++: v3'
        pp $"f an vv="
        let ls = rejection an vv 
        (pa . ppad) ls 
        pp "rMatrixUpperTri ex vx"
        fl
        let ml = rMatrixUpperTri ex vx
        let rrm = zipWith(\n  x -> (replicate n 0) ++ x) [0..] ml 
        pp "RRm = "
        (pa . ppad) rrm 
        pa ml
        writeToFile "/tmp/m.x" $ map  show ml 
        fl
        pp "R matrix="
        (pa .ppad) rrm 

        let d1 = listDots a1 a2
        let d2 = listDots a1 a3
        let d3 = listDots a2 a3
        pp $ "d1=" <<< d1
        pp $ "d2=" <<< d2
        pp $ "d3=" <<< d3 
        fl
        let qq = e1 ++: e2 ++: e3 
        pp "Q="
        pa qq  
        fl
        pp "R="
        pa rr  
        pp "matt="
        pa matt
        fl
        let m = multiMat qq rr
        pp $ "qq x rr=" 
        (pa . ppad) m
        fl
        pp "(qqqm, rrrm) = qrDecompose ex vx"
        let (qqqm, rrrm) = qrDecompose ex vx
        pp $ "qqqm=" <<< qqqm
        fl
        pp $ "rrrm=" <<< rrrm
        fl
        (pa . ppad) $ multiMat qqqm rrrm
        -- let q = (map . map) (\x -> foldr(++) [] x) qqqm
        fl
        let (q', r') = qrDecompose' matt
        pp "q'=" 
        (pa . ppad) q'
        fl
        pp "r'=" 
        (pa . ppad) r'
        fl
        (pa . ppad) $ multiMat q' r' 
        bigm <- toFm $ randomMatrix  4 4 
        fl
        (pa . ppad) bigm
        fl
        let (q1, r1) = qrDecompose' bigm
        (pa . ppad) $ multiMat q1 r1 
        fl
--        pp $ "map(\\x -> foldr(++) [] x) qqqm=" <<< q 
--        pp $ "rrrm=" <<< rrrm
--        (pa . ppad) $ multiMat (join qqqm) rrrm
--        pp $ "det m =" <<< det m
--        pp $ "det rr=" <<< det rr
--        pp $ "det qq=" <<< det qq 
--        writeToFile "/tmp/m.x" $ map show bm
--        let tri = upperTri bm
--        fl
--        pa $ tri
--        tm1 <- randomMatrix 13 13 
--        let tm1' = upperTri tm1  
--        pa $ tm1
--        fl
--        pa $ tm1'
--        fl
--        let toInt = (map . map) realToFrac
--        let tmf = toInt tm1
--        let inv = fst $ inverse $ tmf 
--        writeToFile2dMat "/tmp/m2.x" tmf
--        writeToFile2dMat "/tmp/m1.x" inv 
--        fl
--        (pa . ppad) inv
--        fl
--        (pa . ppad) [[0.3, 0.444, 333], [444, 3, 9], [3, 1, 3]] 
--        pp "dog"

{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes #-}
import Control.Monad
import Data.Char
import qualified Data.Set as DS
import qualified Data.List as DL
import Data.List.Split
import qualified Data.Map as M 
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

import AronModule 

--path = "/Users/cat/myfile/bitbucket/snippets/snippet_test.m"
path = "/Users/cat/myfile/bitbucket/snippets/snippet.m"

remove'::String->Int->String
remove' s n = (take n s) ++ (drop (n + 1) s)


predicateRegex::String->String->Bool
predicateRegex s r = f 
                where 
                    f = matchTest(mkRegex r) s

main = do 
        print "Hello World"
        list <- readFileToList path;
        fl
        let ll = filter(\x -> length x > 0) $ splitWhen(\x -> (length $ trim x) == 0) list
        pa ll
        fl
        let plist = map(\x -> ((partitionStrRegex "[,:]" $ head x), x) ) ll
        let first =  map(\x -> fst x) plist
        let second = map(\x -> snd x) plist
        let pplist = map(\k -> (
                                   unique $ foldr(++) [] (map(\x -> map(\r -> trim r) $ partitionStrRegex "[,:]" x) (fst k)), 
                                   snd k
                               ) 
                        ) plist
        fl
        --pp plist
        fl
        fl
        --pp pplist
        fl

        let keylist = DL.map(\x -> 
                                (foldr(++) [] $ DL.map(\y -> prefix y ) (fst x),
                                 snd x
                                )
                            ) pplist 
        fl
        --pp keylist 
        let mymap = map(\cx -> [(x, y) | x <- fst cx, y <- [snd cx]]) keylist
        let lmap = foldr(++) [] mymap
        let mmap = M.fromList lmap
        fl
        --pp lmap
        fl
        --pp mmap
        print $ M.lookup "a" mmap
        print $ M.size mmap
        fl
        --pp "done!"

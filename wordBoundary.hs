import Text.Regex

main = do
    let r = mkRegex "\\<dog\\>"
    print $ subRegex r "mydog dog" "\\(\\0\\)"

import Data.Char 
import System.IO
import System.Environment
import Text.Regex.Posix
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import System.CPUTime 
import AronModule


main = do 
        print "Hello World"
        let s = [1..200]::[Int]
        let pythagoras = [(x, y, z) | x <- s, y <- s, z <- s, z^2 == x^2 + y^2] 
        print $ length pythagoras

        t <- getCPUTime
        let sec = (fromIntegral t) * 10^^(-12)
        print "done!"
        print sec


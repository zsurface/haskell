#!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
{-# LANGUAGE QuasiQuotes       #-}
-- {{{ begin_fold
-- script
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 
-- {-# LANGUAGE QuasiQuotes       #-}
import Text.RawString.QQ (r)         -- Need QuasiQuotes too 

--import Data.Array
--import Text.Regex.TDFA -- the module will conflict with Text.Regex

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

import AronModule;
import Network.HTTP

htmlBody::String -> String
htmlBody s  = [r|
            <HTML>   
            <HEAD>   
            <meta charset="utf-8">
            <TITLE>Search Code Snippet</TITLE> 
            <LINK rel="stylesheet" type="text/css" href="mystyle.css?rnd=132">
            <LINK rel="stylesheet" type="text/css" href="modifycolor.css?rnd=132"> 
            <script src="aronlib.js" defer></script>
            </HEAD>
            <BODY>  |] <> s <> [r| <div id="searchdata">no data</div> </BODY></HTML> |]
            -- 'searchdata' is used in aronlib.js function delayFun(value)


                  
type HTMLStyle = [String]
type GlobalAttr = String
attrStyle::HTMLStyle -> GlobalAttr
attrStyle cs = " style='" ++ str ++ "' "
  where
    str = concatStr cs []


--    style=     'color:red; background:green'
-- <div id=   'myid'> </div>
-- <div class='myclass'> </div>
-- [(String, [(String, String)])]
data HtmlType a = HtmlType [(String, String)]
                  | HtmlClass String
                  | HtmlId String
                  | Cat (HtmlType String) (HtmlType String)


style_::[(String, String)] -> String
style_ cs = left ++ (concatStr (map concatStyleTuple cs) []) ++ right 
  where
    left = "style='"
    right = "'" 


-- id="myid"
attrId::HTMLStyle -> GlobalAttr
attrId cs = " id='" ++ (concatStr cs []) ++ "' "

attrClass::HTMLStyle -> GlobalAttr
attrClass cs = " class='" ++ (concatStr cs []) ++ "' "

attr::HTMLStyle -> GlobalAttr
attr cs = concatStr cs " "
               
{-|                                                            
   Html textarea                                               
   textArea row col string                                     
   textArea 4 5 "dog"                                          
-}
textAreaNew::GlobalAttr -> String-> String                
textAreaNew style s = createTag name attr s
    where                                                      
       name = "textarea"
       attr = concatStr [style] " "

table::GlobalAttr -> String -> String
table style s = createTag name attr s
    where
      name = "table"
      attr = concatStr [style] " "

td::GlobalAttr -> String -> String
td style s = createTag name attr s
  where
    name = "td"
    attr = concatStr [style] " "

tr::GlobalAttr -> String -> String
tr style s = createTag name attr s
  where
    name = "tr"
    attr = concatStr [style] " "

pre_::GlobalAttr -> String -> String
pre_ style s = createTag name attr s
  where
    name = "pre"
    attr = concatStr [style] " "

div_::GlobalAttr -> String -> String
div_ style s = createTag name attr s
  where
    name = "div"
    attr = concatStr [style] " "

label_::GlobalAttr -> String -> String
label_ style s = createTag name attr s
  where
    name = "label"
    attr = concatStr [style] " "
input_::GlobalAttr -> String -> String
input_ style s = createTag tag attr s
  where
    tag = "input"
    attr = concatStr [style] " "

htmlbr = "<br>"

createTag::String -> String -> String -> String
createTag name attr content = open ++ content ++ close
              where
                open = "<" ++ name ++ " " ++ attr ++ ">"
                close = "</" ++ name ++ ">\n"


ccat ls s = concatStr ls s


main = do
       let style = ["background:green;", "color:blue;"]
       let myid =  ["myid"]
       let myclass = ["myclass"]
       let ls = ["d1", "d2", "d3"]
       
       let lss = [["a1", "a2", "a3"],["b1", "b2", "b3"]]
       let row ls = (ccat (map (\x -> td "" x) ls) "")
       let rowCol lss = ccat (map (\r-> tr "" (row r)) lss) ""
       let style1 = ["color:green; background:black"]
       let tab = table (attrStyle style1) (rowCol lss)
       let str = htmlBody
                     $ textAreaNew (attrStyle style) "this is textarea 1."
                     ++ htmlbr
                     ++ textAreaNew (attrId myid) "this is textarea 2."
                     ++ htmlbr
                     ++ textAreaNew (attrClass myclass) "this is textarea 3."
                     ++ htmlbr
                     ++ table (attrStyle style) "my table"
                     ++ td (attrStyle style) "my td"
                     ++ tr (attrStyle style) "my tr"
                     ++ tr (attrStyle style)  (td (attrStyle []) "row 1")
                     ++ tr (attrStyle style)  (row ls)
                     ++ ccat  (map(\r -> tr (attrStyle style) (row r)) lss) ""
                     ++ tab
                     ++ htmlbr
                     ++ pre_ (attrStyle style) "This is my code"
                     ++ htmlbr
                     ++ div_ (attrStyle style) (pre_ "" "my code")
                     ++ htmlbr
                     ++ label_ (attrStyle style ) "My Label"
                     ++ htmlbr
                     ++ input_ (attr ["type='text'"]) "my input"
                     ++ htmlbr
                     ++ div_ (attrStyle ["background:cyan;"]) (input_ (attr ["name='First Name'", "type='text'", "value='My Value'"]) "my input")
                     ++ htmlbr
                     ++ style_ [("color", "#333333"), ("background", "green")]
                     ++ htmlbr
       pre str
       writeFile "/tmp/try.html" str
       -- sys "open /tmp/try.html"
       pp "done"

       

import System.IO
import System.Environment
import Text.Regex.Posix
import Text.Regex
import AronModule 

main = do
    handle <- openFile "/Users/longshu/try/coral.x" ReadMode
    contents <- hGetContents handle
    let line = lines contents
    let list = map(\x -> splitRegex (mkRegex "[[:space:]]+") x) line
    let dot = map(\x -> reverse x) list 
    
    let fdot = filter(\x -> length x == 3) dot
    fw "fdot"
    print fdot
    let fdot1 = map(\x -> head x ++ "->" ++ last x) fdot
    fw "fdot1"
    pa1 fdot1
    

import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.Set
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

import AronModule 
pdfdir =  "/Users/cat/myfile/bitbucket/math"
lfile= "/Users/cat/myfile/bitbucket/math/haskell_gene.tex"
pdffile = (dropExtension lfile) ++ ".pdf"
headstr = [
        "\\documentclass[UTF8]{article}",
        "\\usepackage{pagecolor,lipsum}",
        "\\usepackage{amsmath}",
        "\\usepackage{amsfonts}",
        "\\usepackage{amssymb}",
        "\\usepackage{amsthm}",
        "\\usepackage{centernot}",
        "\\usepackage{tikz}",
        "\\usepackage{xcolor}",
        "\\usepackage{fullpage}",
        "\\usepackage[inline]{asymptote}",
        "\\usepackage{listings}",
        "\\usepackage{mathtools}",
        "\\usetikzlibrary{arrows,decorations.pathmorphing,backgrounds,positioning,fit,petri}",
        "\\newtheorem{theorem}{Theorem}",
        "\\newtheorem{defintion}{Definition}",
        "\\newtheorem{collorary}{Collorary}",
        "\\newtheorem{example}{Example}",
        "\\newtheorem{remark}{Remark}",
        "\\newtheorem{note}{Note}" ,
        "\\input{aronlib}"
       ]
begin_doc = ["\\begin{document}"]
content = ["hello world"]
matrix = [
            "\\[",
                "\\left| \\begin{array}{ccc}",
                "a & b & c \\\\",
                "d & e & f \\\\",
                "g & h & i \\end{array} \\right|",
            "\\]"
         ]

end_doc = ["\\end{document}"]

main = do 
        print "Hello World"
        writeToFile lfile headstr 
        writeToFileAppend lfile begin_doc 
        writeToFileAppend lfile content 
        writeToFileAppend lfile matrix 
        writeToFileAppend lfile end_doc 
        cd pdfdir 
        run ("pdflatex " ++ lfile)
        run ("open " ++ pdffile) 
        pp "done!"

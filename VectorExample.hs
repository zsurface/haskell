-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}  --
-- 
-- import Turtle                       --
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".

import qualified Data.Vector as V 
import AronModule 


main = do 
        print "Hello World"
        argList <- getArgs
        pp $ length argList
        pp "done!"
        let v = V.fromList [1..10]
        pp $ "v=" <<< v
        let vf = V.filter odd v
        pp $ "vf=" <<< vf
        let ff = V.map(\x -> x+1) v
        pp $ "ff=" <<< ff
        let h = V.head v 
        pp $ "head=" <<< h
        let t = V.last v 
        pp $ "last=" <<< t 
        let vi = V.init v 
        pp $ "init=" <<< vi 
        let t1 = V.take 3 v
        pp $ "t1=" <<< t1 
        let d1 = V.drop 3 v
        pp $ "d1=" <<< d1 
        let vz = V.zipWith(\x y -> x + y) v v
        pp $ "vz=" <<< vz
        let n = 100
        -- let qq = sqVec $ V.fromList [1..n] 
        -- let qq = sqVec $ V.enumFromN 1 n 
        let qq = quickSortAny [1..n] 
        pp "done"
        pp $ "qq=" <<< qq


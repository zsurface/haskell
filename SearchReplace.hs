import AronModule
import Control.Monad
import Data.Char 
import System.Directory
import System.Environment
import System.IO
import Text.Regex.Base
import Text.Regex.Base.RegexLike -- matchAllText
import Control.Monad
import System.Directory
import System.FilePath
import System.Posix.Files

import Data.Array
import qualified Text.Regex.TDFA as TD
import qualified Text.Regex as TR 


searchReplace'::String -> String -> String -> String
searchReplace' s word rep = ls 
            where
                ss = searchSplit s word 
                second = t3 $ last ss
                ls = foldr(\x y -> x ++ y) [] $ (map(\x -> (t1 x) ++ (TR.subRegex (TR.mkRegex word) (t2 x) rep)) $ ss) ++ [second]

                t1 (x, y, z) = x
                t2 (x, y, z) = y 
                t3 (x, y, z) = z 

                -- input Str -> word to be replaced -> return
                searchSplit::String -> String -> [(String, String, String)]
                searchSplit s word = map(\x -> let t1 = fst x; -- 
                                                   t2 = snd x;
                                                   c  = (fst t1) + (snd t1) -- 0
                                                   fs = fst (snd x) -- 6 
                                                   sn = snd (snd x) -- 3
                                                   re = drop c s 
                                                   rs = take (fs - c) $ drop c s -- "mydog "
                                                   rx = drop (fs - c) s -- "dog-- dog pig cat"
                                                   in (rs, take sn $ drop fs s, drop (fs + sn) s)) tp 

                                            where 
                                                r  = makeRegex ("\\<" ++ word ++ "\\>")::TD.Regex
                                                ar = matchAllText r s 
                                                tt = (0, 0):(map(\x -> snd $ x ! 0 ) ar) -- [(0, 0), (6, 3), (12, 3)]
                                                tp = zipWith(\x y -> (x, y)) (init tt) (tail tt) -- [((0, 0), (6, 3)), ((6, 3), (12, 3))]


main = do 
        let s = "mydog dog-- dog pig cat dog fox"
        pp $ searchReplace s "dog" "[\\0]"
        fl

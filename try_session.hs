module Main where

import Data.Default (def)
import Data.String (fromString)
import qualified Data.Vault.Lazy as Vault

import Network.Wai
import Network.Wai.Session (withSession, Session)
import Network.Wai.Session.Map (mapStore_)
import Network.Wai.Handler.Warp (run)
import Network.HTTP.Types (ok200)


--class Session<M, K, V>{
--    lookup(K k){
--    }
--    insert(K k, V v){
--    }
--}

-- Key = Key a 
-- Key (Session IO String String)
-- env = Request
-- session = Vault.Key (Session IO String String)
app :: Vault.Key (Session IO String String) -> Application
app session req = (>>=) $ do
	u <- sessionLookup "u"
	sessionInsert "u" insertThis
    -- maybe:: b -> (a -> b) -> Maybe a -> b
	return $ responseLBS ok200 [] $ maybe (fromString "session some str") fromString u
	where
	insertThis = show $ pathInfo req 
	Just (sessionLookup, sessionInsert) = Vault.lookup session (vault req)

-- type Request = Request { .. vault::Vault ..}
-- type Application = Request -> (Response -> IO ResponseReceived) -> IO ResponseReceived
-- lookup :: Key a -> Vault -> Maybe a 
-- pathInfo :: Request -> [Text]
-- type Session m k v = (k -> m (Maybe v), k -> v -> m ())
-- Type representing a single session (a lookup, insert pair)
-- Vault.newKey::IO (Key a)
main :: IO ()
main = do
        k1 <- Vault.newKey
        let empty = Vault.empty
        let nv = Vault.insert k1 3 empty
        let mb = Vault.lookup k1 nv 
        case mb of  
             Nothing -> print "no value"
             (Just x) -> print x 
        session <- Vault.newKey
        store <- mapStore_
    --    withSession::SessionStore m k v
    --    -> ByteString 
    --    -> SetCookie
    --    -> Key (Session m k v)
    --    -> Middleware
        run 3000 $ withSession store (fromString "SESSION") def session $ app session

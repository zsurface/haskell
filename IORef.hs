import Control.Monad
import Data.Char
import Data.List
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Data.Array.IO

import AronModule 

-- | -------------------------------------------------------------------------------- 
-- | Sun Nov 11 13:33:42 2018 
-- | KEY: mutable array, Array, newArray, readArray, writeArray, IORef, readIORef, modifyIORef, writeIORef example
-- | -------------------------------------------------------------------------------- 
data Count = Count{
    pos :: IORef Int,
    up :: IORef Int
    }

makeCounter::Int->Int-> IO Count
makeCounter c u = do 
            c' <- newIORef c  
            u' <- newIORef u  
            return (Count c' u')  

intPosCounter::Int->Count->IO() 
intPosCounter n (Count c _) = modifyIORef c (+n) 

intUpCounter::Int->Count->IO() 
intUpCounter n (Count _ u) = modifyIORef u (+n) 

showCounter::Count->IO()
showCounter (Count c u) = do   
                     c' <- readIORef c
                     u' <- readIORef u 
                     pp (show c' ++ " " ++ show u') 

data SomeData = SomeData { array :: IOArray Int Float }

-- | read and write the same variable in Haskell
-- imperative programming in Haskell 

main = do 
        print "Haskell newIORef Example, newArray, readArray, writeArray"
        var <- newIORef 2
        x <- readIORef var
        print x 
        writeIORef var 100
        x1 <- readIORef var
        print x1
        nc <- makeCounter 4 20 
        intPosCounter 1 nc
        intUpCounter 1 nc
        showCounter nc
        fl
        arr <- newIORef [1, 2]
        modifyIORef arr (+1)
        n <- readIORef arr
        pp $ "n=" <<< n
        pp "arr"

        -- import Data.Array.IO
        -- IO array inside record
        arr <- newArray (1,255) 1.0
        let d = SomeData { array = arr }
        a <- readArray (array d) 1
        writeArray (array d) 1 64
        b <- readArray (array d) 1 
        print (a,b)
        -- create new array from index 1 to 10, init with 0
        a2 <- newArray (1, 10) 0 :: IO (IOArray Int Int) 
        -- read index 2 from array
        n <- readArray a2 2 
        print n 
        writeArray a2 3 100
        m <- readArray a2 3 
        print m
        --
        -- two dimension array [2--10]
        --                      | 
        --                     [10]
        ds <- newArray((2, 2), (10, 10)) 1 :: IO (IOArray (Int, Int) Int)
        dr <- readArray ds (10, 2)
        dw <- writeArray ds (10, 2) 999
        d1 <- readArray ds (10, 2)
        dx <- readArray ds (2, 2)
        pp $ "dr="<<< dr
        pp $ "d1="<<< d1 
        pp $ "dx="<<< dx 


-- stack --resolver lts-3.9 runghc --package warp --package random

import qualified Data.Vault.Lazy as V
import Network.Wai
import Network.Wai.Handler.Warp
import Network.HTTP.Types
import System.Random
import qualified Data.ByteString.Lazy.Char8 as L8

-- Middleware::Application -> Application
-- middleware::V.Key Int -> Application -> Application
middleware :: V.Key Int -> Middleware
middleware key app req respond = do
    -- Generate a random number
    value <- randomIO
    let vault' = V.insert key value (vault req)
        req' = req { vault = vault' }
    app req' respond

app :: V.Key Int -> Application
app key req respond =
    respond $ responseLBS status200 [] $ L8.pack str
  where
    str =
        case V.lookup key (vault req) of
            Nothing -> "Key not found"
            Just value -> "Random number is: " ++ show value


-- V.newKey::IO (Key a)
main :: IO ()
main = do
    key <- V.newKey
    run 3000 $ middleware key $ app key

import AronModule hiding (clear)
import Graphics.UI.GLUT

-- draw mandelbrot
-- French mathematician who is well known in Fractal geometry

width = 320::GLfloat
height= 320::GLfloat

main::IO()
main = do
    (_proName, _args) <- getArgsAndInitialize
    _window <- createWindow "Draw Circle"
    displayCallback $= display
    reshapeCallback $= Just reshape
    mainLoop

reshape::ReshapeCallback
reshape size = do
        viewport $= (Position 10 20, size)
        postRedisplay Nothing

drawMandelbrot = 
    renderPrimitive Points $ do 
        mapM_ drawColorPoint allPoints
    where
        drawColorPoint (x, y, c) = do
        color c
        vertex $ Vertex3 x y 0 

allPoints::[(GLfloat, GLfloat, Color3 GLfloat)]
allPoints =[(x/width, y/height, colorFromValue $ mandel x y) | 
            x <- [-width..width],
            y <- [-height..height]
           ]

colorFromValue n =
    let 
        t::Int->GLfloat
        t i = 0.5 + 0.5*cos(fromIntegral i/10)
    in
        Color3 (t n) (t (n + 5)) (t (n + 10))

mandel x y =
    let r = 2.0 * x/width
        i = 2.0 * y/height
    in
        f (Complex r i) 0 64

f::Complex->Complex->Int->Int
f c z 0 = 0
f c z n = if (magnitude z > 2) 
          then n
          else f c (z*z + c) (n-1)

display::DisplayCallback
display = do
    clear [ColorBuffer] --make the window black
    loadIdentity        --reset transformation 
    preservingMatrix drawMandelbrot    
    flush

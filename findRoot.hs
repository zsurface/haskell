import GHC.Real
import Control.Monad
import Data.Char
import Data.List
import Data.List.Split
import Data.Time
import Data.Maybe(fromJust, isJust)
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import System.Exit
import System.IO
import Control.Monad
import System.Random

import AronModule


fact = \x -> if x == 0 then 1 else x*(fact (x - 1)) 

ones = 1:ones

two = map(+1) ones 

tentwos = take 10 two

main = do
        let x0 = -200    
        let x1 = 200 
        let eps = 0.000001
        let f x = x*x*x - 1 in case oneRoot f x0 x1 eps of
                                Just r -> print r
                                _      -> print "nothing"
        fl
        let list = map(\x -> (0.01)*x) [x0..x1]
        let ilist= zipWith(\x y->(x, y)) (init list) (drop 1 list)
        let roots= let f x = x^2 - 4 in map(\(x0, x1) -> oneRoot f x0 x1 eps) ilist 
        -- let roots= let f x = cos x in map(\(x0, x1) -> oneRoot f x0 x1 eps) ilist 
        let value= filter(isJust) roots 
        fl
        pp value
        fl
        let value= let f x = x^5 -4*x^4 + 0.1*x^3 + 4*x^2 - 0.5; x0 = -10; x1 = 10 in rootList f x0 x1 eps 100 
        -- let value= let f x = x^2 - 4; x0 = -2; x1 = 2 in rootList f x0 x1 eps 100 
        pp value 
        fl
        -- pp  tentwos
        pp $ take 10 ones 
        pp $ fact 0
        pp $ fact 1 
        pp $ fact 3 
        fl

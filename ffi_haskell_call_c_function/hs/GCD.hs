module GCD where

import Foreign
import Foreign.Ptr
import Foreign.C.Types
import Foreign.C.String
import System.IO.Unsafe

foreign import ccall "gcd"
  c_gcd :: CInt -> CInt -> CInt

-- fromIntegral => Haskell Int => CInt
f_gcd :: Int -> Int -> Int
f_gcd a b = fromIntegral $ c_gcd (fromIntegral a) (fromIntegral b)

foreign import ccall "slen"
  c_slen :: CString -> CInt -> CInt


foreign import ccall "uniqueOrder"
  c_uniqueOrder :: CString -> Ptr CInt -> CInt -> IO () 

-- newCString:  String -> CString = Ptr CChar
-- peekCString: CString -> IO String
f_uniqueOrder :: String -> Int -> (String, Int) 
f_uniqueOrder s n = unsafePerformIO $  
                    alloca $ \nptr -> do       -- nptr = int* n 
                         sptr <- newCString s  -- String => CString = Ptr CChar = char*
                         c_uniqueOrder sptr nptr (fromIntegral n)  -- Int => int
                         count <- peek nptr
                         str   <- peekCString sptr 
                         return (str, fromIntegral count)

-- newCString: convert String to type CString = Ptr CChar
f_slen :: String -> Int -> IO Int
f_slen s n = do 
      charpt <- newCString s
      return $ fromIntegral $ c_slen charpt (fromIntegral n)
      





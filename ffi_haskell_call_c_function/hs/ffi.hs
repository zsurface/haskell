import GCD

main = do 
     mapM_ (print . uncurry f_gcd) [(8, 12), (30, 105), (24, 108)]
     let s = "aabcaabb"
     f_slen s (length s) >>= print
     let num = 26
     let (str, n) = f_uniqueOrder s  num
     print $ str == "abc"
     print $ n == 3 
     print "one";


#include "gcd.h"
#include <stdio.h>

/* Find greatest common divisor. */
int gcd(int m, int r) {
	if (r == 0)
		return m;

	return gcd(r, m % r);
}


int slen(char* s, int n){
    int c = 0;
    for(int i=0; i<n; i++)
        if(s[i] == 'a')
            c++;
    return c;
}

void zeroArray(int* arr, const int n){
    for(int i=0; i<n; i++)
        arr[i] = 0;
}

void uniqueOrder(char* arr, int* n, const int NUM){
    int boolArr[NUM];
    int k = 0;
    zeroArray(boolArr, NUM);
    for(int i=0; i<strlen(arr); i++){
        int r = arr[i] % NUM;
        if(boolArr[r] == 0){
            boolArr[r] = 1;
            arr[k] = arr[i];
            k++;
        }
    }
    arr[k] = '\0';
    *n = k;
}

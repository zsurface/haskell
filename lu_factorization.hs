-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}  --
-- 
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Turtle (shell, empty)                      
import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent(threadDelay)

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".

import AronModule 

-- simple file watcher, useful
-- add more files here
graphic= "/Users/cat/myfile/bitbucket/haskelllib/AronGraphic.hs"
aronmo = "/Users/cat/myfile/bitbucket/haskelllib/AronModule.hs"
wai    = "/Users/cat/myfile/bitbucket/snippets/snippet.m"
pdf    = "/Users/cat/myfile/bitbucket/math/number.tex"
    

-- lu factorization

fun::Int -> [[a]] ->[a]
fun n cx = drop n $ col
    where
        col = (tran cx) !! n

lu k m = multiMatDouble (f k m) m 

lmat::(Fractional a)=>Integer -> [[a]] -> [a]
lmat n m = let c = drop' n $ (tran m) !! (fromIntegral n) in negate $ map(\x ->let h = head c in x/h) $ c

f::Integer -> [[Double]] -> [[Double]]
f n m = tran $ (left ++ [(replicate (fromIntegral n) 0) ++ [1] ++ tail (lmat n m)] ++ right)
    where
        left = mToFrac $ take' n mid 
        right = mToFrac $ tail $ drop' n mid 
        l = length m 
        mid = geneMat (fromIntegral l) Id

main = do 
        print "Hello World"
        let mat' = mToFrac mat
        pa mat'
        let u = lu 0 mat'
        pa $ ppad u 
        fl
        let u = lu 1 $ lu 0 mat'
        pa $ ppad u 
        print "Hello World"

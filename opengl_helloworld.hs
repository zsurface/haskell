-- | Fri Dec  7 20:13:55 2018 
-- | File: /Users/cat/myfile/bitbucket/haskell/opengl_helloworld.hs
-- | Compile: runh opengl_helloworld.hs 

module Main where

import Control.Monad (unless)
import Graphics.Rendering.OpenGL as GL 
import qualified Graphics.UI.GLFW as FW 
import System.Exit
import System.IO

unless' :: Monad m => m Bool -> m () -> m ()
unless' action falseAction = do
    b <- action
    unless b falseAction

maybe' :: Maybe a -> b -> (a -> b) -> b
maybe' m nothingRes f = case m of
    Nothing -> nothingRes
    Just x  -> f x
    
-- type ErrorCallback = Error -> String -> IO ()
errorCallback :: FW.ErrorCallback
errorCallback err description = hPutStrLn stderr description


mymain :: IO ()
mymain = do
  succInit <- FW.init
  if succInit == False then exitFailure else do
      mw <- FW.createWindow 600 600 "Hello World" Nothing Nothing
      maybe' mw (FW.terminate >> exitFailure) $ \window -> do
          FW.makeContextCurrent mw
          mainLoop window 
          FW.destroyWindow window
          FW.terminate
          exitSuccess
          
mainLoop :: FW.Window -> IO ()
mainLoop w = unless' (FW.windowShouldClose w) $ do
    (width, height) <- FW.getFramebufferSize w
    viewport $= (Position 0 0, Size (fromIntegral width) (fromIntegral height))
    GL.clear [ColorBuffer, DepthBuffer]

    FW.swapBuffers w
    FW.pollEvents
    mainLoop w 

main = mymain


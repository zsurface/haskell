import AronModule
import Graphics.UI.GLUT
myPoints::[(GLfloat, GLfloat, GLfloat)]
myPoints = [(sin(2*pi*k/36), cos(2*pi*k/36), 0) | k <- [1..36]]

width = 320::Float
height= 320::Float

main::IO()
main = do
    (_proName, _args) <- getArgsAndInitialize
    _window <- createWindow "Draw Mandelbrot"
    displayCallback $= display
    reshapeCallback $= Just reshape
    mainLoop

reshape::ReshapeCallback
reshape size = do
        viewport $= (Position 10 20, size)
        postRedisplay Nothing

drawMandelbrot = 
    --renderPrimitive Points $ do 
    renderPrimitive LineLoop $ do 
        mapM_ drawColorPoint allPoints
    where
        drawColorPoint (x, y, c) = do
        color c
        vertex $ Vertex3 x y 0 

--allPoints::[(GLfloat, GLfloat, Color3 GLfloat)]
--allPoints =[(realToFrac(x/width), realToFrac(y/height), colorFromValue $ mandel x y) | 
--            x <- [-width..width],
--            y <- [-height..height]
--           ]

-- x <- [-320..320]
-- y <- [-320..320]

allPoints::[(GLfloat, GLfloat, Color3 GLfloat)]
allPoints =[(realToFrac(x/width), realToFrac(y/height), colorFromValue $ mandel x y) | 
            x <- [-width..width], 
            y <- [-height..height]
           ]


colorFromValue n =
    let 
        t::Int->GLfloat
        --t i = 0.5 + 0.5*cos(fromIntegral i/10)
        t i = 0.5 + 0.5*sin(fromIntegral i/10)
    in
        Color3 (t n) (t (n + 5)) (t (n + 10))

mandel x y =
    let r = 2.0 * x/width 
        i = 2.0 * y/height 
    in
        f (Complex r i) 0 64

fun::Float->Float->Int
fun x y = f(Complex r i) 0 64
    where
        r = 2.0*x
        i = 2.0*y


-- f(c, z, 0) = 0 
-- f(c, z, n) = f(c, f(c, z), n-1)
-- f(c, z) = f(c, f(c, z))
f::Complex->Complex->Int->Int
f c z 0 = 0
f c z n = if (magnitude z > 2) 
          then n
          else f c (z*z + c) (n-1)

display::DisplayCallback
display = do
    clear [ColorBuffer] --make the window black
    loadIdentity        --reset transformation 
    --preservingMatrix drawMandelbrot    
    preservingMatrix drawMandelbrot    
    flush

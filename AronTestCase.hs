import Text.Regex
import Text.Read
import Data.List
import System.Process
import System.Environment
import qualified Data.Text as Text
import qualified Data.Vector as Vec 
import AronModule 
import AronTest 


vec = [1, 2, 3]

main = do 
        print "Hello World"
        -- test inverse
        print $ inverse [[0]] == ([[]], [[]]) 
        print $ inverse [[1]] == ([[1]], [["1"]]) 
        print $ inverse [[2]] == ([[0.5]], [["1/2"]]) 
        let m = snd $ inverse mat 
        print $ m == mat' 
        let v = multiMatVec mat vec 
        print $ v == [14, 32, 53]

module Main where


import Control.Monad
import AronModule
import System.CPUTime
import qualified Data.Vector as V

class MyGroup a where
  op::a->a->a
  identity::a

instance MyGroup AddNum where
  op x y = x + y
  identity = 0

  identµity =   

main :: IO ()
main = do
       pp "dog"
       let vx = V.fromList[1..10] 
       let v1 = V.map(\z -> V.map(\y -> V.map(\x -> if  x^2 + y^2 == z^2 then (x, y, z) else (0, 0, 0)) vx ) vx) vx   
       let fv = (fmap . fmap)(\v -> V.filter(\(x, y, z) -> x /= 0) v) v1
       let list = V.toList fv
       let mylist = join [[3]]
       let num = foldr(op) identity [1..10]::Int 
       pp num
       t <- getCPUTime
       let sec = (fromIntegral t) * 10^^(-12)
       pp "done"
       pp sec

import Control.Monad
import Data.Char
import Data.List
import qualified Data.List as LI
import Data.Set
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import qualified Data.Text as T
import qualified Data.Text.Lazy as L
import qualified Data.ByteString.Char8 as B

import AronModule 


-- | Convert String to ByteString
listToByteStr::[[String]]->B.ByteString
listToByteStr s = B.pack $ LI.foldr(\x y-> x ++ "\n" ++ y) [] $ LI.foldr(++) [] s

main = do 
        print "Hello World"
        argList <- getArgs
        pp $ length argList
        let s = [["dog"], ["cat"]]
        pp $ listToByteStr s
        pp "done!"

import Control.Monad
import Data.Char
import Data.List
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Control.Exception as E
import AronModule 

-- Read a file or print an error message and return Nothing.
-- use catch to handle Exeception
-- Control.Exception      catch :: Exception e => IO a -> (e -> IO a) -> IO a
-- System.IO.Error type IOError = IOException
maybeReadFile :: String -> IO (Maybe String)
maybeReadFile fileName = catch (do s <- readFile fileName 
                                   return (Just s)) 
                               (readErrHandler fileName)

readErrHandler :: String -> IOError -> IO (Maybe String)
readErrHandler fileName err =
    do putStr ("Error reading file " ++ fileName
               ++ " " ++ show err ++ ".\n")
       return Nothing

main = do 
        print "Hello World"
        argList <- getArgs
        pp $ length argList
        maybeReadFile "/dog.com"
        pp "done!"

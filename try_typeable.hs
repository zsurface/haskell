-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 

-- import Turtle
-- echo "turtle"

{-# LANGUAGE MultiWayIf        #-}
{-# LANGUAGE QuasiQuotes       #-} -- support raw string [r|<p>dog</p> |]
{-# LANGUAGE ScopedTypeVariables #-} 
import Text.RawString.QQ       -- Need QuasiQuotes too 


-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import Data.Typeable 
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import qualified Text.Regex.TDFA as TD
import qualified Data.ByteString.Lazy      as BL
import qualified Data.ByteString.UTF8      as BSU
import qualified Data.ByteString           as BS
import qualified Data.Text                 as TS    -- strict Text

import qualified Data.Text.Lazy            as TL    -- lazy Text
import qualified Data.Text.Encoding        as TSE
import qualified Data.Text.Lazy.Encoding   as TLE

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

-- shell command template:
-- 
--        argList <- getArgs
--        if len argList == 2 then do
--            let n = stringToInt $ head argList
--            let s = last argList
--            putStr $ drop (fromIntegral n) s
--        else print "drop 2 'abcd'"


import AronModule 

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close

-- cast::(Typeable a, Typeable b) => a -> Maybe b
char::Typeable a => a -> String 
char x = case cast x of
              (Just (x::Char)) -> show x 
              Nothing          -> "char unknown"

string::Typeable a => a -> String
string x = case cast x of
                Just (x::String) -> x
                Nothing          -> "str unknown"

listStr::Typeable a => a -> String
listStr x = case cast x of
                Just (x::[String]) -> show x
                Nothing            -> "listStr unknow"

ppList::[String] -> IO()
ppList x = putStrLn "[" >> mapM_ putStrLn x >> putStrLn "]"

pppList::[[String]] -> IO()
pppList x = putStrLn "[" >> mapM_ ppList x >> putStrLn "]"

pprin::Typeable a => a -> IO() 
pprin x = case cast x of
               Just (x::Char) -> putStrLn $ show x
               _              -> case cast x of 
                                      Just (x::String) -> putStrLn x
                                      _                -> case cast x of
                                                               Just (x::[String]) -> ppList x 
                                                               _                  -> case cast x of 
                                                                                          Just (x::[[String]]) -> pppList x 
                                                                                          _                    -> return ()

--lazyTextToLazyByteString::TL.Text -> BL.ByteString
--lazyTextToLazyByteString = TLE.encodeUtf8

--strictTextToStrictByteString::TS.Text -> BS.ByteString
--strictTextToStrictByteString = TSE.encodeUtf8

{-| 
    === Convert anything to Strict ByteString
-} 
toSBS::Typeable a => a -> BS.ByteString
toSBS x = case cast x of  
               Just (x::BL.ByteString) -> toStrictBS x      -- Lazy ByteString to Strict ByteString
               _  -> case cast x of 
                        Just (x::String) -> s2SBS x   -- String to Strict ByteString
                        _  -> case cast x of
                                 Just (x::TS.Text) -> st2SBS x  -- strict Text To Strict ByteString
                                 _  -> case cast x of                                          
                                       Just (x::TL.Text) -> (lb2SBS . lt2LBS) x  -- lazy Text Text to Strict ByteString
                                       _  -> s2SBS $ showsTypeRep (typeOf x) " => Invalid Type"
            where
                lb2SBS = lazyByteStringToStrictByteString
                lt2LBS = lazyTextToLazyByteString
                st2SBS = strictTextToStrictByteString
                s2SBS  = strToStrictByteString

{-| 
    === Convert anything to Lazy ByteString
-} 
toLBS::Typeable a => a -> BL.ByteString
toLBS x = case cast x of
          Just (x::String) -> s2lbs x
          _ -> case cast x of
               Just (x::TL.Text) -> lt2LBS x
               _ -> case cast x of
                    Just (x::TS.Text) -> lt2LBS .  st2lt $ x
                    _ -> case cast x of
                         Just (x::BS.ByteString) -> sbs2lbs x
                         _ -> case cast x of
                              Just (x::BL.ByteString) -> x
                              _ -> s2lbs $ showsTypeRep (typeOf x) " => Invalid Type"
          where
            s2lbs = strToLazyByteString  
            lt2LBS = lazyTextToLazyByteString
            sbs2lbs = strictByteStringToLazyByteString
            st2lt = strictTextToLazyText                

{-| 
    === Convert anything to Strict Text
-} 
toSText::Typeable a => a -> TS.Text
toSText x = case cast x of
            Just (x::String) -> strToStrictText x  -- String to strict Text
            _ -> case cast x of 
                 Just (x::TL.Text) -> lt2st x    -- Lazy Text to Strict Text
                 _ -> case cast x of
                      Just (x::BS.ByteString) -> sbs2st x  -- Strict ByteString to Strict Text
                      _ -> case cast x of
                           Just (x::BL.ByteString) ->  sbs2st . lb2SBS $ x
                           _ -> case cast x of
                                Just (x::TS.Text) -> x
                                _ -> strToStrictText $ showsTypeRep (typeOf x) " => Invalid Type"
            where
                lb2SBS = lazyByteStringToStrictByteString
                lt2LBS = lazyTextToLazyByteString
                st2SBS = strictTextToStrictByteString
                s2SBS  = strToStrictByteString
                sbs2st = strictByteStringToStrictText
                lt2st  = lazyTextToStrictText 

{-| 
    === Convert anything to Lazy Text
-} 
toLText::Typeable a => a -> TL.Text
toLText x = case cast x of
            Just (x::String) -> strToLazyText x 
            _ -> case cast x of
               Just (x::TS.Text) -> st2lt x
               _ -> case cast x of
                    Just (x::BL.ByteString) -> lbs2lt x
                    _ -> case cast x of
                         Just (x::BS.ByteString) ->  st2lt . sbs2st $ x
                         _ -> case cast x of
                              Just (x::TL.Text) -> x
                              _ -> strToLazyText $ showsTypeRep (typeOf x) " => Invalid Type"
            where
                st2lt = strictTextToLazyText                
                lbs2lt = lazyByteStringToLazyText 
                sbs2st = strictByteStringToStrictText

{-| 
    === Convert anthing to String

    * Lazy Text to String
    * Strict Text to String
    * Lazy ByteString to String
    * Strict ByteString to String
-} 
toStr::Typeable a => a -> String
toStr x = case cast x of
          Just (x::BL.ByteString) -> sb2s . toStrictBS $ x
          _ -> case cast x of 
               Just (x::BS.ByteString) -> sb2s x
               _ -> case cast x of 
                    Just (x::TS.Text) -> st2s x 
                    _ -> case cast x of 
                         Just (x::TL.Text) -> st2s . lt2st $ x
                         _ -> case cast x of
                              Just (x::String) -> x
                              _   -> showsTypeRep (typeOf x) " => Invalid Type"
          where
              sb2s  = strictBSToString 
              lt2st = lazyTextToStrictText 
              st2s  = strictTextToStr

checkType::Typeable a => a -> String
checkType a = str " => Typeable" 
        where
            str s = showsTypeRep (typeOf a) s

data MyRec = MyRec {name :: String}

main = do 
        pp "done!"
        pprin "hi"
        pprin 'a' 
        pprin ["dog", "cat"] 
        pprin [["dog", "cat"], ["cow", "pig", "rat"]]
        let sb = BL.empty
        pre $ toSBS "hi"
        pre $ toSBS sb
        let n = 3 ::Int
        pre $ toSBS n 
        let bb = toSText "This is string typeable {-# LANGUAGE ScopedTypeVariables #-}"
        pre $ toSBS bb
        pre $ checkType n
        pre $ checkType n
        let fn = 3.4::Float
        pre $ checkType fn
        let re = MyRec {name = "ScopedTypeVariables"}
        pre $ checkType re
        pp "done"



















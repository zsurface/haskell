-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

--import Data.Array
--import Text.Regex.TDFA -- the module will conflict with Text.Regex

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

-- zo - open
-- za - close

import AronModule 
import qualified Data.Map.Strict as M

frand = "/Library/WebServer/Documents/zsurface/randomNote.txt"
s = [(["dog", "cat"], 1), (["cow", "cat"], 2)]


-- type MMap = M.Map String [Integer]
type MMap a b = M.Map a [b]

addMore::(Ord e)=>[([e], a)] -> MMap e a-> MMap e a 
addMore [] m = m
addMore (s:cs) m = addMore cs $ add s m 

add::(Ord e)=>([e], a) -> MMap e a -> MMap e a 
add ([], _)   m = m
add (s:cs, n) m = case ls of 
                       Just x -> add (cs, n) $ M.insert s (n:x) m 
                       _      -> add (cs, n) $ M.insert s [n] m
    where
        ls = M.lookup s m -- Maybe a

--add::([String], Integer) -> MMap String Integer -> MMap String Integer
--add ([], _)   m = m
--add (s:cs, n) m = case ls of 
--                       Just x -> add (cs, n) $ M.insert s (n:x) m 
--                       _      -> add (cs, n) $ M.insert s [n] m
--    where
--        ls = M.lookup s m -- Maybe a

geneMap::[String]->M.Map String [[String]]
geneMap [] = M.empty 
geneMap cx = addMore keyMap M.empty 
    where
        blo = filter(\x -> len x > 0) $ splitBlock cx "^[[:space:]]*(---){1,}[[:space:]]*"
        pblock = zip blo [0..] 
        rblock = map(\x ->(snd x, fst x)) $ let bs = "^[[:space:]]*(---){1,}[[:space:]]*" 
                                                ws = "[,. ]" 
                                            in parseFileBlock bs ws cx 
        -- [([k0, k1], ["block1"]), ([k2, k3], ["block2"])]
        keyMap = zipWith(\x y -> (fst x, fst y)) rblock pblock

main = do 
        print "Hello World"
        let empty = M.empty::M.Map String [a]
        argList <- getArgs
        pp $ length argList
        pp "done!"
        fblock <- readFileToList "/tmp/file.x"
        let gmap = geneMap fblock
        pw "gmap" gmap
--        fl
--        let mdata = addMore [(["dog", "cat", "fox"], 1), (["dog", "key"], 4), (["cat"], 4), (["dog"], 9), (["rat"], 4)] empty
--        pp mdata
--        fl
--        pp $ M.toList mdata
--        let list = filter(\x -> length x > 0) $ splitWhen (\x -> matchTest (mkRegex "(---){1,}") x) fblock
--        let bb = (map . map) (\x -> sentence x) list
--        let bbs = (map . map ) (\r -> filter(\w -> (len w > 0) && isWord w) r) bb 
--        let bbss = map(\x -> join x) bbs
--        pp list 
--        fl
--        pp bb
--        fl
--        pp bbs
--        fl
--        pp bbss

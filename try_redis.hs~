-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- import Turtle
-- echo "turtle"
{-# LANGUAGE DuplicateRecordFields #-} 
-- {-# LANGUAGE OverloadedStrings, DeriveGeneric, DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric, DeriveAnyClass #-}
-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import Data.Char
import Data.Maybe(fromJust, fromMaybe)
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
--import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when, liftM, liftM2, liftM3)
import Control.Monad.IO.Class
import Control.Concurrent 
import Database.Redis
import GHC.Generics

import qualified Data.List as L
import qualified Data.Aeson as DA
import qualified Data.ByteString.Lazy as BL
import qualified Data.ByteString.Char8 as BS
import qualified Data.ByteString as SB -- strict ByteString 
import qualified Data.ByteString.UTF8 as BSU
import qualified Data.Text as DT
import qualified Data.Text.Lazy as DL 
import qualified Data.ByteString.Lazy.UTF8 as BLU

import qualified Data.Map.Strict as M
import Data.Aeson.Text (encodeToLazyText)

import AronModule 

type MMap a b = M.Map a [b]

-- geneMap::[String]->M.Map String [[String]]
geneMap::[String]->M.Map String [[String]]
geneMap [] = M.empty 
geneMap cx = addMore keyMap M.empty 
    where
        blo = L.filter(\x -> len x > 0) $ splitBlock cx "^[[:space:]]*(---){1,}[[:space:]]*"
        pblock = L.zip blo [0..] 
        rblock = L.map(\x ->(snd x, fst x)) $ let bs = "^[[:space:]]*(---){1,}[[:space:]]*" 
                                                  ws = "[,. ]" 
                                              in parseFileBlock bs ws cx 
        -- [([k0, k1], ["block1"]), ([k2, k3], ["block2"])]
        keyMap = L.zipWith(\x y -> (fst x, fst y)) rblock pblock

        addMore::(Ord e)=>[([e], a)] -> MMap e a-> MMap e a 
        addMore [] m = m
        addMore (s:cs) m = addMore cs $ add s m 

        add::(Ord e)=>([e], a) -> MMap e a -> MMap e a 
        add ([], _)   m = m
        add (s:cs, n) m = case ls of 
                               Just x -> add (cs, n) $ M.insert s (n:x) m 
                               _      -> add (cs, n) $ M.insert s [n] m
            where
                ls = M.lookup s m -- Maybe a

data Block = Block{bblock::[[DL.Text]]} deriving (Generic, Show)
-- data Block = Block{bblock::[[ByteString]]} deriving (Generic, Show)

instance DA.FromJSON Block 
instance DA.ToJSON Block where
    -- No need to provide a toJSON implementation.

    -- For efficiency, we write a simple toEncoding implementation, as
    -- the default version uses toJSON.
    toEncoding = DA.genericToEncoding DA.defaultOptions

{-| 
    @
    import qualified Data.ByteString.Char8 as BS
    String to ByteString
    @
-} 
strToBS::String -> SB.ByteString
strToBS s = BS.pack s

fun::SB.ByteString -> Redis SB.ByteString
fun key = do
    result <- get key
    case result of
        Left _ -> return $ BSU.fromString "Some error occurred"
        Right v -> return $ fromMaybe (BSU.fromString "Could not find key") v
    
main = do 
       fblock <- readFileToList "/tmp/file.x"
       let gmap = geneMap fblock -- > M.Map [(String, [[String]])]
       pw "gmap" gmap
       fl
       conn2 <- connect defaultConnectInfo
       re <- runRedis conn2 $ fun $ BSU.fromString "k"
       pp re
       
       -- toStrictBS: lazy to strict ByteString
       -- DL.pack: String to DL.Text
       -- DA.encode: encode Block to json
       -- toStrictBS: lazy ByteString to strict ByteString
       let mr = L.map(\s -> (strToBS $ fst s, toStrictBS $ DA.encode $ Block{bblock = (L.map . L.map) (\c -> DL.pack c) $ snd s})) $ M.toList gmap -- > [(ByteString, [[ByteString]])]
       let tt = DT.pack "dog"
       conn <- connect defaultConnectInfo
       runRedis conn $ do
           set (strToBS "hello") (strToBS "v1")
           mset mr
           s <- get (strToBS "Air")
           liftIO $ print s
           liftIO fl 
           s <- get (strToBS "hello")
           liftIO $ print s
       pp "dog" 
       re <- runRedis conn $ fun $ BSU.fromString "dog" -- String to Lazy ByteString
       let ree = DA.decode $ BL.fromStrict re -- Strict => Lazy => Maybe Record
       pw "ree" ree
       let gg = case ree of
                     (Just x) -> case x of
                                      (Just y) -> y
                                      _        -> "nonothing"
                     _        -> "nothing"
       -- Strict ByteString => Layz ByteString => Maybe Block              
       pw "re" $ bblock . fromJust $ (DA.decode $ BL.fromStrict re :: Maybe Block)
       pw "gg" gg

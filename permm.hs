import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

import AronModule 

rmi::[a]->Int->[a]
rmi [] _ = []
rmi s n = (take (n-1) s) ++ (drop n s)

gei::[a]->Int->a
gei s n = head (drop (n-1) s)

perm'::[a]->[a]
perm' [] = []
perm' s = map(\x -> (gei s x):(perm' ( rmi s x))) [1..length s]

main = do 
        print "Hello World"
        argList <- getArgs
        pp $ length argList
        let s = rmi "abc" 1
        pp s
        let s = rmi "abc" 2 
        pp s
        let s = rmi "abc" 3 
        pp s
        let s = gei "cat" 1
        pp s
        let s = gei "cat" 2 
        pp s
        let s = gei "cat" 3 
        pp s
        pp "done!"

import Test.HUnit
import AronModule
import AronGraphic 

-- | All the AronModule test cases are here.
-- | -------------------------------------------------------------------------------- 
-- | HUnit for unit testing 
-- | mergeSort
allTests = runTestTT tests 
        where
            test1 = TestCase (assertEqual "arithmetic test" 7 (3+4)) 
            test2 = TestCase (assertEqual "mergeSort" [3, 4] $ mergeSort [4, 3]) 
            test3 = TestCase (assertEqual "mergeSort" [3, 4] $ mergeSort [3, 4]) 
            test4 = TestCase (assertEqual "mergeSort" [] $ mergeSort []) 
            test5 = TestCase (assertEqual "mergeSort" [1] $ mergeSort [1]) 
            test6 = TestCase (assertEqual "mergeSort" [0..5] $ mergeSort [5, 3, 2, 4, 0, 1]) 

-- | The RIGHT boundary of interval will NOT be evaluated. [-2, 2) => 2 is NOT evaluated by the function
-- | This is why (-2.0) is only root can be found in f(x) = x^2 - 4 = 0 where x0 = -2, x1 = 2 . (f(x) = 0 has two roots: -2 and 2)
            test7 = TestCase (assertEqual "one root " (Just (-2.0)) $ let 
                                                                        f = \x-> x^2 -4; x0 = (-2); x1 = 2; eps = 0.001
                                                                    in 
                                                                        oneRoot f x0 x1 eps) 

            test8 = TestCase (assertEqual "one root " Nothing $ let 
                                                                        f = \x-> x^2 -4; x0 = (-4); x1 = (-2); eps = 0.001
                                                                    in 
                                                                        oneRoot f x0 x1 eps) 
            test9 = TestCase (assertEqual "one root " (Just 2)       $ let 
                                                                        f = \x-> x^2 -4; x0 = 2; x1 = 4; eps = 0.001
                                                                    in 
                                                                        oneRoot f x0 x1 eps) 
            tests = TestList [
                            TestLabel "test1" test1,
                            TestLabel "test2" test2,
                            TestLabel "test3" test3,
                            TestLabel "test4" test4,
                            TestLabel "test5" test5,
                            TestLabel "test6" test6,
                            TestLabel "test7" test7,
                            TestLabel "test8" test8,
                            TestLabel "test9" test9
                            ] 

main = do
        allTests 
        print "done" 

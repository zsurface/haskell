import Data.Char 
import System.IO
import System.Environment
import Text.Regex.Posix
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import AronModule

-- get all sum of first n elements from a list and return the remainer of the list
-- f 2 [1, 2, 3, 4] = (3, [3, 4])

f::Integer->[Integer]->(Integer, [Integer])
f _ [] = (0, [])
f n x  = (sum $ take (fromIntegral n) x, drop (fromIntegral n) x)

main = do 
        print "Hello World"
	let (n, l) = f 2 [1, 2, 3]
	print n
	print l
	print (n, l)
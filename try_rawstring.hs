{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

import Data.Text (Text)
import Text.RawString.QQ (r)
import NeatInterpolation (text)

rQuote :: Text -> Text
rQuote myVariable = [r|line one
line two
line tree
${ myVariable }
line five|]

neatQuote :: Text -> Text
neatQuote myVariable = [text|line one
line two
line tree
$myVariable
line five|]

rText, neatText :: Text
rText    = rQuote    "line four"
neatText = neatQuote "line four"

main = do 
        print "dog"

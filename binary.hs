-- {-# LANGUAGE GeneralizedNewtypeDeriving #-}
import System.IO
import System.Directory
import Data.List
import Text.Regex.Posix
import Text.Regex
import System.Random
import AronModule

True ? x = const x
False ? _ = id

main = do
    print $ ((3 > 2) ? 2 $ 3) 
    print $ ( True ? 1 $ 2) 
    let t0 = Empty 
    print $ sym t0
    let t1 = Node 'x' (Node 'x' Empty Empty) (Node 'x' Empty Empty)
    print $ sym t1
    let t2 = Node 'x' (Node 'x' Empty Empty) Empty 
    let t3 = Node 'x' (Node 'x' Empty Empty) (Node 'x' Empty Empty) 
    print $ sym t2
    print $ sym t3
    let n = 10::Integer
    let root = Node 10 Empty Empty
    let node = Node 5 Empty Empty 
    let node1 = Node 12 Empty Empty 
    let node2 = Node 11 Empty Empty 
    let r3 = Node 'x' (Node 'x' Empty Empty) (Node 'x' Empty Empty)
    let list = inorder r3 
    let len = maxlen r3
    fpp "len" len
    let tr = Empty
    let len1 = maxlen tr
    fpp "len1" len1
    let tt1 = sym (Node 10 Empty Empty)
    fpp "tt1" tt1
    let t5 = Node 10 (Node 3 Empty (Node 1 (Node 4 Empty Empty) (Node 6 Empty Empty))) (Node 12 Empty Empty)
    pp $ lca t5 6 12 
    fl 

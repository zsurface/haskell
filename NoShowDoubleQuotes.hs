import Control.Monad
import System.Directory
import System.Exit
import System.Process

import AronModule 

-- no show double quotes
newtype NoQuotes = NoQuotes String
instance Show NoQuotes where show (NoQuotes str) = str

main = do
    paa ["dog\"", "cat"]
    pp "dog"
    print (NoQuotes "look \"ma, no quotes!")

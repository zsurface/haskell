-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}  --
-- 
-- import Turtle                       --
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".

import AronModule 

fold::(a -> a -> a) -> a -> [a]-> a 
fold f z [] = z 
--fold f z (x:cx) = f (fold f z cx) x  
fold f z cx = f z (fold f x' cx') 
              where
                x' = last cx
                cx' = init cx

f 0 - f 3 - f 2 - f 1 - 0

--fold f z (x1:x2:x3) 
--f (fold z x2:x3) x1
--f (f (fold z x3) x2) x1
--f ((f (fold z [] ) x3) x2) x1
        

--fold f z (x1:x2:x3)
--f x1 (fold f z x2:x3)
--        f x2 (fold f z x3) 
--                f x3 (fold f z [])
--                f x3 z
--        f x2 (f x3 z)      
--     (f x2 (f x3 z))
--f x1 (f x2 (f x3 z))

--  
main = do 
        print "Hello World"
        argList <- getArgs
        pp $ length argList
        pp "done!"
        pp $ fold (\x y -> x - y) 0 [1, 2, 3] --  (0 - 3) - 2

-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}  --
-- 
-- import Turtle                       --
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".

import AronModule 

data Vect a = Vect a a a
data Matr a = Matr a a a

data Pair a = Pair a a

class Group a where
    op:: a -> a -> a
    neg::a -> a

-- | multiple inheritence 
class (Num a, Group a) => Ringx a where
    (+++:)::a -> a -> a  

--instance Ringx (Pair a) where
--    (+++:) (Pair x0 y0) (Pair x1 y1) = Pair (x0 + x1) (y0 + y1)

class Ring v where
    (++:)::(Num a)=>v a -> v a -> v a
    (.:)::(Num a)=>v a -> v a -> a 
    ne::(Num a)=> v a -> v a

instance Ring Vect where
    (++:) (Vect x0 y0 z0) (Vect x1 y1 z1) = Vect (x0 + x1) (y0 + y1) (z0 + z1)
    (.:) (Vect x0 y0 z0) (Vect x1 y1 z1) = x0*x1 + y0*y1 + z0*z1
    ne (Vect x0 y0 z0) = Vect (-x0) (-y0) (-z0)
--
instance Ring Matr where
    (++:) (Matr x0 y0 z0) (Matr x1 y1 z1) = Matr (x0 + x1) (y0 + y1) (z0 + z1)
    (.:) (Matr x0 y0 z0) (Matr x1 y1 z1) = x0*x1 + y0*y1 + z0*z1



main = do 
        print "Hello World"
        let v1 = Vect 1 2 3
        let v2 = Vect 3 4 4
        let t  = v1 .: v2
        pp $ "t=" +: t
        pp "done!"

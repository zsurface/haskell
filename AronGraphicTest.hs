import Test.HUnit
import Test.HUnit.Approx
import Graphics.Rendering.OpenGL as GL 
import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
import qualified Graphics.UI.GLFW as FW 
import AronModule
import AronGraphic
import GHC.Real 
import Data.Maybe



{-| 
    === All the AronModule test cases are here. 

    * KEY: Test.HUnit, test case
    * HUnit for unit testing 
    * mergeSort
-} 
epMaybe::GLfloat -> 
       Maybe (Vertex3 GLfloat, (GLfloat, GLfloat)) -> 
       Maybe (Vertex3 GLfloat, (GLfloat, GLfloat)) -> Bool
epMaybe e Nothing _ = False
epMaybe e _ Nothing = False
epMaybe e (Just (Vertex3 a b c, (x, y))) (Just (Vertex3 a' b' c', (x', y'))) = t1 && t2 && t3 && t4 && t5
        where
            t1 = abs (a - a') < e
            t2 = abs (b - b') < e
            t3 = abs (c - c') < e
            t4 = abs (x - x') < e
            t5 = abs (y - y') < e

epVertex::GLfloat -> Vertex3 GLfloat -> Vertex3 GLfloat -> Bool
epVertex e (Vertex3 x y z) (Vertex3 x' y' z') = t1 && t2 && t3
        where
            t1 = abs (x - x') < e
            t2 = abs (y - y') < e
            t3 = abs (z - z') < e

allTests = runTestTT tests 
    where
        test1 = TestCase (assertEqual "arithmetic test" 7 (3+4))
        test2 = TestCase (assertEqual "mergeSort" [3, 4] $ mergeSort [4, 3]) 
        test3 = TestCase (assertEqual "mergeSort" [3, 4] $ mergeSort [3, 4]) 
        test4 = TestCase (assertEqual "mergeSort" [] $ mergeSort [])
        test5 = TestCase (assertEqual "mergeSort" [1] $ mergeSort [1])
        test6 = TestCase (assertEqual "mergeSort" [0..5] $ mergeSort [5, 3, 2, 4, 0, 1]) 
        trytest = TestCase (assertApproxEqual "try" 0.01  1.001 $ 1.00)

-- | The RIGHT boundary of interval will NOT be evaluated. [-2, 2) => 2 is NOT evaluated by the function
-- | This is why (-2.0) is only root can be found in f(x) = x^2 - 4 = 0 where x0 = -2, x1 = 2 . (f(x) = 0 has two roots: -2 and 2)
        test7 = TestCase (assertEqual "one root " (Just (-2.0)) $ 
            let f = \x-> x^2 -4; x0 = (-2); x1 = 2; eps = 0.001 in oneRoot f x0 x1 eps) 
        test8 = TestCase (assertEqual "one root " Nothing $ 
            let f = \x-> x^2 -4; x0 = (-4); x1 = (-2); eps = 0.001 in oneRoot f x0 x1 eps) 
        test9 = TestCase (assertEqual "one root " (Just 2) $ 
            let f = \x-> x^2 -4; x0 = 2; x1 = 4; eps = 0.001 in oneRoot f x0 x1 eps) 
        test10 = TestCase(assertEqual "isPerpen " True $ 
            isPerpen (Vector3 0.0 1.0 0.0) (Vector3 1.0 0.0 0.0)) 
        test11 = TestCase(assertEqual "isPerpen " False $ 
            isPerpen (Vector3 0.0000001 1.0 0.0) (Vector3 1.0 0.0 0.0)) 
        test12 = TestCase(assertEqual "isColinear" True $ 
            isColinear (Vertex3 0.0 1.0 0.0)(Vertex3 0.0 2.0 0.0) (Vertex3 0.0 3.0 0.0)) 
        test13 = TestCase(assertEqual "isColinear" False $ 
            isColinear (Vertex3 0.0001 1.0 0.0)(Vertex3 0.0 2.0 0.0) (Vertex3 0.0 3.0 0.0)) 
        test14 = TestCase(assertEqual "isColinear" True $ 
            isColinear (Vertex3 1.0 1.0 0.0)(Vertex3 2.0 2.0 0.0) (Vertex3 3.0 3.0 0.0)) 
        test15 = TestCase(assertEqual "isColinear" False $ 
            isColinear (Vertex3 1.0 1.0 0.0)(Vertex3 2.0 2.001 0.0) (Vertex3 3.0 3.0 0.0)) 
        test16 = TestCase(assertEqual "dot3" 2  $ 
            dot3vx (Vertex3 1.0 1.0 0.0)(Vertex3 1.0 1.0 0.0))  
        -- test17 = TestCase(assertEqual "(+:)" (Vertex3 2.0 2.0 2.0)  $ (+:) (Vertex3 1.0 1.0 1.0)(Vertex3 1.0 1.0 1.0))  

--    let inter = let p0 = Vertex3 (x0=0) (y0=0) (z0=0)
--                    p1 = Vertex3 (x1=1) (y1=1) (z1=0)
--                    q0 = Vertex3 (a0=2) (b0=0) (c0=0)
--                    q1 = Vertex3 (a1=2) (b1=1) (c1=0)
--                    in intersectLine p₀ p₁ q₀ q₁ 
        test20 = TestCase(assertEqual "intersectLine p0 p1 q0 q1" (Just ((Vertex3 2.0 2.0 0), [[2.0],[2.0]])) $ 
            intersectLine (Vertex3 0 0 0) (Vertex3 1 1 0) (Vertex3 2 0 0) (Vertex3 2 1 0))

        -- gx http://localhost/image/intersection_line1.svg
        test25 = TestCase(assertEqual "intersectLine p0 p1 q0 q1" (Just ((Vertex3 0.0 0.0 0), [[(-1)], [-0.25]])) $ 
            intersectLine (Vertex3 3 4 0) (Vertex3 6 8 0) (Vertex3 4 (-3) 0) (Vertex3 20 (-15) 0))
--
        test26 = TestCase(assertEqual "intersectLine p0 p1 q0 q1" (Just ((Vertex3 3.0 4.0 0), [[0.0], [-0.25]])) $ 
            intersectLine (Vertex3 3 4 0) (Vertex3 6 8 0) (Vertex3 (4 + 3) ((-3) + 4) 0) (Vertex3 (20 + 3) ((-15) + 4) 0))
--
        -- TODO: fix this test case
        test27 = TestCase(assertEqual "intersectLine p0 p1 q0 q1" (Just ((Vertex3 3.0 4.0 0), [[2.0],[negate $ fromRational (1%4)]])) $ 
            intersectLine (Vertex3 9 12 0) (Vertex3 6 8 0) (Vertex3 (4 + 3) ((-3) + 4) 0) (Vertex3 (20 + 3) ((-15) + 4) 0))

        intersectLine2_1 = let p0 = Vertex3 0 0 0
                               p1 = Vertex3 1 1 0
                               q0 = Vertex3 2 0 0
                               q1 = Vertex3 2 1 0
                               exp= Just (Vertex3 2.0 2.0 0, (2.0, 2.0))
                               in TestCase(assertEqual "intersectLine2_1 p0 p1 q0 q1" exp $ intersectLine2 p0 p1 q0 q1)

        intersectLine2_2 = let p0 = Vertex3 3 4 0
                               p1 = Vertex3 6 8 0
                               q0 = Vertex3 4 (-3) 0
                               q1 = Vertex3 20 (-15) 0
                               exp= Just (Vertex3 0.0 0.0 0, ((-1), -0.25))
                               in TestCase(assertEqual "intersectLine2_2 p0 p1 q0 q1" exp $ intersectLine2 p0 p1 q0 q1)

        intersectLine2_3 = let p0 = Vertex3 3 4 0
                               p1 = Vertex3 6 8 0
                               q0 = Vertex3 (4+3) ((-3) + 4) 0
                               q1 = Vertex3 (20 + 3) ((-15) + 4) 0
                               exp= Just (Vertex3 3.0 4.0 0, (0.0, -0.25))
                               in TestCase(assertEqual "intersectLine2_3 p0 p1 q0 q1" exp $ intersectLine2 p0 p1 q0 q1)

        intersectLine2_4 = let p0 = Vertex3 9 12 0
                               p1 = Vertex3 6 8 0
                               q0 = Vertex3 (4+3) ((-3) + 4) 0
                               q1 = Vertex3 (20 + 3) ((-15) + 4) 0
                               -- eps= 0.0001::GLfloat
                               eps= 0.0001
                               exp= Just (Vertex3 3.0 4.0 0, (2.0, negate $ fromRational (1%4)))
                               val= intersectLine2 p0 p1 q0 q1
                               ebo = True
                               in TestCase(assertEqual "intersectLine2_4 p0 p1 q0 q1" ebo $ epMaybe eps exp val)

        test28 = TestCase(assertEqual "intersectLineR p0 p1 q0 q1" (Just ((Vertex3 (3%1) (4%1) (0%1)), [[2%1],[negate(1%4)]])) $ 
            intersectLineR (Vertex3 9 12 0) (Vertex3 6 8 0) (Vertex3 (4 + 3) ((-3) + 4) 0) (Vertex3 (20 + 3) ((-15) + 4) 0))

        -- point (0 0 0) to line: (2 0 0) (2 1 0)
        test21 = TestCase(assertEqual "pointToLine p0 q0 q1" 2.0 $ 
            pointToLine (Vertex3 0 0 0) (Vertex3 2 0 0) (Vertex3 2 1 0))


        test24 = TestCase(assertEqual "pointToLine p0 q0 q1" 5.0 $ 
            pointToLine (Vertex3 3 4 0) (Vertex3 4 (-3) 0) (Vertex3 20 (-15) 0) )
        
        test241 = TestCase(assertEqual "pointToLine p0 q0 q1" 0.0 $ 
            pointToLine (Vertex3 8 (-6) 0) (Vertex3 4 (-3) 0) (Vertex3 20 (-15) 0) )

        test22 = TestCase(assertEqual "dist p0 p1" 5.0 $ 
            dist (Vertex3 0 0 0) (Vertex3 3 4 0) ) -- 3^2 + 4^2 = 5^2

        test23 = TestCase(assertEqual "sqdist p0 p1" 25.0 $ 
            sqdist (Vertex3 0 0 0) (Vertex3 3 4 0) ) -- 3^2 + 4^2 = 5^2

        sm = [
               [0,     -3, 2],
               [3,    0,  -1],
               [-2,   1,   0]
             ]
        q0 = Vector3 1 2 3
        q1 = Vector3 2 5 6 
        exp= Vector3 (-3.0) 0 1.0
        test29 = TestCase(assertEqual "skew (Vertex3 1 2 3)" sm $ skew (Vertex3 1 2 3) )

        test30 = TestCase(assertEqual "cross (Vertex3 1 2 3) (Vertex3 2 5 6)" exp $ cross q0 q1 ) 

        test31 = let p0 = Vertex3 0 0 0 
                     p1 = Vertex3 1 0 0 
                     q0 = Vertex3 0 1 0 
                     q1 = Vertex3 1 1 0 
                     exp= True
                     in TestCase(assertEqual "isCoplanar p0 p1 q0 q1" exp $ isCoplanar p0 p1 q0 q1) 

        test32 = let p0 = Vertex3 0 0 0 
                     p1 = Vertex3 1 0 0 
                     q0 = Vertex3 0 1 0 
                     q1 = Vertex3 1 1 0.001 
                     exp= False 
                     in TestCase(assertEqual "isCoplanar p0 p1 q0 q1" exp $ isCoplanar p0 p1 q0 q1) 

        test33 = let p0 = Vertex3 0 0 0 
                     p1 = Vertex3 1 0 0 
                     p2 = Vertex3 0 1 0 
                     exp= Vector3 0 0 1 
                     in TestCase(assertEqual "normal3 p0 p1 p1" exp $ fromJust $ normal3 p0 p1 p2) 

        test34 = let p0 = Vertex3 0   0 0 
                     q0 = Vertex3 0.5 0 0 
                     q1 = Vertex3 0.6 0 0
                     exp= False 
                     in TestCase(assertEqual "isInSegment p0 p1 p1" exp $ fromJust $ isInSegment p0 q0 q1) 

        test35 = let p0 = Vertex3 0.499 0 0 
                     q0 = Vertex3 0.5   0 0 
                     q1 = Vertex3 0.6   0 0
                     exp= False 
                     in TestCase(assertEqual "isInSegment p0 p1 p1" exp $ fromJust $ isInSegment p0 q0 q1) 

        test36 = let p0 = Vertex3 0.5 0 0 
                     q0 = Vertex3 0.5   0 0 
                     q1 = Vertex3 0.6   0 0
                     exp= True 
                     in TestCase(assertEqual "isInSegment p0 p1 p1" exp $ fromJust $ isInSegment p0 q0 q1) 

        test37 = let p0 = Vertex3 0.5001 0 0 
                     q0 = Vertex3 0.5    0 0 
                     q1 = Vertex3 0.6    0 0
                     exp= True 
                     in TestCase(assertEqual "isInSegment p0 p1 p1" exp $ fromJust $ isInSegment p0 q0 q1) 

        test38 = let p0 = Vertex3 0.2 0.2 0 
                     q0 = Vertex3 0.5 0.5 0 
                     q1 = Vertex3 0.6 0.6 0
                     exp= False 
                     in TestCase(assertEqual "isInSegment p0 p1 p1" exp $ fromJust $ isInSegment p0 q0 q1) 

        test39 = let p0 = Vertex3 0.5 0.5 0 
                     q0 = Vertex3 0.5 0.5 0 
                     q1 = Vertex3 0.6 0.6 0
                     exp= True 
                     in TestCase(assertEqual "isInSegment p0 p1 p1" exp $ fromJust $ isInSegment p0 q0 q1) 

        test40 = let p0 = Vertex3 0.6 0.6 0
                     q0 = Vertex3 0.5 0.5 0 
                     q1 = Vertex3 0.6 0.6 0
                     exp= True 
                     in TestCase(assertEqual "isInSegment p0 p1 p1" exp $ fromJust $ isInSegment p0 q0 q1) 

        test41 = let p0 = Vertex3 0.5001 0.5001 0
                     q0 = Vertex3 0.5 0.5 0 
                     q1 = Vertex3 0.6 0.6 0
                     exp= True 
                     in TestCase(assertEqual "isInSegment p0 p1 p1" exp $ fromJust $ isInSegment p0 q0 q1) 

        test42 = let p0 = Vertex3 0.51 0.51 0 
                     q0 = Vertex3 0.5 0.5 0 
                     q1 = Vertex3 0.6 0.6 0
                     exp= True 
                     in TestCase(assertEqual "isInSegment p0 p1 p1" exp $ fromJust $ isInSegment p0 q0 q1) 

        test43 = let p0 = Vertex3 0.51 0.5001 0 
                     q0 = Vertex3 0.5 0.5 0 
                     q1 = Vertex3 0.6 0.6 0
                     exp= Nothing 
                     in TestCase(assertEqual "isInSegment p0 p1 p1" exp $ isInSegment p0 q0 q1) 

        ptOnSegment_1 = let p0 = Vertex3 0.5001 0.5001 0 
                            q0 = Vertex3 0.5001 0.5001 0 
                            q1 = Vertex3 0.6    0.6    0
                            exp= OnEndPt 
                            in TestCase(assertEqual "ptOnSegment p0 p1 p1" exp $ ptOnSegment p0 q0 q1) 

--            data PtSeg = OnEndPt    -- ^ Overlapped pt
--                         | InSeg    -- ^ Inside the segment
--                         | OutSeg  deriving (Eq, Show) -- ^ Out the segment
        ptOnSegment_2 = let p0 = Vertex3 0.50001 0.5001 0 
                            q0 = Vertex3 0.5001  0.5001 0 
                            q1 = Vertex3 0.6     0.6    0
                            exp= OutSeg 
                            in TestCase(assertEqual "ptOnSegment p0 p1 p1" exp $ ptOnSegment p0 q0 q1) 

        ptOnSegment_3 = let p0 = Vertex3 0.5001 0.5001 0 
                            q0 = Vertex3 0.5     0.5 0 
                            q1 = Vertex3 0.6     0.6    0
                            exp= InSeg 
                            in TestCase(assertEqual "ptOnSegment p0 p1 p1" exp $ ptOnSegment p0 q0 q1) 

        ptOnSegment_4 = let p0 = Vertex3 0.0     0.0 0 
                            q0 = Vertex3 0.5     0.5 0 
                            q1 = Vertex3 0.6     0.6    0
                            exp= OutSeg 
                            in TestCase(assertEqual "ptOnSegment p0 p1 p1" exp $ ptOnSegment p0 q0 q1) 

        test44 = let p0 = Vertex3 0.0 0.0 1.0 
                     q0 = Vertex3 0.0 0.0 0.0 
                     q1 = Vertex3 1.0 0.0 0.0 
                     q2 = Vertex3 0.0 1.0 0.0
                     exp= Vertex3 (0%1) (0%1) (0%1) 
                     in TestCase(assertEqual "perpPlane p0 p1 p1" exp $ fromJust $ perpPlane p0 q0 q1 q2) 

        test45 = let p0 = Vertex3 1.0 1.0 1.0 
                     q0 = Vertex3 1.0 0.0 0.0 
                     q1 = Vertex3 0.0 1.0 0.0 
                     q2 = Vertex3 0.0 0.0 1.0
                     exp= Vertex3 (1%3) (1%3) (1%3) 
                     in TestCase(assertEqual "perpPlane p0 p1 p1" exp $ fromJust $ perpPlane p0 q0 q1 q2) 

        test46 = let p0 = Vertex3 1.0 1.0 1.0 
                     p1 = Vertex3 0.0 0.0 0.0
                     ve = p1 -: p0
                     q0 = Vertex3 1.0 0.0 0.0 
                     q1 = Vertex3 0.0 1.0 0.0 
                     q2 = Vertex3 0.0 0.0 1.0
                     exp= Vertex3 (1%3) (1%3) (1%3) 
                     in TestCase(assertEqual "lineIntersectPlane p0 v2 q0 q1 q2" exp $ fromJust $ lineIntersectPlane p0 ve q0 q1 q2) 

        test47 = let v  = Vector3 1.0 1.0 1.0                                                                                    
                     exp= (2*pi*(45.0/360), 2*pi*(45.0/360))                                                                             
                     in TestCase(assertEqual "cartToPolar v" exp $ cartToPolar v) 
                                                                                                                                             
        test48 = let v  = Vector3 (sqrt 3) 1.0 1.0                                   
                     exp= (2*pi*(30.0/360), 2*pi*(30.0/360))                    
                     in TestCase(assertEqual "cartToPolar v" exp $ cartToPolar v)

        test49 = let v  = Vector3 (sqrt 3) 1.0 (sqrt 3)                               
                     exp= (2*pi*(30.0/360), 2*pi*(45.0/360))                     
                     in TestCase(assertEqual "cartToPolar v" exp $ cartToPolar v)

        test50 = let v  = [[0], [1], [0]]                          
                     u  = [[1], [1], [0]]
                     exp= [[0], [1], [0]]                                    
                     in TestCase(assertEqual "projn u v" exp $ projn u v)

        test51 = let  v  = [[0], [1], [0]]                                 
                      u  = [[0], [1], [0]]                                 
                      exp= [[0], [1], [0]]                                 
                      in TestCase(assertEqual "projn u v" exp $ projn u v)
        test52 = let  v  = [[0], [1], [0]]                                
                      u  = [[1], [0], [0]]                                
                      exp= [[0], [0], [0]]                                
                      in TestCase(assertEqual "projn u v" exp $ projn u v)

        test53 = let  v  = [[0], [1], [0]]                                                 
                      u  = [[0], [0], [1]]                                
                      exp= [[0], [0], [0]]                                
                      in TestCase(assertEqual "projn u v" exp $ projn u v)

        test54 = let  v  = [[0], [1], [0]]                                
                      u  = [[0], [(-1)], [0]]                                
                      exp= [[0], [(-1)], [0]]                                
                      in TestCase(assertEqual "projn u v" exp $ projn u v)

        test55 = let  v  = [[1], [1], [0]]                                
                      u  = [[0], [1], [0]]                             
                      exp= [[1/2], [1/2], [0]]                             
                      in TestCase(assertEqual "projn u v" exp $ projn u v)

        test56 = let  v  = [[-1], [1], [0]]                                
                      u  = [[0], [1], [0]]                                
                      exp= [[-(1/2)], [1/2], [0]]                            
                      in TestCase(assertEqual "projn u v" exp $ projn u v)

        test57 = let v  = Vector3 0 1 0                                 
                     u  = Vector3 1 1 0                                 
                     exp= Vector3 0 1 0                                 
                     in TestCase(assertEqual "projv u v" exp $ projv u v) 
                                                                        
        test58 = let v  = Vector3 0 1 0                                
                     u  = Vector3 0 1 0                                
                     exp= Vector3 0 1 0                                
                    in TestCase(assertEqual "projv u v" exp $ projv u v)
        test59 = let v  = Vector3 0 1 0                                
                     u  = Vector3 1 0 0                                 
                     exp= Vector3 0 0 0                                
                     in TestCase(assertEqual "projv u v" exp $ projv u v)
                                                                        
        test60 = let v = Vector3 0 1 0                                
                     u  = Vector3 0 0 1             
                     exp= Vector3 0 0 0                                
                     in TestCase(assertEqual "projv u v" exp $ projv u v)
                                                                        
        test61 = let v = Vector3 0 1 0                                
                     u = Vector3 0 (-1) 0                             
                     exp= Vector3 0 (-1) 0                             
                     in TestCase(assertEqual "projv u v" exp $ projv u v)
                                                                        
        test62 = let v = Vector3 1 1 0                                
                     u = Vector3 0 1 0 
                     exp= Vector3 (1/2) (1/2) 0                            
                     in TestCase(assertEqual "projv u v" exp $ projv u v)
                                                                        
        test63 = let v = Vector3 (-1) 1 0                               
                     u = Vector3 0  1 0                                
                     exp= Vector3 (-(1/2)) (1/2) 0                         
                     in TestCase(assertEqual "projv u v" exp $ projv u v)

        test64 = let p0 = Vertex3 1 0 0                                
                     p1 = Vertex3 0 0 0
                     p2 = Vertex3 0 1 0
                     exp= 1.0                       
                     in TestCase(assertEqual "projv u v" exp $ threePtDeterminant p0 p1 p2)

        test65 = let p0 = Vertex3 1 0 0                                                    
                     p1 = Vertex3 0 0 0                                                    
                     p2 = Vertex3 0 1 0                                                    
                     exp= -1.0                                                              
                     in TestCase(assertEqual "projv u v" exp $ threePtDeterminant p2 p1 p0)


        test66 = let p0 = Vertex3 1 0 0                                                    
                     p1 = Vertex3 0 0 0                                                    
                     p2 = Vertex3 0 1 0                                                    
                     exp= True                                                             
                     in TestCase(assertEqual "projv u v" exp $ threePtCW p0 p1 p2)


        test67 = let p0 = Vertex3 1 0 0                                           
                     p1 = Vertex3 0 0 0                                           
                     p2 = Vertex3 0 1 0                                           
                     exp= False                                                    
                     in TestCase(assertEqual "projv u v" exp $ threePtCW p2 p1 p0)


        test68 = let p0 = Vertex3 1 0 0                                            
                     p1 = Vertex3 0 0 0                                            
                     p2 = Vertex3 0 1 0                                            
                     exp= False                                                                                
                     in TestCase(assertEqual "projv u v" exp $ threePtCCW p0 p1 p2)

        test69 = let p0 = Vertex3 1 0 0                                            
                     p1 = Vertex3 0 0 0                                            
                     p2 = Vertex3 0 1 0                                            
                     exp= True                                                    
                     in TestCase(assertEqual "projv u v" exp $ threePtCCW p2 p1 p0)

        test70 = let p0 = Vertex3 1 0 0                                            
                     p1 = Vertex3 0 0 0                                            
                     p2 = Vertex3 0 1 0                                            
                     exp= False                                                     
                     in TestCase(assertEqual "projv u v" exp $ threePtCCW p0 p1 p2)



        -------------------------------------------------------------------------------- 
        test72 = let p0 = Vertex3 0.0 0.0 0.0 
                     p1 = Vertex3 1.0 0.0 0.0 
                     q0 = Vertex3 2.0 0.0 0.0 
                     q1 = Vertex3 3.0 0.0 0.0
                     exp= Colinear4 
                     in TestCase(assertEqual "fourPtColinear (p0, p1), (q0, q1)" exp $ fourPtColinear (p0, p1) (q0, q1)) 

        -- two segments are the same
        -- 
        test73 = let p0 = Vertex3 0.0 0.0 0.0 
                     p1 = Vertex3 1.0 0.0 0.0 
                     q0 = Vertex3 0.0 0.0 0.0 
                     q1 = Vertex3 1.0 0.0 0.0
                     exp= Colinear4 
                     in TestCase(assertEqual "fourPtColinear (p0, p1), (q0, q1)" exp $ fourPtColinear (p0, p1) (q0, q1)) 

        -- two endpoints are overlapped from different segments
        test74 = let p0 = Vertex3 0.0 0.0 0.0 
                     p1 = Vertex3 1.0 0.0 0.0 
                     q0 = Vertex3 1.0 0.0 0.0 
                     q1 = Vertex3 2.0 0.0 0.0
                     exp= Colinear4 
                     in TestCase(assertEqual "fourPtColinear (p0, p1), (q0, q1)" exp $ fourPtColinear (p0, p1) (q0, q1)) 
                     
        -- three endpoints are colinear only
        -- overlapped endpoints
        test75 = let p0 = Vertex3 0.0 0.0 0.0 
                     p1 = Vertex3 1.0 0.0 0.0 
                     q0 = Vertex3 1.0 0.0 0.0 
                     q1 = Vertex3 2.0 0.0001 0.0
                     exp= Colinear3 
                     in TestCase(assertEqual "fourPtColinear (p0, p1), (q0, q1)" exp $ fourPtColinear (p0, p1) (q0, q1)) 

        -- three endpoints are colinear only
        -- no overlapped endpoints
        test76 = let p0 = Vertex3 0.0 0.0 0.0 
                     p1 = Vertex3 1.0 0.0 0.0 
                     q0 = Vertex3 0.9 0.0 0.0 
                     q1 = Vertex3 2.0 0.00001 0.0
                     exp= Colinear3 
                     in TestCase(assertEqual "fourPtColinear (p0, p1), (q0, q1)" exp $ fourPtColinear (p0, p1) (q0, q1)) 

        -- four endpoints are colinear only
        -- no overlapped endpoints
        test77 = let p0 = Vertex3 0.0 0.0 0.0 
                     p1 = Vertex3 1.0 0.0 0.0 
                     q0 = Vertex3 0.001 0.0 0.0 
                     q1 = Vertex3 0.9 0.0 0.0
                     exp= Colinear4 
                     in TestCase(assertEqual "fourPtColinear (p0, p1), (q0, q1)" exp $ fourPtColinear (p0, p1) (q0, q1)) 

        test78 = let p0 = Vertex3 0.0 0.0 0.0 
                     p1 = Vertex3 1.0 0.0 0.0 
                     q0 = Vertex3 0.001 0.0001 0.0 
                     q1 = Vertex3 0.9 0.0001 0.0
                     exp= None 
                     in TestCase(assertEqual "fourPtColinear (p0, p1), (q0, q1)" exp $ fourPtColinear (p0, p1) (q0, q1)) 

        test79 = let p0 = Vertex3 0.0 0.0 0.0 
                     p1 = Vertex3 1.0 0.0 0.0 
                     q0 = Vertex3 0.001 0.0001 0.0 
                     q1 = Vertex3 0.9 0.002 0.0
                     exp= None 
                     in TestCase(assertEqual "fourPtColinear (p0, p1), (q0, q1)" exp $ fourPtColinear (p0, p1) (q0, q1)) 

        --------------------------------------------------------------------------------
        -- four pts are colinear 
        test80 = let p0 = Vertex3 0.0 0.0 0.0 
                     p1 = Vertex3 1.0 0.0 0.0 
                     q0 = Vertex3 1.0 0.0 0.0 
                     q1 = Vertex3 2.0 0.0 0.0 
                     exp= Nothing 
                     in TestCase(assertEqual "intersectSeg (p0, p1), (q0, q1)" exp $ intersectSeg (p0, p1) (q0, q1)) 

        test81 = let p0 = Vertex3 0.0 0.0 0.0 
                     p1 = Vertex3 1.0 0.0 0.0 
                     q0 = Vertex3 0.999 0.0 0.0 
                     q1 = Vertex3 2.0 0.0 0.0 
                     exp= Nothing 
                     in TestCase(assertEqual "intersectSeg (p0, p1), (q0, q1)" exp $ intersectSeg (p0, p1) (q0, q1)) 

        test82 = let p0 = Vertex3 (-0.001) (-0.001) 0.0 
                     p1 = Vertex3 1.001 1.001 0.0 
                     q0 = Vertex3 1.0 0.0 0.0 
                     q1 = Vertex3 0.0 1.0 0.0 
                     eps= 0.0001
                     exp= Vertex3 0.5 0.5 0.0 
                     val= fromJust $ intersectSeg (p0, p1) (q0, q1)
                     bo = True
                     in TestCase(assertEqual "intersectSeg (p0, p1), (q0, q1)" bo $ epVertex eps exp val) 

        test83 = let p0 = Vertex3 (-0.001) (-0.001) 0.0 
                     p1 = Vertex3 1.001 1.001 0.0 
                     q0 = Vertex3 1.0 0.0 0.0 
                     q1 = Vertex3 0.0 1.0 0.0 
                     eps= 0.0001
                     exp= Vertex3 0.5 0.5 0.0 
                     val= fromJust $ intersectSegNoEndPt (p0, p1) (q0, q1)
                     bo = True
                     in TestCase(assertEqual "intersectSegNoEndPt (p0, p1), (q0, q1)" bo $ epVertex eps exp val) 

        test84 = let p0 = Vertex3 0.0 0.0 0.0 
                     p1 = Vertex3 1.0 0.0 0.0 
                     q0 = Vertex3 1.0 0.0 0.0 
                     q1 = Vertex3 0.001 3.001 0.0 
                     exp= Nothing 
                     in TestCase(assertEqual "intersectSeg (p0, p1), (q0, q1)" exp $ intersectSegNoEndPt (p0, p1) (q0, q1)) 
                     
        test85 = let p0 = Vertex3 0.0 0.0 0.0 
                     p1 = Vertex3 1.0 0.0 0.0 
                     q0 = Vertex3 1.0 0.0 0.0 
                     q1 = Vertex3 1.0 1.0 0.0 
                     exp= Nothing 
                     in TestCase(assertEqual "intersectSeg (p0, p1), (q0, q1)" exp $ intersectSegNoEndPt (p0, p1) (q0, q1)) 

        test86 = let p0 = Vertex3 0.0 0.0 0.0 
                     p1 = Vertex3 1.0 0.0 0.0 
                     q0 = Vertex3 2.0 2.0 0.0 
                     q1 = Vertex3 1.0 0.0 0.0 
                     exp= Nothing 
                     in TestCase(assertEqual "intersectSeg (p0, p1), (q0, q1)" exp $ intersectSegNoEndPt (p0, p1) (q0, q1)) 
        -------------------------------------------------------------------------------- 

        test90 = let p0 = Vertex3 0 0 0
                     p1 = Vertex3 1 0 0
                     p2 = Vertex3 0 1 0
                     p3 = Vertex3 0.1 0.1 0
                     pts= [p0, p1, p2, p3]
                     n  = len pts
                     exp= [[p2, p1], [p1, p0], [p0, p2]]
                     in TestCase(assertEqual "convexHull (p0, p1), (q0, q1)" exp $ convexHull n pts) 

        test91 = let p0 = Vertex3 0 0 0
                     p1 = Vertex3 0 1 0
                     pts= [p0, p1]
                     n  = len pts
                     exp= [[p1, p0],[p0, p1]]
                     in TestCase(assertEqual "convexHull (p0, p1), (q0, q1)" exp $ convexHull n pts) 

        -------------------------------------------------------------------------------- 
        test92 = let p0= Vertex3 0.1 0.1 0                                            
                     a = Vertex3 1 0 0                                            
                     b = Vertex3 0 0 0                                            
                     c = Vertex3 0 1 0
                     exp= True                                                    
                     in TestCase(assertEqual "projv u v" exp $ fst $ ptInsideTri p0 (a, b, c))                            

        test93 = let p0= Vertex3 0.1 0.1 0                                            
                     a = Vertex3 1 0 0                                            
                     b = Vertex3 0 0 0                                            
                     c = Vertex3 0 1 0
                     exp= True                                                    
                     in TestCase(assertEqual "projv u v" exp $ fst $ ptInsideTri p0 (b, a, c))                            

        test94 = let p0= Vertex3 0.9 0.1 0                                            
                     a = Vertex3 0 0 0                                            
                     b = Vertex3 1 0 0                                            
                     c = Vertex3 1 0.5 0
                     exp= True                                                    
                     in TestCase(assertEqual "projv u v" exp $ fst $ ptInsideTri p0 (a, b, c))                            

        nonCrossSegmentNoEndPt_1 = let p0= Vertex3 0 0 0                                            
                                       p1 = Vertex3 0.5 0 0                                            
                                       p2 = Vertex3 0 0.5 0                                            
                                       q0 = Vertex3 1 1 0 
                                       ls = [(p0, p1), (p0, p2)]
                                       exp= sort [(p0, q0), (p1, q0), (p2, q0)]                                                    
                                       in TestCase(assertEqual "nonCrossSegmentNoEndPt_1 " exp $ sort $ nonCrossSegmentNoEndPt ls q0)                            

        nonCrossSegmentNoEndPt_2 = let p0= Vertex3 0 0 0                                            
                                       p1 = Vertex3 0.5 0 0                                            
                                       p2 = Vertex3 0 0.5 0                                            
                                       q0 = Vertex3 1 1 0 
                                       ls = [(p0, p1), (p1, p2)]
                                       exp= sort [(p1, q0), (p2, q0)]                                                    
                                       in TestCase(assertEqual "nonCrossSegmentNoEndPt_2 " exp $ sort $ nonCrossSegmentNoEndPt ls q0)

        onePtOverlappedSeg_1 = let p0 = Vertex3 0.0 0.0 0.0
                                   p1 = Vertex3 0.5 0.0 0.0
                                   q0 = Vertex3 0.0 0.0 0.0
                                   q1 = Vertex3 0.0 0.5 0.0
                                   exp= Just (Vertex3 0.0 0.0 0.0, (0.0, 0.0))
                                   in TestCase(assertEqual "onePtOverlappedSeg_1" exp $ onePtOverlappedSeg (p0, p1) (q0, q1))

        onePtOverlappedSeg_2 = let p0 = Vertex3 0.0 0.0 0.0
                                   p1 = Vertex3 0.5 0.0 0.0
                                   q0 = Vertex3 0.5 0.0 0.0
                                   q1 = Vertex3 0.0 0.5 0.0
                                   exp= Just (Vertex3 0.5 0.0 0.0, (1.0, 0.0))
                                   in TestCase(assertEqual "onePtOverlappedSeg_2" exp $ onePtOverlappedSeg (p0, p1) (q0, q1))

        onePtOverlappedSeg_3 = let p0 = Vertex3 0.0 0.0 0.0
                                   p1 = Vertex3 0.5 0.0 0.0
                                   q0 = Vertex3 0.5 0.7 0.0
                                   q1 = Vertex3 0.5 0.0 0.0
                                   exp= Just (Vertex3 0.5 0.0 0.0, (1.0, 1.0))
                                   in TestCase(assertEqual "onePtOverlappedSeg_3" exp $ onePtOverlappedSeg (p0, p1) (q0, q1))

        onePtOverlappedSeg_4 = let p0 = Vertex3 0.0 0.0 0.0
                                   p1 = Vertex3 0.5 0.0 0.0
                                   q0 = Vertex3 0.5 0.7 0.0
                                   q1 = Vertex3 0.0 0.0 0.0
                                   exp= Just (Vertex3 0.0 0.0 0.0, (0.0, 1.0))
                                   in TestCase(assertEqual "onePtOverlappedSeg_4" exp $ onePtOverlappedSeg (p0, p1) (q0, q1))

        tests = TestList [
                        TestLabel "test1" test1,
                        TestLabel "test2" test2,
                        TestLabel "test3" test3,
                        TestLabel "test4" test4,
                        TestLabel "test5" test5,
                        TestLabel "test6" test6,
                        TestLabel "trytest" trytest,
                        TestLabel "test7" test7,
                        TestLabel "test8" test8,
                        TestLabel "test9" test9,
                        TestLabel "test10" test10,
                        TestLabel "test11" test11,
                        TestLabel "test12" test12,
                        TestLabel "test13" test13,
                        TestLabel "test14" test14,
                        TestLabel "test15" test15,
                        TestLabel "test16" test16,
                        -- TestLabel "test17" test17,
                        TestLabel "test20" test20,
                        TestLabel "test21" test21,
                        TestLabel "test22" test22,
                        TestLabel "test23" test23,
                        TestLabel "test24" test24,
                        TestLabel "test241" test241,
--                            TestLabel "test25" test25,
                        TestLabel "test27" test27,
                        TestLabel "test26" test26,
                        TestLabel "test28" test28,
                        TestLabel "test29" test29,
                        TestLabel "test30" test30,
                        TestLabel "test31" test31,
                        TestLabel "test32" test32,
                        TestLabel "test33" test33,
                        TestLabel "test34" test34,
                        TestLabel "test35" test35,
                        TestLabel "test36" test36,
                        TestLabel "test37" test37,
                        TestLabel "test38" test38,
                        TestLabel "test39" test39,
                        TestLabel "test40" test40,
                        TestLabel "test41" test41,
                        TestLabel "test42" test42,
                        TestLabel "test43" test43,
                        TestLabel "test44" test44,
                        TestLabel "test45" test45,
                        TestLabel "test46" test46,
                        TestLabel "test47" test47,
                        TestLabel "test48" test48,
                        TestLabel "test49" test49,
                        TestLabel "test50" test50,
                        TestLabel "test51" test51,
                        TestLabel "test52" test52,
                        TestLabel "test53" test53,
                        TestLabel "test54" test54,
                        TestLabel "test55" test55,
                        TestLabel "test56" test56,
                        TestLabel "test57" test57,
                        TestLabel "test58" test58,
                        TestLabel "test59" test59,
                        TestLabel "test60" test60,
                        TestLabel "test61" test61,
                        TestLabel "test62" test62,
                        TestLabel "test63" test63,
                        TestLabel "test64" test64,
                        TestLabel "test65" test65,
                        TestLabel "test66" test66, 
                        TestLabel "test67" test67,                             
                        TestLabel "test68" test68,                             
                        TestLabel "test69" test69,
                        TestLabel "test70" test70,
                        TestLabel "test73" test73,
                        TestLabel "test74" test74,
                        TestLabel "test75" test75,
                        TestLabel "test76" test76,
                        TestLabel "test77" test77,
                        TestLabel "test78" test78,
                        TestLabel "test79" test79,
                        TestLabel "test80" test80,
                        TestLabel "test81" test81,
                        TestLabel "test82" test82,
                        TestLabel "test83" test83,
                        TestLabel "test84" test84,
                        TestLabel "test85" test85,
                        TestLabel "test86" test86,

                        TestLabel "test90" test90,
                        TestLabel "test91" test91,
                        TestLabel "test92" test92,
                        TestLabel "test93" test93,
                        TestLabel "test94" test94,
                        TestLabel "ptOnSegment_1" ptOnSegment_1,
                        TestLabel "ptOnSegment_2" ptOnSegment_2,
                        TestLabel "ptOnSegment_3" ptOnSegment_3,
                        TestLabel "ptOnSegment_4" ptOnSegment_4,
                        TestLabel "nonCrossSegmentNoEndPt_1" nonCrossSegmentNoEndPt_1,
                        TestLabel "nonCrossSegmentNoEndPt_2" nonCrossSegmentNoEndPt_2,
                        TestLabel "onePtOverlappedSeg_1" onePtOverlappedSeg_1,
                        TestLabel "onePtOverlappedSeg_2" onePtOverlappedSeg_2,
                        TestLabel "onePtOverlappedSeg_3" onePtOverlappedSeg_3,
                        TestLabel "onePtOverlappedSeg_4" onePtOverlappedSeg_4,
                        TestLabel "intersectLine2_1"     intersectLine2_1,
                        TestLabel "intersectLine2_2"     intersectLine2_2,
                        TestLabel "intersectLine2_3"     intersectLine2_3,
                        TestLabel "intersectLine2_4"     intersectLine2_4

                        ] 
main = do
        allTests 
        print "done" 

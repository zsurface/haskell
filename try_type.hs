import Control.Monad
import Data.Char
import Data.List
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 
import AronModule 


-- | class (Num a, Ord a)=>Fractional a where
-- |
-- | parse class signature to follwoing 
-- | => [(Num, Ord), (Fractional)]

fun::(Integral a)=>a->Integer
fun x = toInteger(x)^2 
path = "/Users/cat/try/classfile.txt"
main = do 
        print "Hello World"
        argList <- getArgs
        pp $ length argList
        pp $ fun 3 
        line <- readFileToList path 
        fw "line"
        pp line
        fl
        let spl = filter(matchTest(mkRegex "^[[:space:]]*class[[:space:]]+")) line
        let sp = map(splitRegex(mkRegex "=>")) spl 
        fpp "sp" sp
        --let spp = (map.map)(splitRegex(mkRegex "\\s*class\\s*|\\s+where")) sp
        let spp = map(\x ->filter(matchTest(mkRegex "^[[:space:]]*class[[:space:]]+\\(|where"))x) sp
        fpp "spp" spp
        let spk = filter(\x -> length x > 0) spp
        fpp "spk" spk
        let spe = (map.map)(\x -> trim $ subRegex(mkRegex "class|where|[[:space:]]+[a]") x "") spk 
        fpp "spe" spe
--        let list = (\x -> length x > 0) $ (join) spp
--        fl
--        pp sp
--        fl
--        fl
--        pp list
--        pp "done!"

This text is a literate comment.
This is an example program that prints the 11th Fibonacci number:
This is also a comment.  When we use this style of literate programming,
we have to separate code and comments with a blank line.

\begin{code}

    ff::Int
    ff = 3
    main = print "dog" 

\end{code}

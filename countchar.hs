-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import qualified Data.HashMap.Strict as M 
import qualified Text.Regex.TDFA as TD

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}


-- zo - open
-- za - close

import AronModule 

--  (path : _) <- getArgs
--  h <- openFile path ReadMode
--  hSetEncoding h latin1
--  contents <- hGetContents h 

-- count char from a file
-- generate command in $sym/countchar.sh 
countChar::[(Char, Integer)] -> M.HashMap Char Integer -> M.HashMap Char Integer
countChar [] m = m
countChar (x:cx) m = countChar cx nm
    where
        k = fst x
        c = snd x
        nm = case M.lookup k m of
                Just n -> M.insert k (n+c) m
                Nothing -> M.insert k c m 

main = do 
        argList <- getArgs
        if len argList == 1 then do
            let cmap = M.empty
            -- list <- readFileToList $ head argList 
            list <- readFileLatin1 $ head argList 
            let tu = join $ map(\x -> map(\c -> (c, 1)) x) list
            let newmap = countChar tu cmap
            let mlist = M.toList newmap 
            let sortlsl = let f a b = (fst a) < (fst b) in qqsort f mlist  
            let sl = map(\x -> (show $ fst x) ++ " -> " ++ (show $ snd x)) sortlsl 
            paa sl 
            pp $ "size=" <<< (len sl)
        else do
            pp " Count the number of char in a file"
            pp " Need a file name"

import Control.Monad
import Data.Char
import Data.List
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import AronModule 
------------------------------------------------------------------
-- KEY: Combine and simplify commands 
-- | Sat Aug 25 13:46:13 2018  
-- | todo: add tar -xf file.tar.xz
------------------------------------------------------------------ 
--grep::String->String
--grep s = "grep --color=always --include=\"" ++ s ++ "\" -Hnris " 

grep2::String->String->String
grep2 f s = "grep --color=always --include=\"" ++ f ++ "\" -Hnris -A 5 " ++ s ++ " . "

grepProcess::String->String->IO()
grepProcess s1 s2 = do
                    (Nothing, Just hout, Nothing, ph) <- createProcess p
                    ec <- waitForProcess ph
                    out <- hGetContents hout 
                    mapM_ putStrLn $ lines out
                    where 
                        p = (shell $ grep2 s1 s2 )
                            { std_in  = Inherit
                            , std_out = CreatePipe
                            , std_err = Inherit
                            }

findProcess::String->IO()
findProcess s = do 
                (Nothing, Just hout, Nothing, ph) <- createProcess p
                ec <- waitForProcess ph
                out <- hGetContents hout 
                mapM_ putStrLn $ lines out
                where 
                    p = (shell cmd)
                        { std_in  = Inherit
                        , std_out = CreatePipe
                        , std_err = Inherit
                        }
                    cmd = "/usr/bin/find $PWD -iname \"" ++ s ++ "\" -print"

myhelp::IO()
myhelp = do 
        pp "------------------------------------------                                                "
        pp "cmd [zip]  [foo]                     => zip -r foo.zip foo                                "
        pp "cmd [zip]  [foo] [foo.zip]           => zip -r foo.zip foo                                "
        pp "cmd [gz]   [file.txt]                => gzip foo => foo.gz                                "
        pp "cmd [tar]  [file.txt]                => tar -czvf foo.tar.gz                              "
        pp "------------------------------------------                                                "
        pp "cmd uzip file.txt.zip                => unzip foo.zip foo                                 "
        pp "cmd ugz  file.txt.gz                 => gunzip foo.gz foo                                 "
        pp "cmd utar file.txt.tar.gz             => file.txt                                          "
        pp "------------------------------------------                                                "
        pp "cmd grep '*.hs' pattern              => grep --color --include=\"*.hs\"   -Hnris pattern ."
        pp "cmd grep h pattern                   => grep --color --include=\"*.hs\"   -Hnris pattern ."
        pp "cmd grep j pattern                   => grep --color --include=\"*.java\" -Hnris pattern ."
        pp "------------------------------------------                                                "
        pp "cmd find '*.hs'                      => find -iname \"*.hs\" -print                       "
        pp "cmd find '*profile*'                 => find file name contains 'profile'                 "
        pp "cmd find a1                          => find -iname [\"*.hs\"] -print                     "
        pp "cmd ssh                              => ssh-keygen -C noname                              "
        pp "------------------------------------------"

shellCmd::[String] -> IO () 
shellCmd x = case x of 
        [op]     -> case op of
                         "h"  -> myhelp
                         "k"  -> createProcess (proc "ssh-keygen" ["-C", "noname"]){ cwd = Just "." } >>= \_ -> return ()
                         "ssh" -> do
                                    print cmd
                                    (Nothing, Just hout, Nothing, ph) <- createProcess p
                                    out <- hGetContents hout 
                                    mapM_ putStrLn $ lines out
                                    where 
                                        p = (shell cmd)
                                            { std_in  = Inherit
                                            , std_out = CreatePipe
                                            , std_err = Inherit
                                            }
                                        cmd = "ssh-keygen -C noname"
                         _    -> pp "Invalid option"
        [op, a1] -> case op of
                         "zip" -> createProcess (proc "/usr/bin/zip"    ["-r", (a1 ++ ".zip"), a1]){ cwd = Just "." } >>= \_ -> return () 
                         "gz"  -> createProcess (proc "/usr/bin/gzip"   [a1]){ cwd = Just "." } >>= \_ -> return () 
                         "tar" -> createProcess (proc "/usr/bin/tar"    ["-czvf", (a1 ++ ".tar.gz"), a1]){ cwd = Just "." } >>= \_ -> return () 
                         "utar"-> createProcess (proc "/usr/bin/tar"    ["-xzvf", a1]){ cwd = Just "." } >>= \_ -> return () 
                         "uzip"-> createProcess (proc "/usr/bin/unzip"  [a1]){ cwd = Just "." } >>= \_ -> return () 
                         "ugz" -> createProcess (proc "/usr/bin/gunzip" [a1]){ cwd = Just "." } >>= \_ -> return () 
                         "find"-> do
                                    findProcess a1
                         _     -> print $ "[" ++ op ++ "][" ++ a1 ++ "]"

        [op, a1, a2] -> case op of
                 "zip" -> createProcess (proc "/usr/bin/zip" ["-r", a2, a1]){ cwd = Just "." } >>= \_ -> return () 
                 "grep"-> do
                            case a1 of
                                "h" -> do 
                                        grepProcess "*.hs" a2
                                "j" -> do 
                                        grepProcess "*.java" a2
                                _   -> do 
                                        grepProcess a1 a2
                 _     -> print $ "[" ++ op ++ "][" ++ a1 ++ "][" ++ a2 ++ "]"

        _ -> myhelp 

main =  do
        argList <- getArgs
        curr <- getPwd 
        cd curr
        pp curr
        s <- shellCmd argList
        pp s

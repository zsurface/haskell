import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

import AronModule 

fun::(Num a)=>[a] -> [a]-> [[a]]
fun x y = map(\x1 -> map(\y1 -> x1*y1) y) x

outer'::(Num a) => [[a]] -> [[a]] ->[[[[a]]]]
outer' vx rx = map(\v -> map(\r -> fun v r) rx) vx

re::(Num a)=>[[a]] ->Int->[[a]]
re [] _     = []
re (x:xs) n = ((replicate (len - n) 0) ++ x ++ (replicate n 0)) : re xs (n+1)
        where 
            len = length (x:xs) 

ct::(Integer, Integer)->(Integer, Integer)->(Integer, Integer)
ct  (a1, b1) (a2, b2) = (div s 10, if s > 9 then b2 + (rem s 10) else b2)
                  where
                    s = b1 + a2

main = do 
        print "Hello World"
        argList <- getArgs
        pp $ length argList
        pa $ fun [2, 10] [1, 3, 4]
        fl
        let vx = [
                    [2],
                    [5]
                 ]
        let len = length vx
        let rx = [[1, 3, 4]]
        let ten = outer' (reverse vx) rx
        let m = re mat 0
        let ma = (join . join) ten
        pa $ ma
        fl
        fpp "vx" vx
        fl
        fpp "rx" rx
        fl
        let ten' = re ma 0
        let tent = L.transpose ten'
        let summ = map(\x -> sum x) tent
        pp summ 
        pa $ ten' 
        fl
        let  c = ct (9, 9) (9, 9)
        pp "done!"

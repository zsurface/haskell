#!/usr/bin/env stack
-- stack --resolver lts-9.0 script
import System.Exit

main = exitWith (ExitFailure 42)

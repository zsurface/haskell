#!/usr/bin/env stack
-- stack script --resolver lts-13.28
  
{-# LANGUAGE OverloadedStrings #-}
import           Data.Aeson            (Value)
import qualified Data.ByteString.Char8 as S8
import qualified Data.Yaml             as Yaml
import           Network.HTTP.Simple
                 
--
-- KEY: stack script
--
main :: IO ()
main = do
    -- response <- httpJSON "POST http://httpbin.org/post"
    response <- httpJSON "POST http://127.0.0.1:8080/snippet?id=s vanco"

    putStrLn $ "The status code was: " ++
               show (getResponseStatusCode response)
    print $ getResponseHeader "Content-Type" response
    S8.putStrLn $ Yaml.encode (getResponseBody response :: Value)

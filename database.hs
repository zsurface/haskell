{-# LANGUAGE LambdaCase #-}
module Main where

import           Control.Monad         (forever)
import qualified Database.MySQL.BinLog as MySQL
import qualified System.IO.Streams     as Streams

main :: IO () 
main = do
    conn <- MySQL.connect 
        MySQL.defaultConnectInfo
          { MySQL.ciUser = "username"
          , MySQL.ciPassword = "password"
          , MySQL.ciDatabase = "dbname"
          }
    MySQL.getLastBinLogTracker conn >>= \ case
        Just tracker -> do
            es <- MySQL.decodeRowBinLogEvent =<< MySQL.dumpBinLog conn 1024 tracker False
            forever $ do
                Streams.read es >>= \ case
                    Just v  -> print v
                    Nothing -> return ()
        Nothing -> error "can't get latest binlog position"

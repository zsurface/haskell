{-# LANGUAGE OverloadedStrings #-}
import Control.Applicative ((<$>))
import Control.Monad
import Network.Wai
import Network.Wai.Handler.Warp
import Network.HTTP.Types (status200)
import Network.HTTP.Types.Header (hContentType)
import Blaze.ByteString.Builder.Char.Utf8 (fromString)
import Data.ByteString (ByteString)

main = do
    let port = 3000
    putStrLn $ "Listening on port " ++ show port
    run port app

app req f = f $
    case pathInfo req of
        -- Place custom routes here
        _ -> anyRoute req

anyRoute req =
    let query = queryString req :: [(ByteString, Maybe ByteString)]
        idParam = join $ lookup "id" query :: Maybe ByteString
    in responseBuilder
            status200
            [(hContentType, "text/plain")]
            $ fromString $ "Query parameter: " ++ (show idParam)

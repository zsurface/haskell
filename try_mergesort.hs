-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}  --
-- 
-- import Turtle                       --
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".

import AronModule 

(+++:)::(Ord a)=>[a]->[a]->[a]
(+++:) xs [] = xs 
(+++:) [] ys = ys 
(+++:) (x:xs) (y:ys) = if x <= y then x:(xs +++: (y:ys)) else y:((x:xs) +++: ys)

main = do 
        print "Hello World"
        pp $ [] +++: [1] == [1]  -- left identity
        pp $ [2] +++: [] == [2]  -- right identity
        pp $ [2] +++: [1] == [1, 2] -- a, b \in S, then a `op` b \in S  
        pp $ [1, 2] +++: [0, 1, 9, 10] == [0, 1, 1, 2, 9, 10] 
        -- associativity
        pp $ ([1, 2] +++: [0, 1] +++: [4, 5]) == ([1, 2] +++: ([0, 1] +++: [4, 5])) 
        pp "done!"

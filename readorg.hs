-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE QuasiQuotes #-}
import Text.RawString.QQ         -- Need QuasiQuotes too 
-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import qualified Text.Regex.TDFA as TD
import qualified Data.ByteString as BS
import qualified Data.ByteString.UTF8 as BSU
import qualified Data.ByteString.Lazy.Char8 as LC8 
import qualified Data.ByteString.Internal  as BSI (c2w) 

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName => "file.ext"
--    takeDirectory => "/directory"
--    takeExtension => ".ext"
--    dropExtension => "/directory/file"
--    takeBaseName => "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

import AronModule 

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close

fstLen::(a -> a) -> (a, b) -> (a, b)
fstLen f (a, b) = (f a, b)

org=[r|#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="http://thomasf.github.io/solarized-css/solarized-dark.min.css" />|]

-- Add solarized theme to Org-mode file at the beginning.
-- 

c2BS = charToStrictByteString

main = do 
        let orgbs = BSU.fromString org -- BS.ByteString
        home <- getEnv "HOME"
        let testp = home </> "myfile/bitbucket/testfile/org_mode"
        ht <- getEnv "ht"
        pp ht
        lss <- lsRegexFull testp "\\.org$"
        pa lss
        lsbs <- mapM (\x -> BS.readFile x >>= \s -> return (s, x)) lss  -- [(BS.ByteString, path)]
        let ttls = map (\x -> (BS.break ((==) $ BSI.c2w '\n') $ fst x, snd x)) lsbs
        let tts = filter(\x -> not $ matchTest (makeRegex "HTML_HEAD"::Regex) ((fst . fst) x) ) ttls -- [((BS, BS), String)]
        print tts
        let ttm = map(\x -> (orgbs <> c2BS '\n' <> ((fst . fst) x) <> ((snd . fst) x), snd x)) tts
        mapM_ (\x -> BS.writeFile (snd x) (fst x)) ttm
        let lf = map(\x -> snd x) ttm
        print lf 
        pp "done!"

{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE Rank2Types #-}
{-# LANGUAGE FlexibleContexts #-}
module Graphics.Liveplot.Window where

import Prelude hiding (init)
import Control.Applicative
import Control.Monad
import Control.Monad.Trans.State.Strict as State
import Data.IORef
import Data.Maybe (isNothing)
import Data.Set (Set)
import qualified Data.Set as S
import Data.Time.Clock
import Graphics.UI.GLFW
import Linear
-- moi
import MVC
import qualified MVC.Prelude as MVC
import qualified Pipes.Prelude as Pipes
import Graphics.Rendering.OpenGL
import Graphics.GLUtil.Camera2D
import Data.Vinyl
import Graphics.VinylGL

import Control.Concurrent (threadDelay)

import Pipes.Misc (buffer)
import Control.Concurrent.MVar

import Graphics.GLUtil.Textures (IsPixelData)

import Graphics.Liveplot.Types
import Graphics.Liveplot.Utils
import Graphics.Liveplot.Line

data Event =
    Timestep Double
  | Keys     (Set Key)
  | Buttons  (Set MouseButton)
  | MousePos (V2 Double)
  | WinSize  (V2 Int)
  | Quit
  deriving Show

type GLfloats = [GLfloat]
type Data = GLfloats

data Sensor = ADC | ADC2 | ADC3 | ADC4 | ADC5 | ADC6 | DAC
  deriving (Eq, Show)

data SensorReading = Reading Sensor Data
  deriving (Eq, Show)

type GraphInit a = (IsPixelData a) => Managed (Pipe Data SensorReading IO (),
                   (SensorReading -> IO (), MVar [GLfloat],
                    IO (Maybe a -> AppInfo -> IO ())))

type TotalInput = ((Maybe Data), AppInfo)
type TotalOutput = Either Data Event

metric :: Sensor -> Pipe Data SensorReading IO ()
metric m = Pipes.map (\x -> Reading m x)

rpad :: Int -> a -> [a] -> [a]
rpad n x xs = xs ++ (take (n-(length xs)) $ repeat x)

-- add scroll input callback
-- http://www.glfw.org/docs/latest/input_guide.html#scrolling
ogl :: [(SensorReading -> IO b, MVar a,
         IO (Maybe a -> AppInfo -> IO c))]
       -> Managed (View (Maybe SensorReading, AppInfo), Controller Event)

ogl parts = join $ managed $ \k -> do
  let simpleErrorCallback e s = putStrLn $ unwords [show e, show s]
  let width = 600
      height = 300
      windowTitle = "lala"
  setErrorCallback $ Just simpleErrorCallback
  r <- init
  when (not r) (error "Error initializing GLFW!")

  windowHint $ WindowHint'ClientAPI ClientAPI'OpenGL
  windowHint $ WindowHint'OpenGLForwardCompat True
  windowHint $ WindowHint'OpenGLProfile OpenGLProfile'Core
  windowHint $ WindowHint'ContextVersionMajor 3
  windowHint $ WindowHint'ContextVersionMinor 2

  m@(~(Just w)) <- createWindow width height windowTitle Nothing Nothing
  when (isNothing m) (error "Couldn't create window!")

  makeContextCurrent m

  kbState <- newIORef S.empty
  mbState <- newIORef S.empty
  mpState <- getCursorPos w >>= newIORef . uncurry V2
  wsState <- getWindowSize w >>= newIORef . uncurry V2
  lastTick <- getCurrentTime >>= newIORef
  setKeyCallback w (Just $ keyCallback kbState)
  setMouseButtonCallback w (Just $ mbCallback mbState)
  setCursorPosCallback w (Just $ mpCallback mpState)
  setWindowSizeCallback w $ Just $ \win x y -> do
    wsCallback wsState win x y
    viewport $= (Position 0 0, Size (fromIntegral x) (fromIntegral y))

  blend $= Enabled
  blendFunc $= (SrcAlpha, OneMinusSrcAlpha)

  redraw <- newEmptyMVar
  locai <- newEmptyMVar
  replaceMVar locai appinfo
  let (storefns, mvars, setupfns) = unzip3 parts
  drawfns <- mapM (>>= return) setupfns

  let tick = do pollEvents
                t <- getCurrentTime
                dt <- realToFrac . diffUTCTime t <$> readIORef lastTick
                writeIORef lastTick t

                keys <- readIORef kbState
                buttons <- readIORef mbState
                pos <- readIORef mpState
                wsize <- readIORef wsState

                mredraw <- tryTakeMVar redraw
                case mredraw of
                  Nothing -> threadDelay 10000 >> return ()
                  Just _ -> do
                    ai <- readMVar locai
                    clear [ColorBuffer, DepthBuffer]
                    zipWithM_ (\draw mv -> do
                      dat <- readMVar mv
                      draw (Just dat) ai) drawfns mvars
                    swapBuffers w

                -- XXX: emit only changing values except for timestep
                return $ [
                    Timestep dt
                  , Keys keys
                  , Buttons buttons
                  , MousePos pos
                  , WinSize wsize
                  ]

  -- main drawing function
  let handleApp :: View ((Maybe SensorReading, AppInfo))
      handleApp = asSink $ \(mdat, ai) -> do
        replaceMVar locai ai
        case mdat of
          Nothing -> return ()
          Just dat -> do
            replaceMVar redraw True
            mapM_ ($ dat) storefns

  let steptick :: Producer Event IO ()
      steptick = forever $ lift tick >>= mapM_ yield

  k $ do
    -- Event producer
    evts <- MVC.producer (bounded 1) (steptick)

    return (handleApp, evts)

 -- | Translate and rotate a 'Camera' based on 'UI' input.
moveCam :: (Conjugate a, Epsilon a, RealFloat a) => Set Key -> Camera a -> Camera a
moveCam keys = cnfEndo S.member S.delete 
                  [ ([shift, [Key'Left]], roll na)
                  , ([shift, [Key'Right]], roll pa)
                  , ([[Key'Left]], track (V2 np 0))
                  , ([[Key'Right]], track (V2 pp 0))
                  , ([[Key'Up]], track (V2 0 pp))
                  , ([[Key'Down]], track (V2 0 np)) ]
                  keys
  where shift = [Key'LeftShift, Key'RightShift]
        -- XXX: pass timeScale as well? (Normalize speeds to 60Hz update)
        timeScale = 1 -- realToFrac $ timeStep ui * 60
        pp = 0.08 * timeScale -- 1D speed
        np = negate pp
        pa = 2 * timeScale    -- angular step
        na = negate pa

cam :: Camera GLfloat
cam = camera2D
appinfo :: AppInfo
appinfo = SField =: camMatrix cam

-- transform AppInfo and camera according to events from OpenGL
-- XXX: emit events as well
-- we can probably split this from data
campipe :: (Monad m)
        => AppInfo
        -> Camera GLfloat
        -> MVC.Proxy () (Either a Event) () (Maybe a, AppInfo) m b
campipe ai c = do
  msg <- await
  case msg of
    (Left dat) -> yield (Just dat, ai) >> campipe ai c
    (Right evt) ->
      case evt of
        --Timestep x -> lift $ yield appinfo -- XXX: wrong, we should store appinfo
        Keys k | k == S.empty -> campipe ai c
        Keys k -> do
          let newCam = moveCam k c
          let ni = SField =: (camMatrix newCam)
          campipe ni newCam
          --lift $ yield (Nothing, ni)
        _ -> campipe ai c

rmvc :: IO()
rmvc = runMVC () (asPipe (campipe (SField =: (camMatrix cam)) cam)) withgl

withgl :: Managed (View (Maybe SensorReading, AppInfo), Controller (Either SensorReading Event))
withgl = do
  let pts = (1000 :: Int)
      res = (1000 :: Int)
      pRes = (500 :: Int)
  (l1in, l1setup) <- lineGraph ADC pts res pRes
  (l2in, l2setup) <- lineGraph ADC2 pts res pRes
  (l3in, l3setup) <- lineGraph ADC3 pts res pRes
  (l4in, l4setup) <- lineGraph ADC4 pts res pRes
  (l5in, l5setup) <- lineGraph ADC5 pts res pRes
  (l6in, l6setup) <- lineGraph ADC6 pts res pRes

  (v, c) <- ogl [l1setup, l2setup, l3setup, l4setup, l5setup, l6setup]

  dat <- MVC.producer (bounded 1) (datapipe 5 10 >-> l1in)
  dat2 <- MVC.producer (bounded 1) (datapipe 100 10>-> l2in)
  dat3 <- MVC.producer (bounded 1) (datapipe 200 31 >-> l3in)
  dat4 <- MVC.producer (bounded 1) (datapipe 2000 27 >-> l4in)
  dat5 <- MVC.producer (bounded 1) (datapipe 10000 13 >-> l5in)
  dat6 <- MVC.producer (bounded 1) (datapipe 3000 2 >-> Pipes.map (map ((+0.7).(/4))) >-> l6in)
  return (v, fmap Left (dat <> dat2 <> dat3 <> dat4 <> dat5 <> dat6) <> fmap Right c)

--- XXX: generalize to wrapGraph
lineGraph :: Sensor
             -> Int
             -> Int
             -> Int
             -> GraphInit [GLfloat]
lineGraph s npoints res pres = do
  mv <- liftIO $ newEmptyMVar
  return (metric s, (toMVar s mv, mv, line npoints res pres))

toMVar :: Sensor -> MVar [GLfloat] -> SensorReading -> IO ()
toMVar s mv (Reading cs val) | s == cs = replaceMVar mv val
toMVar _ _ _ = return ()

datapipe :: Float -> Float -> Producer [GLfloat] IO ()
datapipe hz divider =
  dattick hz
  -- moire madness
  -- >-> Pipes.map ((/2). (+1) . (sin) . (fromIntegral) . (`mod` 1024))
  >-> Pipes.map ((/2). (+1) . (sin) . (/divider))
  >-> buffer 1000 []
  >-> Pipes.map (rpad 1000 0.0)

dattick :: (Num t, RealFrac t1) => t1 -> MVC.Proxy x' x () t IO b
dattick hz = run 0
  where
    run n = yield n >> (lift $ threadDelay $ floor $ 1000000/hz) >> run (n+1)

keyCallback :: IORef (Set Key) -> KeyCallback
keyCallback keys _w k _ KeyState'Pressed _mods = modifyIORef' keys (S.insert k)
keyCallback keys _w k _ KeyState'Released _mods = modifyIORef' keys (S.delete k)
keyCallback _ _ _ _ _ _ = return ()

mbCallback :: IORef (Set MouseButton) -> MouseButtonCallback
mbCallback mbs _w b MouseButtonState'Pressed _ = modifyIORef' mbs (S.insert b)
mbCallback mbs _w b MouseButtonState'Released _ = modifyIORef' mbs (S.delete b)

mpCallback :: IORef (V2 Double) -> CursorPosCallback
mpCallback mp _w x y = writeIORef mp (V2 x y)

wsCallback :: IORef (V2 Int) -> WindowSizeCallback
wsCallback ws _w w h = writeIORef ws (V2 w h)

-- | `tryTakeMVar` then `putMVar`
replaceMVar :: MVar a -> a -> IO ()
replaceMVar mv val = do
    tryTakeMVar mv
    putMVar mv val

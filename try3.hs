import AronModule

ll = [ 
        [1..3],
        [4..6],
        [7..9]
    ]

ff::Integer->Integer->[[Integer]]->[[[Integer]]]
ff r c [] = []
ff r c m  = if r >= 0 then removeRowCol (r-1) (c-1) m else m:[] 

main =  do 
        print "h" 
        mapM_ print ll
        let list = [ (n+1):x | x <- ll, let n = 1]
        pa $ removeRowCol 0 0 ll
        print $ dim ll

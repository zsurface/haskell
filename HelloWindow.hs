import Graphics.UI.GLUT as L
import Graphics.Rendering.OpenGL 

main = do
    L.getArgsAndInitialize
    L.createWindow "Hello Window"
    L.mainLoop

createAWindow windowName = do
    L.createWindow windowName
    displayCallback Graphics.Rendering.OpenGL.$= clear[ColorBuffer]

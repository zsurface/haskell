module Main where

import Control.Monad (unless, when)
import Graphics.Rendering.OpenGL as GL 
import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
import qualified Graphics.UI.GLFW as G
import System.Exit
import System.IO
import Control.Monad
import System.Random
import Data.Set(Set) 
import Data.IORef 
import qualified Data.Set as S 
import Linear.V3
import Linear.V3(cross) 
import Linear.Vector
import Linear.Matrix
import Linear.Projection as P
import Linear.Metric(norm, signorm)

import AronModule
import AronGraphic


-- | Draw line from (x0, x1) and tangent at (c, f(c))
-- | /Library/WebServer/Documents/zsurface/image/tangleline.png
-- | -------------------------------------------------------------------------------- 
-- | 1. Given a function f, and x0 on x-Axis
-- | 2. Derive a tangent line at (c, f(c)) with slop = f'(c)
-- | 3. Interpolate (x0, f(x0)) and (x1, f(x1))  
-- | -------------------------------------------------------------------------------- 
tangent::(GLfloat->GLfloat)->(GLfloat, GLfloat)->GLfloat->[(GLfloat, GLfloat, GLfloat)]
tangent f (x0, x1) c = lineInterpolate p0 p1
        where
            y0 = tangent f x0 c 
            y1 = tangent f x1 c 
            p0 = (x0, y0, 0)
            p1 = (x1, y1, 0)

renderCoordinates::IO()
renderCoordinates = do
    renderPrimitive Lines $ do
        -- x-Axis
        color  (Color3 1 0 0 :: Color3 GLdouble)
        vertex (Vertex3 (-1) 0 0 ::Vertex3 GLdouble)
        color  (Color3 1 0 0 :: Color3 GLdouble)
        vertex (Vertex3 1 0 0 :: Vertex3 GLdouble)
        -- y-Axis
        color  (Color3 0 1 0 :: Color3 GLdouble)
        vertex (Vertex3 0 (-1) 0 :: Vertex3 GLdouble)
        color  (Color3 0 1 0 :: Color3 GLdouble)
        vertex (Vertex3 0 1 0 :: Vertex3 GLdouble)
        -- z-Axis
        color  (Color3 0 0 1 :: Color3 GLdouble)
        vertex (Vertex3 0 0 (-1) :: Vertex3 GLdouble)
        color  (Color3 0 0 1 :: Color3 GLdouble)
        vertex (Vertex3 0 0 1 :: Vertex3 GLdouble)

renderTriangle::IO()
renderTriangle = do
    renderPrimitive Triangles $ do
        color  (Color3 1 0 0 :: Color3 GLdouble)
        vertex (Vertex3 (negate 0.6) (negate 0.4) 0 :: Vertex3 GLdouble)
        color  (Color3 0 1 0 :: Color3 GLdouble)
        vertex (Vertex3 0.6 (negate 0.4) 0 :: Vertex3 GLdouble)
        color  (Color3 0 0 1 :: Color3 GLdouble)
        vertex (Vertex3 0 0.6 0 :: Vertex3 GLdouble)

triangle::[(GLfloat, GLfloat, GLfloat)]
triangle = [
               (0.9, 0.4, 0),
               (0.6, 0.4, 0),
               (0.4, 0.1, 1)
            ]


bool :: Bool -> a -> a -> a
bool b falseRes trueRes = if b then trueRes else falseRes

-- | line segements from points with LineStrip
-- | e.g. draw curve, line
renderCurve::[(GLfloat, GLfloat, GLfloat)]->IO()
renderCurve points = do 
    renderPrimitive LineStrip $ mapM_(\(x, y, z) -> vertex $ Vertex3 x y z) points

unless' :: Monad m => m Bool -> m () -> m ()
unless' action falseAction = do
    b <- action
    unless b falseAction

maybe' :: Maybe a -> b -> (a -> b) -> b
maybe' m nothingRes f = case m of
    Nothing -> nothingRes
    Just x  -> f x
    
-- type ErrorCallback = Error -> String -> IO ()
errorCallback :: G.ErrorCallback
errorCallback err description = hPutStrLn stderr description


-- type KeyCallback = Window -> Key -> Int -> KeyState -> ModifierKeys -> IO ()
keyBoardCallBack :: IORef (Set G.Key) -> G.KeyCallback
keyBoardCallBack ref window key scanCode keyState modKeys = do
    putStrLn $ "inside keyBoardCallBack=>keyState" ++ show keyState ++ " " ++ "keyBoardCallBack=>key=" ++ show key
    case keyState of
        G.KeyState'Pressed -> modifyIORef' ref (S.insert key) >> readIORef ref >>= \x -> print $ "inside keyBoardCallBack=> readIORef ref=>" ++ show x 
        G.KeyState'Released -> modifyIORef' ref (S.delete key)
        _ -> return ()
    when (key == G.Key'Escape && keyState == G.KeyState'Pressed)
        (G.setWindowShouldClose window True)


-- getKey :: Enum a => a -> IO KeyButtonState
-- IO keyIsPressed is monad => it is functor
-- so fmap can be used here
keyIsPressed::G.Window -> G.Key -> IO Bool
keyIsPressed win key = isPress `fmap` G.getKey win key
                where
                    isPress::G.KeyState -> Bool
                    isPress G.KeyState'Pressed   = True
                    isPress G.KeyState'Repeating = True
                    isPress   _                  = False

lightDiffuse ::Color4 GLfloat
lightDiffuse = Color4 0.6 1.0 0.5 0.6

lightAmbient ::Color4 GLfloat
lightAmbient = Color4 0.0 0.0 1.0 1.0

lightPosition ::Vertex4 GLfloat
lightPosition = Vertex4 1.0 1.0 1.2 0.0

lightSpecular ::Color4 GLfloat
lightSpecular = Color4 1.0 0.7 1.0 0.8

--GLfloat light_ambient[] = { 0.0, 0.0, 0.0, 1.0 };
--GLfloat light_diffuse[] = { 1.0, 1.0, 1.0, 1.0 };
--GLfloat light_specular[] = { 1.0, 1.0, 1.0, 1.0 };
--GLfloat light_position[] = { 1.0, 1.0, 1.0, 0.0 };
--
--glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
--glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
--glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
--glLightfv(GL_LIGHT0, GL_POSITION, light_position);

v3ToVertex3::V3 GLfloat-> Vertex3 GLfloat 
v3ToVertex3 (V3 x y z) = (Vertex3 x y z::Vertex3 GLfloat)


mymain :: IO ()
mymain = do
  G.setErrorCallback (Just errorCallback)
  -- finally add double buffer
  successfulInit <- G.init
  G.windowHint (G.WindowHint'DoubleBuffer True)
  -- if init failed, we exit the program
  bool successfulInit exitFailure $ do
      mw <- G.createWindow 600 600 "Simple example, haskell style" Nothing Nothing
      maybe' mw (G.terminate >> exitFailure) $ \window -> do
          G.makeContextCurrent mw
          mainLoop window
          G.destroyWindow window
          G.terminate
          exitSuccess
          
mainLoop :: G.Window -> IO ()
mainLoop w = unless' (G.windowShouldClose w) $ do
    (width, height) <- G.getFramebufferSize w
    let ratio = fromIntegral width / fromIntegral height
    viewport $= (Position 0 0, Size (fromIntegral width) (fromIntegral height))
    GL.clear [ColorBuffer, DepthBuffer]
    -- lighting info
    diffuse  (Light 0) $= lightDiffuse
    ambient  (Light 0) $= lightAmbient
    specular (Light 0) $= lightSpecular
    position (Light 0) $= lightPosition
    light    (Light 0) $= Enabled
    --lighting           $= Enabled
    depthFunc  $= Just Lequal
    blend          $= Enabled
    lineSmooth     $= Enabled
    -- end lighting info

    matrixMode $= Projection
    matrixMode $= Modelview 0
    loadIdentity
    let fovy = 60.0; aspect = 1.0; zNear = 2.0; zFar = (-2)
        in GM.perspective fovy aspect zNear zFar 
    --ortho (negate ratio) ratio (negate 1.0) 1.0 1.0 (negate 1.0)

    loadIdentity
    --gluLookAt(0, 0, 1.2, 0, 0, 0, 0, 1, 0);
    --GM.lookAt (Vertex3 0 0 1.3::Vertex3 GLdouble) (Vertex3 0 0 0:: Vertex3 GLdouble) (Vector3 0 1 0 :: Vector3 GLdouble)
    -- this is bad, but keeps the logic of the original example I guess

    ------------------------------------------------------------------   
    -- I think there is issue here, in keyBoardCallBack function
    -- can write the key to (Set G.Key), but can't read it with readIORef

    ref <- newIORef S.empty
    G.setKeyCallback w (Just $ keyBoardCallBack ref)
    keyDown <- readIORef ref
    print $ "from setKeyCallback keyDown=>" ++ (show keyDown)

    renderCoordinates
    mapM renderCurve $ let f x = x*x*x*x in map(\x -> tangent f (0, 1) x) [ k*v | k <- [-100..100], let v = 0.02]  
    
    G.swapBuffers w
    G.pollEvents
    mainLoop w

main = mymain


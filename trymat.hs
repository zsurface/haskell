import Control.Monad
import Data.Char
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 
import Data.Ratio 

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

import AronModule 


fun::Rational->Rational
fun n = n + 1

σ::Int->Int
σ n = n

main = do 
        print "Hello World"
        argList <- getArgs
        pp $ length argList
        let s1 = [2]
        let s2 = [1, 2]
        let s3 = map(\x1 -> map(\x2 -> x1*x2) s2) s1
        let s4 = outer s1 s2    
        let s11 = [[1],
                   [2]]
                   
        let s22 = [[2, 3]]
        let s33 = (join . join) $ map(\x2 -> map(\x1 -> outer x1 x2) s11) s22
        pm "s33" s33 
        pa mat
        let vc = vcol mat
        let vr = take 1 mat
        pm "mat" mat
        let ma = zipWith2 (\x y -> x+y) mat mat
        pm "ma" ma
        let r = fun 3 
        print r
        pp "done!"
        
        -- 


#!/usr/bin/env runhaskell

{-# LANGUAGE OverloadedStrings #-}

import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

import AronModule 

helpme::IO()
helpme = pp "run svg file.gv" >> 
         pp "run png file.gv" >> 
         pp "dot -Tsvg file.gv > file.svg" >> 
         pp "dot -Tpng file.gv > file.png"
main = do 
        pwd
        helpme
        argList <- getArgs
        pp $ length argList
        if (length argList) == 2 
        then case head argList of 
            "svg" -> run cmd >> run ("ls " ++ fn) >>= \x -> pp x 
                    where 
                        h = head $ drop 1 argList; 
                        fn = (dropExtension h) ++ ".svg"
                        cmd = "dot -Tsvg " ++ h ++ " > " ++ fn 
            "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
                    where 
                        h = head $ drop 1 argList; 
                        fn = (dropExtension h) ++ ".png"
                        cmd = "dot -Tpng " ++ h ++ " > " ++ fn 
            _     -> print "[rungraphviz svg file.gv] [rungraphviz png file.gv]" 
        else print "Need Two arguments" 
        pp "done!"

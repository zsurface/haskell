import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.Set
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 
import AronModule 
import System.Console.Pretty (Color (..), Style (..), bgColor, color,
                           style, supportsPretty)


-- Mon Oct 22 21:57:55 2018 
-- run ghc with AronModule and -O2
hlib="/Users/cat/myfile/bitbucket/haskelllib"
main = do 
        pp "runh [file.hs]"
        pp "ghc -i/Users/cat/myfile/bitbucket/haskelllib -o file file.hs "
        argList <- getArgs
        pp $ len argList
        if len argList > 0 
        then do
            let fn = head argList
            let dext = dropExtension $ takeFileName $ head argList
            run $ "ghc -i/Users/cat/myfile/bitbucket/haskelllib -O2  -o " ++ dext ++ " " ++ fn 
            pp $ "Gene binary: " ++ dext
            run' $ "./" ++ dext
        else do
            putStrLn $ (color Red "Need one argument")
            error "Need one argumet"
        time <- getTime
        pp $ "Time:" ++ time
        pp "done!!!"

import Data.IORef
import Graphics.Rendering.OpenGL as GL
import Graphics.UI.GLUT as GLUT

initfn :: IO ()
initfn = do 
  matrixMode $= Projection
  loadIdentity
  ortho 0.0 1.0 0.0 1.0 (-1.0) 1.0


display :: IORef GLfloat -> IO ()
display spinref = do 
  spin <- readIORef spinref
  clear [ ColorBuffer ]
  preservingMatrix $ do
    rotate spin (Vector3 0.0 0.0 1.0)
    color (Color3 (1.0::GLfloat) 1.0 1.0)
    rect  (Vertex2 (-25.0::GLfloat) (-25.0)) (Vertex2 25.0 25.0)
  swapBuffers



spinDisplay :: IORef GLfloat -> IO ()
spinDisplay sref = do 
    modifyIORef sref $ \spin ->  
      let spin' = spin + 2.0
      in if spin' > 360.0 then spin' - 360 else spin'
    postRedisplay Nothing 

 
 
reshape :: Size -> IO ()
reshape s@(GLUT.Size w h) = do
  writeIORef GLUT.viewport (GLUT.Position 0 0,s)
  writeIORef GLUT.matrixMode GLUT.Projection 
  GLUT.loadIdentity
  GLUT.ortho (-50.0) 50.0 (-50.0) 50.0 (-1.0) 1.0
  writeIORef GLUT.matrixMode (GLUT.Modelview 0)
  GLUT.loadIdentity



mouse :: IORef GLfloat -> MouseButton -> KeyState -> GLUT.Position -> IO ()
mouse sref LeftButton Down (GLUT.Position x y) = writeIORef idleCallback (Just (spinDisplay sref))
mouse sref LeftButton _ _ = return ()
mouse sref MiddleButton Down (GLUT.Position x y) = writeIORef  idleCallback Nothing
mouse sref MiddleButton _ _ = return ()
mouse sref _ _ _ = return ()


main :: IO ()
main = do 
  spinref <- newIORef 0.0
  (progname,_) <- getArgsAndInitialize 
  createWindow "Hello World"
  -- displayCallback $= (display spinref) 
  -- reshapeCallback $= Just reshape 
  -- mouseCallback $= Just (mouse spinref)
  writeIORef displayCallback (display spinref)
  writeIORef reshapeCallback (Just reshape)
  writeIORef mouseCallback (Just spinref)
  initfn 
  mainLoop


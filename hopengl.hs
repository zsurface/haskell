{-# LANGUAGE DataKinds #-}
module Main where

import Control.Concurrent (threadDelay)
import Control.Monad (unless, when)
import Graphics.Rendering.OpenGL as GL 
import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
import qualified Graphics.UI.GLFW as G 
import System.Exit
import System.IO
import Control.Monad
import System.Random
import Data.Set(Set) 
import Data.IORef 
import Data.Maybe
import qualified Data.Set as S 
import Linear.V3
import Linear.V3(cross) 
import Linear.Vector
import Linear.Matrix
import Linear.Projection as P
import Linear.Metric(norm, signorm)

import Text.Regex
import AronModule
import AronGraphic

bool :: Bool -> a -> a -> a
bool b falseRes trueRes = if b then trueRes else falseRes

unless' :: Monad m => m Bool -> m () -> m ()
unless' action falseAction = do
    b <- action
    unless b falseAction

maybe' :: Maybe a -> b -> (a -> b) -> b
maybe' m nothingRes f = case m of
    Nothing -> nothingRes
    Just x  -> f x
    
-- type ErrorCallback = Error -> String -> IO ()
errorCallback :: G.ErrorCallback
errorCallback err description = hPutStrLn stderr description

keyBoardCallBack :: IORef (Set G.Key) -> G.KeyCallback
keyBoardCallBack ref window key scanCode keyState modKeys = do
    putStrLn $ "keyBoardCallBack=>keyState" ++ show keyState ++ " " ++ "keyBoardCallBack=>key=" ++ show key
    case keyState of
        G.KeyState'Pressed -> modifyIORef ref (S.insert key) >> readIORef ref >>= \x -> print $ "inside keyBoardCallBack=> readIORef ref=>" ++ show x 
        G.KeyState'Released -> modifyIORef ref (S.delete key)
        _ -> return ()
    when (key == G.Key'Escape && keyState == G.KeyState'Pressed)
        (G.setWindowShouldClose window True)


projection xl xu yl yu zl zu = do
    matrixMode $= Projection
    loadIdentity    
    GL.ortho xl xu yl yu zl zu
    matrixMode $= Modelview 0
    
frustumProj left right top bot near far = do 
    matrixMode $= Projection
    loadIdentity
    GL.frustum left right top bot near far
    matrixMode $= Modelview 0

mymain :: IO ()
mymain = do
  G.setErrorCallback (Just errorCallback)
  -- finally add double buffer
  successfulInit <- G.init
  G.windowHint (G.WindowHint'DoubleBuffer True)
  -- if init failed, we exit the program
  bool successfulInit exitFailure $ do
      mw <- G.createWindow 1000 1000 "Simple example, haskell style" Nothing Nothing
      maybe' mw (G.terminate >> exitFailure) $ \window -> do
          G.makeContextCurrent mw
          mainLoop window 
          G.destroyWindow window
          G.terminate
          exitSuccess
          
mainLoop :: G.Window -> IO ()
mainLoop w = unless' (G.windowShouldClose w) $ do
    lastFrame <- maybe 0 realToFrac <$> G.getTime

    (width, height) <- G.getFramebufferSize w
    let ratio = fromIntegral width / fromIntegral height
    viewport $= (Position 0 0, Size (fromIntegral width) (fromIntegral height))
    GL.clear [ColorBuffer, DepthBuffer]
    -- lighting info
    diffuse  (Light 0) $= lightDiffuse
    ambient  (Light 0) $= lightAmbient
    specular (Light 0) $= lightSpecular
    position (Light 0) $= lightPosition
    light    (Light 0) $= Enabled
    -- disable to show color
    -- lighting           $= Enabled
    depthFunc  $= Just Lequal
    blend          $= Enabled
    lineSmooth     $= Enabled
    -- end lighting info
    loadIdentity
--    matrixMode $= Projection
--    matrixMode $= Modelview 0

    let left = -right 
        right = 1 
        bot   = -top
        top   = 1
        far   = 10 
        near  = 0 
        in frustumProj left right top bot near far 

--    let fovy = 60.0; aspect = 1.0; zNear = 2.0; zFar = (-2)
--        in GM.perspective fovy aspect zNear zFar 
    --ortho (negate ratio) ratio (negate 1.0) 1.0 1.0 (negate 1.0)

    -- lookAt(Vertex3 pos of camera, Vertex3 point at, Vector up)
    -- GM.lookAt (Vertex3 0.1 0.1 0.5::Vertex3 GLdouble) (Vertex3 0 0 0:: Vertex3 GLdouble) (Vector3 0 1 0 :: Vector3 GLdouble)
--    GM.lookAt (Vertex3 0 0 1::Vertex3 GLdouble) (Vertex3 0 0 0:: Vertex3 GLdouble) (Vector3 0 1 0 :: Vector3 GLdouble)
    ref <- newIORef S.empty
    G.setKeyCallback w (Just $ keyBoardCallBack ref)
    k <- readIORef ref 
    pp $ "key=" <<< k
    
    let f x = x*x*x 
    drawCurve f (0, 1) green
    drawTangentLine f (0.3 - 0.1, 0.3 + 0.1) 0.3 blue 
    drawNormalLine f (0.3, 0.3 + 0.1) 0.3 green 
     


    G.swapBuffers w
    G.pollEvents
    mainLoop w 
main = mymain


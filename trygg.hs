-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}  --
-- 
-- import Turtle                       --
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import Graphics.Rendering.OpenGL as GL 
import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
import qualified Graphics.UI.GLFW as G
import Data.Set(Set) 
import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".

import AronModule 

fun::Double -> GLfloat 
fun n = realToFrac n

fun'::GLfloat -> Double
fun' n = realToFrac n

mat = [
      [1, 2, 3],
      [4, 5, 6],
      [7, 8, 10]
      ]

v1 =[[1], 
     [4], 
     [7]]
v2 =[[2], 
     [5], 
     [8]]

v3 =[[3], 
     [6], 
     [10]]

(-:) m1 m2 = zipWith2 (-) m1 m2
(+:) m1 m2 = zipWith2 (+) m1 m2
(*:) m1 m2 = zipWith2 (*) m1 m2

(++:) v1 v2 = tran $ v1' ++ v2' 
        where
            v1' = tran v1 
            v2' = tran v2 

a1 = v1
a2 = v2 -: (projn v2 v1)
a3 = v3 -: ((projn v3 v1) +: (projn v3 v2) )

e1 = nl a1 
e2 = nl a2 
e3 = nl a3

sqsum x = sum $ join $ zipWith2 (*) x x

nl = normList
nd = listDots

main = do
        let s = [[1],[2], [3]]
        let p = projn v2 v1
        pp $ "p=" <<< p 
        fl
        pp $ "a1=" <<< a1
        pp $ "a2=" <<< a2
        pp $ "a3=" <<< a3
        pp $ "e1=" <<< e1 
        pp $ "e2=" <<< e2 
        pp $ "e3=" <<< e3 
        pp $ "|e1|=" <<< sqsum e1
        pp $ "|e2|=" <<< sqsum e2
        pp $ "|e3|=" <<< sqsum e3
        pp $ "<e1 e2> = "<<< nd e1 e2
        pp $ "<e1 e3> = "<<< nd e1 e3
        pp $ "<e2 e3> = "<<< nd e2 e3
        let ev11 = listDots e1 v1

        let ev12 = listDots e1 v2
        let ev22 = listDots e2 v2

        let ev13 = listDots e1 v3
        let ev23 = listDots e2 v3
        let ev33 = listDots e3 v3

        pp $ "ev11="<<< ev11 
        fl

        pp $ "ev12="<<< ev12 
        pp $ "ev22="<<< ev22
        fl

        pp $ "ev13="<<< ev13 
        pp $ "ev23="<<< ev23
        pp $ "ev33="<<< ev33
        fl

        let d1 = listDots a1 a2
        let d2 = listDots a1 a3
        let d3 = listDots a2 a3
        pp $ "d1=" <<< d1
        pp $ "d2=" <<< d2
        pp $ "d3=" <<< d3 
        fl
        let qq = e1 ++: e2 ++: e3 
        pa qq  
        pp "done!"

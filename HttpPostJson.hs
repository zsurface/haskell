#!/Users/aaa/.ghcup/bin/stack
{- stack script
 --resolver lts-13.28
 --ghc-options -i/Users/aaa/myfile/bitbucket/haskelllib
 --package "array"
-}

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric, DeriveAnyClass #-}
  
import           Data.Aeson
import qualified Data.ByteString.Char8 as S8
import qualified Data.Yaml             as Yaml
import           Network.HTTP.Simple
import qualified Data.Aeson as DA
import GHC.Generics
import AronModule


-- KEY: Http post JSON, Http Json
       
{-|
    var obj = new Object()
    obj.pid = 0
    obj.begt = Date.now();
    obj.endt = 0
    obj.newcode = newcode 
    let data = JSON.stringify(obj)
    xhr.send(data)
-}
data Person = Person Int Int Int String
instance ToJSON Person where
    toJSON (Person pid begt endt newcode) = object
        [ "pid"      .= pid
        , "begt"     .= begt
        , "endt"     .= endt
        , "newcode"  .= newcode
        ]

data UpdateCodeBlock = UpdateCodeBlock{pid::Integer, newcode::String, begt::Integer, endt::Integer} deriving (Generic, Show)
instance DA.FromJSON UpdateCodeBlock
instance DA.ToJSON UpdateCodeBlock where
    toEncoding = DA.genericToEncoding DA.defaultOptions

people :: [Person]
people = [Person 0 0 0 "a:*:hcode\nline 1"]

main :: IO ()
main = do
    argList <- getArgs
    if len argList == 1 then do
      let updateCodeBlock = UpdateCodeBlock{pid = 0, newcode = "a:*:hcode\nline 1", begt = 0, endt = 0}
      -- let request = setRequestBodyJSON people $ "POST https://httpbin.org/post"
      let request = setRequestBodyJSON updateCodeBlock $ "http://localhost:8080/insertcode"
      response <- httpJSON request

      putStrLn $ "The status code was: " ++
                 show (getResponseStatusCode response)
      print $ getResponseHeader "Content-Type" response
      S8.putStrLn $ Yaml.encode (getResponseBody response :: Value)
      else do
      putStrLn "Need one argument:  \"a:*:hcode\n line 1\""

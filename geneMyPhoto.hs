{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

import Prelude hiding ((+)) 
import Text.RawString.QQ
import Control.Monad
import Data.Char
import Data.List
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.List.Split

import AronModule 

-- Tue Mar 10 21:46:20 2020 
-- KEY: my photo, myphoto, gallery, my picture, mypicture, generate photo, generate picture
--
-- path = "/Library/WebServer/Documents/xfido/myphoto"
phtml = "/Users/cat/myfile/bitbucket/haskell/text/myphoto.html"

imgTag::String->String
imgTag s = [r| <div class='blocks'><a target='_blank' href='../image/|] <> s <> [r|'><img src='../image/|] <> s <> [r|' alt='Cinque Terre' width='200' height='200'>|]

openTable = [r|<table width='80%' style='margin: 0 auto; border:1px solid;text-align:center' class='fixed'> |]

closeTable = [r|</table>|]

div_close::String
div_close = [r| </a></div> |]

closeOpenDiv::String->String
closeOpenDiv s = [r|<div class='wrap'> |] <> (imgTag s) <> div_close 

closeOpenDivNotDiv::String->String
closeOpenDivNotDiv s = [r|</div><div class='wrap'> |] <> (imgTag s) <> div_close 

(+) = (++)

imagePath = "/Library/WebServer/Documents/xfido/image"
htmlFile = "/Library/WebServer/Documents/xfido/html/indexMyImage.html"

{-| 
    === Generate gallery photos from given directory that contain all the pics.

    >FilePath = "dir/to/photo"
-} 
htmlImageTable::FilePath -> IO [String]
htmlImageTable fp = do 
        fls <- lsFile fp  
        let nPicRow = 4
        html <- readFileLatin1ToList photoTemplateHTML 
        print html 
        -- split html to two part
        let list = splitWhen(\x -> (length $ trimWS x) == 0) html
        let headHTML = head list
        let lastHTML = last list
        let ff = zipWith(\x y -> if mod y nPicRow == 0 then (if y == 0 then closeOpenDiv x else closeOpenDivNotDiv x) else (imgTag x + div_close)) fls [0..] 
        let allRows = ff + ["</div>"] 
        return $ [openTable] + allRows + [closeTable] 
    where
      (+) = (++)
      photoTemplateHTML = "/Users/cat/myfile/bitbucket/haskell/text/myphoto.html"


nPicRow = 4
main = do 
--        f <- lsFile imagePath  
        html <- readFileLatin1ToList phtml 
--        print html 
--        -- split html to two part
        let list = splitWhen(\x -> (length $ trimWS x) == 0) html
        let headHTML = head list
        let lastHTML = last list
--        let ff = zipWith(\x y -> if mod y nPicRow == 0 then (if y == 0 then closeOpenDiv x else closeOpenDivNotDiv x) else (imgTag x + div_close)) f [0..] 
--        let allRows = ff + ["</div>"] 
        table <- htmlImageTable imagePath  
        let picList = headHTML + table + lastHTML 
        writeToFile htmlFile picList

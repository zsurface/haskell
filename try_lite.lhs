This literate program prompts the user for a number
   and prints the factorial of that number:

$\sqrt{3}$

>tsort []     = []
>tsort (x:xs) = tsort [y | y<-xs, y>x] ++ [x] ++ tsort [y | y<-xs, y<=x]
>
>main :: IO ()
>main = do putStr "Enter a number: "
>          putStr "n!= "

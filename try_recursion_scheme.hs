-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"
-- import Data.Set   -- collide with Data.List 
{-# LANGUAGE DeriveFunctor, DeriveFoldable, DeriveTraversable #-}
import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

--import Data.Array
--import Text.Regex.TDFA -- the module will conflict with Text.Regex

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}


-- zo - open
-- za - close

import AronModule 


data List a list = Nil | Con a list deriving (Functor)

{-| 
    Signature as algebra types
    And a way to review lists as the canonical(initial) structure for this type:
-} 

project :: [a] -> List a [a]
project []     = Nil
project (a:as) = Con a as

embed::List a [a] ->[a]
embed Nil        = []
embed (Con a as) = a:as

--  embed and project forms isomorphism

sumAlg::(Num a)=> List a a -> a 
sumAlg Nil = 0
sumAlg (Con a b) = a + b

productAlg::(Num a)=> List a a -> a
productAlg Nil = 1
productAlg (Con a b) = a * b

main = do 
        print "Hello World"
        argList <- getArgs
        pp $ length argList
        pp $ "embed=" <<< embed (Con 3 [4])
        pp $ "sumAlg=" <<< sumAlg (Con 1 2)
        pp $ "productAlg=" <<< productAlg (Con 1 2) 
        pp "done!"

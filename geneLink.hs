import Control.Monad
import Data.Char
import Data.List
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import AronModule 

--MySymbin="/Users/cat/myfile/symbin"
--SName="photo"
--MyBin="/Users/cat/myfile/mybin"

photo="/Users/cat/myfile/mybin/geneMyPhoto"
symbin="/Users/cat/myfile/symbin"
name="photo"
str = "ghc -i/Users/cat/myfile/bitbucket/haskell -O2 /Users/cat/myfile/bitbucket/haskell/geneMyPhoto.hs -o " ++ photo 

link = "ln -s " ++ photo ++ " " ++ symbin </> name 

main = do 
        argList <- getArgs
        s <- run str 
        ss <- fExist (symbin </> name) >>= \x -> if x then rm (symbin </> name) else return () 
        run link
        pp "done!"
         

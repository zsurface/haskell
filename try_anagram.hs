-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}  --
-- 
-- import Turtle                       --
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import qualified Data.List as L
import qualified Data.HashMap.Strict as M
import Data.List.Split
import Data.Maybe(fromJust)
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

-- zo - open
-- za - close

import AronModule 


--{-| 
--    === Find all anagrams from a list of strings
---} 
--anagram::String->[String] -> [String]
--anagram s cx = let mlist = M.lookup (sort s) map in if mlist == Nothing then [] else fromJust mlist 
--    where
--        map = insertMap M.empty cx 
--
--        insertMap::M.HashMap String [String] -> [String] -> M.HashMap String [String]
--        insertMap m [] = m
--        insertMap m (x:cx) = insertMap (insertStr m x) cx
--                where 
--                    insertStr::M.HashMap String [String] -> String -> M.HashMap String [String]
--                    insertStr m s = nmap 
--                        where
--                            nmap = let s' = sort s; v = M.lookup s' m 
--                                    in if v == Nothing then M.insert s' [s] m 
--                                                       else let k = fromJust v in M.insert s' (s:k) m 
--
main = do 
        print "Hello World"
        argList <- getArgs
        pp $ length argList
        pp "done!"
        let em = M.empty -- empty map
        let ls = ["dog", "cat", "god"]
        let word = "odg"
        pp $ "anagram=" <<< anagram word ls 
        pp "done"

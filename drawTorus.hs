module Main where

import Control.Monad (unless, when)
import Graphics.Rendering.OpenGL as GL 
import qualified Graphics.UI.GLFW as G
import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
import System.Exit
import System.IO
import Control.Monad
import Data.Set(Set) 
import Data.IORef 
import qualified Data.Set as S 

-- import Graphics.UI.GLUT
import Graphics.UI.GLUT.Callbacks.Global
import AronModule  hiding (rw)
import AronGraphic hiding (dist)
import AronOpenGL

-- | -------------------------------------------------------------------------------- 
-- | Mon Jan  6 23:01:22 2020 
-- | The Glorious Glasgow Haskell Compilation System, version 8.4.3
-- | add some comments, nothing else 
-- | compile: vim it, run it <F9>  
-- |
-- | ghc -i/Users/cat/myfile/bitbucket/haskelllib -o file file.hs
-- | 
-- | KEY: keyboard example, keypress example, modifyIORef example,  
-- | -------------------------------------------------------------------------------- 

mc::(GLfloat, GLfloat) -> (GLfloat, GLfloat) -> (GLfloat, GLfloat)
mc (a, b) (a', b') = (a*a' - b*b', a*b' + a'*b)

-- (a + bi)(a' + b'i)
-- (aa' - bb') + (ab' + ba')i
-- (a, b) * (a', b') = (aa' - bb', ab' + a'b)

-- conform= [(0.1 * (fst $ mc c c'), 0.1* (snd $ mc c c'), 0) 
--            | c <- zip (map(*0.01) [1..aa]) (map(*0.01) [1..bb]), c' <- zip (map(*0.01) [1..aa]) (map(*0.01) [1..bb])]
conform::[[Vertex3 GLfloat]]
conform= [[Vertex3 (fst $ mc c1 c2) (snd $ mc c1 c2)  0 | c1 <- c] | c2 <- c] 
--conform= [[(fst $ c1, snd $ c1 , 0) | c1 <- c] | c2 <- c] 
        where 
            fa = 0.1 
            aa = map(\x -> fa * x) [1..10]
            bb = map(\x -> fa * x) [1..3]
            c = foldr(++) [] $ [[(a, b) | a <- aa] | b <- bb]

grid::[[[Vertex3 GLfloat]]]
grid =[[[Vertex3 a b (a*a - b*b) | a <- aa] | b <- bb] | c <- cc] 
        where 
            n  = 10
            fa = 1/n 
            aa = map(\x -> fa * x) [1..n]
            bb = map(\x -> fa * x) [1..n]
            cc = map(\x -> fa * x) [1..n]

grid2::[[Vertex3 GLfloat]]
grid2 =[[Vertex3 a b (a*a - b*b) | a <- aa] | b <- bb] 
        where 
            n  = 30
            fa = 1/(2*n)
            aa = map(\x -> fa * x) [-n..n]
            bb = map(\x -> fa * x) [-n..n]

-- f z = z*z
-- 
-- a    => x
-- b    => y
-- re c => z
-- im c => color
grid4::[[(Vertex3 GLfloat, GLfloat)]]
grid4 =[[ let c = (C a b)*(C a b) in (Vertex3 a b (re c), im c) | a <- aa] | b <- bb]
        where 
            ne = [[ let c = sqrtC' (C a b) in (Vertex3 a b (re c), im c) | a <- aa] | b <- bb]
            n  = 20 
            fa = 1/(1.5*n)
            aa = map(\x -> fa * x) [-n..n]
            bb = map(\x -> fa * x) [-n..n]

grid4' = (map . map) (\x -> fst x) grid4

trig s1 s2 = map(\x -> foldr(++) [] x) $ (zipWith . zipWith)(\x y -> [x, y]) (init s1) (tail s2)

segment1 = zipWith(\x y -> [Vertex3 (-1) 0 0, y]) [1..] test_circle

test_circle::[Vertex3 GLfloat]
test_circle=[ let x = (1-t*t)/(1 + t*t); 
                  y = (2*t)/(1 + t*t) in Vertex3 x y 0 | t <- aa]
        where 
            n  = 50 
            fa = 1/(0.1*n)
            aa = map(\x -> fa * x) [-n..n]

splitPt::Int->[(GLfloat, GLfloat, GLfloat)]->[[(GLfloat, GLfloat, GLfloat)]]
splitPt _ [] = []
splitPt n xs = take n xs : (splitPt n $ drop n xs)


mergeChunk::Int->[(GLfloat, GLfloat, GLfloat)]->[(GLfloat, GLfloat, GLfloat)]
mergeChunk n c = mergeList  (take n c)  (take n $ drop n c) 

bigChunk::Int->[(GLfloat, GLfloat, GLfloat)]->[[(GLfloat, GLfloat, GLfloat)]]
bigChunk n xs = splitPt n xs 

renderSurface::[(GLfloat, GLfloat, GLfloat)]->IO()
renderSurface xs = do 
        iterateList (bigChunk 80 xs) (\chunk -> do
                        let len = length chunk
                        let n = div len 2 
                        let c = mergeChunk n chunk
                        let cc = zipWith(\x y -> (x, y)) c [1..]
                        renderPrimitive TriangleStrip $ mapM_(\((x, y, z), n) -> do 
                            case mod n 3 of 
                                0 -> do 
                                        color(Color3 0.8 1 0 :: Color3 GLdouble) 
                                1 -> do 
                                        color(Color3 0 0.5 1 :: Color3 GLdouble) 
                                _ -> do 
                                        color(Color3 1 0 0.7 :: Color3 GLdouble) 
                            normal $ (Normal3 x y z::Normal3 GLfloat)
                            vertex $ Vertex4 x y z 0.8) cc  
                            )

mymain :: IO ()
mymain = do
  -- finally add double buffer
  successfulInit <- G.init
  G.windowHint (G.WindowHint'DoubleBuffer True)
  -- if init failed, we exit the program
  bool successfulInit exitFailure $ do
      mw <- G.createWindow 1000 1000 "Simple example, haskell style" Nothing Nothing
      maybe' mw (G.terminate >> exitFailure) $ \window -> do
          G.makeContextCurrent mw
          ref <- newIORef initCam 
          refStep <- newIORef initStep 
          mainLoop window ref refStep
          G.destroyWindow window
          G.terminate
          exitSuccess
{-| 
    ===  mainLoop

    >ref     => data Cam = Cam{alpha::Double, beta::Double, gramma::Double, dist::Double} deriving(Show)
    >refStep => data Step = Step{xx::Double, yy::Double, zz::Double, ww::Double} deriving(Show)
-} 
mainLoop :: G.Window -> IORef Cam -> IORef Step -> IO ()
mainLoop w ref refStep = unless' (G.windowShouldClose w) $ do
    (width, height) <- G.getFramebufferSize w
    viewport $= (Position 0 0, Size (fromIntegral width) (fromIntegral height))
    GL.clear [ColorBuffer, DepthBuffer]

    -- ref     => data Cam = Cam{alpha::Double, beta::Double, gramma::Double, dist::Double} deriving(Show)
    -- refStep => data Step = Step{xx::Double, yy::Double, zz::Double, ww::Double} deriving(Show)
    G.setKeyCallback w (Just $ keyBoardCallBack refStep)

    matrixMode $= Modelview 0
    loadIdentity
    lightingInfo

    -- see keyboardCallback for all the keys
    -- NOTE: not sure why refStep is not modified here
    keyboardRot ref refStep 1.0 1.0
    renderCoordinates
--    drawTriangle

    -- render code here

--    drawPrimitive LineLoop red torus 
--    mapM_ (\lo -> drawPrimitive LineLoop red lo) $ torusR 0.1 0.2 
--    mapM_ (\lo -> drawPrimitive LineLoop blue lo) $ tran $ torusR 0.1 0.2 
--    mapM_ (\lo -> drawPrimitive LineLoop blue lo) $ surface1  
--    mapM_ (\lo -> drawPrimitive LineLoop red lo) $ tran $ surface1  
--    mapM_ (\lo -> drawPrimitive LineLoop green lo) $ surface2  
--    mapM_ (\lo -> drawPrimitive LineLoop blue lo) $ tran $ surface2  

    -- mapM_ renderSurface $ surface3
    -- renderSurface torus 
    -- drawSurfaceR(\x y -> 2*(x^2) + 3*(y^2)) 2 
    drawParamSurf(\u v -> u + v^2) (\u -> u^3) (\v ->v^2)
    -- drawSurface(\x y -> x^2 + y^2)

    G.swapBuffers w
    G.pollEvents
    mainLoop w ref refStep
main = mymain


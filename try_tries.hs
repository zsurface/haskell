-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}  --
-- 
-- import Turtle                       --
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Maybe(fromJust)
import Data.Time
import qualified Data.HashMap.Strict as M 
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

-- zo - open
-- za - close

import AronModule 




data XNode = XNode (M.HashMap Char XNode)  Bool deriving(Eq, Show)

{-| 
    === insert operation for Tries data structure
-} 
insertTries::String -> XNode -> XNode
insertTries [] (XNode m _) = XNode m True  -- it is a word
insertTries (x:cx) (XNode m b) = case xn of 
                                 Nothing  -> XNode (M.insert x (insertTries cx (XNode M.empty False)) m) b 
                                 Just xn' -> XNode (M.insert x (insertTries cx xn') m) b 
                where
                    xn = M.lookup x m 

{-| 
    === contain operation for Tries data structure
-} 
containsTries::String -> XNode -> Bool
containsTries [] (XNode m b) = True 
containsTries (x:cx) (XNode m b) = case xn of 
                                   Nothing -> False
                                   Just xn' -> containsTries cx xn'
    where
        xn = M.lookup x m

main = do 
        print "Hello World"
        argList <- getArgs
        pp $ length argList
        let xn = insertTries "a" (XNode M.empty False)
        pp $ "xnode=" <<< xn 
        let xxn = insertTries"ab" xn
        pp $ "xxn=" <<< xxn
        pp $ "containsTries=" <<< (containsTries "a" xxn == True)
        pp $ "containsTries=" <<< (containsTries "ab" xxn == True)
        pp $ "containsTries=" <<< (containsTries "aba" xxn == False)
        pp $ "containsTries=" <<< (containsTries "abb" xxn == False)
        pp $ "containsTries=" <<< (containsTries "b" xxn == False)
        pp $ "containsTries=" <<< (containsTries "bb" xxn == False)
        pp $ "empty =" <<< insertTries "" (XNode M.empty False)
        pp "done!"


{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes #-}
import Control.Monad
import Data.Char
import qualified Data.Set as DS
import qualified Data.List as DL
import Data.List.Split
import qualified Data.Map as M 
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 
import AronModule 

-- | Sun Sep  9 12:28:57 2018 
-- | try to move file types for vim to the end of header
-- |----------------------------------------------------------------------------- 
path = "/Users/cat/myfile/bitbucket/snippets/snippet_test.m"
--path = "/Users/cat/myfile/bitbucket/snippets/snippet.m"

remove'::String->Int->String
remove' s n = (take n s) ++ (drop (n + 1) s)

rmit::[a]->Int->[a]
rmit [] _ = []
rmit cx n = (take (n - 1) cx) ++ (drop n cx) 


predicateRegex::String->String->Bool
predicateRegex s r = f 
                where 
                    f = matchTest(mkRegex r) s

main = do 
        print "Hello World"
        list <- readFileToList path;
        fl
        let ll = filter(\x -> length x > 0) $ splitWhen(\x -> (length $ trim x) == 0) list
        pa ll
        fl
        let plist = map(\x -> ((partitionStrRegex "[,:]" $ head x), (drop 1 x)) ) ll
        pp plist
        let pplist = map(\x -> 
                        let 
                            h = fst x 
                            len = length h in if len > 1 then (rmit h 2) ++ [last (take 2 h)] else h 
                        )plist 
        fl
        pp pplist
        pp $ rmit [1, 2, 3] 1 
        pp $ rmit [1, 2, 3] 2
        pp $ rmit [1, 2, 3] 3 
        fl


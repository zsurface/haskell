import Control.Monad
import Data.Char
import Data.List
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import AronModule 

main = do 
        print "Hello World"
        argList <- getArgs
        pp $ length argList

        ref <- newIORef 1 
        replicateM_ 4 $ modifyIORef ref (+5)
        readIORef ref >>= print

        ref <- newIORef 2
        replicateM_ 3 $ modifyIORef ref (+2)
        readIORef ref >>= print

        pp "done!"

-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

--import Data.Array
--import Text.Regex.TDFA -- the module will conflict with Text.Regex

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}


-- zo - open
-- za - close

import AronModule 
fname = "/Users/cat/myfile/bitbucket/javalib/Aron.java"

regexJavaMethod="([a-zA-Z]+[[:space:]]*\\([^)]*\\))"


--geneMap2::String->String-> [String] -> [([String], Integer, [String])]
--geneMap2 _ _ [] = [] 
redisMap::[String] -> [([String], Integer, [String])]
redisMap [] = []
redisMap cx = rMap
    where
        list = filter(\e -> matchTest (mkRegex "public static") e) cx 
        ls = zipWith(\n x -> (n, x)) [0..] list
        m = map(\x -> (let may = matchRegex (mkRegex regexJavaMethod ) (snd x)
                       in case may of
                                Just e -> head e 
                                _      -> [] 
                       , fst x, snd x)
                         
               ) ls   
        lss = map(\x -> (takeWhile(\e -> isLetter e || isDigit e) (t1 x), t2 x, t3 x)) m 
        rMap = map(\x -> (prefix $ t1 x, t2 x, [t3 x])) lss

main = do 
        print "Hello World"
        ls <- readFileToList fname
        let list = filter(\e -> matchTest (mkRegex "public static") e) ls
        pa list
        let ls = zipWith(\n x -> (n, x)) [0..] list
        fl
        pp $ matchRegex (mkRegex "([a-zA-Z]+[[:space:]]+\\()") "123 abc ("   
        let m = map(\x -> (let may = matchRegex (mkRegex regexJavaMethod ) (snd x)
                        in case may of
                                Just e -> head e 
                                _      -> [] 
                        , fst x, snd x)
                         
                    ) ls   
        let lss = map(\x -> (takeWhile(\e -> isLetter e || isDigit e) (t1 x), t2 x, t3 x)) m 
        let redisMap = map(\x -> (prefix $ t1 x, t2 x, [t3 x])) lss
        fl
        pp lss
        fl
        pp redisMap
        pp "done!"

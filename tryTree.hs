import Control.Monad
import Data.Char
import Data.List
import Data.Set
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import AronModule 


data MyTree a = Empty 
                | Node a (MyTree a) (MyTree a) 

main = do 
        print "Hello World"
        argList <- getArgs
        pp $ length argList
        pp "done!"

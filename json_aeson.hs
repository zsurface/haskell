{-# LANGUAGE OverloadedStrings, DeriveGeneric, DeriveAnyClass #-}
{-# LANGUAGE DuplicateRecordFields #-} 


-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 
import qualified Data.Aeson as DA

import GHC.Generics
import Data.Text.Lazy (Text)
import Data.Text.Lazy.IO as I
import Data.Aeson.Text (encodeToLazyText)
import Data.Aeson (ToJSON, decode, encode)

import qualified Data.ByteString               as B
import qualified Data.ByteString.Internal      as BI
import qualified Data.ByteString.Lazy          as BL
import qualified Data.ByteString.Lazy.Internal as BLI

import AronModule 

{-| 
    Convert Lazy ByteString to Strict ByteString
-} 

toStrict1 :: BL.ByteString -> B.ByteString
toStrict1 = B.concat . BL.toChunks


{-| 
    Json exmaple, Json tutorial, serialize Json, deserialize Json
-} 
data Person = Person {
      name :: Text
    , age  :: Int
    , list  :: [String] 
    } deriving (Generic, Show)

instance DA.ToJSON Person where
    -- No need to provide a toJSON implementation.

    -- For efficiency, we write a simple toEncoding implementation, as
    -- the default version uses toJSON.
    toEncoding = DA.genericToEncoding DA.defaultOptions

instance DA.FromJSON Person
    -- No need to provide a parseJSON implementation.
-- read and write json

--instance DA.ToJSON Person where
--    -- this generates a Value
--    toJSON (Person name age) =
--        object ["name" .= name, "age" .= age]
--
--    -- this encodes directly to a bytestring Builder
--    toEncoding (Person name age) =
--        pairs ("name" .= name <> "age" .= age)

data Cat = Cat { name :: Text, 
                 age :: Int, 
                 list :: [String] 
               } deriving (Show, Generic, DA.ToJSON)
meowmers = Cat { name = "meowmers", 
                 age = 1, 
                 list = ["dog", "cat", "independency Injection"] 
               }
-- KEY: write json to file, aeson to file           
main = do
    let arr = [meowmers]
    I.writeFile "./text/myfile.json" (encodeToLazyText arr)
    -- encode: Record to Json
    print $ DA.encode (Person {name = "Joe", age = 12, list = ["dog", "cat"]})
    -- decode Json to Record
    let mp = DA.decode "{\"name\":\"Joe\",\"age\":12, \"list\":[\"dog\", \"cow\"]}" :: Maybe Person 
    case mp of
        Nothing -> return ()
        (Just p) -> print p

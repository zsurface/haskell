#!/usr/bin/env stack
-- stack --resolver lts-9.0 script
import System.Exit
import Control.Concurrent.Async

main = concurrently
  (exitWith (ExitFailure 41))
  (exitWith (ExitFailure 42))

import GHC.Real
import Control.Monad
import Data.Char
import Data.List
import Data.List.Split
import Data.Time
import Data.Maybe(fromJust, isJust)
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import Control.Monad (unless, when)
import Graphics.Rendering.OpenGL as GL 
import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
import qualified Graphics.UI.GLFW as G
import System.Exit
import System.IO
import Control.Monad
import System.Random
--import Data.Set(Set) 
import Data.IORef 
--import qualified Data.Set as S 
import Linear.V3
import Linear.V3(cross) 
import Linear.Vector
import Linear.Matrix
import Linear.Projection as P
import Linear.Metric(norm, signorm)

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

import AronDraw
import AronModule

mycircle::[(GLfloat, GLfloat, GLfloat)]
mycircle =[ let alpha = (pi2*n)/num in (r*_sin(alpha), r*_cos(alpha), 0) | n <- [1..num]]
        where
            num = 40 
            r = 0.5 
            pi2 = 2*pi::Float 

curve1::[(GLfloat, GLfloat, GLfloat)]
curve1 = [let x = d*k  in 
            (x, f x, 0) | k <- [a..b]] 
            where
                a = -300
                b = 300
                d = 0.01
                f x = x*x*x

main = do
        let list = lineInterpolate (1, 0.3, 0) ((-1), 0.3, 0)
--      draw list
--        let x0 = 0
--        let x1 = 1
--         
--        let y0 = tangent fun x0 1 
--        let y1 = tangent fun x1 1 
--        let p0 = (x0, y0, 0)
--        let p1 = (x1, y1, 0)
--        --draw curve1  
--        
--        draw $ lineInterpolate p0 p1  
--        print $ (0, y0) 
--        print $ (1, y1) 
        let x0 = -200    
        let x1 = 200 
        let eps = 0.000001
        let f x = x*x*x - 1 in case oneRoot f x0 x1 eps of
                                Just r -> print r
                                _      -> print "nothing"
        fl
        let list = map(\x -> (0.01)*x) [x0..x1]
        let ilist= zipWith(\x y->(x, y)) (init list) (drop 1 list)
        let roots= let f x = x^2 - 4 in map(\(x0, x1) -> oneRoot f x0 x1 eps) ilist 
        -- let roots= let f x = cos x in map(\(x0, x1) -> oneRoot f x0 x1 eps) ilist 
        let value= filter(isJust) roots 
        fl
        pp value
        fl
        let value= let f x = x^5 -4*x^4 + 0.1*x^3 + 4*x^2 - 0.5; x0 = -10; x1 = 10 in rootList f x0 x1 eps 100 
        -- let value= let f x = x^2 - 4; x0 = -2; x1 = 2 in rootList f x0 x1 eps 100 
        pp value 
        fl

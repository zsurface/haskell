-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List
{-|
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import qualified Text.Regex.TDFA as TD
import AronModule
-}
import Data.Char

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}



-- p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close



data Operator = Times | Plus | Minus | Div deriving(Show, Eq)
       
data Token = TokOp    Operator
           | TokIdent String
           | TokNum   Int
           | TokAssign
           | TokLParent
           | TokRParent
           | TokSpace
           | Digit
           | Alpha 
           | Ope
           | SpaceT
           | TokEnd deriving (Show, Eq)


isOpe::Char -> Bool
isOpe c = elem c "+-*/"

operator::Char -> Operator
operator c   | c == '+' = Plus
             | c == '-' = Minus
             | c == '*' = Times
             | c == '/' = Div
             | otherwise = error "Invalid character"


tokenizer::String -> [Token]
tokenizer [] = []
tokenizer (c:cs) | isDigit  c = Digit                : tokenizer cs
                 | isLetter c = Alpha                : tokenizer cs
                 | isSpace  c = SpaceT               : tokenizer cs
                 | isOpe    c = (TokOp $ operator c) : tokenizer cs
                 
charToInt::Char -> Int 
charToInt c = ord c - ord '0'
  
tokenizerChar::Char -> Token
tokenizerChar c | isDigit  c = TokNum $ charToInt c
                | isLetter c = TokIdent [c]
                | isSpace  c = TokSpace
                | isOpe    c = TokOp $ operator c
                | otherwise = error $ "Invalid Character" ++ [c]

{-|
     ab+c => ("ab", "+c")

     a  b+c
     ab  "+c"
     return ("ab" "+c")
-}
alnums::String ->(String, String)
alnums []     = ([], [])
alnums (c:cs) = als "" (c:cs)
  where
    als acc []     = (acc, [])
    als acc (x:xs) | isAlphaNum x = let (acc', xs') = als acc xs
                                    in (x:acc', xs')
                   | otherwise = (acc, (x:xs))

digits::String -> (String, String)
digits [] = ([], [])
digits (c:cs) = dig "" (c:cs)
  where
    dig acc [] = (acc, [])
    dig acc (x:xs) | isDigit x = let (acc', xs') = dig acc xs
                                 in  (x:acc', xs')
                   | otherwise = (acc, (x:xs))

{-|
   === KEY:

   See Prelude 'span'

   @
     span::(a -> Bool) -> [a] -> ([a], [a])
   @
-}
spanA::(a -> Bool) -> [a] -> ([a], [a])
spanA _ [] = ([], [])
spanA f (c:cs) = spanAcc [] (c:cs)
  where
    spanAcc acc []                 = (acc, [])
    spanAcc acc (x:xs) | f x       = let (acc', xs') = spanAcc acc xs
                                     in  (x:acc', xs')
                       | otherwise = (acc, (x:xs))


number c cs =
  let (dig, cs') = digits cs
  in TokNum (read (c:dig)) : tokenizer2 cs'

identifier c cs =
  let (aln, cs') = alnums cs
  in TokIdent (c:aln) : tokenizer2 cs'

number2 c cs =
  let (dig, cs') = spanA isDigit cs
  in TokNum (read (c:dig)) : tokenizer2 cs'
     
identifier2 c cs =
  let (ss, cs') = spanA isAlphaNum cs
  in TokIdent (c:ss) : tokenizer2 cs'

     
{-|
   === KEY: mutual recursion

   tokenizer2 ---- number
             |        |
             |        ⊥ - - → tokenizer2 
             |
             ⊥---- identifier
                     |
                     ⊥ - -  → tokenizer2
-}
tokenizer2::String -> [Token]
tokenizer2 [] = []
tokenizer2 (c:cs) | isDigit c     = number2     c cs      -- 123
                  | isAlpha c     = identifier2 c cs      -- x1
                  | isSpace c     = tokenizer2  cs        -- ' '
                  | elem c "+-*/" = (TokOp $ operator c) : tokenizer2 cs  -- TokOp Plus
                  | c == '='      = TokAssign            : tokenizer2 cs  
                  | c == '('      = TokLParent           : tokenizer2 cs
                  | c == ')'      = TokRParent           : tokenizer2 cs
                  | otherwise     = error $ "Error =>" ++ [c]


data PTree = SumNode Operator PTree PTree
           | ProdNode Operator PTree PTree
           | AssignNode String PTree
           | UnaryNode Operator PTree
           | NumNode Double
           | ValNode String
     deriving (Show)

lookAhead::[Token] -> Token
lookAhead [] = TokEnd
lookAhead (c:cs) = c

accept::[Token] -> [Token]
accept [] = error "Nothing to accept"
accept (c:cs) = cs

{-|

   Expression <- Term [+-] Expression
               | Identifier '=' Expression
               | Term
   Term       <- Factor [*/] Expression
               | Factor
   Factor     <- Number
               | Identifier
               | [+-] Factor
               | '(' Expression ')'


   Term [+-] Expression
   Factor [*/] Expression [+-] Expression
   Identifier [*/] Expression [+-] Expression
   Number [*/] Expression [+-] Expression
   '(' Expression ')' [*/] Expression [+-] Expression
   
-}


{-|
  Ref:
    https://www.schoolofhaskell.com/user/school/starting-with-haskell/basics-of-haskell/7-tokenizer-higher-order-functions
-}
main = do
       print "hi"
       let s = "abc123+1* + "
       print $ tokenizer s
       print $ operator '+'
       let ls = map tokenizerChar s
       print "ok"
       print ls
       print $ alnums "abAD9 + c"
       print $ digits "123ab"
       print $ tokenizer2 "111aaa44"
       print $ tokenizer2 "(12 + 14) / x1 = 28/x1 = xy"
       -- print $ spanA isDigit "123abc99cc"
       



       


{-# LANGUAGE UnicodeSyntax #-}

import Data.Char 
import AronModule 
dict = ["dog", "cat", "scrucity", "scrutinise"]


--  [12, 24, 0] 
-- (1, 2) (2, 4)
--  (1, 2)  2:(f n 1 24) => (2, 5) => 5:2:(f n 2 [])
--                                 => 5:2
--

type ℤ = Integer

ζ::ℤ → ℤ
ζ n = n

filterMM::(a -> Maybe a) -> [a] -> [a]
filterMM f [] = []
filterMM f (x:cs) = maybeConst (f x) $ filterMM f cs
  where
    maybeConst::Maybe a ->[a] -> [a]
    maybeConst Nothing l = l
    maybeConst (Just a) l = a:l


main = do
        print "Hello World"
        let f x = if x `mod` 2 == 0 then (Just x) else Nothing
        pp $ filterMM f [1, 2, 3, 4, 5]
        pp $ filterMM (\x -> if x `mod` 2 == 0 then Nothing else (Just (10*x))) [1..10]
        -- (4, 8) (6, 2)
        -- 8 8 2

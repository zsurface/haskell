-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}  --
-- 
-- import Turtle                       --
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".

import AronModule 


beg = "\\begin{bmatrix} \n "
end = "\\end{bmatrix} \n "
sl  = "\\\\ \n"
nl  = "\n"

str1  =concat [ beg 
              ,"1 & 0 & 0 " ++ sl
              ,"0 & 1 & 0 " ++ sl 
              ,"0 & 0 & 1 " ++ nl 
              ,end
              ] 
str2 =concat [ beg 
              ,"a_1 & a_2 & a_3 " ++ sl
              ,"b_1 & b_2 & b_3 " ++ sl 
              ,"c_1 & c_2 & c_3 " ++ nl 
              ,end
              ] 

pt = putStrLn

main = do 
        argList <- getArgs
        let len = length argList
        pt str1
        pt str2
        input <- getLine
        pp $ "input=" +: input
        case input of
            "m1" -> putStrLn str1 
            "m2" -> putStrLn str2
            _   -> pp "nothing"
        unless (len == 0) $ print "dog"
        pp "done!"

-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

{-# LANGUAGE MultiWayIf        #-}
{-# LANGUAGE QuasiQuotes       #-} -- support raw string [r|<p>dog</p> |]
{-#  Language GADTs #-}
{-#  Language KindSignatures #-}
import Text.RawString.QQ       -- Need QuasiQuotes too 


-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import qualified Text.Regex.TDFA as TD

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}


shell command template:
 
        argList <- getArgs
        if len argList == 2 then do
            let n = stringToInt $ head argList
            let s = last argList
            putStr $ drop (fromIntegral n) s
       else print "drop 2 'abcd'"

>import Prelude hiding(Maybe(..))
>import Data.Kind

>import AronModule 

>p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

 kind 
 MyNothhing has kind *
 MyJust has kind * -> *
 MyMaybe a => List<T> 
 MyMaybe Int => List<Integer>
 int fun(List<T> list){ 
        return list.size();
 }
 
 
 data MyMaybe a = Nothing | Just a
  Just a => Myabe a 
 Just (a) -> Maybe a
 zo - open
 za - close

 There are three Value constructor
 Expr => is type constructor
 I Int => is value constructor 
 Add Expr Expr is value constructor
 Mul Expr Expr is value constructor
 
Fractional is subtype of Num,  

class (Num a) => Fractional a where
        (/):: a -> a -> a 
        recip :: a -> a
        fromRational :: a -> a



>toInt::Float -> Int                                            
>toInt x = floor x

>data Expr = I Int 
>            | F Float
>            | B Bool
>            | S String
>            | Add Expr Expr 
>            | Sub Expr Expr 
>            | Div Expr Expr 
>            | Eq Expr Expr 
>            | Mul Expr Expr deriving (Show)

we can pattern-match all the constructors

>evaluate::Expr -> Int 
>evaluate (I n) = n 
>evaluate (F n) = floor n
>evaluate (S s) = strToInt s 
>evaluate (Add e1 e2) = (evaluate e1) + (evaluate e2)
>evaluate (Sub e1 e2) = (evaluate e1) - (evaluate e2)
>evaluate (Mul e1 e2) = (evaluate e1) * (evaluate e2)
>evaluate (Div e1 e2) = div (evaluate e1) (evaluate e2)

>evaluatef::Expr -> Float
>evaluatef (I n) = fromIntegral n 
>evaluatef (F n) = n 
>evaluatef (S s) = (fi . strToInt) s 
>evaluatef (Add e1 e2) = (evaluatef e1) + (evaluatef e2) 
>evaluatef (Sub e1 e2) = (evaluatef e1) - (evaluatef e2) 
>evaluatef (Mul e1 e2) = (evaluatef e1) * (evaluatef e2) 
>evaluatef (Div e1 e2) = (evaluatef e1) / (evaluatef e2) 

>main = do 
>        pp "done!"
>        let e1 = I 2
>        let e2 = I 3
>        let val =  evaluate $ Add e1 e2
>        let val2 = evaluate $ Mul e1 e2
>        let val3 = evaluate $ Mul (Mul e1 e2) (Mul e1 e2)
>        let s1 = S "1"
>        let s2 = S "9"
>        let val4 = evaluate $ Add s1 s2 
>        let val5 = evaluate $ Div e1 e2 
>        let val6 = evaluatef $ Div e1 e2 
>        let val7 = evaluatef $ Div s1 s2 
>        let val8 = evaluatef $ Div (F 0.3) (F 0.4) 
>        let val9 = evaluate $ Div  (F 2.3) (F 2.4) 
>        let val10 = evaluatef $ Add (F 2.3) (F 2.4) 
>        let val11 = evaluatef $ Sub (F 2.3) (F 2.4) 
>        pw "val" val
>        pw "val2" val2
>        pw "val3" val3
>        pw "val4" val4
>        pw "val5" val5
>        pw "val6" val6
>        pw "val7" val7
>        pw "val8" val8
>        pw "val9" val9
>        pw "val10" val10
>        pw "val11" val11

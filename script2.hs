{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE UnicodeSyntax #-}
import System.IO
import System.Environment
import Text.Regex.Posix
import Data.List
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import AronModule
import AronTest


mat1 = [
      [1, 2, 3],
      [4, 5, 6],
      [7, 8, 10]
      ]

mat2 = [
      [0, 2, 3],
      [0, 0, 0],
      [7, 8, 0]
      ]

fun::[[Integer]]->Int->Int->Int
fun [] _ _ = 0
fun m i j = if i < length m && j < length m then (max (fun m (i+1) j) (fun m i (j+1))) + fromIntegral (m !! i !! j) else 0 


main :: IO ()
main = do
        let i = 0 
        let j = 0 
        let n = fun mat1 i j
        print n 
        

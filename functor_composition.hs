import Control.Monad
import Data.Char
import Data.List
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import AronModule 

-- (f . g) x = f(g x)
-- (fmap . fmap) f = fmap (fmap f))
(.:)::(Functor f, Functor g)=>(a -> b) -> f (g a) -> f (g b)
(.:) = fmap . fmap 


main = do 
        print "Hello World"
        argList <- getArgs
        pp $ length argList
        pp "done!"
        print $ (.:) show [[1, 2], [3, 4]]
        pp "dog"

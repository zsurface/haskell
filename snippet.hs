import System.IO
import System.Environment
import Text.Regex.Posix
import Text.Regex
import AronModule
import Data.List.Split
import qualified Data.Text as Text

-- Mon Jan  1 21:53:03 PST 2018
-- 0. Read snippet file and generate autocmd for vim auto completion
-- 1. Fix home ++ "/myfile/bitbucket/snippets/snippet.vimrc" is not generated
-- Reason: outFile        = home ++ "/myfile/private/secret.vimrc" instead of snippets.vimrc
-- 2. Parse Chinese characters causes error:
-- Fix: remove Chinese char from snippets.m file
-- 
-- Wed May  8 09:07:41 2019 
-- Rename snippet.m => snippets.org  Emacs Org mode
-- 
-- UPDATE: Sat 17 Sep 11:20:28 2022 
-- SEE: ~/.vimrc file
-- NOTE: Not been used so far
--------------------------------------------------------------------------------------------

headerError = "\n\tError Header: Need to be following form\n\tjlist:*.java: java list, list\n" 
mystrip::String->String
mystrip s  = Text.unpack(Text.strip (Text.pack s ))

-- replace | => \| => '|' is special char in Vimrc file
sub = \x -> subRegex (mkRegex "\\|") x "\\\\\\0"

main = do
    home <- getEnv "HOME"
    let inFile_snippet = home ++ "/myfile/bitbucket/snippets/snippet.hs"
    let inFile_secret  = home ++ "/myfile/private/secret.m"
    --let outFile        = home ++ "/myfile/private/secret.vimrc"
    let outFile        = home ++ "/myfile/bitbucket/snippets/snippet.vimrc"


    line_snippet <- readFileToList inFile_snippet
    line_secret <- readFileToList inFile_secret

    let line = line_snippet ++ line_secret 
    fl
    mapM_ (print) line 
    fl
    let list = filter(\x -> length x > 0) line
    let list2 = filter(\x-> length x > 0) $ splitWhen(\x -> (length $ trimWS x) == 0) line
    fl
    mapM_ (print) list2
    fl
    let listHead = map(\x-> " " ++ trim(head x) ++ " ") $ list2
    mapM(print) listHead

    fl
    -- split header => jlist :*.java: java list, list 
    let splitListThree = map(map(trim)) $ map(splitRegex(mkRegex ":")) listHead
    mapM(print) splitListThree

    let splitList = map(\x -> if len x < 2 then error headerError else take 2 x) splitListThree 
    mapM(print) splitList 
    fl

    let listCR =  map(map(\x -> "\\<CR>"++ (sub $ trim x))) $ map(\x-> tail x) $ list2
    mapM(mapM(print)) listCR 
    fl
    let listLeft =  map(\x-> "" ++ (head x) ) listCR 
    let listRight =  map(\x-> (last x) ++""  ) listCR 
    fl
    mapM(print) listLeft 
    fl
    mapM(print) listRight 
    fl

    let newfull = map(map(++"\n")) listCR 
    mapM(print) newfull 
    fl

    let auto  = "autocmd BufEnter *.vimrc,*.tex,*.java,*.h,*.m iabbr <buffer> "
    let comList = map(\x -> "autocmd BufEnter " ++ last(x) ++ " iabbr <buffer> " ++ head(x) ++ " \n" ) splitList
    mapM(print) comList 
    fl

    mapM(print) comList 
    fl
    let comList2 = map(\x -> x:[]) comList
    mapM(print) comList2
    fl

    let codeList = map(foldr(\x y -> x ++ y) "\n") newfull 
    fl
    mapM(print)codeList 
    fl
    let autoCode = zipWith(\x y -> x ++ y) comList codeList
    fl
    mapM(print) autoCode
    fl
    let blockCodeList = foldl (\x y -> x ++ y) "" $  autoCode 
    writeFile outFile $ blockCodeList  

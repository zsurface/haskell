import Debug.Trace

evens (x:xs) = x : odds (trace (show (zip [0..] xs)) xs)
evens [] = []
odds (_:xs) = evens (trace (show (zip [0..] xs)) xs)
odds [] = []

evens' (x:xs) = x : odds' ((zip [0..] xs) xs)
evens' [] = []
odds' (_:xs) = evens' ((zip [0..] xs) xs)
odds' [] = []


main = do 
    print (evens "abcdefg")
    print (evens [1..10])


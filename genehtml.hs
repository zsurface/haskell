{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE QuasiQuotes #-}
module Main(main) where

import Control.Monad (filterM, liftM, zipWithM)
import Data.Char 
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix  -- "</>"
import System.IO
import System.Posix.Unistd
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Text.RawString.QQ         -- Need QuasiQuotes too 
import AronModule

-- KEY: xfido page, generate html, generate xfido static html page, generate static page, gene html, gene xfido, gene www
--
-- Wed Aug 22 08:54:12 2018  
-- use iterateList from AronModule 
------------------------------------------------------------------ 
-- Sat Aug  3 23:30:22 2019 
-- Add Emacs Solarized theme css dark/light: orgDark/orgLight
-- Remove some useless code
------------------------------------------------------------------ 
-- Sat 10 Apr 00:10:59 2021 
-- Add more tag name
------------------------------------------------------------------ 
-- Sat  8 May 22:20:37 2021  
-- Add more tag name
------------------------------------------------------------------ 

array = [
        "What I did today",
        "All My Projects",
        "Haskell Examples",
        "Python",
        "Java Compile classpath sourcepath jar file",
        "Haskell Wai Server",
        "Local PDF",
        "Mysql",
        "Apache Solr tutorial",
        "Insert a node into a binary tree",
        "Hermitian matrix and real eigenvalues",
        "SQL",
        "Shell Script Examples",
        "Haskell Stack Tool",
        "Haskell Functor Monoid and Monad",
        "Proof infinite number of primes",
        "C Language",
        "J Language",
        "Java Spring and Hibernate Database Driver and ORM",
        "Java Remove duplicate character from a string",
        "Java Find rectangle in two dimension array",
        "Java Different ways to concat string",
        "Editor",
        "Java LinkedHashMap Example",
        "Java Comparable and Comparator",
        "Haskell Implement an Dictionary in 4 lines",
        "Haskell Regex Examples",
        "Find the intersection of two sorted lists",
        "Java IO Java file reader Java file writer",
        "Understand OpenGL",
        "Intersection of two segments",
        "Householder Matrix",
        "Why Haskell so bad in Number",
        "Check Whether a point is inside a Triangle",
        "Convex Hull Algorithm",
        "OpenCV Image Blending",
        "OpenCV Change Image Contrast and Brightnes",
        "OpenCV Convert Image to Grayscale Image",
        "OpenCV Read Image and Show them",
        "OpenCV Linear Interpolation",
        "OpenCV from Scratch",
        "Agda and Haskell",
        "Haskell Typeclass",
        "Tmux Commands",
        "Find the square root of an Integer",
        "Haskell OpenGL Hello World",
        "Haskell OpenGL Primitive Mode",
        "Haskell Maybe",
        "Haskell Prove List satifies two laws",
        "Haskell Prove Maybe satifies two laws",
        "Haskell Read and Write Variable with newIORef",
        "Haskell Date.Set Set Example",
        "Haskell run GHCi in command line with here-string",
        "Haskell Implement Iterator with Monad IO",
        "Haskell Get Maximum value in Constant time from Stack",
        "Haskell String Text and ByteString",
        "What is Monad in Haskell",
        "Haskell Collection of Monoid",
        "Isomorophism in Haskell",
        "Understand foldr and foldl in Haskell",
        "Haskell Call C function FFI",
        "Haskell concatMap confusing",
        "GHC compile options in Haskell",
        "Type Constructor and Data Constructor in Haskell",
        "Multiple lines in GHCi in Haskell",
        "Install HDBC.Sqlite3 in Haskell",
        "Haskell Install Remove GHCi On Mac OSX",
        "Haskell Tutorial",
        "Haskell Guard Syntax",
        "Haskell Quick Sort",
        "Compare Haskell, Swift and C speed in Pythagorean Triples",
        "Stop and Restar Apache in MacOS",
        "Vim Remove Tailing Whitespace",
        "Vim Select the whole line",
        "Vim Copy string to Clipboard in MacOS",
        "Vim Replace Cursor Character with rX in Insert Mode",
        "Vim Global and Chain Ex Command",
        "Vim Copy Column and Paste",
        "Vim How to insert new line in Ex mode",
        "Vim Set environment in Ex mode",
        "Vim Add file path to register in Ex mode",
        "Vim Scroll the current line to the middle of the screen",
        "Vim Select word under cursor",
        "Vim Redirect Ex command output to file",
        "Vim Non-Greedy Regex",
        "Vim substitute example",
        "Vim Delete empty line",
        "Vim Tricks",
        "How to Define new command in latex in separate file",
        "How to Declare Clousre Syntax in Swift",
        "How to Use Loop Syntax in Swift",
        "Swift Dispatch Wait and Notify",
        "Swift Code",
        "Command Line Tricks",
        "Shell Script",
        "PDF files",
        "Latex Matrix",
        "Latex Example",
        "Emacs Evil Mode for Vim Users",
        "Xcode Shortcut Key",
        "Xcode Build Tools",
        "MyBookMark",
        "Random Note",
        "English Note",
        "Daily Note",
        "Math Definition",
        "The beauty of Torus",
        "Bezier Curve", 
        "Cubic Curve", 

        "From Matrix to Lie Group", 
        "Change Matrix Basis", 
        "Similar Matrix", 
        "Find the eigenvalue and eigenvector of 2 by 2 matrix", 
        "Find the eigenvalue of skew-symmetic matrix", 
        "Computer Graphic Matrix",
        "Compute the Determiant of 2x2 Matrix", 
        "How to solve the system equation", 

        "Euler Formula", 
        "Computing determinant with cofactor expansion", 
        "Differentiated", 
        "Algebraic Curve", 
        "Projective Geometry",
        "Proof On Some Sequences",
        "Square Root of Two is irrational", 
        "Square Root of Three is irrational", 
        "Visualize Binomial Theorem",
        "Binomial Identity",
        "Proof Fermat Little Theorem", 
        "Functional Picture",
        "How to plot a linear equation", 

        "My Photos",
        "World Cup 2018",

        "one proof a day 5",
        "one proof a day 4",
        "one proof a day 3",
        "one proof a day 2",
        "one proof a day 1",

        "Html is not refreshing in Github",

        "Markov Chain Algorithm Generate Text",
        "Multiple Thread LinkedList Queue HashMap",

        "ffmpeg example",
        "Tableau Interview Questions",
        "Print Previous element from Binary Tree",
        "Tower Hanoi",
        "Polish Notation and Preporder Traveral",

        "OpenGL draw point",
        "OpenGL draw line",
        "OpenGL draw triangle",
        "OpenGL draw polygon",
        "OpenGL Vertex Array",
        "OpenGL Vertex Array Triangle",
        "Draw Triangle OpenGL IOS",
        "Message Queue",
        "Observer Pattern",
        "Publish Subscribe Pattern",

        "Povray draw box",
        "Maximum difference on an array",
        "Maximum Continuous Sum List",
        "Maximum non consecutive sum",
        "Auto Complete with Map and List",
        "The power of image",
        "Eight Queen Solver",
        "Build Tree from pre and post",
        "Build Tree from preorder and inorder",
        "Serialize Binary Tree with Binary Heap",
        "Serialize Binary Tree Ternary Tree",
        "Tokenize Simple XML",
        "Print All Binary",
        "Tokenize Simple XML",
        "Find the Lowest Common Ancestor in a Binary Tree",
        "Find a pair of elements from an array whose sum equals a given number",
        "Array to Balanced BST",
        "Implement HashMap with array",
        "Find the First Non-Repeating Char from Stream Char",
        "Excel Column Header",
        "Minimum Distance Between two given words",
        "Set up Mutt for Gmail Account",
        "Clone Skip List One pass or NO Extra Space",
        "Insert node to sorted list",
        "Graph Implementation",
        "Merge Sorted Array with Extra Space",
        "Google Pill Interview Question",
        "Nim Game",
        "Compress String",
        "Design URL shortener",
        "Design Vendor Machina",
        "Design Coin Change Machina",
        "Design Parking Lot",
        "Graph Loop and Path",
        "Range Query",
        "Find the maximum children of given tree",
      
        "Level order without queue",
        "Merge Sorted Single LinkedList",
        "Facebook Interview Question",
        "Carmichael Number",
        "Merge Intervals",
        "The power of next next",
        "Traversal Iteration",
        "Binary Iterator",
        "Debug With LLDB",
        "Count Occurrence pattern String",
        "Check Prime Number Algorithm",
        "Expedia Tech Interview",
        "Directed Graph",
        "Binary Tree Definition",
        "Fix Swap Two nodes in Binary Search Tree",
        "Swap Two nodes in Binary Search Tree",
        "Java read file write to file read line",
        "Java Implement Stack with Array",
        "Java Least Common Ancestor",
        "Java Rotate 2d Array 90 degrees",
        "Java Longest component",
        "Java Priority Heap",
        "Java Thread",
        "Java Double LinkedList",
        "Java Typing Type Covariant Contravariant Subtype",
        "Singleton in Java",
        "Java Lambda Expression",
        "Json Parser in Java",
        "Least Recent Used LRU in Java",
        "Throw Throws Throwable in Java",
        "Get All methods in Java",
        "Java Matrix Multiply",
        "Microsoft Tech Interview Questions",

        "C++ tutorial",
        "C++ Return By Reference",
        "C++ Function Template and Class Template",
        "C++ Eigen Library for Matrix Operation",
        "C++ Copy Constructor",
        "C++ Clone Double Linked List",
        "C++ Clone Single Linked List",
        "C++ Single Linked List",
        "C++ All children at K dist from a Node",
        "C++ Print nodes at K distance from Root",
        "C++ Invert Binary Tree",
        "C++ Queue with Two Stacks",
        "C++ product of all integers except current one",
        "C++ Print Sprial from Rectangle",
        "C++ Rotate 2d array 90 degree",
        "C++ Draw Circle Using Symmetric property",
        "C++ Reverse Byte",
        "C++ Least Operation Add one Multiply two",
        "C++ Power of Two",
        "C++ Print All Binary With Binary Tree",
        "C++ Print All Binary",
        "C++ Decimal To Binary",
        "C++ Binary Tree Insert",
        "C++ Tries",
        "C++ Data Structure",
        "C++ Merge three sorted arrays",
        "C++ Merge two sorted arrays",
        "C++ overload Operator",
        "C++ find path in maze",
        "C++ Binary Search",
        "C++ Quick Sort",
        "C++ two stacks get minimum",
        "C++ Smart Pointers",
        "C++ Double Linked List With Smart Pointer",
        "C++ Virtual Class",
        "C++ Dangling Pointer",
        "C++ Regular Expression",
        "C++ Data Structure",
        "C++ And C Build Compile Option",
        "Generate n prime in C++",
        "Allocate 2D array in C and C++",
        "C and C++ unsigned Integer Gotcha",
        "C struct and union",
        "Maximum internval overlap",

        "Magic Square Solver",
        "Sudoku Solver",
        "How to create animation gif",
        "Property of determinant", 
        "Image Processing", 


        "UIAlert in iOS",
        "TableViewController in iOS",
        "Switch different RootController",

        "IOS Center Two Button",
        "IOS Subdivision Curve",
        "IOS Capture Screenshot",
        "IOS Load Image",
        "IOS UIBezierPath closePath",
        "IOS Animation",
        "IOS Simple Animation",
        "IOS Bezier Curve",
        "IOS Stop Watch",
        "IOS Rotate Different CAShapeLayers",
        "IOS Anchor Point Bounds Frame",
        "IOS Rotate CAShapeLayer",
        "IOS Transform Rotation 3",
        "IOS Transform Rotation 2",
        "IOS Transform Rotation 1",
        "IOS CAShapeLayer Gradient",
        "IOS Draw Hexagon",
        "IOS Click Inside Circle or Rectangle",
        "IOS Remove CAShapeLayer",
        "IOS CAShaperLayer StrokeColor",
        "IOS Slide Up Controller",
        "IOS List Buttons",
        "IOS Different Fonts",
        "IOS Play Sound",
        "IOS Demo",
        "IOS Simple App Demo",

        "Haskell Doc",
        "Java Doc",
        "Cpp Doc",
        "Rename Project in Xcode 6.1",
        "Touches Event in iOS",
        "Draw Circle Rectangle Oval in iOS", 
        "Xcode C++ Standard Library",
        "Xcode Add Header Search Path for CPP",
        "Xcode Remove Add Unit Test",
        "Xcode Launch Screen File",
        "Xcode Change Build from iPhone iPad",
        "Xcode SnapShot Directory",
        "Xcode Remove Cache",
        "Xcode No such file or directory",
        "Xcode Screenshots",
        "Xcode Add Resource as Group",
        "Xcode Add Resource Bundle",
        "Xcode Run Your Test Cases Without Running Your App",

        "ObjectiveC pass double pointers",
        "ObjectiveC String Format Specifiers",
        "ObjectiveC Block",
        "ObjectiveC Static Method",
        "ObjectiveC Double and Triple Pointer",

        "Tries Data Structure",
        "Java Tries With Array",
        "Cycle Array",
        "Stack and Queue in Binary Tree",
        "Binary Search One Dimensions",
        "Binary Search Two Dimensions",
        "Longest Increasing Subsequence Recursion",
        "Coin Change Algorithm", 
        "Coin Change Dynamic Programming", 
        "Count Number of Way for Coin Change", 

        "Binary Tree Generate Graphviz file",
        "Binary Maximum Sum Path",
        "Binary Tree Mirror",
        "Binary Tree Isomorphic",
        "Binary Tree Symmetric",
        "Binary Tree Maximum Height",

        "Java Command Snippet",
        "Java Regular Expression",
        "Java Algorithm",
        "Java Data Structure",
        "Java Help Compile Package ClassPath",

        "Check Square Number in Java",


        "Reverse a LinkedList in Java",
        
        "Scala Tutorial", 
        "Blender Help",

        "Mac OSX Change Terminal Cursor Color",
        "Mac OSX Turn Off Voice or Speech Notification",
        "Mac OSX Full Screen Tab-Command Not working",
        "Install Terminal Color Theme in Mac OSX",
        "Gradle add package name file",
        "Gradle Add resources folder to Project Layout",
        "Gradle Add Local Jar",
        "Intellij Add Groovy Library",
        "Intellij Create Executable Jar file",
        "Intellij Add Resources or Test Directory",
        "Intellij Remove Base Package",
        "Intellij Import your package",
        "Intellij Add Java File",
        "Intellij Path Hell",
        "Intellij Change Class Output Path",
        "Intellij SetUp",
        "Intellij Add External Library",
        "Intellij Add Maven to proejct",
        "Intellij Add dependency to Maven",
        "Intellij Add Framework Support",
        "Intellij Add Scala SDK",

        "Prime Number",
        "Merge Two Sorted Array",
        "Print All Permutation of an Array",
        "Quick Sort in C and C++",
        "Print all combination of r in a given array of size n",
        "Shuffle algorithm in C++",
        "Rotated Sorted Array", 
        "Multiply Array in Dynamic Programming",
        "Java Long Multiplication Integer Array",
        "Multiply Long Integer",
        "Add Long Integer",
        "C++ Multiply Long Integer",
        "Print Spiral in 2D array",

        "Install Maven in Mac OSX ",
        "Setup The Play Framework",
        "TestIt",
        "TestIt1",
        "TestIt2",
        "TestIt3",
        "Test4",
        "Test5"
        ] 

-- Emacs Org-mode with solarized theme css 
orgDark=[r|#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="http://thomasf.github.io/solarized-css/solarized-dark.min.css" />|]
orgLight=[r|#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="http://thomasf.github.io/solarized-css/solarized-light.min.css" />|]


{-| 
    === Try to generate Html from Haskell
-} 
textArea::String -> String
textArea s = "<textarea>" ++ s ++ "</textarea>"

{-| 
    >t = "color:red;"
    >span' "color:red;" "file"
-} 
span::String->String->String
span t s = "<span style=\"" ++ t ++ "\">" ++ s ++ "</span>"



{-| 
    === generate table
-} 
table::[[String]] -> [[String]] 
table s = [["<table>"]] ++ (map(\x -> ["<tr>"] ++ (map(\y -> "<td>" ++ y ++ "</td>" ) x) ++ ["</tr>"] ) s)  ++ [["</table>"]]
-- | -------------------------------------------------------------------------------- 

-- Mon Jun 25 23:31:02 PDT 2018
-- "cat dog" => indexCatDog.html => cat_dog.html
-- "cat dog" => "cat_dog", this will be used in new html file name
spaceToUnderScore::String->String
spaceToUnderScore s = tail $ foldl(\x y -> x ++ "_" ++ y) "" l
                    where l = splitRegex(mkRegex "[[:space:]]+") s

-- numbers of blocks per row in the gallery page
colNum = 6 

data Local = HomeMac | AmaMac deriving(Show)
getHost::Local->String
getHost HomeMac = "/Library/WebServer/Documents/zsurface"
getHost AmaMac  = "/Library/WebServer/Documents"

getHome::Local->String
getHome HomeMac = "/Users/cat"
getHome AmaMac  = "/Users/xxxx"

mathfont::String->String
mathfont s = "" ++ s 
--mathfont s = "$\\textcal{" ++ s ++ "}$"

-- | -------------------------------------------------------------------------------- 
-- | Sun Dec  2 15:29:29 2018 
-- | Change new index.html page to gallery layout
-- | -------------------------------------------------------------------------------- 
oldIndexFile = (getHost HomeMac) ++ "/indexOld.html"
newIndexFile = (getHost HomeMac) ++ "/index.html"
open_li = [r|<li><a style='text-decoration:none;' href='|]
href_li = [r|'</a>|]
close_li = [r|</li>|]

-- For live web site
remoteHost = "http://xfido.com/html/"

indexHtmlFile = (getHome HomeMac) ++ "/myfile/bitbucket/haskell/text/index.html"
pageFile = (getHome HomeMac) ++ "/myfile/bitbucket/haskell/text/page.html"

localTuple = ("html/", "../html/") 
remoteTuple= (remoteHost, remoteHost)

-- read html files and gene newhtml dir
htmlDir = (getHost HomeMac) ++ "/html"
newDir  = (getHost HomeMac) ++ "/newhtml" 
tmpHtml = (getHost HomeMac) ++ "/html_" 

randomName::IO String
randomName = getPOSIXTime >>= \t  -> return( tmpHtml ++ (show $ ceiling t))

-------------------------------------------------------------------------------- 
-- new index page code here

div_close::String
div_close = [r|</a></td>|]

--imgTag::RowBlock->String
--imgTag s = "<td style=\"width:25%;height:200px;\"><a href=\"" ++ htmlURL s ++ "\"><div style=\"text-align:center;\">" ++ title s ++ "</div>"
imgTag::RowBlock->String
imgTag s = [r|<td style='width:25%;height:200px;'><a href='|] <> htmlURL s <> [r|'><div style='text-align:center;'> |] <> title s <> [r|</div>|]
-- imgTag s = "<td style=\"width:25%;height:200px;\"><a href=\"" ++ htmlURL s ++ "\"><div style=\"text-align:center;\">" ++ title s ++ "<img src=\"" ++ imgURL s ++ "\" alt=\"My Photos\" width=\"140\" height=\"140\"></div>"

closeOpenDiv::RowBlock->String
closeOpenDiv s = "\n\n<tr>\n" ++ (imgTag s) ++ div_close 

closeOpenDivNotDiv::RowBlock->String
closeOpenDivNotDiv s = "</tr>\n\n<tr>\n" ++ (imgTag s) ++ div_close 
photoPath = "/Library/WebServer/Documents/zsurface/myphoto"
-------------------------------------------------------------------------------- 
-- for each block in the index page
data RowBlock = RowBlock{
    htmlURL :: String,
    title   :: String,
    imgURL  :: String
    } deriving (Show)

helpEx::IO()
helpEx = do 
        pp "------------------------------------------------------------------"
        pp "Usage: [ genehtml.hs l -> html for localhost ][ genehtml.hs r -> html for remote ]"
        pp "------------------------------------------------------------------"
        exitWith(ExitFailure 1) -- echo $? => 1

main = do 
        argList <- getArgs 
        if length argList == 0 
        then do
            helpEx
        else 
            pp "Take a while... if there are lots of html file:)"
        mapM_ print argList
        let twohost = case argList of
                            ("l":_) -> localTuple
                            ("r":_) -> remoteTuple 
                            _       -> ("", "")
        pp argList
        -- nothing <- getLine 
        pp "Plz check everything is OK, Enter to Cont."
        let host = fst twohost
        let pageHost = snd twohost
        putStrLn host
        putStrLn pageHost
        
        -- doesDirectoryExist newDir >>= \x -> if x then rm newDir else putStrLn "newDir does not exist" >> mkdir newDir
        doesDirectoryExist newDir >>= \x -> if x then rm newDir
                                            else putStrLn "newDir does not exist" >> mkdir newDir
        -- mkdir newDir
        -- let photos = replicate 400 "/image/curve11.svg"
        let photos = replicate (len array) "/image/blockimage.svg"
        indexHtmlContent <- readFileLatin1ToList indexHtmlFile 
        let htmlArr = map(\x -> "index" ++ (removeSpace x) ++ ".html") array -- gene html file names from array
        let orgArr  = map(\x -> "index" ++ (removeSpace x) ++ ".org") array -- gene html file names from array
        let fileArray = htmlArr ++ orgArr
        let pathList  = map(\x -> htmlDir </> x) fileArray -- gene full path html file names from array: root/html/indexFoo.html
        let hostList  = map(\x -> pageHost ++ x) fileArray -- ["indexMyPDf.html"] 
        newHtmlFile <- filterM((liftM not).doesFileExist) pathList -- extract new html file name which is not in root/html
        let zipList   = zip hostList array -- [("/path/indexMyPhoto.com", "My Photo")]
        let blockList = zipWith3(\x y z ->RowBlock{
                                        htmlURL = x, 
                                        title = y, 
                                        imgURL = z}) hostList array photos
        let menuHtml  = map(\x -> open_li ++ (fst x) ++ href_li ++ (mathfont (snd x)) ++ close_li) zipList -- gene html menu from array
        -- menuHtml = "<li><a style=\"text-decoration:none;\" href=\"xfido.com\"></a>My Photo</li>"
        --------------------------------------------------------------------------------------------------
        -- TODO add new index page here 
        let tableHtml = zipWith(\x y -> if mod y colNum == 0 then
                                          (if y == 0 then closeOpenDiv x else closeOpenDivNotDiv x)
                                        else (imgTag x ++ div_close)) blockList [0..] 

        let newIndexHtml = ["<table>"] ++ tableHtml ++ ["</tr></table>"] 
        writeToFile "/Library/WebServer/Documents/zsurface/indexTest.html" newIndexHtml 

        let newIndexPageContent = map(\x -> if length (removeSpace x) > 0 then x else unlines newIndexHtml) indexHtmlContent
        writeToFile newIndexFile newIndexPageContent 
        --------------------------------------------------------------------------------------------------
        allHtmlDirFiles <- listDirFilter htmlDir "\\.html$" -- get all the html file from html folder, e.g.  root/html
        mapM print allHtmlDirFiles

       
        -- read file content from root/html
        -- [[f1, f2..], [f3, f4..]]
        let list200 = partList 200 allHtmlDirFiles -- read 200 files only, if too many files are read, there is issue with memory
        print "Print List ->"
        iterateList list200 (\files -> do
                        pList <- filterM(\x -> doesFileExist $ htmlDir </> x) files 
                        let fullList= map(\x -> htmlDir </> x) pList 
                        let newDirList = map(\x -> newDir </> x) files 
                        ffList <- mapM(\fn -> readFile fn >>=(\contents -> return(contents))) fullList 
                        let contents = [ [x] | x <- ffList, length x > 0]
                        zipWithM(\fn list -> writeToFile fn list) newDirList contents 
                        fl
                        mapM print files
                        fl 
                      )
        ---------------------------------------------------------------------------------- 
        -- write to index.html 
        -- update index.html with new menuHtml
        -- new menu insert into space in haskll/text/index.html
        -- 
        --  index.html
        --  <html>
        --  menuHtml
        --  </html>
        -- -------------------------------------------------------------------------------- 
        -- indexHtmlContent <- readFileLatin1ToList indexHtmlFile 
        let indexPageContent = map(\x -> if length (removeSpace x) > 0 then x else unlines menuHtml ) indexHtmlContent
        writeToFile oldIndexFile indexPageContent 
        ----------------------------------------------------------------------------
        -- gene NEW html with content in pageFile if name in the array is not found in root/html 
        pagelist <- readFileLatin1ToList pageFile 

        let htmlPage = map(\x -> if length (removeSpace x) > 0 then x else "add content here") pagelist 

        -- if .html => write htmlPage content
        -- if .org  => write solarized theme css Dark
        -- orgLight => light theme
        mapM(\x ->let name = takeExtension x 
                  in if name == ".org" then writeToFile x [orgDark] else writeToFile x htmlPage) newHtmlFile  -- add new html page to root/html
        pp "done" 
        ----------------------------------------------------------------------------

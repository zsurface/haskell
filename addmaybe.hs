import Control.Monad
import Data.Char
import Data.List
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import AronModule 

addMaybe::Maybe Int->Maybe Int->Maybe Int
addMaybe m1 m2 = do
            a <- m1
            b <- m2
            return (a + b)

main = do 
        print "Hello World"
        argList <- getArgs
        pp $ length argList
        --pp $ unwrap $ addMaybe (Just 3) (Nothing)
        unwrap $ addMaybe (Just 4) (Nothing)
        pp "done!"

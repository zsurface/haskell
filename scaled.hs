import Control.Lens
import Control.Monad
import Data.Char
import Data.List
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.Traversable 
import AronModule 
import Linear.V3
import Linear.Vector
import Linear.Metric(norm)

fu::(Int->Int)->Int->Int
fu f x = f x + 1 
    where
        f a = a + 10

main = do 
        print "Hello World"
        argList <- getArgs
        pp $ length argList
        pp "done!"
        let l = [1..100]
        pp "dog"
        let v1 = V3 1 2 3 
        let vc = v1 ^/ (norm v1) 
        print $ show vc
        fl

import Control.Monad
import Data.Char
import Data.List
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import AronModule 

region::Int->Int
region n | n == 0 = 1
         | otherwise = region (n - 1) + n 

main = do 
        print "Hello World"
        print $ region 0 == 1
        print $ region 1 == 2 
        print $ region 2 == 4 
        pp "done!"

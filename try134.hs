import Data.Char 
import AronModule 

data Local = HomeMac | AmaMac deriving(Show)
getHost::Local->String
getHost HomeMac = "/Library/WebServer/Documents/zsurface"
getHost AmaMac  = "/Users/longshu/myfile/github/bsdshell.github.io"

data MyMaybe a = Nothing | Just a

for::[a]->(a -> IO()) -> IO String
for [] f = return "" 
for (x:xs) f = f x >> for xs f

heads::[[a]]->[a]
heads ll = [ h | (h:_) <- ll]

main = do 
        let html = "dog" 
        print "Hello World"
        let host = getHost HomeMac
        print host
        let host = getHost AmaMac 
        print host
        
        str <- for [1..10] (\x -> do
            print x
            let html = x 
            print "cat"
            ) 
        print str 
        let ll = [["dog", "cat"], ["cow"], []]
        let ls = heads ll
        let ss = [ head x | x <- ll, length x > 0]
        print ls
        print ss

        return ()

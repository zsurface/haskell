import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.Set
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

import AronModule 

mlist::[Integer]->[Integer]->[Integer]
mlist s []  = s
mlist [] s' = s'
mlist (x:xs) (x':xs') = if x < x' then [x, x'] ++ (mlist xs xs') else [x', x] ++ (mlist xs xs')

main = do 
        print "Hello World"
        let m = mlist [1] [3]
        pp m
        let m = mlist [3] [1]
        pp m
        let m = mlist [] [1]
        pp m
        let m = mlist [1, 4, 7] [2, 9]
        pp m
        pp "done!"

import Control.Monad
import Data.Char
import Data.List
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import AronModule 

-- given coefficient list, evaluate the polynomial
-- poly [1, 2, 3] [-1, 0, 1]
-- p(x) = 1x^0 + 2x^1 + 3x^2, where x = [-1, 0, 1]
--
-- p1(-1)= 1(-1)^0 + 2(-1)^1 + 3(-1)^2
-- p2(0) = 1(0)^0 + 2(0)^1 + 3(0)^2
-- p3(1) = 1(1)^0 + 2(1)^1 + 3(1)^2
poly::[Double]->[Double]->[Double]
poly [] _ = [] 
poly _ [] = [] 
poly xs sx = map(\s -> sum s) $ map(\x -> map(\(c, p) -> c*(x^p)) xx ) sx
            where
                xx = zip xs [0..]

-- 1 + x + x^2 = x = 0
-- 1 + x + x^2 = x = 1 
-- 1 + x + x^2 = x = 2 
main = do 
        print "Hello World"
        let c = [2, 1, 3]
        let tup = zip c [0..]
        -- [(2, 0), (1, 1), (3, 2)]
        let s1 = map(\x -> map(\(c, p) -> c*(x^p)) tup ) [1..10]
        pp $ join s1
        --let ss = map(\x -> map(\(c, p) x -> c*(x^p)) zp) $ map(\x -> 0.1*x) [-10..10] 
        pp "done!"
        let p = poly [1, 1, 1] [0, 1, 2]
        let rr = map(\x -> round x) p
        pp rr 

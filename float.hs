import Control.Monad
import Data.Char
import Data.List
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import AronModule 


main = do 
        print "Hello World"
        let n = 3::Int 
        let d = 4::Double
        let e = 4::Integer 
        pp $ sqrt (fromIntegral n)
        pp $ _sqrt n
        pp $ sqrt d
        pp $ _sqrt e 
        let m = 1::Int 
        pp $ _cos m 
        let k = 2::Integer
        pp $ _cos k
        pp "done"

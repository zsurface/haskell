import Control.Monad
import Data.Char
import Data.List
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import AronModule 



-- | Wed Aug 29 16:10:45 2018  
-- | open #pdf open pdf file for Vim,  vopen.hs, generate bin file in $ff/mybin/vopen
-- run:[ hasekllbin vopen.hs vopen ] to generate $ff/mybin/vopen
-- run vopen.hs /Library/WebServer/Documents/zsurface/pdf/vimtips.pdf
main = do 
        print "Hello World"
        argList <- getArgs
        let len =  length argList
        let file = head argList
        let list = splitRegex(mkRegex ".") $ file 
        unless (last list == "pdf") $ run ("open " ++ file ) >> return ()
        pp "done"

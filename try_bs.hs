-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}  --
-- 
-- import Turtle                       --
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".

import AronModule 

binarySearch::(Ord a)=>a -> [a]-> Bool
binarySearch k [] = False
binarySearch k cx = if l == 1 then k == head cx 
                     else (if k < head re 
                           then binarySearch k le 
                           else (if k > head re then binarySearch k (tail re) else True))
    where
        l = length cx
        m = div l 2
        le = take m cx
        re = drop m cx

main = do 
        print "Hello World"
        argList <- getArgs
        pp $ length argList
        pp "done!"
        let s = [1..10]
        pp $ "binarySearch =" <<< (binarySearch 3 s == True)
        pp $ "binarySearch =" <<< (binarySearch 0 s == False)
        pp $ "binarySearch =" <<< (binarySearch 9 s == True)
        pp $ "binarySearch =" <<< (binarySearch 1 [1] == True)
        pp $ "binarySearch =" <<< (binarySearch 2 [1] == False)
        pp $ "binarySearch =" <<< (binarySearch 2 []  == False)

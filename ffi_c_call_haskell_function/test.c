#include <HsFFI.h>
#ifdef __GLASGOW_HASKELL__
#include "Safe_stub.h"
#endif
#include <stdio.h>

int main(int argc, char *argv[])
{
    int i;
    hs_init(&argc, &argv);

    i = fibonacci_hs(10);
    printf("Fibonacci: %d\n", i);
    printf("fun_hs: %d\n", fun_hs(10));

    hs_exit();
    return 0;
}

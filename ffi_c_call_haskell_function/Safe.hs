{-# LANGUAGE ForeignFunctionInterface #-}

module Safe where

import Foreign.C.Types

fibonacci :: Int -> Int
fibonacci n = fibs !! n
    where fibs = 0 : 1 : zipWith (+) fibs (tail fibs)

fun:: Int -> Int
fun x = x + 1

fibonacci_hs :: CInt -> CInt
fibonacci_hs = fromIntegral . fibonacci . fromIntegral

fun_hs :: CInt -> CInt
fun_hs = fromIntegral . fun . fromIntegral


foreign export ccall fibonacci_hs :: CInt -> CInt
foreign export ccall fun_hs :: CInt -> CInt

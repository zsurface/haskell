import Control.Monad
import Data.Char
import Data.List
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.List.Split

import AronModule 

photoPath = "/Library/WebServer/Documents/zsurface/myphoto"
phtml = "/Users/cat/myfile/bitbucket/haskell/text/myphoto.html"

--imgTag::String->String
--imgTag s = "<div class=\"blocks\"><a target=\"_blank\" href=\"/Library/WebServer/Documents/zsurface/myphoto/" ++ s ++ "\"><img src=\"/Library/WebServer/Documents/zsurface/myphoto/" ++ s ++ "\" alt=\"Cinque Terre\" width=\"200\" height=\"200\">"


--    <tr>
--    <td><a href="../pdf/polyring.pdf">Polynomial, Ring, Ideal, Fermat Little Theorem<img src="../../image/curve11.svg" width="100%" height="100%" /></a> </td>
--    <td><a href="../pdf/latexnote.pdf">Latex Snippet<img src="../../image/curve11.svg" width="100%" height="100%" /></a></td> 
--    <td><a href="../pdf/phonetic_symbol.pdf">Latex Phonetic Symbols<img src="../../image/curve11.svg" width="100%" height="100%" /></a></td>
--    <td><a href="../pdf/mywords.pdf">All Words with Phonetic Symbols<img src="../../image/curve11.svg" width="100%" height="100%" /></a></td>
--    <tr>

array = [
        "OpenCV Image Blending",
        "OpenCV Change Image Contrast and Brightnes",
        "OpenCV Convert Image to Grayscale Image",
        "OpenCV Read Image and Show them",
        "OpenCV Linear Interpolation",
        "OpenCV from Scratch",
        "Agda and Haskell",
        "Tmux Commands",
        "Find the square root of an Integer",
        "Haskell Maybe"
        ]

imgTag::String->String
imgTag s = "<td><a href=\"../pathToHtmlFile/" ++ s ++ "\"><div class=\"mytext\">Haskell tutorial, cool stuff This is nice description</div><img src=\"../myphoto/" ++ s ++ "\" alt=\"My Photos\" width=\"200\" height=\"200\">"

imgTag'::(String, String)->String
imgTag' s = "<td><a href=\"../pathToHtmlFile/" ++ "xxx.html" ++ "\"><div class=\"mytext\">" ++ (fst s) ++ "</div><img src=\"../myphoto/" ++ (snd s) ++ "\" alt=\"My Photos\" width=\"200\" height=\"200\">"


div_close::String
div_close = "</a></td>\n"

closeOpenDiv::String->String
closeOpenDiv s = "\n\n<tr>\n" ++ (imgTag s) ++ div_close 

closeOpenDiv'::(String, String)->String
closeOpenDiv' s = "\n\n<tr>\n" ++ (imgTag' s) ++ div_close 

closeOpenDivNotDiv::String->String
closeOpenDivNotDiv s = "</tr>\n\n<tr>\n" ++ (imgTag s) ++ div_close 

closeOpenDivNotDiv'::(String, String)->String
closeOpenDivNotDiv' s = "</tr>\n\n<tr>\n" ++ (imgTag' s) ++ div_close 

main = do 
        photos <- lsFile photoPath 
        let fp = zipWith(\x y -> (x, y)) array photos 
        html <- readFileToList phtml 
        -- print html 
        -- split html to two part
        let list = splitWhen(\x -> (length $ trimWS x) == 0) html
        let hsp = head list
        let tsp = last list
        let ff = zipWith(\x y -> if mod y 4 == 0 then (if y == 0 then closeOpenDiv' x else closeOpenDivNotDiv' x) else (imgTag' x ++ div_close)) fp [0..] 
        let fff = ff ++ ["</tr>"] 
        writeToFile "/tmp/fff.html" fff 
        let all = hsp ++ fff ++ tsp 
        -- writeToFile "/Users/cat/try/trymenu.html" all
        -- writeToFile "/Library/WebServer/Documents/zsurface/html/trytest1.html" all
        writeToFile "/Library/WebServer/Documents/zsurface/indexTest.html" all

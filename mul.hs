import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.Set
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

import AronModule 

import Data.List (transpose, inits)
import Data.Char (digitToInt)
 
digits :: Integer -> [Integer]
digits = fmap (fromIntegral . digitToInt) . show
 
lZZ :: [[Integer]]
lZZ = inits $ repeat 0
 
table :: (Integer -> Integer -> Integer) -> [Integer] -> [Integer] -> [[Integer]]
table f x = fmap $ flip fmap x . f
 
polymul :: [Integer] -> [Integer] -> [Integer]
polymul xs ys = fmap sum (transpose (zipWith (++) lZZ (table (*) xs ys)))
 
longmult :: Integer -> Integer -> Integer
longmult x y = foldl1 ((+) . (10 *)) (polymul (digits x) (digits y))
 
main :: IO ()
main = do
       print $ (2 ^ 64) `longmult` (2 ^ 64)
       print $ polymul [1, 2, 3] [4, 4]

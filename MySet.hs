import Control.Monad
import Data.Char
import Data.List
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.Set(Set)
import qualified Data.Set as S 
import AronModule

main = do 
        print "Hello World"
        let sset = S.fromList [1..10]
        print $ show sset
        let s1 = S.insert 23 sset
        print $ show s1 
        let ds = S.delete 10 sset
        print $ show ds 
        print $ (S.member 1 ds)

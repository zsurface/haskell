import Control.Monad
import Data.Char
import Data.List
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import AronModule 

run::String->IO [String]
run s = do 
        (Nothing, Just hout, Nothing, ph) <- createProcess p
        ec <- waitForProcess ph
        hGetContents hout >>= \out -> return $ lines out
        where 
            p = (shell s)
                { std_in  = Inherit
                , std_out = CreatePipe
                , std_err = Inherit
                }

main = do 
        argList <- getArgs
        if length argList > 0 then mkdirp $ head argList
            else
                pp "Invalid arg"
        

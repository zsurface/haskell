let SessionLoad = 1
if &cp | set nocp | endif
let s:cpo_save=&cpo
set cpo&vim
inoremap <Right> :tabn
inoremap <Left> :tabp
map! <F3> :exec 'put *' 
map! <D-v> *
nnoremap <silent>  :nohlsearch
map ,rwp <Plug>RestoreWinPosn
map ,swp <Plug>SaveWinPosn
map ,n :b #
noremap ,i :call ToggleIgnoreCase()
noremap ,k :call ToggleCompletefunc()
noremap ,s :call ToggleSpell()
nmap <silent> ,c :call CopyJavaMethod()
nmap <silent> ,p :!vopen <cfile>
nmap <silent> ,d :!open dict://<cword>
vmap 2[ :s/\%V\S.*\S\%>v/\\[ \0 \\]/ 
vnoremap a$ F$of$
onoremap <silent> a$ :normal! F$vf$
vmap gx <Plug>NetrwBrowseXVis
nmap gx <Plug>NetrwBrowseX
nnoremap gO :!open <cfile>
vnoremap i$ T$ot$
onoremap <silent> i$ :normal! T$vt$
vnoremap xu :s/\%V\_^\s*\zs--\%V//g 
vnoremap xx :s/\%V\_^\%V/--/g 
map <F9> :w! | :call CompileHaskell()
vnoremap <silent> <Plug>NetrwBrowseXVis :call netrw#BrowseXVis()
nnoremap <silent> <Plug>NetrwBrowseX :call netrw#BrowseX(expand((exists("g:netrw_gx")? g:netrw_gx : '<cfile>')),netrw#CheckIfRemote())
nmap <silent> <Plug>RestoreWinPosn :call RestoreWinPosn()
nmap <silent> <Plug>SaveWinPosn :call SaveWinPosn()
map <F12> :Bf o
map <F8> :w!
map <F5> :tabnew $HOME/myfile/bitbucket/snippets/snippet.m 
map <PageDown> :tabc
map <PageUp> :tabnew
nnoremap <Right> :tabn
nnoremap <Left> :tabp
vmap <F2> "*y | :echo 'copy [' . @* . '] to clipboard' 
map <F6> :call ToggleBracketGroup() 
vmap <BS> "-d
vmap <D-x> "*d
vmap <D-c> "*y
vmap <D-v> "-d"*P
nmap <D-v> "*P
imap  :call AddBegin()
inoremap ,s =ToggleSpell()
inoremap ,i =ToggleIgnoreCase()
inoremap ,k =ToggleCompletefunc()
cmap BB s//\[\0\]/  
cmap SV vim // **/*.m
cmap SS .,$s///gc
cmap White /\S\zs\s\+$
cmap ff s//<strong>\0<\/strong>/   
cmap hh s//\\text{ \0 }/  
iabbr openn From: Aron <aronsitu00@gmail.com>Subject: I'm open to new opportunitiesSubject: Software Engieer Seeking New Opportunity Hi,A little about me:I graduated from University of Waterloo in 2012.With a Computer Science degree from University of Waterloo, I have thorough understanding of data structures/algorithms  and knowledgeable of back-end development best practice. I also have experience in learning and excelling at new technologies as needed.I'm a fast learning and hard working person with a great sense of humor Please find attached my resume for your review.Phone: 408-844-4280 Thanks Aron
iabbr mee With a Computer Science degree from University of Waterloo, I have thorough understanding of data structures/algorithms  and knowledgeable of back-end development best practice. I also have experience in learning and excelling at new technologies as needed.I'm a fast learning and hard working person with a great sense of humor
iabbr thxx Hi,  Thank you for taking the time to speak with me today.Phone: 408-844-4280Thanks
iabbr foll Hi Thanks for the prompt response.Phone: 408-844-4280Thanks
iabbr rep Hi Thank you for taking the time to speak with me today.Phone: 408-844-4280Thanks
iabbr repa Hi I'm avaiable in the moring(9:30-12:00)PST from Monday to Friday  Phone: 408-844-4280Thanks
iabbr rep_available HiThank you for contacting me.I'm free on Tuesday and Wednesday.I prefer the call in the moring from 9:30 - 11:30 if you can schedule it.Phone: 408-844-4280
iabbr mm From: Aron<aronsitu00@gmail.com>From: Shu<shu334sit@gmail.com>From: Shu<0aronsitu@gmail.com>
iabbr emm From: Shu<shu334sit@gmail.com>From: Aron<aronsitu00@gmail.com>From: Aron<aron314s@gmail.com>From: Shu<shusitu123@gmail.com>From: Aron<aronsit@gmail.com>From: Aron<aronsitu1@gmail.com>From: Aron<aronsitu@outlook.com>From: Aron<aronsitus@gmail.com>From: Aron<0aronsitu@gmail.com>From: Long<longaron00@gmail.com>From: Aron<aronsitu@outlook.com>From: 314<3141590i@gmail.com>From: Peter<petersitu11@gmail.com>From: Elliptic<ellipticcode0@gmail.com>From: Elliptic<ellipticcode@gmail.com>
iabbr jobb To Whom it may concern,I'm open to new opportunities currently.Please find attached my resume for your review.A little bit about me:With a Computer Science degree from University of Waterloo, I have thorough understanding of data structures/algorithms  and knowledgeable of back-end development best practice. I also have experience in learning and excelling at new technologies as needed.I'm a fast learning and hard working person with a great sense of humor. ThanksAron
iabbr im_available I'm avaiable at xxx (PST) on Monday to Friday.
iabbr aronsitu Aron<aronsitu00@gmail.com>
iabbr shu_shu334sit From: Shu<shu334sit@gmail.com>
iabbr 314 3141590i@gmail.com
cabbr Kre :let @*="/Users/cat/GoogleDrive/NewResume"
cabbr Kr :let @*="rootmath@gmail.com"
cabbr Kph :let @*="Phone: 408-844-4280"
cabbr Kp :let @*="408-844-4280"
cabbr Kn :let @*="3141590i@gmail.com"
cabbr vnote :Bf o
iabbr <expr> ~~~ repeat('~', 80) 
iabbr <expr> +++ repeat('+', 80) 
iabbr <expr> === repeat('=", 80) 
iabbr <expr> --- repeat('-', 80) 
iabbr <expr> ddd strftime('%c')
cabbr se :call SetEnvVar() 
cabbr pl :call ListTabPage() 
cabbr bufm :call ToggleBufferManager() 
cabbr Ma :call MaximizeToggle() 
cabbr Ela :tabnew /Library/WebServer/Documents/zsurface/html/indexLatexMatrix.html
cabbr Info :tabnew ~/.viminfo
cabbr Ta call TagsSymlink() 
cabbr Tb :!/Applications/Utilities/Terminal.app/Contents/MacOS/Terminal /bin/bash & 
cabbr Tf :!/Applications/Utilities/Terminal.app/Contents/MacOS/Terminal /bin/fish & 
cabbr Tz :!/Applications/Utilities/Terminal.app/Contents/MacOS/Terminal /bin/zsh & 
cabbr Ky :let @*=expand("%:p")
cabbr Job :tabnew /Users/cat/GoogleDrive/job/recruiter_email.txt 
cabbr Ran :tabnew /Library/WebServer/Documents/zsurface/randomNote.txt  
cabbr No :tabnew $HOME/myfile/bitbucket/note/noteindex.txt 
cabbr Ep :tabnew /Users/cat/myfile/vimprivate/private.vimrc
cabbr Ec :tabe /Library/WebServer/Documents/zsurface/html/indexCommandLineTricks.html  
cabbr Eng :tabe /Library/WebServer/Documents/zsurface/html/indexEnglishNote.html  
cabbr Evimt :tabe /Library/WebServer/Documents/zsurface/html/indexVimTricks.html
cabbr Enote :tabe /Library/WebServer/Documents/zsurface/html/indexDailyNote.html
cabbr Esty :tabe /Library/WebServer/Documents/zsurface/style.css
cabbr Sty :%!astyle --style=java 
cabbr Int :!open /Users/cat/myfile/github/text/intellij_shortcut.pdf    
cabbr Cmmc :! /Users/cat/myfile/script/CodeManager.sh
cabbr Cmm :! /Users/cat/myfile/github/javabin/CodeManager/run.sh 
cabbr Res :!open /Users/cat/GoogleDrive/NewResume/aronsitu00resume.pdf     
cabbr Wo :tabe /Users/cat/myfile/github/vim/myword.utf-8.add    " My words file
cabbr jf :tabe /tmp/f.x
cabbr mylib :tabe /Users/cat/myfile/github/cpp/MyLib
cabbr mm :marks
cabbr qn :tabe $HOME/myfile/github/quicknote/quicknote.txt " quick node
cabbr esess :tabe $HOME/myfile/vimsession/sess.vim 
cabbr mk :mksession! $HOME/myfile/vimsession/sess.vim
cabbr ep :tabnew /etc/profile 
cabbr mmax printf '\ePtmux;\e\e[9;1t\e\\'
cabbr mhalf printf '\ePtmux;\e\e[8;100;200t\e\\'
cabbr full printf '\e[10;1t' " full-screen mode 
cabbr umax printf '\e[10;0t' " undo full-screen mode 
cabbr half printf '\e[8;100;200t \e[3;1410;0t' " resize to 200x100/move to right
cabbr maxh printf '\e[9;3t' " maximize horizontally
cabbr maxv printf '\e[9;2t' " maximize vertically
cabbr max printf '\e[9;1t' " maximize
cabbr min printf '\e[2t' " minimize window
cabbr jaron :tabnew $jlib/Aron.java
cabbr bk :tabnew $HOME/myfile/bitbucket/bookmark/bookmark.txt
cabbr sll :call StatusLineManager()
cabbr ev :tabe $HOME/myfile/bitbucket/vim/vim.vimrc
cabbr sv :source $HOME/myfile/bitbucket/vim/vim.vimrc 
cabbr big :tabe  $HOME/cat/myfile/github/java/big.java
cabbr kk .g/\S*\%#\S*/y | let @*=@" 
cabbr ip :call  InitDir()
cabbr fp :call  CopyCurrentFilePath()
cabbr up :tabnew /Users/longshu/myfile/backup/up.x
cabbr aa %s/\(,\)\(\w\)/\1\r\2/g
cabbr mmm %s/--- a/\/home\/longshu\/myfile\/workspace\/ContraCogsFunding\/src\/ContraCogsFundingBookingLibrary/g
cabbr eb :tabe ~/.bashrc
let &cpo=s:cpo_save
unlet s:cpo_save
set autochdir
set autoindent
set autoread
set backspace=2
set complete=k/Users/cat/myfile/bitbucket/math/mybook.tex,k$HOME/myfile/bitbucket/haskell/*.hs
set completefunc=CompleteAbbre
set expandtab
set fileencodings=ucs-bom,utf-8,default,latin1
set helplang=en
set hlsearch
set ignorecase
set laststatus=2
set listchars=eol:$,tab:>-,trail:~,extends:>,precedes:<
set path=.,/usr/include,,,**
set ruler
set shiftwidth=4
set showcmd
set smartindent
set spellfile=~/myfile/bitbucket/vim/myword.utf-8.add
set statusline=%t\ [%1.5n]\ %l:%c\ %r\ %m
set noswapfile
set tabstop=4
set tags=./tags,tags,~/.vim/tags/java.tags,~/myfile/bitbucket/java/.tag,~/myfile/bitbucket/javalib/.tag,~/myfile/bitbucket/haskell/tags,~/myfile/bitbucket/haskell_webapp/tags
set notimeout
set timeoutlen=100
set ttimeout
set wildmenu
set nowritebackup
let s:so_save = &so | let s:siso_save = &siso | set so=0 siso=0
let v:this_session=expand("<sfile>:p")
silent only
cd ~/myfile/bitbucket/haskell
if expand('%') == '' && !&modified && line('$') <= 1 && getline(1) == ''
  let s:wipebuf = bufnr('%')
endif
set shortmess=aoO
badd +406 ~/myfile/bitbucket/haskell/plot_glfw.hs
badd +1 ~/myfile/bitbucket/opengl
badd +51 ~/myfile/bitbucket/opengl/HelloWorld3D/helloworld3d.cpp
badd +41 ~/myfile/bitbucket/haskell/IORef.hs
badd +104 ~/myfile/bitbucket/haskell/keyboardpress.hs
badd +0 ~/myfile/bitbucket/haskell/keyboardpress_tmp.hs
argglobal
silent! argdel *
argadd plot_glfw.hs
edit ~/myfile/bitbucket/haskell/keyboardpress_tmp.hs
set splitbelow splitright
set nosplitbelow
set nosplitright
wincmd t
set winheight=1 winwidth=1
argglobal
iabbr <buffer> address_richmond_address 9811 Ferndale Rd #14, Richmond BC
iabbr <buffer> email_aron_shu334_0aronsitu From: Shu<shu334sit@gmail.com>From: Shu<0aronsitu@gmail.com>
iabbr <buffer> email_all From: Shu<shu334sit@gmail.com>From: Aron<aronsitu00@gmail.com>From: Aron<aron314s@gmail.com>From: Shu<shusitu123@gmail.com>From: Aron<aronsit@gmail.com>From: Aron<aronsitu1@gmail.com>From: Aron<aronsitu@outlook.com>From: Aron<aronsitus@gmail.com>From: Aron<0aronsitu@gmail.com>From: Long<longaron00@gmail.com>From: Aron<aronsitu@outlook.com>From: 314<3141590i@gmail.com>From: Peter<petersitu11@gmail.com>From: Elliptic<ellipticcode0@gmail.com>From: Elliptic<ellipticcode@gmail.com>
iabbr <buffer> phone_cell 778-232-2175
iabbr <buffer> phone_US 408-844-4280Phone: 408-844-4280
iabbr <buffer> secret_mystuff 408-844-4280
iabbr <buffer> Intellij_shortcut_key // intellij shortcut keyCtrl Shift      // comment block codeShift Tab       // move block of code(code selection) to leftTab/Shift+Tab   // indent/unintented selected lineAlt Right/Left  // cycle through tabsAlt Shift F7    // goto usageAlt Shit f      // global searchAlt g           // find nameAlt UP/Down     // move to previous/next methodCtrl F12        // show all current methodsCtrl W          // select code block. e.g {..}Ctrl-Shift F12  // max/unmaxed windowCtrl UP/Down    // move window up/downCtrl [/]        // Move to code block star/endCtrl Shift #    // book mark line, bookmark lineCtrl #          // goto bookmark #, goto book mark #Ctrl Alt B      // goto implementationCtrl E          // go back to implementationCtrl Alt F7     // shows usagesCtrl w          // select whole wordCtrl Left/Right // move cursor previous/nextCtrl Shift R    // find and replaceCtrl Shift j    // join lineCommand F4      // close tabALT + Command L // reformat code
iabbr <buffer> bookmark https://alexey.radul.name/ideas/2013/introduction-to-automatic-differentiation/    automatic differentiation in Haskellhttp://5outh.blogspot.com/2013/05/symbolic-calculus-in-haskell.html  nice haskell implementation differentiationhttp://www.danielbrice.net/research/https://github.com/friedbrice/AutoDiff/blob/master/README.mdhttps://justindomke.wordpress.com/2009/02/17/automatic-differentiation-the-most-criminally-underused-tool-in-the-potential-machine-learning-toolbox/https://github.com/friedbrice/AutoDiffhttps://ca.indeed.com/jobs?q=software%20developer&l=Vancouver%2C%20BC&start=190&vjk=0996c65a4307dfcbhttps://ca.indeed.com/jobs?q=haskell&l=Vancouver%2C%20BC&vjk=b89c80555ddff9dbhttps://ca.indeed.com/jobs?q=haskell&l=Vancouver%2C%20BC&vjk=da1811c5255cb09bhttps://ca.indeed.com/jobs?q=software%20engineer&l=Vancouver%2C%20BC&start=60&vjk=cf6a1197f8820967https://ca.indeed.com/jobs?q=software%20engineer&l=Vancouver%2C%20BC&start=150&vjk=87fa4dcd66e9f427https://jobs.lever.co/skyboxlabs/ea330293-4a2b-479c-8fcc-4a8b1202a377https://ca.indeed.com/jobs?q=software%20engineer&l=Vancouver%2C%20BC&start=190&vjk=2c52530e2bb82943https://ca.indeed.com/jobs?q=software%20engineer&l=Vancouver%2C%20BC&start=190&vjk=4e210f52e43a8075http://www.cs.nott.ac.uk/~pszgmh/pearl.pdfhttps://www.vex.net/~trebla/haskell/index.xhtmlhttps://ca.indeed.com/jobs?q=software%20engineer&l=Vancouver%2C%20BC&fromage=last&start=10&vjk=513ff4b9ceead549https://stackoverflow.com/questions/41630650/are-join-and-fmap-join-equals-in-haskell-from-category-theory-point-of-viewhttps://kseo.github.io/posts/2016-12-30-write-you-an-interpreter.htmlhttp://www.stephendiehl.com/llvm/       write your a compile in Haskellhttps://www.indeed.ca/jobs?q=software%20engineer&l=Vancouver%2C%20BC&start=180&vjk=72bb8ab7962118b6http://colah.github.io/https://github.com/Gabriel439/slides/blob/master/regex/regex.mdhttps://lettier.github.io/posts/2017-02-25-matrix-inverse-purescript.htmlhttps://ca.indeed.com/jobs?q=mathematics&l=Vancouver%2C%20BC&start=20&vjk=99779df35972b7c2
iabbr <buffer> cpp_filter_map std::vector<int> mVec = {1,4,7,8,9,0};// filter out values > 5auto gtFive = select_T<int>(mVec, [](auto a) {return (a > 5); });// or > targetint target = 5;auto gt = select_T<int>(mVec, [target](auto a) {return (a > target); });
iabbr <buffer> cpp_vector // constructing vectors#include <iostream>#include <vector>int main () {// constructors used in the same order as described above:std::vector<int> first;                                // empty vector of intsstd::vector<int> second (4,100);                       // four ints with value 100std::vector<int> third (second.begin(),second.end());  // iterating through secondstd::vector<int> fourth (third);                       // a copy of third// the iterator constructor can also be used to construct from arrays:int myints[] = {16,2,77,29};std::vector<int> fifth (myints, myints + sizeof(myints) / sizeof(int) );//std::cout << "The contents of fifth are:";for (std::vector<int>::iterator it = fifth.begin(); it != fifth.end(); ++it)std::cout << ' ' << *it;std::cout << '\n';return 0;}
iabbr <buffer> cpp_vector_iterator std::vector<std::string> results;for ( auto itr = _addresses.begin(), end = _addresses.end(); itr != end; ++itr) {// call the function passed into findMatchingAddresses and see if it matchesif ( func( *itr ) ) {results.push_back( *itr );}}
iabbr <buffer> emacs_key C-x 2                        // split window horizonallyC-x 3                        // split window verticallyC-x 0                        // close current bufferC-x 1                        // maximize current bufferC-z                          // goto background fg => back to foregroundC-u M-!                      // shell_command => Vim:lsM-x shell                    // run shell within EmacsC-v M-v                      // page up, page downC-a C-e                      // move cursor to begin and End of lineC-n C-p                      // move line down, line upC-f                          // move cursor character/word ForwardC-b                          // move cursor character/word BackwardC-k                          // kill all word to end of line or end of sentenceC-u/C-_                      // undoC-r C-r                      // backward search goto next occurrentC-s C-s                      // search goto next occurrentC-x C-f                      // find fileC-s                          // search file I-searchC-x C-s                      // save fileC-x b                        // switch bufferC-x o                        // switch to other windowC-x Spc                      // set markM-w                          // copyC-y                          // pasteC-x C-b                      // list bufferC-x e                        // evaluate lispM-:                          // MacOS (alt shift) :M-:                          // caps lock : => evaluate elispM-g M-g                      // Alt g, Alt g => goto-lineC-x C-c                      // exit EmacsF1                           // help commandC-x RET C-\ TeX RET          // input latex charaC-x -/+                      // increase decrease font--------------------------------------------------------------------------------M-x list-packages  'i' => mark, 'x' => install   // select package to installC-c C-c => compile, C-c C-c => view   // LatexC-c C-h => See all Auctex shortcut  // AuctexC-c ] => close latex environmentF3 => start define MacroF4 => end define MacroF4 => repeat Macro---------------------------------------------------------------------------------1. run: agda-mode setup from command line2. M-x load-library3. M-x agde2 => M-x agda2-mode // Agda mode--------------------------------------------------------------------------------M-x describe-input-methodremaping many Emacs default keys binding
iabbr <buffer> encry_key `[*GPG* *gpg* *gpg_key* *gen_key*gpg -d -o password.txt password.txt.gpg // decrypt gpg file to password.txtgpg -c dir.tar.gzgpg -c -o dir.tar.gz dir.tar.gz.gpg     // encrypt your tar filegpg --gen-key                           // generate new keygpg --list-keys                         // list keygpg --delete-keys C3B95227              // delete key uid=C3B95227tar -czf dir.tar.gz dir                 // tar filetar -xzf dir.tar.gz dir                 // untar file`]
iabbr <buffer> eng_at_in_on Prepositions of Time - at, in, onWe use:at for a PRECISE TIMEin for MONTHS, YEARS, CENTURIES and LONG PERIODSon for DAYS and DATESat PRECISE TIMEin MONTHS, YEARS, CENTURIES and LONG PERIODSon DAYS and DATES//at 3 o'clock	in May	on Sundayat 10.30am	in summer	on Tuesdaysat noon	in the summer	on 6 Marchat dinnertime	in 1990	on 25 Dec. 2010at bedtime	in the 1990s	on Christmas Dayat sunrise	in the next century	on Independence Dayat sunset	in the Ice Age	on my birthdayat the moment	in the past/future	on New Year's Eve
iabbr <buffer> eng_base Base - base caseBases - plural baseBasic - adjestiveBasics - noun (plural)Basis - noun (plural bases)Bases - noun (plural base and basis)
iabbr <buffer> eng_interest interested - are you interested in the job.interesting - it will be very interesting to see what they can come up with.
iabbr <buffer> filee //[ file=kk.html title=""
iabbr <buffer> git_command https://ariya.io/2013/09/fast-forward-git-merge
iabbr <buffer> git_command_all_cmds [Git command git branch show on command prompt]:git branch myfeature_branchcreate                   // create new feature branch:git branch -d delete_branchdelete                   // [git delete] feature branch:git branch -D delete_branchdelete                   // unmerged feature branch:git checkout master                                 // switch to master branch:git merge --no-ff feature_branch                    // merge feature_branch to master without fast forwrad.:git merge feature_branch                            // merge feature_branch to master with fast forward.:git rm -r --cached myfile                           // remove file from your index:git refloghow                                       // all the reference log:git reset --hard 828c8c0b1995                       // revert to previous commit:git diff --stat hash1  hash2                        // diff two commits from two hashes:git log -- specific_file                            // get log on specific file:git commit --amend -m "new message"                 // change the most recent message, amend your most recent message.:git rev-list --all --remotes --pretty=oneline foo   // show all previous commit on foo:git rev-list --all foo                              // show all previous commit on foo:git stash                                           // save your modified tracked files to a stack and that you can reapply late.:git stash list                                      // list all the stashes:git rm -r myfile                                    // remove file from local repository and file system.:git rm --cached rm_repos_file                       // remove file only from local repository but from file system.:git commit -m "msg":.git/info/exclude                                   // for local file that does't need to be shared, just add file or dir pattern//// update .gitignore file:git rm --cached <file>                              // remove specific tracked file:git rm --cached -r .                                // remove all tracked files:git add .                                          // track all filesparse_git_branch(){git branch 2> /dev/null | sed -e 's/* \(.*\)/ \(\1)/'}export PS1="\u$(parse_git_branch)\w$"\u - Username\w - Full path\h - Host nameimg,/Users/cat/try/gitfastforward.png
iabbr <buffer> git_conf [core]repositoryformatversion = 0filemode = truebare = falselogallrefupdates = trueignorecase = trueprecomposeunicode = false[remote "origin"]url = https://github.com/bsdshell/tiny3.giturl = https://zsurface@bitbucket.org/zsurface/tiny3.gitfetch = +refs/heads/*:refs/remotes/origin/*[branch "master"]remote = originmerge = refs/heads/master
iabbr <buffer> git_fast_forward https://ariya.io/2013/09/fast-forward-git-merge
iabbr <buffer> git_ignore_file **/*.class# Package Files #*.war*.ear.idea/dictionaries/cat.xml.idea/workspace.xml.idea/vcs.xml.idea/misc.xml
iabbr <buffer> gitignore # Ignore all*--------------------------------------------------------------------------------# Unignore all with extensions!*.*--------------------------------------------------------------------------------# Unignore all dirs!*/--------------------------------------------------------------------------------### Above combination will ignore all files without extension ###--------------------------------------------------------------------------------# Ignore files with extension `.class` & `.sm`*.class*.sm--------------------------------------------------------------------------------# Ignore `bin` dirbin/# or*/bin/*--------------------------------------------------------------------------------# Unignore all `.jar` in `bin` dir!*/bin/*.jar--------------------------------------------------------------------------------# Ignore all `library.jar` in `bin` dir*/bin/library.jar--------------------------------------------------------------------------------# Ignore a file with extensionrelative/path/to/dir/filename.extension--------------------------------------------------------------------------------# Ignore a file without extensionrelative/path/to/dir/anotherfile
iabbr <buffer> google_book_path Reading multiple thread in linkedlist, queue and hashmap./Users/cat/GoogleDrive/Books/thread_concurrent_linkedlist.pdf
iabbr <buffer> gpg_tar_encrypted_decrypted_file gpg -d -o password.txt password.txt.gpg // decrypt gpg file to password.txttar -czf dir.tar.gz dir                 // tar filegpg -c dir.tar.gzgpg -c -o dir.tar.gz dir.tar.gz.gpg     // encrypt your tar filegpg --gen-key                           // generate new keygpg --list-keys                         // list keygpg --delete-keys C3B95227              // delete key uid=C3B95227tar -czf dir.tar.gz dir                 // tar file, tar folder, tar directory, tar directoriestar -xzf dir.tar.gz dir                 // untar file
iabbr <buffer> haskell_convert read "3.14"::Floatread "123"::Intread "123"::Integerreads "123aa"::[(Int, String)] => [(123, "aa")]reads "aa123"::[(Int, String)] => []http://stackoverflow.com/questions/2468410/convert-string-to-integer-float-in-haskell
iabbr <buffer> haskell_file_stuff listDirectorydoesFileExistdoesDirectoryExisttakeFileName gives "file.ext"takeDirectory gives "/directory"takeExtension gives ".ext"dropExtension gives "/directory/file"takeBaseName gives "file""/directory" </> "file.ext"."/directory/file" <.> "ext"."/directory/file.txt" -<.> "ext".
iabbr <buffer> haskell_import_module import Control.Monadimport Data.Charimport Data.Listimport Data.Timeimport Data.Time.Clock.POSIXimport System.Directoryimport System.Environmentimport System.Exitimport System.FilePath.Posiximport System.IOimport System.Posix.Filesimport System.Posix.Unistdimport System.Processimport Text.Readimport Text.Regeximport Text.Regex.Baseimport Text.Regex.Base.RegexLikeimport Text.Regex.Posiximport Control.Exceptionimport AronModule
iabbr <buffer> haskell_map import qualified Data.Map as MapphoneBook = Map.fromList [(1234, "Erik"), (5678, "Patrik")]main = doprint phoneBookprint $ Map.lookup 1234 phoneBookprint $ (Map.empty :: Map.Map Int Int)print $ Map.singleton 3 5print $ Map.insert 1 "abc" Map.emptyprint $ Map.null phoneBookprint $ Map.size phoneBookprint $ Map.toList phoneBookprint $ Map.keys phoneBookprint $ Map.elems phoneBook//import Prelude hiding (null, lookup, map, filter)import Data.HashMap.Lazyimport Data.CharhashMap = fromList [(1 :: Int, 'a'), (2, 'b'), (3, 'c')]main = doprint $ hashMapprint $ keys hashMapprint $ elems hashMap//print $ null hashMapprint $ size hashMap//print $ member 1 hashMapprint $ member 5 hashMap//print $ lookup 1 hashMapprint $ lookup 5 hashMap//print $ hashMap ! 1print $ lookupDefault 'N' 5 hashMap//print $ insert 4 'd' hashMapprint $ delete 2 hashMap//print $ map (toUpper) hashMapprint $ filter (> 'a') hashMap
iabbr <buffer> haskell_package ghc-pkg listghc-pkg find-module  the module belongs to which packages
iabbr <buffer> hhead import Data.Charimport Data.List.Splitimport AronModuleimport Text.Regex.Base.RegexLikeimport Text.Regeximport System.Environmentimport System.Console.GetOpt
iabbr <buffer> html_code <pre class="cc">create Lib under [root.Project.name="MyProject"] from setting.gradleadd following to build.gradledependencies {compile fileTree('Lib')  // this includes all the files under 'Lib'}</pre>
iabbr <buffer> html_title <div class="mytitle">My Title</div><div class="mytext">The App shows how to use simple animation on iPhone.<br>1. Load images to array<br></div><br><div class="cen"><img src="http://localhost/zsurface/image/kkk.png" width="40%" height="40%" /><br><a href="https://github.com/bsdshell/xcode/tree/master/OneRotateBlockApp">Source Code</a></div>
iabbr <buffer> img_3d_draw img,/Users/cat/try/draw13.png
iabbr <buffer> img_draw img,/Users/cat/try/draw1.pngimg,/Users/cat/try/draw10.pngimg,/Users/cat/try/draw11.pngimg,/Users/cat/try/draw12.pngimg,/Users/cat/try/draw13.pngimg,/Users/cat/try/draw14.pngimg,/Users/cat/try/draw15.pngimg,/Users/cat/try/draw16.pngimg,/Users/cat/try/draw17.pngimg,/Users/cat/try/draw18.pngimg,/Users/cat/try/draw19.pngimg,/Users/cat/try/draw2.pngimg,/Users/cat/try/draw20.pngimg,/Users/cat/try/draw21.pngimg,/Users/cat/try/draw22.pngimg,/Users/cat/try/draw23.pngimg,/Users/cat/try/draw3.pngimg,/Users/cat/try/draw4.pngimg,/Users/cat/try/draw5.pngimg,/Users/cat/try/draw6.pngimg,/Users/cat/try/draw7.pngimg,/Users/cat/try/draw8.pngimg,/Users/cat/try/draw9.pngimg,/Users/cat/try/exitpath.pngimg,/Users/cat/try/fold_left_right.pngimg,/Users/cat/try/gitfastforward.pngimg,/Users/cat/try/gitrebase.pngimg,/Users/cat/try/glortho.pngimg,/Users/cat/try/isbstpic.pngimg,/Users/cat/try/javapath.png
iabbr <buffer> iphone_info `[--------------------------------------------------------------------iPhone 4swidth=[320.000000]height=[480.000000]scale =[2.000000]nativeWidth=[640.000000]nativeHeight=[960.000000]scale =[2.000000]--------------------------------------------------------------------iPhone 5 and upwidth=[320.000000]height=[568.000000]scale =[2.000000]nativeWidth=[640.000000]nativeHeight=[1136.000000]scale =[2.000000]--------------------------------------------------------------------iPad 2Switch Backwidth=[768.000000]height=[1024.000000]scale =[1.000000]nativeWidth=[768.000000]nativeHeight=[1024.000000]scale =[1.000000]iPad--------------------------------iPad Airwidth=[768.000000]height=[1024.000000]scale =[2.000000]nativeWidth=[1536.000000]nativeHeight=[2048.000000]scale =[2.000000]--------------------------------ipad Air 2Switch Backwidth=[768.000000]height=[1024.000000]scale =[2.000000]nativeWidth=[1536.000000]nativeHeight=[2048.000000]scale =[2.000000]--------------------------------iPad Prowidth=[768.000000]height=[1024.000000]scale =[2.000000]nativeWidth=[1536.000000]nativeHeight=[2048.000000]scale =[2.000000]iPad Retina--------------------------------width=[768.000000]height=[1024.000000]scale =[2.000000]nativeWidth=[1536.000000]nativeHeight=[2048.000000]scale =[2.000000]--------------------------------`]
iabbr <buffer> java_code /Users/cat/myfile/github/java/TextAreaLineHeightExample/src/Main.java/Users/cat/myfile/github/java/TextAreaSimple/src/Main.java/Users/cat/myfile/github/java/TextAreaFontAlignment/src/Main.javaimg, /Users/cat/myfile/github/image/textarealine.pngimg, /Users/cat/myfile/github/image/textflow_multiple_line.pngimg, /Users/cat/myfile/github/image/textareafont.png
iabbr <buffer> jgradle_gradle img, /Users/cat/myfile/github/image/gradle_resources.png
iabbr <buffer> mytitle_div <div class="mytitle"></div><pre class="prettyprint"></pre>
iabbr <buffer> pdf_file img, /Users/cat/myfile/github/math/theorem.pdfvoicemailvoicemailemai
iabbr <buffer> pdf_view /Users/cat/myfile/github/java/PDFBoxViewer
iabbr <buffer> sed_shell :sed 's/.*\(Name=\"[^"]*\"\)\s\(MyDir=\"[^"]*\"\).*/\1 \2/g' f1.txt > f2.txt //Match Name=xx, MyDir=xx 2 col:sed -i -e 's/oldstring/newstring/g' *  // replace oldstr to newstr, replace all, sed replace:sed '/line3/,/line9/d' oldfile > newfile  // delete lines from line contains line3 to line contains line9:gsed -i_backup 's/oldstr/newstr/' *.html  // file is backed up with *.html_backup
iabbr <buffer> shell_script for arg in $*doecho "[\$*]=".$argdone#[$*]=.n1#[$*]=.n2#[$*]=.n3for arg in "$*"doecho "[\$*]=".$argdone#[$*]=.n1 n2 n3for arg in $@doecho "[\$@]=".$argdone#[$@]=.n1#[$@]=.n2#[$@]=.n3for arg in "$@"doecho "[\$@]=".$argdone#[$@]=.n1 n2#[$@]=.n3
iabbr <buffer> stdin_stdout :cmd 1 > /etc/null   # redirect stdout to /etc/null:cmd 2 > /etc/null   # redirect stderr to /etc/null:cmd 2 > &1          # redirect stderr to stdout:cmd &> file        # redirect stderr and stdout to file
iabbr <buffer> test_only line 5line 6
iabbr <buffer> tmux_key prefix C => new windowprefix ? => help windowprefix % => split paneprefix o => change pane
iabbr <buffer> vancouver_lib 21383027629265
iabbr <buffer> vim_function :echo matchstrpos("testing", "ing")    => ["ing", 4, 7]:echo matchstrpos('test$ing', '$ing')  => ['$ing', 4, 7]:echo matchstrpos('test\ing', '\ing')  => ['ing', 4, 7]:echo matchstrpos('test\ing', '\\ing') => ['\ing', 4, 7]:echo matchstrpos('test^ing', '^ing')  => ['\ing', 4, 7]:echo matchstrpos('test^ing', '^ing')  => ['', 4, 7]:echo matchstrpos('test^ing', '\^ing') => ['', 4, 7]:echo matchstrpos('testing$', 'ing$')  => ['', 4, 7]:echo matchstrpos('testing$', 'ing\$') => ['ing$', 4, 7]let dumm = matchstrpos('$\mathbb', escape('\math', '$\'))
iabbr <buffer> vim_mode Q => visual to exit, the most important mode
iabbr <buffer> vim_script " Fix the missing -cp option in java" Assume java file is in current bufferfunction! CompileJava()let path = expand("%")exec ":!javac " . path let jclassName = expand("%:p:t:r")let className = expand("%:p:t:r")let cwd = getcwd()let full = ":!java -cp " . cwd . ":. " . jclassNameexec  fullendfunction//:str =~ "n"    //compare string, return 1 if str == "n" else return 0if str =~ "\s*"" str is empty or whitespaceendif//*expand-example*:expand("%")       => file.txt:expand("%:p")     => /home/file.txt:expand("%:p:t")   => file.txt:expand("%:p:r")   => /home/file:expand("%:p:t:r") => file:fnamemodify('/dog/f.txt', ':h') => /dog//" get current file dir:getcwd()"/home//" read file:readfile(fname)//" check whether a file with the name exists:filereadable(fname)//" write file:writefile(fname, "a"):writefile(fname, "b") "b" binary mode//[:h Dictionary]" dictionarylet dict={'key': 'value'}//" replace valuedict.key = 'dog'//" Iterate through Dictionaryfor [key, value] in items(dict)echo key . ':' . valueendfor//" remove keylet value = remove(dict, 'key')echo value  " dog//[:h List]" list as valuelet list = ['a', 'b']add(dict.key, 'abc')echo dict.key   " ['a', 'b', 'abc']//" list, string, length, lenlet list = []let list = ["cat", "dog"]let len = len(list)let fname = split(expand("%:p"), "/")let length = length("this is cool")//" remove list item:unlet list[3] " remove fourth item:unlet list[3:] " remove foruth to last//" remove dictionary item:unlet dict['two']:unlet dict.two//" This is especially useful to clean up global variables:unlet global_var//" sort a listlet list = ['b', 'a']sort(list)//" sort a list in-place, a list remain unmodifiedlet copylist = sort(copy(list))//" Iterate buffer listlet bufcount = bufr("$")let curr = 1while curr <= bufcountlet currbufname = bufname(curr)if buflisted(currbufname) && strlen(currbufname) > 0echo currbufnameendiflet curr = curr + 1endwhile//" Iterate through Listfor item in mylistecho itemendfor//" for looplet index = 0while index < len(mylist)echo mylist[index]let index = index + 1endwhile//" list of open windows:echo range(1, winnr('$'))//" To determine how many split-windows are openwinnr('$')//" How many in the current pagetabpagewinnr(tabpagenr(), '$')//http://vim.1045645.n5.nabble.com/how-to-get-list-of-open-windows-td1164662.html" List all split window by tab pagewhile t <= tabpagenr('$')echo 'tab' t . ':'let bufs = tabpagebuflist(t)let w = 0while w < tabpagewinnr(t, '$')echo "\t" . bufname(bufs[w])let w += 1endwhilelet t += 1endwhileunlet t w bufs//" split window on the right hand side:set splitright//" statusline format%-0{minwid}.{maxwid}{item}The - forces the item to be left alignedThe 0 forces numeric value to be left padded with zeros//" statusline evaludate functionstatusline+=%10{expand('%:p:h')}//// vim variableb:	  Local to the current buffer.w:	  Local to the current window.t:	  Local to the current tab page.g:	  Global.l:	  Local to a function.s:	  Local to a :source ed Vim script.a:	  Function argument (only inside a function).v:	  Global, predefined by Vim.//:let l:pos = getpos(".")    // keep cursor position, restore cursor position:call setpos(".", l:pos)//:filter /^\u\w\+/ cab    // show all first letter of command abbreviations is uppercase.//*v_dict*  *vscr_dict* *vim_dict*:if has_key(dict, 'foo')	" TRUE if dict has entry with key "foo":if empty(dict)			" TRUE if dict is empty:let l = len(dict)		" number of items in dict:let big = max(dict)		" maximum value in dict:let small = min(dict)		" minimum value in dict:let xs = count(dict, 'x')	" count nr of times 'x' appears in dict:let s = string(dict)		" String representation of dict:call map(dict, '">> " . v:val')  " prepend ">> " to each item//*v_change_dir* *v_dir*:autochdir acd noautochdir
iabbr <buffer> vim_tips --------------------------------------------------------------------------------multiple highlightmat[ch] TODO /pattern/2mat[ch] TODO /pattern/3mat[ch] TODO /pattern/--------------------------------------------------------------------------------:record q and letter a, replay @a // vim recording:1/pat                          // search from the first line, cool tip:1?pat                          // search backward, from last line, cool tip:zz                             // move the cursor to middle of screen:zt                             // move the cursor to Top of screen:zb                             // move the cursor to Botton screen:echo v:this_session            // show current vim session file// All in insert modeCTRL-D		command-line completionCTRL-N		command-line completionCTRL-P		command-line completionCTRL-A		command-line completionCTRL-L		command-line completionCTRL-W      delete world before the cursor // insert modeCTRL-U      delete remove all characters between the cursor and the beginning of the line--------------------------------------------------------------------------------s/\%V\_^/\/\//gc  [:h \_^]   // \_^ can be used anywhere in the pattern comment all selected lines, selection match visual mode%s/\[\(\w\+\)\s\+objectAtIndex:\([^]]\+\)\]/\1\[\2\]/gc // replace [array objectAtIndex:index] with array[index] in ObjectiveC//Copy text from vim to clipboard [:h clipboard]"* is special Register that stores selection"*y Copy text from vim to clipboard"*p Paste text from clipboard to vim--------------------------------------------------------------------------------:3dd // delete below 3 line from the current line--------------------------------------------------------------------------------:let x = 0 | g/$/s//\='[' . x . ']'/ | let x = x + 1  // append [x] to the end of line:CTRL-O     //Jump Older cursor position in jump list [:h jumps]:CTRL-I     //Jump newer cursor position in jump list backward:vimgrep /MyString/ /home/mycode/**/*.m   // Search MyString pattern recursively[**/*.m] two arsterisks:copen      // Open your quickfix:gf         // open the file under the currsor:find will search the directories/files in path option [:h :find]:vim $(find ./ -name \*.java -print)                // open all java file from find:set path? //will show current path option [:h path]:set path +=/home/mycode //You can add different directories to path option in Vim" reset to default:set path& [:h set]:set complete=k/home/myfile/*,k~/home/dir" auto completion search your own file" [:h complete]" k{dict} scan the file {dict}. Servan flags can be given--------------------------------------------------------------------------------In Vim Script, there are many options to handle file names, paths:h cmdline:bufdo e!                                                                                       // Save reload all buffers :syntax on  to enable syntax highlight:bufdo %s/pattern/replacement/ge | update                                                       // vim replace all buffers:zf                // Create fold under cursor:zd                // Delete fold under cursor:5gt               // goto tab 5, [in insert mode]:tabmove 4         // move the current tab to position 4 [help :tabmove]:tabc 11           // close tab 11:set modeline      // display file name in each tabs page:set ls=2:set modeline      // display full file name in statusline [:help :statusline]:set statusline=%F:help debug-mode                             // debug vim script                                                                             :echo getline(".")                           // get current line string:echo match("mystring", "st")                // return 2:Align \*\zs\s\+ =                           // *'  ' and '=' separator:\%V                                         // visual selection block [:help \%V]:'<,>'sort /\ze\%V/                          // sort all selected lines in visual mode:'< and >'                                   // first cursor position/last cursor position in registers in visual mode/selection:g/^$/d                                      // delete empty line:let @a=substitue(@a, 'pattern', 'sub', 'g') // substitute 'pattern' with 'sub' in register @a:q: or q/                                    // vim command history:hi Search guifg=Brown guibg=Gray:hi Search cterm=Brown ctermbg=Gray          // change search backgroun/foreground [:h hl-Search]:sort n                                      // sort lines by the first digital in the line, [n] is first decimal number, [x] is first hexadecimal number [:help :sort]:sort! n                                     // reverse sorting:syn match MyKeyWord /MyName/                // Highlight defined keyword [:help syn-match]:highlight MyKeyWord guifg=Green:'<,'>s/^\s*\zs\w/#\0/gc                     // comment all selected lines in python:set ma                                      // set file modifiable on  [:h modifiable]:ctrl-]                                      // class definition:ctrl-t                                      // back to previous window:s/pattern/\=@a.submatch(0).@b/gc            // substitute with two registers:redir @a                                    // redirect ex command to register:ls:redir END:redir @* | ls | redir END                   // redirect all the name of file to reg @* in buffer:tabmove 4                                   // move the current tab to position 4 [help :tabmove]:/pattern\c #ignorecase:hi clear SpellBad # Reset SpellBad bad spell highlight to default:g/-\s*(\w\+)\w+/z#.1                        // print all xcode methods with line number #:/\(if\)\ze\(then\)                          // if follows then "[if]then":/\(if\)\zs\(then\)                          // then starts with if "if[then]":g*         / search without word boundry:let i=1 | g/dog/s//\='cat' . i/gc | let i=i+1                   //  g/pattern/[cmd] [h :g] [h sub-replace-expression] searchkey: increase:let i=2 | .,$g/dog\d\+/s//\='cat' . i/gc | let i=i+1            //  vim increase number:for i in range(1,10) | if i % 2 == 0 | put = i | endif | endfor //  put even numbers from cursor position vertically:echo synIDattr(synIDtrans(hlID("Normal")), "bg") [:h hlID]      //  get background color:set hidden                                                      //  hidden an unsaved file, ow, file has to be saved with !:.,$s/some\(stuff\)/& . \1/gc                                    //  copy all i think % = \0 here:normal mode:%s/{\zs\(\_.}\@!\)*//gc                                         //  negative operator block of code { hello world }:\@!                                                             //  negative operator, subtract operator, exclusive operator:echo g:colors_name                                              //  get current color scheme:/usr/share/vim/vim73/colors                                     //  vim scheme, vim directory vim folder:autocmd BufEnter *.*  setlocal completefunc=CompleteAbbre       //  file type evil, autocmd will not detect file without extension. e.g. "file"// http://vim.1045645.n5.nabble.com/autocmd-pattern-exclusion-td5712330.html:autocmd BufEnter *\(.txt)\@<!  TTT exec 'echo expand("%:p")'     //  excluding file type// start PERL mode, vim play modegQ
iabbr <buffer> vim_visual vmap pp :s/\%V.*\%V/<strong>\0<\/strong>/ 
iabbr <buffer> vimcommand_vim_command :set spell " enable spelling:set nospell " disable spellingzg      " add word to spellfile, spell file, spell-file, spelling file, spellingfileC-X C-U " e.g. jlist user defined completionC-X C-D " definitionC-X C-K " dictionaryC-X C-X " suggestionC-X C-L " line completionC-X C-N " local keyword completion:fp     " copy current full path:ip     " change to init pathF7 copyF8 past
iabbr <buffer> xcode_shortcut Shift ALT Command Left/Right  fold all code/unfolded all codeCTRL 5 search files name(.h *.m)                                // xcode searchCTRL 6 type search show all current methodsCTRL WindowKey left/right  navigate backward/backwardCTRL 1 show recent itemsCTRL C delete current lineALT Left/Right  move word by wordCommand ALT .(dot) change focusCommand ALT Left/Right code fold/unfoldedCommand ALT 0 close utilities windowCommand left/right move cursor to begin/end of line(no highligh)Command , show setting(xcode)Command 3 global searchCommand w close window (not just xcode)Command [] shift hightlight code left/rightCommand double click jump to definitionCommand D mark lineCommand L goto lineCommand E, Command F use selection to findCommand E + Command F search word under cursorCommand Shift O Quick Search fileCommand Shift Y close debug windowCommand Shift 2 Open OrganizerCommand Shift f  global findCommand Shift Left/Right  selection whole lineCommand Shift right/left highlight lineCommand CTRL UP/Down .h and .m fileCommand CTRL J jump to definitionCommand CTRL Left jump backCommand CTRL F full/maximize screen modeCommand CTRL Y debug/continueCommand `  window fit screenCommand /  comment out code
setlocal keymap=
setlocal noarabic
setlocal autoindent
setlocal backupcopy=
setlocal nobinary
setlocal nobreakindent
setlocal breakindentopt=
setlocal bufhidden=
setlocal buflisted
setlocal buftype=
setlocal nocindent
setlocal cinkeys=0{,0},0),:,0#,!^F,o,O,e
setlocal cinoptions=
setlocal cinwords=if,else,while,do,for,switch
setlocal colorcolumn=
setlocal comments=s1:/*,mb:*,ex:*/,://,b:#,:%,:XCOMM,n:>,fb:-
setlocal commentstring=/*%s*/
setlocal complete=k/Users/cat/myfile/bitbucket/math/mybook.tex,k$HOME/myfile/bitbucket/haskell/*.hs
setlocal concealcursor=
setlocal conceallevel=0
setlocal completefunc=CompleteAbbre
setlocal nocopyindent
setlocal cryptmethod=
setlocal nocursorbind
setlocal nocursorcolumn
setlocal nocursorline
setlocal define=
setlocal dictionary=
setlocal nodiff
setlocal equalprg=
setlocal errorformat=
setlocal expandtab
if &filetype != 'haskell'
setlocal filetype=haskell
endif
setlocal fixendofline
setlocal foldcolumn=0
setlocal foldenable
setlocal foldexpr=0
setlocal foldignore=#
setlocal foldlevel=0
setlocal foldmarker={{{,}}}
setlocal foldmethod=manual
setlocal foldminlines=1
setlocal foldnestmax=20
setlocal foldtext=foldtext()
setlocal formatexpr=
setlocal formatoptions=tcq
setlocal formatlistpat=^\\s*\\d\\+[\\]:.)}\\t\ ]\\s*
setlocal grepprg=
setlocal iminsert=0
setlocal imsearch=0
setlocal include=
setlocal includeexpr=
setlocal indentexpr=
setlocal indentkeys=0{,0},:,0#,!^F,o,O,e
setlocal noinfercase
setlocal iskeyword=@,48-57,_,192-255
setlocal keywordprg=
setlocal nolinebreak
setlocal nolisp
setlocal lispwords=
setlocal nolist
setlocal makeprg=
setlocal matchpairs=(:),{:},[:]
setlocal modeline
setlocal modifiable
setlocal nrformats=bin,octal,hex
setlocal nonumber
setlocal numberwidth=4
setlocal omnifunc=
setlocal path=
setlocal nopreserveindent
setlocal nopreviewwindow
setlocal quoteescape=\\
setlocal noreadonly
setlocal norelativenumber
setlocal norightleft
setlocal rightleftcmd=search
setlocal noscrollbind
setlocal shiftwidth=4
setlocal noshortname
setlocal signcolumn=auto
setlocal smartindent
setlocal softtabstop=0
setlocal nospell
setlocal spellcapcheck=[.?!]\\_[\\])'\"\	\ ]\\+
setlocal spellfile=~/myfile/bitbucket/vim/myword.utf-8.add
setlocal spelllang=en
setlocal statusline=
setlocal suffixesadd=
setlocal noswapfile
setlocal synmaxcol=3000
if &syntax != 'haskell'
setlocal syntax=haskell
endif
setlocal tabstop=4
setlocal tagcase=
setlocal tags=
setlocal textwidth=0
setlocal thesaurus=
setlocal noundofile
setlocal undolevels=-123456
setlocal nowinfixheight
setlocal nowinfixwidth
setlocal wrap
setlocal wrapmargin=0
silent! normal! zE
let s:l = 3 - ((2 * winheight(0) + 17) / 34)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
3
normal! 0
lcd ~/myfile/bitbucket/haskell
tabnext 1
if exists('s:wipebuf')
  silent exe 'bwipe ' . s:wipebuf
endif
unlet! s:wipebuf
set winheight=1 winwidth=20 shortmess=filnxtToO
let s:sx = expand("<sfile>:p:r")."x.vim"
if file_readable(s:sx)
  exe "source " . fnameescape(s:sx)
endif
let &so = s:so_save | let &siso = s:siso_save
doautoall SessionLoadPost
unlet SessionLoad
" vim: set ft=vim :

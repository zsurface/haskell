-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import qualified Data.List as L
--import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import qualified System.FilePath.Posix as PP
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Applicative
import Control.Concurrent 


-- KEY: evaluate expression, expr, parser

-- data MyType a = Parser a
-- parse :: String -> [(a, String)]
--
-- record syntax is still allowed, but only for one field
--
-- newtype State s a = State { runState :: s -> (s, a) }
--
-- State :: (s -> (s, a)) -> State s a
-- runState :: State s a -> (s -> (s, a))
--
-- s = State s a
-- runState::(State s a) -> ((State s a), a)
--
--
-- runState:: s -> (s, a)
-- runState s -> (s, (s, a))      # (s, a) => a
-- runState::(State s a) -> (s, (s, a))  # State s a => s
       
newtype Parser a = Parser {parse::String -> [(a, String)]} 

runParser::Parser a -> String -> a
runParser m s =
    case parse m s of  -- parse m => String -> [(a, String)]
        [(res, [])] -> res
        [(_, _)] -> error "Parser did not consume entire stream."
        _         -> error "Parser error."

-- Parser is a Monad ?
item::Parser Char
item = Parser $ \s ->
    case s of
        [] -> []
        (c:cs) -> [(c, cs)]

-- [(a1, S),(a2, S)] => [(b1, S), (b2, S)]
--
-- bind::(Monad m) => m a -> (a -> m b) -> m b
bind::Parser a -> (a -> Parser b) -> Parser b
bind p f = Parser $ \s -> concatMap(\(a, s') -> parse (f a) s') $ parse p s

-- class Monad m where
--   return:: a -> m a
--   bind:: m a -> (a -> m b) -> m b
--
-- f x = \x -> x + 1
-- f x = \x -> (x, x)
-- f x = \x -> [(x, x)]
-- instance Monad Maybe where
unit :: a -> Parser a
unit a = Parser (\s -> [(a, s)])

--
-- newtype State s a = State{runState:: s -> (a, s)}
--
-- Parser cs = Parser (String -> [(a, String)])
-- => cs = String -> [(a, String)]
-- => cs s => [(a, String)]
-- => (a, b) <- cs s
--
-- newType Parser a = Parser {parse::String -> [(a, String)]}
-- Parser a => only has one value constructor => Parser{parse::String -> [(a, String)]}
-- 
instance Functor Parser where
    fmap f (Parser cs) = Parser (\s -> [(f a, b) | (a, b) <- cs s])

-- extract function from the first functor and apply inside the second functor
-- (Just (+1)) <*> Just (2)
-- ((+1), s1) <- cs1 s
-- (a, s2)   <- cs2 s1
-- [((+1) a, s2) | ...]
instance Applicative Parser where
    pure = return 
    (Parser cs1) <*> (Parser cs2) = Parser (\s -> [(f a, s2) | (f, s1) <- cs1 s, (a, s2) <- cs2 s1])

instance Monad Parser where
    return = unit
    (>>=) = bind



instance MonadPlus Parser where
    mzero = failure
    mplus = combine

instance Alternative Parser where
    empty = mzero
    (<|>) = option

combine::Parser a -> Parser a -> Parser a
combine p q = Parser (\s -> parse p s ++ parse q s)

failure::Parser a
failure = Parser (\cs -> [])

option::Parser a -> Parser a -> Parser a 
option p q = Parser $ \s -> 
    case parse p s of
        [] -> parse q s
        res -> res

-- class Functor => Monad m where
--        fmap::(a -> b) -> [a] -> [b]
-- | one or more
--some::(Alternative f)=> f a -> f [a]
--some v = some_v
--    where
--       many_v = some_v <|> pure []        
--       some_v = (:) <$> v <*> many_v
--
---- | zero or more
--many::(Alternative f)=> f a -> f [a]
--many v = many_v
--    where
--       many_v = some_v <|> pure [] 
--       some_v = (:) <$> v <*> many_v

satisfy::(Char -> Bool) -> Parser Char
satisfy p = item `bind` \c ->
    if p c
    then unit c 
    else failure 

oneOf::[Char] -> Parser Char
oneOf s = satisfy (flip elem s)

chainl::Parser a -> Parser (a -> a -> a) -> a -> Parser a
chainl p op a = (p `chainl1` op) <|> return a

chainl1::Parser a -> Parser (a -> a -> a) -> Parser a
p `chainl1` op = do {a <- p; rest a}
    where rest a = (do f <- op
                       b <- p
                       rest (f a b)) <|> return a
                    

char::Char -> Parser Char
char c = satisfy (c ==)

---- read "123"::Integer => 123
natural::Parser Integer
natural = read <$> some (satisfy isDigit)

-- char c >> string cs >> return (c:cs)
string::String -> Parser String
string [] = return []  
string (c:cs) = do { char c; string cs; return (c:cs)}
--
token::Parser a -> Parser a
token p = do { a <- p; spaces ; return a}
--
reserved::String -> Parser String
reserved s = token (string s)

spaces::Parser String
spaces = many $ oneOf " \n\r"

digit::Parser Char
digit = satisfy isDigit

number::Parser Int
number = do
    s <- string "-" <|> return []
    cs <- some digit
    return $ read (s ++ cs)

parents::Parser a -> Parser a
parents m = do
    reserved "("
    n <- m
    reserved ")"
    return n

data Expr 
    = Lit Int
    | Add Expr Expr
    | Sub Expr Expr
    | Mul Expr Expr
    deriving (Show)

eval::Expr -> Int
eval (Lit a) = a 
eval (Add e1 e2) = (+) (eval e1) (eval e2)
eval (Sub e1 e2) = (-) (eval e1) (eval e2)
eval (Mul e1 e2) = (*) (eval e1) (eval e2)

int::Parser Expr 
int = do
    n <- number
    return (Lit n)

expr::Parser Expr
expr = term `chainl1` addop

term::Parser Expr
term = factor `chainl1` mulop 

factor::Parser Expr
factor = int <|> parents expr

infixOp::String -> (a -> a -> a) -> Parser (a -> a -> a)
infixOp x f = reserved x >> return f

addop::Parser (Expr -> Expr -> Expr)
addop = (infixOp "+" Add) <|> (infixOp "-" Sub)

mulop::Parser (Expr -> Expr -> Expr)
mulop = infixOp "*" Mul

myrun::String -> Expr
myrun = runParser expr

main::IO()
main = forever $ do 
        putStr ">"
        a <- getLine
        print $ eval $ myrun a

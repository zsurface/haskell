module Main where

import Control.Monad (unless, when)
import Graphics.Rendering.OpenGL as GL 
import qualified Graphics.UI.GLFW as G
import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
import System.Exit
import System.IO
import Control.Monad
import Data.Set(Set) 
import Data.IORef 
import qualified Data.Set as S 

-- import Graphics.UI.GLUT
import Graphics.UI.GLUT.Callbacks.Global
import AronModule  hiding (rw)
import AronGraphic hiding (dist)
import AronOpenGL

-- | -------------------------------------------------------------------------------- 
-- | compile: runh keyboardpress_simple.hs  
-- | ghc -i/Users/cat/myfile/bitbucket/haskelllib -o file file.hs
-- | 
-- | KEY: keyboard example, keypress example, modifyIORef example,  
-- | -------------------------------------------------------------------------------- 

mc::(GLfloat, GLfloat) -> (GLfloat, GLfloat) -> (GLfloat, GLfloat)
mc (a, b) (a', b') = (a*a' - b*b', a*b' + a'*b)

-- (a + bi)(a' + b'i)
-- (aa' - bb') + (ab' + ba')i
-- (a, b) * (a', b') = (aa' - bb', ab' + a'b)

-- conform= [(0.1 * (fst $ mc c c'), 0.1* (snd $ mc c c'), 0) 
--            | c <- zip (map(*0.01) [1..aa]) (map(*0.01) [1..bb]), c' <- zip (map(*0.01) [1..aa]) (map(*0.01) [1..bb])]

conform::[[Vertex3 GLfloat]]
conform= [[Vertex3 (fst $ mc c1 c2) (snd $ mc c1 c2)  0 | c1 <- c] | c2 <- c] 
--conform= [[(fst $ c1, snd $ c1 , 0) | c1 <- c] | c2 <- c] 
        where 
            fa = 0.1 
            aa = map(\x -> fa * x) [1..10]
            bb = map(\x -> fa * x) [1..3]
            c = foldr(++) [] $ [[(a, b) | a <- aa] | b <- bb]

--grid::[[[Vertex3 GLfloat]]]
--grid =[[[Vertex3 a b (a*a - b*b) | a <- aa] | b <- bb] | c <- cc] 
--        where 
--            n  = 10
--            fa = 1/n 
--            aa = map(\x -> fa * x) [1..n]
--            bb = map(\x -> fa * x) [1..n]
--            cc = map(\x -> fa * x) [1..n]

grid2::[[Vertex3 GLfloat]]
grid2 =[[Vertex3 a b (a*a - b*b) | a <- aa] | b <- bb] 
        where 
            n  = 30
            fa = 1/(2*n)
            aa = map(\x -> fa * x) [-n..n]
            bb = map(\x -> fa * x) [-n..n]


plotPts::Color3 GLdouble -> [GLfloat] ->IO()
plotPts co cx = do
                let n = length cx
                let xx = let del = 1/(rf n) in map(\x -> del*(rf x)) [0..n]
                let xx' = map(\x -> rf x) xx
                let vl = zipWith(\x y -> Vertex3 x y 0) xx' cx 
                let pair = join $ zipWith(\x y -> [x, y]) (init vl) (tail vl)
                mapM_ (\x -> drawCircleColor x red 0.01) vl 
                drawPrimitive' Lines co pair 



-- f z = z*z
-- 
-- a    => x
-- b    => y
-- re c => z
-- im c => color
--grid4::[[Vertex3 GLfloat]]
--grid4 =[[ let c = (C a b) in Vertex3 (re c) (im c) 0 | a <- aa] | b <- bb]
--        where 
--            ne = [[ let c = sqrtC' (C a b) in (Vertex3 a b (re c), im c) | a <- aa] | b <- bb]
--            n  = 30 
--            fa = 1/(1.5*n)
--            aa = map(\x -> fa * x) [-n..n]
--            bb = map(\x -> fa * x) [-n..n]

czz::[[Vertex3 GLfloat]]
czz =[[ let c = (C a b)*(C a b)*(C a b) in Vertex3 (re c) (im c) 0 | a <- aa] | b <- bb]
        where 
            ne = [[ let c = sqrtC' (C a b) in Vertex3 a b 0 | a <- aa] | b <- bb]
            n  = 30 
            fa = 1/(1.5*n)
            aa = map(\x -> fa * x) [-n..n]
            bb = map(\x -> fa * x) [-n..n]


trig s1 s2 = map(\x -> foldr(++) [] x) $ (zipWith . zipWith)(\x y -> [x, y]) (init s1) (tail s2)

segment1 = zipWith(\x y -> [Vertex3 (-1) 0 0, y]) [1..] test_circle

test_circle::[Vertex3 GLfloat]
test_circle=[ let x = (1-t*t)/(1 + t*t); 
                  y = (2*t)/(1 + t*t) in Vertex3 x y 0 | t <- aa]
        where 
            n  = 50 
            fa = 1/(0.1*n)
            aa = map(\x -> fa * x) [-n..n]

splitPt::Int->[(GLfloat, GLfloat, GLfloat)]->[[(GLfloat, GLfloat, GLfloat)]]
splitPt _ [] = []
splitPt n xs = take n xs : (splitPt n $ drop n xs)


mergeChunk::Int->[(GLfloat, GLfloat, GLfloat)]->[(GLfloat, GLfloat, GLfloat)]
mergeChunk n c = mergeList  (take n c)  (take n $ drop n c) 

bigChunk::Int->[(GLfloat, GLfloat, GLfloat)]->[[(GLfloat, GLfloat, GLfloat)]]
bigChunk n xs = splitPt n xs 

renderSurface::[(GLfloat, GLfloat, GLfloat)]->IO()
renderSurface xs = do 
        iterateList (bigChunk 80 xs) (\chunk -> do
                        let len = length chunk
                        let n = div len 2 
                        let c = mergeChunk n chunk
                        let cc = zipWith(\x y -> (x, y)) c [1..]
                        renderPrimitive TriangleStrip $ mapM_(\((x, y, z), n) -> do 
                            case mod n 3 of 
                                0 -> do 
                                        color(Color3 0.8 1 0 :: Color3 GLdouble) 
                                1 -> do 
                                        color(Color3 0 0.5 1 :: Color3 GLdouble) 
                                _ -> do 
                                        color(Color3 1 0 0.7 :: Color3 GLdouble) 
                            normal $ (Normal3 x y z::Normal3 GLfloat)
                            vertex $ Vertex4 x y z 0.8) cc  
                            )

mymain :: IO ()
mymain = do
  -- finally add double buffer
  successfulInit <- G.init
  G.windowHint (G.WindowHint'DoubleBuffer True)
  -- if init failed, we exit the program
  bool successfulInit exitFailure $ do
      mw <- G.createWindow 1400 1400 "Simple example, haskell style" Nothing Nothing
      maybe' mw (G.terminate >> exitFailure) $ \window -> do
          G.makeContextCurrent mw
          ref <- newIORef initCam 
          refStep <- newIORef initStep 
          mainLoop window ref refStep
          G.destroyWindow window
          G.terminate
          exitSuccess
          
mainLoop :: G.Window -> IORef Cam -> IORef Step -> IO ()
mainLoop w ref refStep = unless' (G.windowShouldClose w) $ do
    (width, height) <- G.getFramebufferSize w
    viewport $= (Position 0 0, Size (fromIntegral width) (fromIntegral height))
    GL.clear [ColorBuffer, DepthBuffer]
    G.setKeyCallback w (Just $ keyBoardCallBack refStep)

    matrixMode $= Modelview 0
    loadIdentity
    lightingInfo

    keyboardRot ref refStep
    renderCoordinates
--    drawTriangle

    -- render code here

--    drawPrimitive Lines red torus 
--    pp conform
    -- mapM_ (\x -> drawPrimitive Lines red x) conform 
    -- let tu (a, b, c) = (a, b) in (mapM_) (\x -> drawSegmentWithEndPt red x) conform
    -- pp grid4
    -- (mapM_ . mapM_ ) (\c -> let r = 0.01 in drawCircleXYZColor c 0.01 red) grid4

--    (mapM_ ) (\row -> drawPrimitive' Lines red row) grid4
--    (mapM_ ) (\row -> drawPrimitive' Lines red row) $ tran grid4

    -- translate (Vector3 1 0.0 0.0 :: Vector3 GLdouble)
--    (mapM_ ) (\row -> drawPrimitive' Lines blue row ) czz 
--    (mapM_ ) (\row -> drawPrimitive' Lines blue row ) $ tran czz 
--    drawSegmentFromTo red $ take 4 vertexList
    mapM_ (\row -> drawSegmentFromTo red row ) grid 
    mapM_ (\row -> drawSegmentFromTo red row ) $ tran grid 


--    drawPrimitive' Lines red test_circle
--    mapM_(\x -> drawSegmentWithEndPt green x) segment1

    -- renderSurface sphere 

    G.swapBuffers w
    G.pollEvents
    mainLoop w ref refStep
main = mymain


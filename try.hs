{-# LANGUAGE QuasiQuotes #-}
-- base
import Control.Monad (when, forM_)
import Control.Exception (bracket)
import Data.Bits
import Foreign -- includes many sub-modules
import Foreign.C.String (withCAStringLen, newCString)
import AronModule

--              [a33]  [x3] [b3]
--        [a22] [a23]  [x2] [b2]
--  [a11] [a12] [a13]  [x1] [b1]
--       

--backsub []      _ = []
--backsub (r:ca) x (b:cb)     = backsub a' x b' 
--backsub (r:ca) x (b:cb) = backsub ca x' cb
--    where
--        a' = reverse a
--        b' = reverse b
--        tm = utri (r:ca) 
--        ha = head r 
--        hb = head b
--        s  = (+) <$> ca <*> x 
--        x'  = [(hb - s)/ha]

--f ca x cb= (fr ca' x cb)
-- strT
--    where
--        cb'          = reverse cb
--        fr ca' x cb' = ca'
--        ca'          = utri ca

     
data MyMaybe a = MyNothing | MyJust a

  
fun::MyMaybe a -> String
fun MyNothing = "nothing"
fun (MyJust a) = "something"

data NonEmpty a = a :| [a]

head1::NonEmpty a -> a
head1  (x:|_) = x

main = do
    pp "do"
    let ls = [1, 2]
    let me = 0 :| ls :: NonEmpty Int
    pp $ head1 me
    
    
    
     
    
    
    

    

    





{-# LANGUAGE OverloadedStrings #-}

import AronModule

import Turtle (empty, shellStrictWithErr, ExitCode)
import Data.Text (Text)

runSh :: Text -> IO (ExitCode, Text, Text)
runSh x' = shellStrictWithErr x' empty

main :: IO ()
main = do
  (e, so, si') <- runSh "top\r"
  print so 
  print e
  ss <- run "top\n\r"
  pp ss

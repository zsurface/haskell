{-# LANGUAGE QuasiQuotes #-}
-- base
import Control.Monad (when, forM_)
import Control.Exception (bracket)
import Data.Bits
import Foreign -- includes many sub-modules
import Foreign.C.String (withCAStringLen, newCString)
import AronModule


--              [a33]  [x3] [b3]
--        [a22] [a23]  [x2] [b2]
--  [a11] [a12] [a13]  [x1] [b1]
--       

--backsub []      _ = []
--backsub (r:ca) x (b:cb)     = backsub a' x b' 
--backsub (r:ca) x (b:cb) = backsub ca x' cb
--    where
--        a' = reverse a
--        b' = reverse b
--        tm = utri (r:ca) 
--        ha = head r 
--        hb = head b
--        s  = (+) <$> ca <*> x 
--        x'  = [(hb - s)/ha]

--f ca x cb= (fr ca' x cb)
--    where
--        cb'          = reverse cb
--        fr ca' x cb' = ca'
--        ca'          = utri ca
utri m = reverse $ zipWith(\n r -> take' n r) [len m, ((len m) - 1) ..1] m


--bs (r:xa) x (b:xb) = let x' = bs' r x b in x':(bs xa (x':x) xb)
--    where
--        bs' r x b = xi
--            where 
--                s = sum $ (tail r) * x -- a[i][1]x[1] + a[i][2]x[2]
--                a = head r 
--                xi = (b - s)/a  --  x_i = (b_ii - sum ())/ a_i
{-| 
    === Backward substitute

    bs m x b = f (reverse m) x (reverse b)
        where
            f []  _  _        = []
            f _   _  []       = []
            f (r:xa) x (b:xb) = let x' = bs' r x b in x':(f xa (x':x) xb)
                where
                    bs' r x b = xi
                        where 
                            s = sum $ (tail r) * x -- a[i][1]x[1] + a[i][2]x[2]
                            a = head r 
                            xi = (b - s)/a  --  x_i = (b_ii - sum ())/ a_i
            
-} 
--bs::[[Double]] -> [Double] -> [Double] -> [Double]
--bs []  _  _        = []
--bs _   _  []       = []
--bs m x b = f (reverse m) x (reverse b)
--    where
--        f []  _  _        = []
--        f _   _  []       = []
--        f (r:xa) x (b:xb) = let x' = bs' r x b in x':(f xa (x':x) xb)
--            where
--                bs' r x b = xi
--                    where 
--                        s = sum $ (tail r) * x -- a[i][1]x[1] + a[i][2]x[2]
--                        a = head r 
--                        xi = (b - s)/a  --  x_i = (b_ii - sum ())/ a_i
        
data Sub = Backward | Forward

triSubstitute::Sub -> [[Double]] -> [Double] -> [Double] -> [Double]
triSubstitute s []  _  _        = []
triSubstitute s _   _  []       = []
triSubstitute s m x b = f (g m) x (g b)
    where
        g = case s of
                Backward -> reverse 
                Forward  -> id 

        f []  _  _        = []
        f _   _  []       = []
        f (r:xa) x (b:xb) = let x' = tri r x b in x':(f xa (x':x) xb)
            where
                tri r x b = xi
                    where 
                        su= sum $ (init $ reverse r) * x -- a[i][1]x[1] + a[i][2]x[2]
                        a = last $ reverse r 
                        xi= (b - su)/a  --  x_i = (b_ii - sum ())/ a_i
main = do
    pp "do"
    let tm = utri mat
    fl
    pa tm
    let m = [[1, 2], 
                [4]] 
    let m' = [[1], 
              [2, 4]]

    fl
    pp $ bs m  [] [2, 4]  -- x = [1.0, 0.0]
    pp $ fs m' [] [2, 4]  -- x = [2.0, 0.0]
    pp $ triSubstitute Backward m  [] [2, 4]  -- x = [2.0, 0.0]
    pp $ triSubstitute Forward  m' [] [2, 4]  -- x = [2.0, 0.0]

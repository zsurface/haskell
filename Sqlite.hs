{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DuplicateRecordFields #-}

module Main where

import           Control.Monad (mapM_)
import           Data.Int (Int64)
import           Data.Text (Text)
import qualified Data.Text as T
import           Database.SQLite.Simple
import           Database.SQLite.Simple.FromRow
import           Database.SQLite.Simple.FromField
import           Database.SQLite.Simple.ToField
import           Database.SQLite.Simple.Internal
import           Database.SQLite.Simple.Ok
import           AronModule

-- This code does not need sqld running.
-- code from: http://www.mchaver.com/posts/2017-06-28-sqlite-simple.html
data Person =
  Person 
    { personId   :: Int64
    , personName :: Text
    , personAge  :: Text
    } deriving (Eq,Read,Show)

data PDFInfo = PDFInfo 
    { pdfId :: Int64
    , title    :: Text
    , desc     :: Text
    , path     :: Text
    } deriving (Eq,Read,Show)

instance FromRow Person where
  fromRow = Person <$> field <*> field <*> field

instance FromRow PDFInfo where
  fromRow = PDFInfo <$> field <*> field <*> field <*> field

-- when inserting a new Person, ignore personId. SQLite will provide it for us.
instance ToRow Person where
  toRow (Person _pId pName pAge) = toRow (pAge, pName)

instance ToRow PDFInfo where
  toRow (PDFInfo _pId title desc path) = toRow (title, desc, path)

testdb = "/Users/cat/myfile/bitbucket/testfile/test.db"        
texFile = "/Users/cat/myfile/bitbucket/math"        
pdfdb = "/Users/cat/myfile/bitbucket/database/sqlite3_pdfhtml.db"

main:: IO ()
main= do
  conn <- open pdfdb 
  execute_ conn "CREATE TABLE IF NOT EXISTS people (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, age TEXT)"
  execute conn "INSERT INTO people (name, age) VALUES (?,?)" (Person 0 "Justina" "15")
  execute conn "INSERT INTO people (name, age) VALUES (?,?)" (Person 0 "Jordi" "11")
  people <- query_ conn "SELECT id, name, age from people" :: IO [Person]
  close conn

  conn <- open pdfdb 
  execute_ conn "CREATE TABLE IF NOT EXISTS pdftable (id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, desc TEXT, path TEXT)"
  execute conn "INSERT INTO pdftable (title, desc, path) VALUES (?,?,?)" (PDFInfo 0 "nice title" "cool description" "pdf full path")
  pdfQuery <- query_ conn "SELECT id, title, desc, path from pdftable" :: IO [PDFInfo]
  fw "people"
  print people
  fw "pdfQuery"
  print pdfQuery

-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

{-# LANGUAGE MultiWayIf        #-}
{-# LANGUAGE QuasiQuotes       #-} -- support raw string [r|<p>dog</p> |]
{-# LANGUAGE FlexibleContexts, ScopedTypeVariables #-}

import Text.RawString.QQ       -- Need QuasiQuotes too 


-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import qualified Text.Regex.TDFA as TD

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

-- shell command template:
-- 
--        argList <- getArgs
--        if len argList == 2 then do
--            let n = stringToInt $ head argList
--            let s = last argList
--            putStr $ drop (fromIntegral n) s
--        else print "drop 2 'abcd'"


import Data.Array.IO
import AronModule 

import Data.Array.MArray
import Data.Array.IO
import Control.Monad
import System.Random

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close

type AArray = IO(IOArray Int Int)


{-| 
    === zipWith two mutable array

    TODO: change Int to generic type with forall a. ?
    Problem: when you initialize the array, you need a concrete type such as Int, Float
    Solution: constrain to (Num a) type
-} 
--zipWithArr::(Num a)=>(a -> a -> a) -> IOArray Int a -> IOArray Int a  -> IO(IOArray Int a) 
--zipWithArr f iarr1 iarr2 = do 
--                            bound <- getBounds iarr1
--                            narr <- newArray bound 0 
--                            let b1 = fst bound
--                            let b2 = snd bound
--                            mapM (\x -> do 
--                                           e1 <- readArray iarr1 x
--                                           e2 <- readArray iarr2 x
--                                           writeArray narr x (f e1 e2) 
--                                  ) [b1..b2]
--                            return narr 



permute :: forall a. (MArray IOArray a IO) => [a] -> IO [a]
permute xs = do
  let n = length xs - 1
  arr0 <- (newListArray (0, n) xs :: IO (IOArray Int a))
  arr <- foldM swap arr0 [n..1]
  getElems arr
      where swap arr n = do
                           x <- readArray arr n
                           r <- randomRIO (0, n)
                           y <- readArray arr r
                           writeArray arr n y
                           writeArray arr r x
                           return arr



dotArr::(Num a)=>(IOArray Int a) -> (IOArray Int a) -> IO(IOArray Int a)
dotArr arr1 arr2 = do
                    arr <- zipWithArr arr1 arr2 (*) 
                    bound <- getBounds arr 
                    m1 = fst bound
                    m2 = snd bound
                        
                    

main = do 
        pp "done!"
        v1 <- newArray (0, 10) 1 :: AArray
        v2 <- newArray (0, 10) 2 :: AArray    

        v3 <- zipWithArrToList (*) v1 v2  
        pp $ "v3" <<< v3

        ls <- permute [1..10]
        pp ls

        pp "hi"









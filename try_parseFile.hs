-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

--import Data.Array
--import Text.Regex.TDFA -- the module will conflict with Text.Regex

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}


-- zo - open
-- za - close

import AronModule 

{-| 
    === split file with Regex e.g. empty line

    @
    ls <- readFileToList "/tmp/x.x"
    let lss = splitBlock2 ls "^[[:space:]]*"  => split file with whitespace
    pp lss

    let lss = splitBlock2 ls "^[[:space:]]*(---){1,}[[:space:]]*"  => split file "---"
    let lss = splitBlock2 ls "^[[:space:]]*(===){1,}[[:space:]]*"  => split file "==="
    @

    <file:///Users/cat/myfile/bitbucket/haskell/splitBlock2.hs TestFile>
-} 
splitBlock2::[String] -> String -> [[String]]
splitBlock2 [] _ = []
splitBlock2 cx pat = splitWhen (\x -> matchTest (mkRegex pat) x) cx

{-| 
    === parse file and partition it into blocks according to patterns: bs ws

    >bs = "^[[:space:]]*(---){1,}[[:space:]]*" -- split with delimiter "---"
    >ws = "[,. ]"                              -- split with [,. ]
    >parseFileBlock bs ws ["rat", 
    >                      "-----", 
    >                      "my dog eats my cat, my cat eats my dog."
    >                     ]
    >[(0, ["rat"]), (1, ["my", "dog", "eats", "my", "cat"])]

    <file:///Users/cat/myfile/bitbucket/haskell/parseRandom.hs Test_File>
-} 
parseFileBlock2::String->String-> [String] -> [(Integer, [String], [String])]
parseFileBlock2 _ _ [] = [] 
parseFileBlock2 bs ws cx = zblock 
    where
     block = filter(\x -> len x > 0) $ splitBlock2 cx bs 
     sblock = map(\k -> (unique $ join $ map(\x -> filter(\e -> len e > 0 && isWord e) $ splitStrRegex ws x) k, k)) block
     zblock = zipWith(\x y -> (x, fst y, snd y)) [0..] sblock

--parseFileBlock2::String->String-> [String] -> [(Integer, [String])]
--parseFileBlock2 _ _ [] = [] 
--parseFileBlock2 bs ws cx = lt 
--    where
--        bl = filter(\x -> length x > 0) $ splitBlock2 cx bs
--        ls = map(\k -> (map(\x -> splitStrRegex ws x) k, k)) bl -- [([[Str]], [Str])]
--        ds = map(\k -> map(\r -> filter(\w -> (len w > 0) && isWord w) r) $ fst k) ls
--        sb = map(\x -> unique $ join x) ds --                                = > [["dog", "cat"], ["cow"]]
--        lt = zip [0..] sb --                                                 = > [(0, ["dog", "cat"])]

main = do 
        let ws = "[,. ]"
        fblock <- readFileToList "/tmp/file.x"
        let bs = "^[[:space:]]*(---){1,}[[:space:]]*" -- split with delimiter "---"
        let ws = "[,. ]"                              -- split with [,. ]
        let zblock = parseFileBlock2 bs ws fblock -- [(Integer, [String], [String])]
--        let block = filter(\x -> len x > 0) $ splitBlock2 fblock "^[[:space:]]*(---){1,}[[:space:]]*"
--        let sblock = map(\k -> (unique $ join $ map(\x -> filter(\e -> len e > 0 && isWord e) $ splitStrRegex ws x) k, k)) block
--        let zblock = zipWith(\x y -> (x, fst y, snd y)) [0..] sblock
--        pw "block" block
--        pw "sblock" sblock
        pw "zblock" zblock
        print "Hello World"
        pp "done!"

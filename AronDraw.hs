module AronDraw where

import Control.Monad (unless, when)
import Graphics.Rendering.OpenGL as GL 
import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
import qualified Graphics.UI.GLFW as G
import System.Exit
import System.IO
import Control.Monad
import System.Random
import Data.Set(Set) 
import Data.IORef 
import qualified Data.Set as S 
import Linear.V3
import Linear.V3(cross) 
import Linear.Vector
import Linear.Matrix
import Linear.Projection as P
import Linear.Metric(norm, signorm)

import AronModule

-- tiny utility functions, in the same spirit as 'maybe' or 'either'
-- makes the code a wee bit easier to read
-- compile: ghc -o glfw glfw.hs

-- | Shorten realToFrac 
rf:: (Real a, Fractional b) => a -> b
rf = realToFrac

-- | mod for Double, 6.28 `fmod` 2 => 0.28
fmod::Double->Integer->Double
fmod  a n = a - du
            where     
                num = realToFrac (div (round a) n)
                du = realToFrac ((round num)*n)

mergeChunk::Int->[(GLfloat, GLfloat, GLfloat)]->[(GLfloat, GLfloat, GLfloat)]
mergeChunk n c = mergeList  (take n c)  (take n $ drop n c) 


nStep::Float
nStep = 40

-- sphere parametic equation center at (0, 0, 0)
sphere::[(GLfloat, GLfloat, GLfloat)]
sphere = [(cos(del*k)*cos(del*i), 
          _sin(del*k), 
          cos(rf del*k)*sin(rf del*i)) | k <- [1..n], i <-[1..n]]
            where 
                del = rf(2*pi/(n-1))
                n = nStep 

shear f = do
   m <-  (newMatrix RowMajor [1,f,0,0
                             ,0,1,0,0
                             ,0,0,1,0
                             ,0,0,0,1])
   multMatrix (m:: GLmatrix GLfloat)

poly::[Double]->[Double]->[Double]
poly [] _ = [] 
poly _ [] = [] 
poly xs sx = map(\s -> sum s) $ map(\x -> map(\(c, p) -> c*(x^p)) po) sx
            where
                po = zip xs [0..]

-- torus paremater equation center at (0, 0, 0)
-- Torus equation: http://xfido.com/html/indexThebeautyofTorus.html 
torus::[(GLfloat, GLfloat, GLfloat)]
torus= [((br + r*cos(del*i))*cos(del*k), 
        sin(rf del*i), 
        (br + r*cos(rf del*i))*sin(rf del*k) ) | i <- [1..n], k <-[1..n]]
        where 
            del = rf(2*pi/(n-1))
            n = nStep 
            r = 0.2
            br = 0.3

-- preservingMatrix::IO a-> IO a
coordTip::IO()
coordTip = do 
    renderPrimitive Lines $ mapM_(\(x, y, z) -> do
        color (Color3 0 1 0 :: Color3 GLdouble)
        vertex (Vertex3 x y z ::Vertex3 GLfloat)
        ) conic 

coordTipX::IO()
coordTipX = do 
    preservingMatrix $ do
        translate (Vector3 1 0 0 :: Vector3 GLdouble)
        --rotate (90)$ (Vector3 0 0 1 :: Vector3 GLdouble)
        coordTip

coordTipY::IO()
coordTipY = do 
    preservingMatrix $ do
        translate (Vector3 0 1 0 :: Vector3 GLdouble)
        rotate (90)$ (Vector3 0 0 1 :: Vector3 GLdouble)
        coordTip

coordTipZ::IO()
coordTipZ = do 
    preservingMatrix $ do
        translate (Vector3 0 0 1 :: Vector3 GLdouble)
        rotate (-90)$ (Vector3 0 1 0 :: Vector3 GLdouble)
        coordTip

-- | draw line from two points
-- | /Users/cat/myfile/bitbucket/math/drawLine.pdf
-- | ----------------------------------------------------------------------------- 
-- | p &= (1-t) p0 - t p_1
-- | ---------------------------------------------------------------------------- 
lineInterpolate::(GLfloat, GLfloat, GLfloat)->(GLfloat, GLfloat, GLfloat)->[(GLfloat, GLfloat, GLfloat)]
lineInterpolate (x0, y0, z0) (x1, y1, z1) = map(\k -> let t = sm*k in ((1-t) `px` (x0, y0, z0)) `ax`  (t `px` (x1, y1, z1)) ) [0..10] 
                    where
                        (dx, dy, dz) = (x1 - x0, y1 - y0, z1 - z0)

                        px::GLfloat->(GLfloat, GLfloat, GLfloat)->(GLfloat, GLfloat, GLfloat)
                        px d (x, y, z) = (d*x, d*y, d*z)

                        ax::(GLfloat, GLfloat, GLfloat)->(GLfloat, GLfloat, GLfloat)->(GLfloat, GLfloat, GLfloat)
                        ax (x0, y0, z0) (x1, y1, z1) = (x0 + x1, y0 + y1, z0 + z1)

                        sm = 1/10


drawFrame::IO()
drawFrame= do
    renderPrimitive Lines $ do
        let x = 0.5 
        let y = 0.5 
        -- top x-Axis
        color  (Color3 1 0 0 :: Color3 GLdouble)
        vertex (Vertex3 (-x) y 0 ::Vertex3 GLdouble)
        color  (Color3 1 0 0 :: Color3 GLdouble)
        vertex (Vertex3 x y 0 :: Vertex3 GLdouble)
        -- bottom x-Axis
        color  (Color3 0 1 0 :: Color3 GLdouble)
        vertex (Vertex3 (-x) (-y) 0 :: Vertex3 GLdouble)
        color  (Color3  0     1   0 :: Color3 GLdouble)
        vertex (Vertex3 x     (-y) 0:: Vertex3 GLdouble)

        -- left y-Axis
        color  (Color3 1 0 0 :: Color3 GLdouble)
        vertex (Vertex3 x (-y) 0 ::Vertex3 GLdouble)
        color  (Color3 1 0 0 :: Color3 GLdouble)
        vertex (Vertex3 x y 0 :: Vertex3 GLdouble)
        -- right y-Axis
        color  (Color3 0 1 0 :: Color3 GLdouble)
        vertex (Vertex3 (-x) (-y) 0 :: Vertex3 GLdouble)
        color  (Color3  0     1   0 :: Color3 GLdouble)
        vertex (Vertex3 (-x)  y 0:: Vertex3 GLdouble)

renderCoordinates::IO()
renderCoordinates = do
    renderPrimitive Lines $ do
        -- x-Axis
        color  (Color3 1 0 0 :: Color3 GLdouble)
        vertex (Vertex3 (-1) 0 0 ::Vertex3 GLdouble)
        color  (Color3 1 0 0 :: Color3 GLdouble)
        vertex (Vertex3 1 0 0 :: Vertex3 GLdouble)
        -- y-Axis
        color  (Color3 0 1 0 :: Color3 GLdouble)
        vertex (Vertex3 0 (-1) 0 :: Vertex3 GLdouble)
        color  (Color3 0 1 0 :: Color3 GLdouble)
        vertex (Vertex3 0 1 0 :: Vertex3 GLdouble)
        -- z-Axis
        color  (Color3 0 0 1 :: Color3 GLdouble)
        vertex (Vertex3 0 0 (-1) :: Vertex3 GLdouble)
        color  (Color3 0 0 1 :: Color3 GLdouble)
        vertex (Vertex3 0 0 1 :: Vertex3 GLdouble)

renderTriangle::IO()
renderTriangle = do
    renderPrimitive Triangles $ do
        color  (Color3 1 0 0 :: Color3 GLdouble)
        vertex (Vertex3 (negate 0.6) (negate 0.4) 0 :: Vertex3 GLdouble)
        color  (Color3 0 1 0 :: Color3 GLdouble)
        vertex (Vertex3 0.6 (negate 0.4) 0 :: Vertex3 GLdouble)
        color  (Color3 0 0 1 :: Color3 GLdouble)
        vertex (Vertex3 0 0.6 0 :: Vertex3 GLdouble)

triangle::[(GLfloat, GLfloat, GLfloat)]
triangle = [
               (0.9, 0.4, 0),
               (0.6, 0.4, 0),
               (0.4, 0.1, 1)
            ]

renderSurface::[(GLfloat, GLfloat, GLfloat)]->IO()
renderSurface xs = do 
        iterateList (bigChunk 80 xs) (\chunk -> do
                        let len = length chunk
                        let n = div len 2 
                        let c = mergeChunk n chunk
                        let cc = zipWith(\x y -> (x, y)) c [1..]
                        renderPrimitive TriangleStrip $ mapM_(\((x, y, z), n) -> do 
                            case mod n 3 of 
                                0 -> do 
                                        color(Color3 0.8 1 0 :: Color3 GLdouble) 
                                1 -> do 
                                        color(Color3 0 0.5 1 :: Color3 GLdouble) 
                                _ -> do 
                                        color(Color3 1 0 0.7 :: Color3 GLdouble) 
                            normal $ (Normal3 x y z::Normal3 GLfloat)
                            vertex $ Vertex4 x y z 0.8) cc  
                            )


                    

torus2::[(GLfloat, GLfloat, GLfloat)]
torus2= [((br + r*_cos(del*i))*_cos(del*k) + _cos(del*i), 
        _sin(del*i)*_cos(del*k), 
        (br + r*_cos(del*i))*_sin(del*k) ) | i <- [1..n], k <-[1..n]]
        where 
            del = 2*pi/(n-1)
            n = nStep 
            r = 0.1
            br = 0.2

circle::[(GLfloat, GLfloat, GLfloat)]
circle =[ let alpha = (pi2*n)/num in (r*_sin(alpha), r*_cos(alpha), 0) | n <- [1..num]]
        where
            num = 40 
            r = 0.5 
            pi2 = 2*pi::Float 

renderCurve::[(GLfloat, GLfloat, GLfloat)]->IO()
renderCurve points = do 
    renderPrimitive LineStrip $ mapM_(\(x, y, z) -> vertex $ Vertex3 x y z) points

conic::[(GLfloat, GLfloat, GLfloat)]
conic= [ let r' = r - rd*i in (d'*i, r'*_sin(del*k), r'*_cos(del*k)) | i <- [0..m], k <-[1..n]]
        where 
            n = 40::Float 
            pi2 = 2*pi::Float 
            del = pi2/n
            h = 0.2
            m = 20
            r = 0.1 
            d' = h/m
            rd = r/m

twopt10::[(GLfloat, GLfloat, GLfloat)]
twopt10 = take 20 sphere

twopt20::[(GLfloat, GLfloat, GLfloat)]
twopt20 = take 20 $ drop 20 sphere

bigChunk::Int->[(GLfloat, GLfloat, GLfloat)]->[[(GLfloat, GLfloat, GLfloat)]]
bigChunk n xs = splitPt n xs 

fourPt::[(GLfloat, GLfloat, GLfloat)]
fourPt = mergeList twopt10 twopt20 

testPt1::[(GLfloat, GLfloat, GLfloat)]
testPt1 = [(-1, 0, 0), (1, 0, 0), (2, 0, 0)]

testPt2::[(GLfloat, GLfloat, GLfloat)]
testPt2 = [(-1, 1, 0), (1, 1, 0), (2, 1, 0)]

testPt3 = mergeList testPt1 testPt2

splitPt::Int->[(GLfloat, GLfloat, GLfloat)]->[[(GLfloat, GLfloat, GLfloat)]]
splitPt _ [] = []
splitPt n xs = take n xs : (splitPt n $ drop n xs)

toStr::(Show a)=>[a]->[String]
toStr [] = [] 
toStr xs = map(\x -> show x) xs 


block = splitPt 10 sphere
pt100 = (join $ take 1 $ drop 0 block)::[(GLfloat, GLfloat, GLfloat)]
pt200 = (join $ take 1 $ drop 1 block)::[(GLfloat, GLfloat, GLfloat)]

pp1 = splitPt 2 pt100
pp2 = splitPt 2 pt200
zipPt = zipWith(\x y -> x ++ y) pp1 pp2
flatPt = join zipPt 

bool :: Bool -> a -> a -> a
bool b falseRes trueRes = if b then trueRes else falseRes

unless' :: Monad m => m Bool -> m () -> m ()
unless' action falseAction = do
    b <- action
    unless b falseAction

maybe' :: Maybe a -> b -> (a -> b) -> b
maybe' m nothingRes f = case m of
    Nothing -> nothingRes
    Just x  -> f x
    
-- type ErrorCallback = Error -> String -> IO ()
errorCallback :: G.ErrorCallback
errorCallback err description = hPutStrLn stderr description


-- type KeyCallback = Window -> Key -> Int -> KeyState -> ModifierKeys -> IO ()
keyBoardCallBack :: IORef (Set G.Key) -> G.KeyCallback
keyBoardCallBack ref window key scanCode keyState modKeys = do
    putStrLn $ "keyBoardCallBack=>keyState" ++ show keyState ++ " " ++ "keyBoardCallBack=>key=" ++ show key
    case keyState of
        G.KeyState'Pressed -> modifyIORef ref (S.insert key) >> readIORef ref >>= \x -> print $ "callback=" ++ show x 
        G.KeyState'Released -> modifyIORef ref (S.delete key)
        _ -> return ()
    when (key == G.Key'Escape && keyState == G.KeyState'Pressed)
        (G.setWindowShouldClose window True)

data Camera = Camera {
        cameraPos::V3 GLfloat,
        cameraFront::V3 GLfloat,
        cameraUp::V3 GLfloat
        } deriving Show


-- getKey :: Enum a => a -> IO KeyButtonState
-- IO keyIsPressed is monad => it is functor
-- so fmap can be used here
keyIsPressed::G.Window -> G.Key -> IO Bool
keyIsPressed win key = isPress `fmap` G.getKey win key
                where
                    isPress::G.KeyState -> Bool
                    isPress G.KeyState'Pressed   = True
                    isPress G.KeyState'Repeating = True
                    isPress   _                  = False

isPressed::G.KeyState->Bool
isPressed G.KeyState'Pressed   = True
isPressed G.KeyState'Repeating = True
isPressed _                    = False

pressUp::G.Window->IO Bool
pressUp win = isPressed `fmap` G.getKey  win G.Key'Up

updateCamera::Set G.Key -> GLfloat -> Camera -> Camera
updateCamera keySet speed cam = S.foldr(\key cam@(Camera cameraPos cameraFront cameraUp) -> case key of
                                                G.Key'F1 -> cam{cameraPos = (V3 3 4 5),
                                                           cameraFront = (V3 3 4 5),
                                                           cameraUp = (V3 3 4 5)}
                                                _ -> cam{cameraPos = (V3 3 4 5),
                                                     cameraFront = (V3 3 4 5),
                                                     cameraUp = (V3 3 4 5)}
                                        ) cam keySet


keyCallback :: G.KeyCallback
keyCallback window key scancode action mods 
                    | (key == G.Key'Escape && action == G.KeyState'Pressed) = G.setWindowShouldClose window True
                    | otherwise = do 
                                    shear 0.3 

data MouseInfo = MouseInfo {
            lastXY::Maybe (Double, Double),
            oldPitchYaw::(Double, Double),
            frontVec::V3 GLfloat
                } deriving Show


toViewMatrix::Camera -> M44 GLfloat
toViewMatrix (Camera pos front up) = P.lookAt pos (pos ^+^ front) up

lightDiffuse ::Color4 GLfloat
lightDiffuse = Color4 0.6 1.0 0.5 0.6

lightAmbient ::Color4 GLfloat
lightAmbient = Color4 0.0 0.0 1.0 1.0

lightPosition ::Vertex4 GLfloat
lightPosition = Vertex4 1.0 1.0 1.2 0.0

lightSpecular ::Color4 GLfloat
lightSpecular = Color4 1.0 0.7 1.0 0.8

v3ToVertex3::V3 GLfloat-> Vertex3 GLfloat 
v3ToVertex3 (V3 x y z) = (Vertex3 x y z::Vertex3 GLfloat)

oldCamera = Camera (V3 1 1 1) (V3 1 0 0) (V3 0 1 0)

draw::[(GLfloat, GLfloat, GLfloat)]-> IO ()
draw listpoints = do
  G.setErrorCallback (Just errorCallback)
  -- finally add double buffer
  successfulInit <- G.init
  G.windowHint (G.WindowHint'DoubleBuffer True)
  -- if init failed, we exit the program
  bool successfulInit exitFailure $ do
      mw <- G.createWindow 600 600 "Simple example, haskell style" Nothing Nothing
      maybe' mw (G.terminate >> exitFailure) $ \window -> do
          G.makeContextCurrent mw
          mainLoop listpoints window
          G.destroyWindow window
          G.terminate
          exitSuccess
          
mainLoop::[(GLfloat, GLfloat, GLfloat)]-> G.Window -> IO ()
mainLoop listpoints w = unless' (G.windowShouldClose w) $ do
    writeToFile "/tmp/g1.x" $ toStr torus  
    lastFrame <- maybe 0 realToFrac <$> G.getTime

    (width, height) <- G.getFramebufferSize w
    pp $ "width=" ++  show width
    pp $ "height=" ++  show height 
    let ratio = fromIntegral width / fromIntegral height
    viewport $= (Position 0 0, Size (fromIntegral width) (fromIntegral height))
    clear [ColorBuffer, DepthBuffer]
    -- lighting info
    diffuse  (Light 0) $= lightDiffuse
    ambient  (Light 0) $= lightAmbient
    specular (Light 0) $= lightSpecular
    position (Light 0) $= lightPosition
    light    (Light 0) $= Enabled
    --lighting           $= Enabled
    depthFunc  $= Just Lequal
    blend          $= Enabled
    lineSmooth     $= Enabled
    -- end lighting info

    matrixMode $= Projection
    matrixMode $= Modelview 0
    loadIdentity
    let fovy = 60.0; aspect = 1.0; zNear = 2.0; zFar = (-2)
        in GM.perspective fovy aspect zNear zFar 
    --ortho (negate ratio) ratio (negate 1.0) 1.0 1.0 (negate 1.0)

    loadIdentity
    --gluLookAt(0, 0, 1.2, 0, 0, 0, 0, 1, 0);
    --GM.lookAt (Vertex3 0 0 1.3::Vertex3 GLdouble) (Vertex3 0 0 0:: Vertex3 GLdouble) (Vector3 0 1 0 :: Vector3 GLdouble)
    -- this is bad, but keeps the logic of the original example I guess

    --loadIdentity
    --G.setKeyCallback w (Just keyCallback)


    G.getTime >>= \(Just t) -> rotate ((realToFrac t) * 10) $ (Vector3 1 0 1 :: Vector3 GLdouble)
    timeValue <- maybe 0 realToFrac <$> G.getTime
    let deltaTime = timeValue - lastFrame 
    let cameraSpeed = 5*deltaTime

    ------------------------------------------------------------------   
    -- I think there is issue here, in keyBoardCallBack function
    -- can write the key to (Set G.Key), but can't read it with readIORef
    ref <- newIORef S.empty
    G.setKeyCallback w (Just $ keyBoardCallBack ref)
    keyDown <- readIORef ref
    let sset = Set
    modifyIORef ref (S.insert G.Key'F1)
    keyDown1 <- readIORef ref
    print $ "keyDown=" ++ (show keyDown)
    print $ "keyDown1=" ++ (show keyDown1)
    ------------------------------------------------------------------ 
    keyP <- keyIsPressed w G.Key'F1
    if keyP then print "press=>F1" else print "no pressed F1"

    modifyIORef ref (S.insert G.Key'W)
    keyPress <- readIORef ref
    let camera = updateCamera keyPress cameraSpeed oldCamera
    print camera

    let pos   = v3ToVertex3 $ cameraPos camera
    let front = v3ToVertex3 $ cameraFront camera
    let up    = v3ToVertex3 $ cameraUp camera
    print $ " pos=" ++ show pos ++ " font=" ++ show front ++ " up=" ++ show up

    --print deltaTime

    mouseRef <- newIORef $ MouseInfo Nothing (0, (-90)) (V3 0 0 (-1))
    --G.setCursorInputMode w G.CursorInputMode'Disabled
    --G.setCursorPosCallback w (Just $ cursorPosCallBack mouseRef)
     
    --shear 1 
    --render sphere
    renderCoordinates
    --coordTip
    coordTipX
    coordTipY
    coordTipZ

    -- renderSurface torus 
    -- renderTriangle 
    renderCurve listpoints 
    drawFrame

    G.swapBuffers w
    G.pollEvents
    mainLoop listpoints w


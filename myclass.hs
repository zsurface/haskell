import AronModule
--import qualified Prelude as P

class MyType a where
    dotProduct::[a]->[a]->[a]

instance MyType Double where
    dotProduct u v = zipWith(\x y -> x*y) u v  
  
instance MyType Int where
    dotProduct u v = zipWith(\x y -> x*y) u v  

instance MyType String where
    dotProduct u v = zipWith(\x y -> x++y) u v  

main =  do 
        pp "dog"
        let s1 = [0.1::Double, 0.3]
        let s2 = [0.2, 0.4]
        let ss = dotProduct s1 s2
        pp "dog"


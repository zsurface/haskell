{-# LANGUAGE DeriveAnyClass    #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}

import           Data.Aeson           (ToJSON)
import           Data.Aeson.Text      (encodeToLazyText)
import qualified Data.ByteString.Lazy as B
import           Data.Text.Lazy       (Text)
import           Data.Text.Lazy.IO    as I
import           GHC.Generics

-- read and write json
-- ref: https://www.schoolofhaskell.com/school/starting-with-haskell/libraries-and-frameworks/text-manipulation/json
-- url: http://hackage.haskell.org/package/aeson-1.4.2.0/docs/Data-Aeson.html

jsonFile::FilePath
jsonFile = "./text/json.json"

getJSON::IO B.ByteString
getJSON = B.readFile jsonFile

data Expr a = Con a |     Add (Expr a)    (Expr a)


data Cat = Cat { name :: Text, age :: Int } deriving (Show, Generic, ToJSON)
meowmers = Cat { name = "meowmers", age = 1 }
main = do
        I.writeFile "myfile.json" (encodeToLazyText meowmers)
        bs <- getJSON
        print bs

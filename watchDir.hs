-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}  --
{-# LANGUAGE MultiWayIf #-}  --
-- 
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Turtle (shell, empty)                      
import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent(threadDelay)

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".

import AronModule 

path = "/Users/cat/myfile/bitbucket/haskelllib"

main = do 
        home <- getEnv "HOME"
        let graphic= home </> "myfile/bitbucket/haskelllib/AronGraphic.hs"
        let aronmo= home </> "myfile/bitbucket/haskelllib/AronModule.hs"
        let waiLib= home </> "myfile/bitbucket/haskelllib/WaiLib.hs"
        let wai= home </> "myfile/bitbucket/snippets/snippet.hs"
        let pdf= home </> "myfile/bitbucket/math/number.tex"
        let aronGraphicTest= home </> "myfile/bitbucket/haskell/AronGraphicTest.hs"
        let aronopengl= home </> "myfile/bitbucket/haskelllib/AronOpenGL.hs"
        let javalib= home </> "myfile/bitbucket/javalib/Aron.java"
        let passworddir= home </> "myfile/password"
        let haskelllib= home </> "myfile/bitbucket/haskelllib"

        let javadoc= "cd $b/javalib && javadoc -cp $(ls -d $jlib/jar/*.jar | tr '\n' ':') -noqualifier all -d /Library/WebServer/Documents/zsurface/htmljavadoc  *.java"

        begTime <- timeNowSecond 
        begTimeIO <- newIORef begTime 
        forever $ do
            bo <- watchDir path
            print bo
            pp "done!"


-- {{{ begin_fold
module Main where

import Control.Monad (unless, when)
import Graphics.Rendering.OpenGL as GL 
import qualified Graphics.UI.GLFW as G
import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
import System.Exit
import System.IO
import Control.Monad
import Data.Set(Set) 
import Data.IORef 
import qualified Data.Set as S 

-- import Graphics.UI.GLUT
import Graphics.UI.GLUT.Callbacks.Global
import AronModule  hiding (rw)
import AronGraphic hiding (dist)
import AronOpenGL

-- | -------------------------------------------------------------------------------- 
-- | compile: runh keyboardpress_simple.hs  
-- | ghc -i/Users/cat/myfile/bitbucket/haskelllib -o file file.hs
-- | 
-- | KEY: keyboard example, keypress example, modifyIORef example,  
-- | -------------------------------------------------------------------------------- 

mymain :: IO ()
mymain = do
  -- finally add double buffer
  successfulInit <- G.init
  G.windowHint (G.WindowHint'DoubleBuffer True)
  -- if init failed, we exit the program
  bool successfulInit exitFailure $ do
      mw <- G.createWindow 600 600 "Simple example, haskell style" Nothing Nothing
      maybe' mw (G.terminate >> exitFailure) $ \window -> do
          G.makeContextCurrent mw
          ref <- newIORef initCam 
          refStep <- newIORef initStep 
          mainLoop window ref refStep
          G.destroyWindow window
          G.terminate
          exitSuccess

initWin:: G.Window -> IORef Cam -> IORef Step -> IO ()
initWin w ref refStep = do
        (width, height) <- G.getFramebufferSize w
        viewport $= (Position 0 0, Size (fromIntegral width) (fromIntegral height))
        GL.clear [ColorBuffer, DepthBuffer]
        G.setKeyCallback w (Just $ keyBoardCallBack refStep)

        matrixMode $= Modelview 0
        loadIdentity
        keyboardRot ref refStep
        renderCoordinates

-- | ,}}} end_fold

-- zo - open fold
-- za - close fold

mainLoop :: G.Window -> IORef Cam -> IORef Step -> IO ()
mainLoop w ref refStep = unless' (G.windowShouldClose w) $ do
    initWin w ref refStep
--    drawTriangle

    -- render code begin 

    let f x = x*x*x 
    let s = map(\x0 -> (Vertex3 x0 (f x0) 0,  Vertex3 (x0 + 0.1) (tangent f (x0 + 0.1) x0) 0) ) $ map(0.1*) [-10..10]

    pp $ "s=" <<< s
    mapM_ (drawSegment red) s

    drawCurve f (-1.0, 1.0) green
    mapM_ (\p -> drawNormalLineNew f p p red) $ map(0.1*) [-10..10]



    -- render code end

    G.swapBuffers w
    G.pollEvents
    mainLoop w ref refStep
main = mymain


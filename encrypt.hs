import Crypto.Cipher
import Crypto.Cipher.Types
import qualified Data.ByteString.Char8 as B

keyString = B.pack "It a 128-bit key"

Right key = makeKey keyString

aes128 :: AES128
aes128 = cipherInit key

ptext = B.pack "16 bytte ssecret"

ctext = ecbEncrypt aes128 ptext

main = putStrLn $ B.unpack ctext

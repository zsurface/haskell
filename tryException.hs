#!/usr/bin/env stack
-- stack --resolver lts-9.0 script
import Control.Exception.Safe
import System.Exit
import Control.Concurrent
import Control.Concurrent.Async

--main = tryAny foo >>= print


main = do
        print "start"
        concurrently (exitWith(ExitFailure 3)) $ do 
        -- forkIO(exitWith(ExitFailure 3))
        threadDelay 1000000
        print "dog"

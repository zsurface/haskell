import Control.Monad
import Data.Char
import Data.List
import Data.Set
import Data.List.Split
import Data.Time.Clock.POSIX
import Data.Time
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 
import AronModule 

-- | import the following Modules
import qualified Data.Text as T
import qualified Data.Text.Lazy as L
import qualified Data.ByteString.Char8 as B

-- | Convert String to Text, Text to String, 
-- | Convert Text to ByteString, ByteString to Text
-- | Convert String to ByteString, ByteString to String
main = do 
        print "String, ByteString, Lazy String, Strict String"
        argList <- getArgs
        -- String to Text
        let text = L.pack "String to Text"
        pp text 
        -- Text to String
        let str = L.unpack text
        pp $ rw $ str 
        -- String to ByteString
        let byteString = B.pack "String to ByteString"
        pp byteString 
        -- ByteString to String
        let string = B.unpack byteString
        pp $ rw $ string
        -- Lazy to Strict
        let strictStr = L.toStrict text 
        pp strictStr 
        -- Strict to Lazy
        let lazyStr = L.fromStrict strictStr
        pp lazyStr
        pp "done!"

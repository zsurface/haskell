-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

{-# LANGUAGE MultiWayIf        #-}
{-# LANGUAGE QuasiQuotes       #-} -- support raw string [r|<p>dog</p> |]
import Text.RawString.QQ       -- Need QuasiQuotes too 


-- import Data.Set   -- collide with Data.List 
import Data.Maybe
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import qualified Data.Word8 as DW
import Data.Text (Text)  -- strict Text
import qualified Data.Text as TS               -- strict Text         
import qualified Data.Text.Lazy                 as DL 
import qualified Data.Text.IO                   as TIO 

import qualified Control.Concurrent             as Concurrent
import qualified Data.List as L
import qualified Control.Exception              as Exception
import qualified Safe

import qualified Data.ByteString.UTF8 as BU
import qualified Data.ByteString.Lazy.Internal as IN (ByteString)
import qualified Data.ByteString.Char8      as S8 (unpack,pack, putStrLn)   -- strict ?
import qualified Data.ByteString.Lazy       as LA (writeFile, fromChunks, fromStrict)
import qualified Data.ByteString.Lazy.Char8 as LC 
import qualified Data.ByteString            as BS
import qualified Data.ByteString.Internal   as BI (c2w, w2c)

import qualified Text.Regex.TDFA as TD
import qualified Data.HashMap.Strict as M 

import           Data.Int (Int64)
import           Database.SQLite.Simple
import           Database.SQLite.Simple.FromRow
import           Database.SQLite.Simple.FromField
import           Database.SQLite.Simple.ToField
import           Database.SQLite.Simple.Internal
import           Database.SQLite.Simple.Ok


--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

-- shell command template:
-- 
--        argList <- getArgs
--        if len argList == 2 then do
--            let n = stringToInt $ head argList
--            let s = last argList
--            putStr $ drop (fromIntegral n) s
--        else print "drop 2 'abcd'"


import AronModule 

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close

dbname = "webappdb"
configFile = "/Users/cat/myfile/bitbucket/testfile/snippet_config.txt"

lookupJust s m = fromJust $ M.lookup s m

data CodeBlock = 
    CodeBlock 
    { id        :: Int64
    , header    :: TS.Text
    , codeblock :: TS.Text
    } deriving (Eq, Read, Show)

instance FromRow CodeBlock where
  fromRow = CodeBlock <$> field <*> field <*> field

-- What is 'Only'
-- https://hackage.haskell.org/package/postgresql-simple-0.4.9.0/docs/Database-PostgreSQL-Simple.html#t:ToRow
instance ToRow CodeBlock where
  toRow (CodeBlock _pId pHeader pCode) = toRow (pHeader, pCode)

-- queryDatabaseToFile::FilePath -> Connection -> IO()
-- queryDatabaseToFile fp conn = do
              -- codeBlocks <- query_ conn "SELECT id, header, codeblock from CodeBlock" :: IO [CodeBlock]
              -- let codeList = map (\x -> lines . toStr . codeblock $ x) codeBlocks 
              -- mapM_ (\b -> do 
                           -- writeToFileAppend fp b
                    -- ) codeList
              -- return ()
    
-- KEY: sqlite example, sql example, select table and write to file
main :: IO ()
main = do
    home <- getEnv "HOME"
    configMap <- readConfig configFile 
    os <- getOS
    let userinputdb = lookupJust dbname $ lookupJust os configMap
    let host = lookupJust "host" $ lookupJust os configMap
    let snippet = lookupJust "snippetpath" $ lookupJust os configMap
    conn <- open $ home </> userinputdb 
    queryDatabaseToFile "/tmp/a.x" conn
    pp "done"

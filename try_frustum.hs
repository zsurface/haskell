import Graphics.Rendering.OpenGL as GL
import Graphics.UI.GLUT as GLUT


myRect width height =
    displayPoints [(w,h,0),(w,-h,0),(-w,-h,0),(-w,h,0)] Quads
        where
            w = width/2
            h = height/2

square width = myRect width width

main = do
    (progName,_) <- getArgsAndInitialize
    initialDisplayMode $= [Depth]
    createWindow progName
    depthFunc $= Just Less
    displayCallback $= display

    matrixMode $= Projection
    loadIdentity
    let near = 1
        far = 40
        right = 1
        top = 1
    frustum (-right) right (-top) top near far
    matrixMode $= Modelview 0

    clearColor $= GLUT.Color4 1 1 1 1
    mainLoop

display = do
    clear [ColorBuffer,DepthBuffer]
    GL.loadIdentity
    translate (GLUT.Vector3 0 0 (-2::GLfloat))
    GLUT.currentColor GLUT.$= GLUT.Color4 1 0 0 1
    square 1

    GL.loadIdentity
    GLUT.translate (GLUT.Vector3 4 4 (-5::GLfloat))
    GLUT.currentColor GLUT.$= GLUT.Color4 0 0 1 1
    square 1
    GLUT.flush

import AronModule
import Control.Monad
import Data.Char 
import System.Directory
import System.Environment
import System.IO
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix

import Control.Monad
import System.Directory
import System.FilePath
import System.Posix.Files

--pre::FilePath -> Bool
--pre f = matchTest reg f 
--                where
--                    reg = mkRegex "\\*" 

pre::FilePath->Bool
pre f = True

jpgFile::FilePath->Bool
jpgFile f = matchTest reg $ takeFileName f
                where
                    reg = mkRegexWithOpts "(\\.jpg)" True False

isDir::FilePath->IO Bool
isDir path = getFileStatus path >>= \s -> return (isDirectory s)

-- gx http://stackoverflow.com/questions/13297896/is-there-some-directory-walker-in-haskell
-- | Traverse from 'top' directory and return all the files by
-- filtering out the 'exclude' predicate.
-- there is issue with getDirectoryContents in the orginal code
-- fix with listDirectory
traverseDir :: FilePath -> (FilePath -> Bool) -> IO [FilePath]
traverseDir top predict = do
  ds <- listDirectory top
  paths <- forM (filter (predict) ds) $ \d -> do
    let path = top </> d
    --let path = "\"" ++ pp ++ "\""
    s <- getFileStatus path
    if isDirectory s
      then traverseDir path predict
      else return [path]
  return (concat paths)

myDir = "/Users/cat/try/Pictures"
top = "/Users/cat/try/kkk"
source="/tmp/kkkk"
path = "/Users/cat/try/kkk/dir space" 
newpath = "/Users/cat/try/kkk/dirspace/space.jpg" 

testDir = "/Users/cat/try/kkk"

main = do 
        print "List all file recursively in the current dir"
        allFiles <- traverseDir testDir pre 
        mapM print allFiles
        let spaceList = map(\x -> takeDirectory x) allFiles 
        let noSpaceList = map(\x -> removeSpace $ takeDirectory x) allFiles 
        let m1 = map(\x -> let p = takeDirectory x in p) allFiles
        let m2 = map(\x -> let p = takeDirectory x in removeSpace p) allFiles
        mapM print m1
        pa m1 
        pa m2
        let zp = zipWith(\x y ->(x, y)) m1 m2
        fl
        mapM print zp 
        mapM(\x -> renameDirectory (fst x) (snd x)) zp 
        fl
        renameDirectory "/Users/cat/try/kkk/dir1 s1" "/Users/cat/try/kkk/dir1s1"
        fl

        

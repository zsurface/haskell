import Control.Monad
import Data.Char
import Data.List
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import AronModule 

--createDirectoryIfMissing

main = do 
        argList <- getArgs
        print argList
        f <- lsRegex "/tmp" "\\.hi"
        en <- getEnv "tt"
        pp f
        let full = map(\x -> "/tmp" </> x) f
        mapM(\x -> copyFileToDir x en) full
        pp full
        fl
        let l = sfilter "try" f
        pp l 
        pp "dog"

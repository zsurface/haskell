import Control.Monad
import Data.Char
import Data.List
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Control.Exception
import AronModule 

-- generate symbol link in ~/.vim/doc for helptags
-- Thu Aug 16 14:18:45 2018 
-- noteindex.txt needs special header(deal breaker) in order to genereate tags for helptags
-- =try error example, exception
arr =[
        (note,    vimdoc </> "note/noteindex.txt")
--        (haskell, vimdoc </> takeFileName haskell),
--        (java,    vimdoc </> takeFileName java)
     ]
     where
        vimdoc  = "/Users/cat/.vim/doc"
        note    = "/Users/cat/myfile/bitbucket/note/noteindex.txt"
        haskell = "/Users/cat/myfile/bitbucket/haskell"
        java    = "/Users/cat/myfile/bitbucket/java"

link::String->String->String
link s t = ln ++ s ++ " " ++ t 
        where 
            ln = "ln -s "

doit::String->String->IO()
doit source target = do
            err <- try (removeLink target)::IO (Either SomeException  () )
            case err of 
                Left _ -> putStrLn $ "SomeException:" ++ source 
                Right () -> putStrLn $ "removeLink "
            run $ link source target 
            return ()

main = do 
        mapM(\x -> doit (fst x) (snd x)) arr
        pp "done!"
        

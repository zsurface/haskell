
-- zo - open
-- za - close

import AronModule 

import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as BC
hello      = "你好"
helloBytes = BC.pack hello


bytestring = BC.pack "I'm a ByteString, not a [Char]"

main = do
    BC.putStrLn bytestring
    print $ B.head bytestring
    print $ BC.head bytestring
    fw "byteString and unicode"
    putStrLn hello
    BC.putStrLn helloBytes
    print $ BC.length helloBytes

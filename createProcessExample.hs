import Control.Monad
import Data.Char 
import Data.List
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import AronModule

-- pass std_out to std_in throught pipe
main = do                                                                         
        (_, Just so, _, ph1)  <- createProcess (proc "ls" ["-ltah", "."])
                                   { std_out = CreatePipe } 
        (_, _, _, ph2) <- createProcess (proc "sort" []) { std_in = UseHandle so }
        waitForProcess ph1
        waitForProcess ph2
        print "foo"

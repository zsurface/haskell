#!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib

-- stack --install-ghc runghc --package turtle
                                    -- #!/bin/bash
{-# LANGUAGE OverloadedStrings #-}  --
                                    --
import Turtle                       --
import AronModule

--ff = \x -> x + 1

ff = \((a, b), c) -> (a, b, c)
--
f1 = \((a, b), c) -> a
f2 = \((a, b), c) -> b 
f3 = \((a, b), c) -> c 

data RowType = RowType{
    htmlURL :: String,
    title   :: String,
    imgURL  :: String
    } deriving (Show)

toType x = RowType{ htmlURL = (f1 x), title = (f2 x), imgURL = (f3 x)}



main = do
       echo "Hello, world!"         -- echo Hello, world!
       pp "do"
       let t = toType (("url.com", "title"), "img.jpeg")
       pp t
       pp $ htmlURL t
       pp $ title t 
       pp $ imgURL t 

import Control.Monad
import Data.Char
import Data.List
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import AronModule 

data MyTree a = Leaf a
                | Branch (MyTree a) (MyTree a) 

-- "a + b + c"
data Expr = C Float
            | Expr :+ Expr
            | Expr :* Expr
            | Expr :/ Expr
            | Expr :- Expr

evaluate:: Expr -> Float
evaluate (C x) = x 
evaluate (e1 :+ e2) = evaluate e1 + evaluate e2
evaluate (e1 :* e2) = evaluate e1 * evaluate e2
evaluate (e1 :/ e2) = evaluate e1 / evaluate e2
evaluate (e1 :- e2) = evaluate e1 - evaluate e2

e1 = (C 1 :+ C 2)  :* (C 4 :* C 5)

main = do 
        print "Hello World"
        argList <- getArgs
        pp $ length argList
        pp "done!"
        print $ evaluate e1

import Control.Monad
import Data.Char
import Data.List
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import AronModule 

arr2tuple::[a]->(a, a, a)
arr2tuple [a, b, c] = (a, b, c)
list = [[1, 2, 3], [4, 5, 6]]

div3::Int->Int->Maybe Int
div3 a b = if b == 0 then Nothing else Just (div a b) 

getJust::Maybe Int -> Int 
getJust Nothing = 0 
getJust (Just a) = a

main = do 
        print "Hello World"
        argList <- getArgs
        pp $ length argList
        pp "done!"
        let s = arr2tuple [1, 2, 3]
        pp s
        --map(\x -> map(\t -> arr2tuple t ) x) list
        let ss = map(\xs -> arr2tuple xs) list
        pp "dog"
        pp ss
        print $ div3 4 3
        let aa = map(\x -> getJust $ div3 4 x) [1, 3, 0]
        pp aa



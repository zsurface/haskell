import qualified Crypto.Hash.SHA3 as SHA3

main = putStrLn $ show $ digest
  where digest = SHA3.finalize ctx
        ctx    = foldl' SHA3.update iCtx (map Data.ByteString.pack [ [1,2,3], [4,5,6] ]
        iCtx   = SHA3.init 224

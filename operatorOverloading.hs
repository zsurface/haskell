import Control.Monad (filterM, liftM)
import Data.Char 
import Data.Time
import Data.Time.Clock.POSIX
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix  -- "</>"
import System.IO
import System.Posix.Unistd
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import AronModule

(#)::Int->Int
(#) x = x + 1

(@:)::Int->Int
(@:) x = x + 1

(-.)::Int->Int
(-.) x = x + 1

(+.)::Int->Int
(+.) x = x + 1

(+>)::Int->Int
(+>) x = x + 1

(++>)::Int->Int
(++>) x = x + 1

(~>)::Int->Int
(~>) x = x + 1

(~.)::Int->Int
(~.) x = x + 1

(~:)::Int->Int
(~:) x = x + 1

(~~)::Int->Int
(~~) x = x + 1

(<>)::Int->Int
(<>) x = x + 1

(>:)::Int->Int
(>:) x = x + 1

(<:)::Int->Int
(<:) x = x + 1

(<.)::Int->Int
(<.) x = x + 1

(<~)::Int->Int
(<~) x = x + 1

(<+)::Int->Int
(<+) x = x + 1

(<|)::Int->Int
(<|) x = x + 1

(|>)::Int->Int
(|>) x = x + 1

(%>)::Int->Int
(%>) x = x + 1

(<%)::Int->Int
(<%) x = x + 1

(<@)::Int->Int
(<@) x = x + 1


(<?)::Int->Int
(<?) x = x + 1

(?.)::Int->Int
(?.) x = x + 1

(&.)::Int->Int
(&.) x = x + 1

(&:)::Int->Int
(&:) x = x + 1

(&|)::Int->Int
(&|) x = x + 1

(.|)::Int->Int
(.|) x = x + 1

(||)::Int->Int
(||) x = x + 1

(~|)::Int->Int
(~|) x = x + 1

(+|)::Int->Int
(+|) x = x + 1

(+@)::Int->Int
(+@) x = x + 1

(+^)::Int->Int
(+^) x = x + 1

(^.)::Int->Int
(^.) x = x + 1

(.^)::Int->Int
(.^) x = x + 1

(.^.)::Int->Int
(.^.) x = x + 1

(~!)::Int->Int
(~!) x = x + 1

(.!)::Int->Int
(.!) x = x + 1

(!.)::Int->Int
(!.) x = x + 1

(/.)::Int->Int
(/.) x = x + 1

(//)::Int->Int
(//) x = x + 1

(\.)::Int->Int
(\.) x = x + 1

(\>)::Int->Int
(\>) x = x + 1

(/>)::Int->Int
(/>) x = x + 1

(/&)::Int->Int
(/&) x = x + 1

(/\)::Int->Int
(/\) x = x + 1

(\/)::Int->Int
(\/) x = x + 1

(\&)::Int->Int
(\&) x = x + 1

(#:)::Int->Int
(#:) x = x + 1

(#.)::Int->Int
(#.) x = x + 1

(#|)::Int->Int
(#|) x = x + 1

(|#)::Int->Int
(|#) x = x + 1

(<->)::Int->Int
(<->) x = x + 1

(<=>)::Int->Int
(<=>) x = x + 1

(</>)::Int->Int
(</>) x = x + 1

(<\>)::Int->Int
(<\>) x = x + 1



main = do 
        pp "dog"

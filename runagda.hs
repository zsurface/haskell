import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import AronModule 

main = do 
        argList <- getArgs
        let len = length argList
        pp len
        pp argList
        case len of 
             1 -> do
                let cmd = "agda -c " ++ (head argList) in run cmd >> return ()  
                run bin >>= \x -> pp x >> return () 
                    where bin = dropExtension (head argList)
             _  -> print "runagda file.agda => ./file" >> print "[agda -c file.agda]"
        pp "done!"

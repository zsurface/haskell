module Main where

import Control.Monad (unless, when)
import Graphics.Rendering.OpenGL as GL 
import qualified Graphics.UI.GLFW as G
import System.Exit
import System.IO
import Control.Monad
import Data.Set(Set) 
import Data.IORef 
import qualified Data.Set as S 

-- | -------------------------------------------------------------------------------- 
-- | compile: runh keyboardpress_simple.hs  
-- | ghc -i/Users/cat/myfile/bitbucket/haskelllib -o file file.hs
-- | 
-- | KEY: keyboard example, keypress example, modifyIORef example,  
-- | -------------------------------------------------------------------------------- 


renderTriangle::IO()
renderTriangle = do
    renderPrimitive Triangles $ do
        color  (Color3 1 0 0 :: Color3 GLdouble)
        vertex (Vertex3 (negate 0.6) (negate 0.4) 0 :: Vertex3 GLdouble)
        color  (Color3 0 1 0 :: Color3 GLdouble)
        vertex (Vertex3 0.6 (negate 0.4) 0 :: Vertex3 GLdouble)
        color  (Color3 0 0 1 :: Color3 GLdouble)
        vertex (Vertex3 0 0.6 0 :: Vertex3 GLdouble)

bool :: Bool -> a -> a -> a
bool b falseRes trueRes = if b then trueRes else falseRes

unless' :: Monad m => m Bool -> m () -> m ()
unless' action falseAction = do
    b <- action
    unless b falseAction

maybe' :: Maybe a -> b -> (a -> b) -> b
maybe' m nothingRes f = case m of
    Nothing -> nothingRes
    Just x  -> f x
    
-- type ErrorCallback = Error -> String -> IO ()
errorCallback :: G.ErrorCallback
errorCallback err description = hPutStrLn stderr description

-- type KeyCallback = Window -> Key -> Int -> KeyState -> ModifierKeys -> IO ()
--keyBoardCallBack :: IORef (Set G.Key) -> G.KeyCallback
--keyBoardCallBack ref window key scanCode keyState modKeys = do
--    putStrLn $ "inside =>" ++ show keyState ++ " " ++ "keyBoardCallBack=>key=" ++ show key
--    case keyState of
--        G.KeyState'Pressed -> modifyIORef' ref (S.insert key) >> readIORef ref >>= \x -> print $ "inside =>" ++ show x 
--        _ -> return ()
--    when (key == G.Key'Escape && keyState == G.KeyState'Pressed)
--        (G.setWindowShouldClose window True)

-- modifyIORef::(newIORef a)=>a->(a->a)->a 
keyBoardCallBack::IORef (Set G.Key) -> G.KeyCallback
keyBoardCallBack ref window key scanCode keyState modKeys = do
    putStrLn $ "inside =>" ++ show keyState ++ " " ++ show key
    case keyState of
        G.KeyState'Pressed -> modifyIORef ref (S.insert key) >> readIORef ref >>= \x ->print $ "inside Pressed  =>" ++ show x 
        -- G.KeyState'Repeating -> modifyIORef ref (S.insert key) >> readIORef ref >>= \x ->print $ "inside Repeating =>" ++ show x
        G.KeyState'Released -> modifyIORef ref (S.delete key)
        _ -> return ()
    when (key == G.Key'Escape && keyState == G.KeyState'Pressed)
        (G.setWindowShouldClose window True)

mymain :: IO ()
mymain = do
  -- finally add double buffer
  successfulInit <- G.init
  G.windowHint (G.WindowHint'DoubleBuffer True)
  -- if init failed, we exit the program
  bool successfulInit exitFailure $ do
      mw <- G.createWindow 600 600 "Simple example, haskell style" Nothing Nothing
      maybe' mw (G.terminate >> exitFailure) $ \window -> do
          G.makeContextCurrent mw
          -- ref <- newIORef 1 
          ref <- newIORef S.empty
          mainLoop window ref
          G.destroyWindow window
          G.terminate
          exitSuccess
          
mainLoop :: G.Window -> IORef (Set G.Key) -> IO ()
mainLoop w ref = unless' (G.windowShouldClose w) $ do
    (width, height) <- G.getFramebufferSize w
    viewport $= (Position 0 0, Size (fromIntegral width) (fromIntegral height))
    GL.clear [ColorBuffer, DepthBuffer]

    -- ref <- newIORef S.empty
    G.setKeyCallback w (Just $ keyBoardCallBack ref)
    keyDown <- readIORef ref
    print $ "outside setKeyCallback keyDown=>" ++ (show keyDown)

    renderTriangle
    
    G.swapBuffers w
    G.pollEvents
    mainLoop w ref

main = mymain


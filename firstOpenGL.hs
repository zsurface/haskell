import AronModule
import Graphics.UI.GLUT

fun::Complex->Complex
fun c = c*c

myPoints::[(GLfloat, GLfloat, GLfloat)]
--myPoints = [(sin(2*pi*k/12), cos(2*pi*k/12), 0) | k <- [1..12]]
myPoints   = [(x, x*x, x*x*x) | k <- [-100..100], 
                                let x = 0.1*k]

gridPoint::[(GLfloat, GLfloat, GLfloat)]
gridPoint = [(k*x, k*y, 0) | x <- [-10..10], y <- [-10..10], let k = 0.4]

main::IO()
main = do
    (_progName, _args) <- getArgsAndInitialize
    _window <- createWindow "Hello World"
    displayCallback $= display
    mainLoop

display::DisplayCallback
display = do
    clear [ ColorBuffer]
    --renderPrimitive Lines $ mapM_ (\(x, y, z) -> vertex $ Vertex3 x y z) myPoints 
    --renderPrimitive LineStrip $ mapM_ (\(x, y, z) -> vertex $ Vertex3 x y z) $ [(k*x, k*y, 0.0) | x <- [-1..100], y <- [1..100], let k = 0.1::Float] 
    --renderPrimitive LineStrip $ mapM_ (\(x, y, z) -> vertex $ Vertex3 x y z) $ [(k*x, 2.0, 0.0) | x <- [-10..10], let k = 0.1::Float] 
    renderPrimitive LineStrip $ mapM_ (\(x, y, z) -> vertex $ Vertex3 x y z) $ gridPoint 
    flush

{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes #-}
import Control.Monad
import Data.Char
import qualified Data.Set as DS
import qualified Data.List as DL
import Data.List.Split
import qualified Data.Map as M 
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import AronModule 
import qualified AronModule as A

-- | sort snippets: "/Users/cat/myfile/bitbucket/snippets/snippet.m"
path     = "/Users/cat/myfile/bitbucket/snippets/snippet.m"
tmpfile  = "/tmp/snippet_tmp.m"
nname    = "snippet_tmp.m"
sortName = "snippet_sort.m"

main = do 
        copyRename path nname 
        let dir = takeDirectory path
        list <- readFileToList path;
        let ll = filter(\x -> length x > 0) $ splitWhen(\x -> (length $ trim x) == 0) list
        pp $ "Number of Blocks:" ++ (show $ length ll)
        -- fpp "ll" ll 
        let newList = map(\x -> x ++ ["\n"]) ll
        -- fpp "newList" newList 
        let sortList = qqsort(\x y -> trim(head x) > trim(head y)) newList 
        createFile $ dir </> sortName 
        mapM_ (writeToFileAppend (dir </>  sortName)) sortList
        rm path
        A.rename (dir </> sortName) path 
        -- fpp "sortList" sortList
        pp $ "sort file:" ++ path
